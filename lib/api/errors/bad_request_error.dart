import 'package:dio/dio.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';

class BadRequestError extends DioError {
  final BaseErrorEntity baseErrorEntity;

  BadRequestError(this.baseErrorEntity);
}
