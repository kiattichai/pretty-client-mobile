import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pretty_client_mobile/app/app_state.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/upcoming/mybooking_upcoming_view_model.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/orders_response_entity.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/order_detail_route.dart';
import 'package:intl/intl.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:provider/provider.dart';

class MybookingUpcomingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MybookingUpcomingState();
  }
}

class MybookingUpcomingState
    extends BaseStateProvider<MybookingUpcomingScreen, MybookingUpcomingViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = MybookingUpcomingViewModel();
    viewModel
      ..onMybookingError = handleError
      ..scrollController.addListener(() {
        if (viewModel.scrollController.position.extentAfter == 0) {
          viewModel.setPage(viewModel.page + 1);
          viewModel.getOrdersList(viewModel.limit, viewModel.page, "waiting,complete");
        }
      });
  }

  @override
  Widget build(BuildContext context) {
    appState = Provider.of<AppState>(context);
    appLocal = AppLocalizations.of(context);
    return BaseWidget<MybookingUpcomingViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        color: Design.theme.backgroundColor,
        progressIndicator: CircleLoading(),
        inAsyncCall: viewModel.loading,
        child: Container(
          color: Design.theme.backgroundColor,
          child: SafeArea(
            child: Builder(builder: (context) {
              return RefreshIndicator(
                  color: Design.theme.colorMainBackground,
                  backgroundColor: Design.theme.colorPrimary,
                  key: model.refreshIndicatorKey,
                  onRefresh: () {
                    model.resetOrdersRecord();
                    return model.getOrdersList(model.limit, model.page, "waiting,complete");
                  },
                  // child: model.ordersRecord.length == 0
                  child: model.itemEmpty
                      ? ListView(
                          physics: const AlwaysScrollableScrollPhysics(),
                          controller: model.scrollController,
                          children: <Widget>[
                              Padding(
                                  padding: const EdgeInsets.only(top: 150.0),
                                  child: Center(
                                      child: Column(children: <Widget>[
                                    SvgPicture.asset(
                                      'images/inbox_fill.svg',
                                    ),
                                    SizedBox(
                                      height: Screen.convertHeightSize(10),
                                    ),
                                    Text(
                                      appLocal.mybookingEmptyText,
                                      style: TextStyle(color: Colors.white),
                                    )
                                  ])))
                            ])
                      : ListView.builder(
                          padding: EdgeInsets.only(bottom: 10.0),
                          physics: const AlwaysScrollableScrollPhysics(),
                          itemCount: model.ordersRecord.length,
                          controller: model.scrollController,
                          itemBuilder: (context, i) {
                            int n = i;
                            if (model.load_more && n >= 0) {
                              return Container(
                                margin: EdgeInsets.only(top: 22),
                                child: CupertinoActivityIndicator(),
                              );
                            }

                            return InkWell(
                                child: itemOrdersRecord(model.ordersRecord, i),
                                onTap: () {
                                  RouterService.instance.navigateTo(OrderDetailRoute.buildPath,
                                      data: {"order_id": model.ordersRecord[n].id, "ispop": true});
                                });
                          }));
            }),
          ),
        ),
      ),
    );
  }

  // Widget itemOrdersRecord(List<OrdersRecord> ordersRecord, i) {
  //   int index = i;
  //   var order = ordersRecord[i];
  //   //print(ordersRecord[index].orderNo);
  //   return Container(
  //     margin: EdgeInsets.only(top: 20, left: 12, right: 12),

  //     decoration: BoxDecoration(
  //       color: Design.theme.backgroundColorSecondary,
  //       borderRadius: BorderRadius.circular(8),
  //       boxShadow: [
  //         BoxShadow(
  //           color: Color(0x35000000),
  //           offset: Offset(0,2),
  //           blurRadius: 5,
  //           spreadRadius: 0
  //         )
  //       ],
  //     ),
  //     child: Row(
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: <Widget>[

  //         // Container(
  //         //   width: 87,
  //         //   height: 107,
  //         //   decoration: BoxDecoration(
  //         //     // shape: BoxShape.rectangle,
  //         //     image: DecorationImage(
  //         //       fit: BoxFit.fill,
  //         //       image: NetworkImage("https://images.unsplash.com/photo-1580305089686-d730d63b85ab?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80")
  //         //     ),
  //         //     borderRadius: BorderRadius.only(
  //         //       topLeft: Radius.circular(8),
  //         //       bottomLeft: Radius.circular(8)
  //         //     )

  //         //   ),
  //         // ),
  //         Container(
  //           // width: 87,
  //           // height: 107,
  //           child: ClipRRect(
  //             borderRadius: BorderRadius.only(
  //               topLeft: Radius.circular(8),
  //               bottomLeft: Radius.circular(8)
  //             ),
  //             child: Hero(
  //               child: CachedNetworkImage(
  //                 fit: BoxFit.fitWidth,
  //                 imageUrl: order.products.first.image.url,
  //                 width: 87,
  //                 height: 107,
  //               ),
  //               tag: index,
  //             ),
  //           ),
  //         ),
  //         Container(
  //           padding: EdgeInsets.only(top: 8.0, left: 12.0),
  //           width: Screen.width - 120,
  //           child: Column(
  //             crossAxisAlignment: CrossAxisAlignment.start,

  //             children: <Widget>[
  //               Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                 mainAxisSize: MainAxisSize.max,
  //                 children: <Widget>[
  //                   Expanded(
  //                     flex: 0,
  //                     child: Container(
  //                       child: Text(order.products.first.pretty.nickName,
  //                         textAlign: TextAlign.left,
  //                         style: TextStyle(
  //                           fontFamily: 'Sarabun',
  //                           color: Design.theme.fontColorInactive,
  //                           fontSize: 16,
  //                           fontWeight: FontWeight.w700,
  //                           fontStyle: FontStyle.normal,
  //                         )
  //                       ),
  //                     )
  //                   ),
  //                   SizedBox(width: 10),
  //                   Expanded(
  //                     flex: 0,
  //                     child: Container(
  //                       child: Text(
  //                         appLocal.mybookingNumber,
  //                         // textDirection: TextDirection.ltr,
  //                         textAlign: TextAlign.right,
  //                         style: TextStyle(
  //                           fontFamily: 'Sarabun',
  //                           color: Design.theme.colorText,
  //                           fontSize: 12,
  //                           fontWeight: FontWeight.w400,
  //                           fontStyle: FontStyle.normal,
  //                         ),
  //                       )
  //                     )
  //                   )
  //                 ],
  //               ),
  //               Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                 mainAxisSize: MainAxisSize.max,
  //                 children: <Widget>[
  //                   Expanded(
  //                     flex: 0,
  //                     child: Container(
  //                       child: Text(order.products.first.jobAt.time,
  //                         textAlign: TextAlign.left,
  //                         style: TextStyle(
  //                           fontFamily: 'Sarabun',
  //                           color: Design.theme.fontColorSecondary,
  //                           fontSize: 16,
  //                           fontWeight: FontWeight.w400,
  //                           fontStyle: FontStyle.normal,
  //                         )
  //                       ),
  //                     )
  //                   ),
  //                   SizedBox(width: 10),
  //                   Expanded(
  //                     flex: 0,
  //                     child: Container(
  //                       child: Text(order.orderNo,
  //                         // textDirection: TextDirection.ltr,
  //                         textAlign: TextAlign.right,
  //                         style: TextStyle(
  //                           fontFamily: 'Sarabun',
  //                           color: Design.theme.colorInactive,
  //                           fontSize: 14,
  //                           fontWeight: FontWeight.w400,
  //                           fontStyle: FontStyle.normal,
  //                         ),
  //                       )
  //                     )
  //                   )
  //                 ],
  //               ),
  //               Container(
  //                 child: Text(convertDateFromString(order.products.first.jobAt.value),
  //                   textAlign: TextAlign.left,
  //                   style: TextStyle(
  //                     fontFamily: 'Sarabun',
  //                     color: Design.theme.colorText,
  //                     fontSize: 14,
  //                     fontWeight: FontWeight.w400,
  //                     fontStyle: FontStyle.normal,
  //                   )
  //                 )
  //               ),
  //               Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                 mainAxisSize: MainAxisSize.max,
  //                 children: <Widget>[
  //                   Expanded(
  //                     flex: 1,
  //                     child: Container(),
  //                   ),
  //                   Expanded(
  //                     flex: 0,
  //                     child: Container(
  //                        padding: EdgeInsets.fromLTRB(20, 8, 20, 8),
  //                       decoration: BoxDecoration(
  //                         color: Color(0xff525252).withOpacity(0.7616954985119048),
  //                         borderRadius: BorderRadius.circular(16)
  //                       ),
  //                       child: Text(order.products.first.status.text,
  //                         style: TextStyle(
  //                         fontFamily: 'Sarabun',
  //                         color: Design.theme.fontColorInactive,
  //                         fontSize: 14,
  //                         fontWeight: FontWeight.w500,
  //                         fontStyle: FontStyle.normal,
  //                         )
  //                       )
  //                     )
  //                   )
  //                 ],
  //               )
  //             ],
  //           )
  //         )
  //       ],
  //     )
  //   );
  // }

  Widget itemOrdersRecord(List<OrdersResponseDataRecord> ordersRecord, i) {
    int index = i;
    var order = ordersRecord[i];
    return Container(
        height: 120,
        margin: EdgeInsets.only(top: 20, left: 12, right: 12),
        decoration: BoxDecoration(
          color: Design.theme.backgroundColorSecondary,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
                color: Color(0x35000000), offset: Offset(0, 2), blurRadius: 5, spreadRadius: 0)
          ],
        ),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: 10,
            ),
            Center(
              child: Container(
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  child: Hero(
                    child: CachedNetworkImage(
                      fit: BoxFit.fitWidth,
                      imageUrl: order.products.first.image.url,
                      width: 90,
                      height: 90,
                    ),
                    tag: index,
                  ),
                ),
              ),
            ),
            Container(
                padding: EdgeInsets.only(top: 18.0, left: 12.0),
                width: Screen.width - 140,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                            flex: 0,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  child: Text(order.products.first.productName.toString(),
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontFamily: 'Sarabun',
                                        color: Design.theme.fontColorInactive,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                      )),
                                ),
                              ],
                            )),
                        SizedBox(width: 10),
                        Expanded(
                            flex: 0,
                            child: SvgPicture.asset(
                              'images/icon_booking_active.svg',
                              color: Design.theme.colorPrimary,
                              width: 15,
                            )),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                            flex: 0,
                            child: Container(
                                child: Text(
                              "${appLocal.mybookingMenusId} : ${order.products.first.productCode}",
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                fontFamily: 'Sarabun',
                                color: Design.theme.colorText,
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              ),
                            ))),
                        Expanded(
                            flex: 0,
                            child: Text(
                              convertDateFromString(order.shippingAt.value),
                              style: TextStyle(
                                  color: Design.theme.colorText2, fontWeight: FontWeight.w500),
                            ))
                      ],
                    ),
                    SizedBox(height: 5),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        SizedBox(width: 4),
                        Expanded(
                            flex: 1,
                            child: Container(
                                child: Text('',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontFamily: 'Sarabun',
                                      color: Design.theme.fontColorInactive,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )))),
                        Expanded(
                            flex: 0,
                            child: Text(
                              order.shippingAt.time,
                              style: TextStyle(color: Colors.white, fontWeight: FontWeight.w700),
                            ))

//
//                        Expanded(
//                            flex: 1,
//                            child: Container(
//                                child: Text(convertDateFromString(order.products.first.jobAt.value),
//                                    textAlign: TextAlign.left,
//                                    style: TextStyle(
//                                      fontFamily: 'Sarabun',
//                                      color: Design.theme.fontColorInactive,
//                                      fontSize: 14,
//                                      fontWeight: FontWeight.w400,
//                                      fontStyle: FontStyle.normal,
//                                    )))),
                      ],
                    ),
                    SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        SvgPicture.asset(
                          'images/map_pin_line.svg',
                          color: Design.theme.colorText2.withOpacity(0.7),
                          width: 16,
                          height: 16,
                        ),
                        SizedBox(width: 4),
                        Expanded(
                            flex: 1,
                            child: Container(
                                child: Text(order.shippingAddress,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontFamily: 'Sarabun',
                                      color: Design.theme.colorText2.withOpacity(0.7),
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )))),
                      ],
                    )
                  ],
                ))
          ],
        ));
  }

  void handleError(BaseError error) {
    print(error.toJson());
  }

  String convertDateFromString(String strDate) {
    DateTime aDate = DateTime.parse(strDate);
    String dateStr = DateFormat('dd MMM yyyy', appState.appLocal.languageCode).format(aDate);
    print(dateStr);
    return dateStr;
  }
}
