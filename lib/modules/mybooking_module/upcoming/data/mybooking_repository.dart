import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/modules/booking_module/data/order_api.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/orders_response_entity.dart';
import 'package:inject/inject.dart';

class MyBookingRepository extends BaseRepository {
  final OrderApi orderApi;
  OrdersResponseEntity ordersResponseEntity;
  @provide
  MyBookingRepository(this.orderApi);

  Future<OrdersResponseEntity> ordersList(limit, page) async {
    final response = await orderApi.ordersList(limit, page, BaseErrorEntity.badRequestToModelError);
    print(response);
    if (response.data != null) {
      return response;
    } else {
      return null;
    }
  }

  Future<OrdersResponseEntity> ordersListByStutus(limit, page, orderStatus) async {
    final response = await orderApi.ordersListByStutus(limit, page, orderStatus, BaseErrorEntity.badRequestToModelError);
    if (response.data != null) {
      return response;
    } else {
      return null;
    }
  }

  Future<OrdersResponseEntity> ordersListSuccess(limit, page, orderStatus) async {
    final response = await orderApi.ordersListSuccess(limit, page, orderStatus, BaseErrorEntity.badRequestToModelError);
    if (response.data != null) {
      return response;
    } else {
      return null;
    }
  }
  
}