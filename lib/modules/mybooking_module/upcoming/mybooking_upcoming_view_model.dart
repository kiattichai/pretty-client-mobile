import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/orders_response_entity.dart';


class MybookingUpcomingViewModel extends BaseViewModel {
  Function onMybookingError;
  List<OrdersResponseDataRecord> ordersRecord = [];
  bool itemEmpty = false;
  ScrollController scrollController;
  int limit = 6;
  int page = 1;
  bool is_complete = false;
  bool load_more = false;
  String orderStatus = "waiting,complete";
  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;

  MybookingUpcomingViewModel() {
    refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    scrollController = ScrollController();
  }

  @override
  void postInit() {
    getOrdersList(limit, page, orderStatus);
  }

  Future<void> getOrdersList(limit, page, orderStatus) async {
    if (page == 1) {
      setLoading(true);
    } else {
      setLoadMore(true);
    }

    catchError(() async {
      OrdersResponseEntity tmp = null;
      if (is_complete == false) {
        //tmp = await di.myBookingRepository.ordersList(limit, page);
        tmp = await di.myBookingRepository.ordersListByStutus(limit, page, orderStatus);
      }
      if (tmp !=null && tmp.data.record.length > 0 && tmp.data.pagination.currentPage <= tmp.data.pagination.lastPage) {
        ordersRecord..addAll(tmp.data.record);
        print(ordersRecord[0].products[0].toJson());
        print("length : ${ordersRecord.length}");
        if (ordersRecord.length == 0) {
          itemEmpty = true;
        } else {
          itemEmpty = false;
        }
      } else {
        if (ordersRecord.length == 0) {
          itemEmpty = true;
        } else {
          itemEmpty = false;
        }
        setIsComplete(true);
      }
      setLoading(false);
      setLoadMore(false);
    });
  }

  void setPage(int value) {
    page = value;
  }

  void setIsComplete(bool value) {
    is_complete = value;
  }

  void setLoadMore(bool value) {
    load_more = value;
  }

  void resetOrdersRecord() {
    setPage(1);
    setIsComplete(false);
    ordersRecord.clear();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    print(error);
    onMybookingError(error);
  }
}
