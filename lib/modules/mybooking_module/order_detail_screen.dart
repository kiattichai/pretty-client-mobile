import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/env_config.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/order_view_model.dart';
import 'package:pretty_client_mobile/modules/main_module/main_screen_route.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/device_info.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/custom_modal.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';

class OrderDetailScreen extends StatefulWidget {
  final dynamic data;

  const OrderDetailScreen({Key key, this.data}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return OrderDetailState();
  }
}

class OrderDetailState
    extends BaseStateProvider<OrderDetailScreen, OrderViewModel> {
  final String imgCheckbox = "images/checkbox_circle_line.svg";
  final String imgUncheckbox = "images/uncheckbox_circle_line.svg";
  final numberFormat = new NumberFormat("#,###.##", "en_US");

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    viewModel = OrderViewModel();
    viewModel
      ..setOrderId(widget.data["order_id"])
      ..onErrorOrder = handleError;
  }

  Widget lineBottom() {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: Design.theme.colorLine.withOpacity(0.6),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Screen(context);
    appLocal = AppLocalizations.of(context);
    return BaseWidget<OrderViewModel>(
      model: viewModel,
      builder: (context, model, child) {
        return Scaffold(
          appBar: CustomHeaderDetail(
            height: Screen.convertHeightSize(55),
            title: appLocal.bookingDetailTitle,
            onTap: () {
              if (widget.data["ispop"] == true) {
                print("is pop()");
                RouterService.instance.pop();
              } else {
                RouterService.instance.navigateTo(MainScreenRoute.buildPath,
                    data: 2, clearStack: true);
              }
            },
          ),
          body: ProgressHUD(
            color: Colors.transparent,
            progressIndicator: CircleLoading(),
            inAsyncCall: model.loading,
            child: RefreshIndicator(
              color: Design.theme.colorMainBackground,
              backgroundColor: Design.theme.colorPrimary,
              key: model.refreshIndicatorKey,
              onRefresh: () {
                model.resetData();
                return model.getOrderDetail(model.orderId);
              },
              child: Builder(
                builder: (context) {
                  if (viewModel.orderDetail != null) {
                    int statusStep = 1;
                    List<String> listDate = [];
                    List<String> listTime = [];

                    listDate.add(viewModel.orderDetail.createdAt.date);
                    listTime.add(viewModel.orderDetail.createdAt.time);
                    if (viewModel.orderDetail.paymentAt != null &&
                        viewModel.orderDetail.products[0].status.id == 1) {
                      statusStep = 2;
                      listDate.add(viewModel.orderDetail.paymentAt.date);
                      listTime.add(viewModel.orderDetail.paymentAt.time);
                    } else if (viewModel.orderDetail.products[0].status.id >=
                        2) {
                      statusStep = 3;
                      listDate.add(viewModel.orderDetail.updatedAt.date);
                      listTime.add(viewModel.orderDetail.updatedAt.time);
                    }

                    return Container(
                      color: Design.theme.colorMainBackground,
                      child: ListView(
                        padding: EdgeInsets.only(
                            top: 25,
                            left: Screen.convertWidthSize(15),
                            right: Screen.convertWidthSize(15),
                            bottom: 15),
                        children: <Widget>[
                          Text(
                            appLocal.bookingStatus,
                            style: TextStyle(
                              color: Color(0xffd5d5d5),
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 20),
                            child: statusBarVertical(),
                          ),
                          SizedBox(
                            height:
                                viewModel.orderDetail.products[0].status.id >= 2
                                    ? 30
                                    : 15,
                          ),
                          Text(
                            appLocal.bookingDetailTitle,
                            style: TextStyle(
                              color: Color(0xffd5d5d5),
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
//                          lineBottom(),

                          Container(
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                              color: Design.theme.colorLine.withOpacity(0.2),
                              borderRadius: BorderRadius.all(
                                Radius.circular(16.0),
                              ),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                //======================== CreateAt Box ===========================//
                                Container(
                                  margin: EdgeInsets.only(bottom: 10),
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        width: (Screen.width -
                                                Screen.convertWidthSize(60)) /
                                            2,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              appLocal.bookingNo,
                                              style: TextStyle(
                                                color: Color(0xff636362),
                                                fontSize: 12,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Text(
                                              viewModel.orderDetail.orderNo,
                                              style: TextStyle(
                                                color: Color(0xffa4a4a4),
                                                fontSize: 14,
                                                fontWeight: FontWeight.w700,
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              appLocal.transactionDate,
                                              style: TextStyle(
                                                  color: Color(0xff636362),
                                                  fontSize: 12),
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Text(
                                              "${viewModel.orderDetail.createdAt.date} ${viewModel.orderDetail.createdAt.time}",
                                              style: TextStyle(
                                                color: Color(0xffa4a4a4),
                                                fontSize: 14,
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),

                                //======================== Order Status Box ===========================//
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        appLocal.status,
                                        style: TextStyle(
                                          color: Color(0xff636362),
                                          fontSize: 12,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 4,
                                      ),
                                      Text(
                                        viewModel.orderDetail.orderStatus.name,
                                        style: TextStyle(
                                          color: Design.theme.primaryColor,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ],
                                  ),
                                  padding: EdgeInsets.only(
                                    bottom: 15,
                                  ),
                                ),
                                lineBottom(),

                                //======================== JobAt Box ===========================//
                                Container(
                                  padding: EdgeInsets.only(top: 15, bottom: 15),
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        width: (Screen.width -
                                                Screen.convertWidthSize(60)) /
                                            2,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              appLocal.appointmentDate,
                                              style: TextStyle(
                                                  color: Color(0xff636362),
                                                  fontSize: 12),
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Text(
                                              "${viewModel.orderDetail.shippingAt.date}",
                                              style: TextStyle(
                                                color: Color(0xffa4a4a4),
                                                fontSize: 16,
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              appLocal.appointmentTime,
                                              style: TextStyle(
                                                color: Color(0xff636362),
                                                fontSize: 12,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Text(
                                              viewModel
                                                  .orderDetail.shippingAt.time,
                                              style: TextStyle(
                                                color: Color(0xffa4a4a4),
                                                fontSize: 16,
                                                fontWeight: FontWeight.w700,
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),

                                //======================== Venue Box ===========================//
                                Container(
                                  margin: EdgeInsets.only(bottom: 15),
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        width: (Screen.width -
                                                Screen.convertWidthSize(60)) /
                                            2,
                                        child: InkWell(
                                          onTap: () async {
                                            print("Open map");
                                            if (await MapLauncher
                                                .isMapAvailable(
                                                    MapType.google)) {
                                              await MapLauncher.launchMap(
                                                mapType: MapType.google,
                                                coords: Coords(
                                                    viewModel.orderDetail
                                                        .shippingLat,
                                                    viewModel.orderDetail
                                                        .shippingLong),
                                                title: viewModel.orderDetail
                                                    .shippingAddress,
                                                description: viewModel
                                                    .orderDetail
                                                    .shippingAddress,
                                              );
                                            } else if (await MapLauncher
                                                .isMapAvailable(
                                                    MapType.apple)) {
                                              await MapLauncher.launchMap(
                                                mapType: MapType.apple,
                                                coords: Coords(
                                                    viewModel.orderDetail
                                                        .shippingLat,
                                                    viewModel.orderDetail
                                                        .shippingLong),
                                                title: viewModel.orderDetail
                                                    .shippingAddress,
                                                description: viewModel
                                                    .orderDetail
                                                    .shippingAddress,
                                              );
                                            }
                                          },
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                appLocal.venue,
                                                style: TextStyle(
                                                  color: Color(0xff636362),
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w700,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 4,
                                              ),
                                              Text(
                                                viewModel.orderDetail
                                                    .shippingAddress,
                                                style: TextStyle(
                                                  color: Color(0xffa4a4a4),
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w700,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      viewModel.orderDetail.products[0]
                                                      .productOption !=
                                                  null &&
                                              viewModel.orderDetail.products[0]
                                                      .productOption !=
                                                  ''
                                          ? Container(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    appLocal.theme,
                                                    style: TextStyle(
                                                        color:
                                                            Color(0xff636362),
                                                        fontSize: 12),
                                                  ),
                                                  SizedBox(
                                                    height: 2,
                                                  ),
                                                  Text(
                                                    viewModel
                                                        .orderDetail
                                                        .products[0]
                                                        .productOption,
                                                    style: TextStyle(
                                                      color: Color(0xffa4a4a4),
                                                      fontSize: 15,
                                                      fontWeight:
                                                          FontWeight.w700,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                ),

                                //======================== Pretty Box ===========================//
                                lineBottom(),
                                Container(
                                  padding: EdgeInsets.only(top: 15, bottom: 15),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
//                                  crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            alignment: Alignment.centerLeft,
                                            margin: EdgeInsets.only(right: 6),
                                            child: Image.network(
                                              getImgUrl(
                                                  viewModel.orderDetail
                                                      .products[0].image.id,
                                                  viewModel.orderDetail
                                                      .products[0].image.name,
                                                  105,
                                                  120),
                                              width:
                                                  Screen.convertWidthSize(65),
                                              height:
                                                  Screen.convertHeightSize(70),
                                            ),
                                          ),
                                          Container(
                                            width: Screen.convertWidthSize(viewModel.orderDetail.total>0?60:100),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  appLocal.editProfileName,
                                                  style: TextStyle(
                                                      color: Color(0xff636362),
                                                      fontSize: 11),
                                                ),
                                                SizedBox(
                                                  height: 2,
                                                ),
                                                Text(
                                                  viewModel.orderDetail
                                                      .products[0].productName,
                                                  style: TextStyle(
                                                    color: Color(0xffa4a4a4),
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w700,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          lineVertical(14),
                                          Container(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  appLocal.code,
                                                  style: TextStyle(
                                                      color: Color(0xff636362),
                                                      fontSize: 11),
                                                ),
                                                SizedBox(
                                                  height: 2,
                                                ),
                                                Text(
                                                  viewModel.orderDetail
                                                      .products[0].productCode,
                                                  style: TextStyle(
                                                    color: Color(0xffa4a4a4),
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w700,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),

                                          viewModel.orderDetail.total>0?
                                          lineVertical(14):
                                          Container(),

                                          viewModel.orderDetail.total>0?
                                          Container(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  appLocal.fee,
                                                  style: TextStyle(
                                                      color: Color(0xff636362),
                                                      fontSize: 11),
                                                ),
                                                SizedBox(
                                                  height: 2,
                                                ),
                                                Text(
                                                  "${numberFormat.format(viewModel.orderDetail.products[0].price)} coins",
                                                  style: TextStyle(
                                                    color: Color(0xffa4a4a4),
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w700,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ):
                                          Container(),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),

                                viewModel.orderDetail.total>0?
                                lineBottom():
                                Container(),

                                viewModel.orderDetail.total>0?
                                Container(
                                  alignment: Alignment.centerRight,
                                  margin: EdgeInsets.only(top: 13),
                                  child: RichText(
                                    overflow: TextOverflow.ellipsis,
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                          text: "${appLocal.totalFee}      ",
                                          style: TextStyle(
                                            color: Color(0xffa4a4a4),
                                          ),
                                        ),
                                        TextSpan(
                                          text:
                                              "${numberFormat.format(viewModel.orderDetail.total)} ",
                                          style: TextStyle(
                                            color: Color(0xffd5d5d5),
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        TextSpan(
                                          text: "coins",
                                          style: TextStyle(
                                            color: Color(0xffa4a4a4),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ):
                                Container(),
                              ],
                            ),
                          ),
                          (viewModel.orderDetail != null &&
                                  (viewModel
                                          .orderDetail.products[0].status.id ==
                                      1))
                              ? Container(
                                  margin: EdgeInsets.only(top: 20),
                                  alignment: Alignment.bottomCenter,
                                  child: MaterialButton(
                                    textColor: Color(0xffd5d5d5),
                                    color: Color(0xff525252),
                                    height: 40,
                                    minWidth: 150,
                                    child: Text(
                                      appLocal.bookingCancel,
                                      style: TextStyle(
                                        fontFamily: 'Sarabun',
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(26),
                                    ),
                                    onPressed: () async {
                                      _showDialogConfirm();
                                    },
                                  ),
                                )
                              : Container(
                                  height: 1,
                                ),
                        ],
                      ),
                    );
                  } else {
                    return Container();
                  }
                },
              ),
            ),
          ),
//          bottomNavigationBar: (viewModel.orderDetail != null &&
//                  (viewModel.orderDetail.products[0].status.id == 1))
//              ? Container(
//                  height: Screen.convertHeightSize(45),
//                  alignment: Alignment.bottomCenter,
////                  padding:
////                      EdgeInsets.only(left: 15, right: 15, top: 15, bottom: 15),
//                  child: MaterialButton(
//                    textColor: Color(0xffd5d5d5),
//                    color: Color(0xff525252),
//                    height: 40,
//                    minWidth: 150,
//                    child: Text(
//                      appLocal.bookingCancel,
//                      style: TextStyle(
//                        fontFamily: 'Sarabun',
//                        fontSize: 16,
//                        fontWeight: FontWeight.w500,
//                        fontStyle: FontStyle.normal,
//                      ),
//                    ),
//                    shape: RoundedRectangleBorder(
//                      borderRadius: BorderRadius.circular(26),
//                    ),
//                    onPressed: () async {
//                      _showDialogConfirm();
//                    },
//                  ),
//                )
//              : Container(
//                  height: 1,
//                ),
        );
      },
    );
  }

  void handleError(BaseError error) {
    viewModel.setLoading(false);
    _showAlert(error.message.toString());
    print(error.toJson());
  }

  void _showAlert(String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomModal(
            desc: message,
            appLocal: appLocal,
          );
        });
  }

  void _showDialogConfirm() async {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return CustomModal(
          title: appLocal.confirmation,
          desc: appLocal.confirmationBookingCancel,
          type: ModalType.CANCEL,
          appLocal: appLocal,
          onTapCancel: () {},
          onTapConfirm: () async {
            await viewModel.cancelOrder();
          },
        );
      },
    );
  }

  String getImgUrl(String id, String name, double w, double h) {
    String result = "https://admin.prettyhub.me/images/image-empty.png";
    result =
        "${EnvConfig.instance.resUrl}/w_${w.round()},h_${h.round()},c_crop/${id}/${name}";
    return result;
  }

  Widget lineVertical(double height) {
    return Container(
      padding: EdgeInsets.only(
          left: Screen.convertWidthSize(13),
          right: Screen.convertWidthSize(13),
          top: height,
          bottom: height),
      height: Screen.convertHeightSize(70),
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            right: BorderSide(
              width: 1,
              color: Color(0xff525252),
            ),
          ),
        ),
      ),
    );
  }

  Widget statusBarVertical() {
    Color colorLine = Color(0xffa4a4a4);
    List<double> lineStep = [4, 6];
    int statusId = viewModel.orderDetail.products[0].status.id;
    if (statusId >= 2) {
      colorLine = Design.theme.primaryColor;
      lineStep = [1, 0];
    }
    Path customPath = Path()
      ..moveTo(10.5, 15)
      ..lineTo(10.5, 65);
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Stack(
            children: <Widget>[
              DottedBorder(
                customPath: customPath,
                color: colorLine,
                dashPattern: lineStep,
                strokeWidth: 1.5,
                child: Container(),
              ),
              SvgPicture.asset(
                statusId >= 1 ? imgCheckbox : imgUncheckbox,
                width: 22,
              ),
              Container(
                margin: EdgeInsets.only(top: 60),
                child: SvgPicture.asset(
                  statusId >= 2 ? imgCheckbox : imgUncheckbox,
                  width: 22,
                ),
              ),
            ],
          ),
          Container(
              height: Device.get().isIos ? 100 : 98,
              margin: EdgeInsets.only(left: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          appLocal.storeReceivedYourBooking,
                          style: TextStyle(
                            color: statusId >= 1
                                ? Color(0xffd5d5d5)
                                : Color(0xff636362),
                          ),
                        ),
                        statusId >= 1
                            ? Text(
                                "${viewModel.orderDetail.createdAt.date} ${viewModel.orderDetail.createdAt.time}",
                                style: TextStyle(
                                  color: Color(0xff636362),
                                ),
                              )
                            : Container(
                                child: Text(
                                  "10/10/2020 ",
                                  style: TextStyle(color: Colors.transparent),
                                ),
                              ),
                      ],
                    ),
                  ),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          statusId >= 3
                              ? appLocal.bookingCanceled
                              : appLocal.bookingApproved,
                          style: TextStyle(
                            color: statusId >= 2
                                ? Color(0xffd5d5d5)
                                : Color(0xff636362),
                          ),
                        ),
                        statusId >= 2
                            ? Text(
                                "${viewModel.orderDetail.updatedAt.date} ${viewModel.orderDetail.updatedAt.time}",
                                style: TextStyle(
                                  color: Color(0xff636362),
                                ),
                              )
                            : Container(
                                child: Text(
                                  "10/10/2020",
                                  style: TextStyle(color: Colors.transparent),
                                ),
                              ),
                      ],
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
