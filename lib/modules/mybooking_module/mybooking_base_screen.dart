import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/custome_header.dart';

import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/mybooking_base_view_model.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/upcoming/mybooking_upcoming_screen.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/history/mybooking_history_screen.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/favourites/mybooking_favourites_screen.dart';

class MybookingBaseScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MybookingBaseState();
  }
}

class MybookingBaseState extends BaseStateProvider<MybookingBaseScreen, MybookingBaseViewModel>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    viewModel = MybookingBaseViewModel();
    _tabController = new TabController(length: 3, vsync: this);
    //_tabController.addListener(_setActiveTabIndex);
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    return BaseWidget<MybookingBaseViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        color: Design.theme.colorMainBackground,
        progressIndicator: CircleLoading(),
        inAsyncCall: viewModel.loading,
        child: Scaffold(
          appBar: CustomHeader(
            height: Screen.convertHeightSize(55),
            title: appLocal.mybookingTitle,
          ),
          body: SafeArea(
            child: Builder(builder: (context) {
              return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        // constraints: BoxConstraints(maxHeight: 150.0),
                        child: new Material(
                          color: Design.theme.colorMainBackground,
                          child: new TabBar(
                            labelColor: Design.theme.colorPrimary,
                            unselectedLabelColor: Design.theme.colorInactive,
                            labelStyle: TextStyle(
                              fontFamily: 'Sarabun',
                              color: Design.theme.colorPrimary,
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                            ),
                            unselectedLabelStyle: TextStyle(
                              fontFamily: 'Sarabun',
                              color: Design.theme.colorInactive,
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                            indicator: UnderlineTabIndicator(
                              borderSide: BorderSide(
                                width: 2.0,
                                color: Design.theme.colorPrimary,
                              ),
                            ),
                            tabs: [
                              Tab(text: appLocal.mybookingTabTitleUpcoming),
                              Tab(text: appLocal.mybookingTabTitleHistory),
                              Tab(text: appLocal.mybookingTabTitleFavourites),
                            ],
                            controller: _tabController,
                          ),
                        ),
//                        decoration: new BoxDecoration(
//                          border:
//                              Border(bottom: BorderSide(width: 1.0, color: Design.theme.colorLine)),
//                        )
                    ),
                    Expanded(
                      flex: 1,
                      child: TabBarView(
                        controller: _tabController,
                        children: [
                          Tab(child: MybookingUpcomingScreen()),
                          Tab(child: MybookingHistoryScreen()),
                          Tab(child: MybookingFavouritesScreen()),
                        ],
                      ),
                    )
                  ]);
            }),
          ),
        ),
      ),
    );
  }
}
