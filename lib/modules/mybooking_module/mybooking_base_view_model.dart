import 'package:pretty_client_mobile/core/base_view_model.dart';

class MybookingBaseViewModel extends BaseViewModel {
  Function onMybookingError;

  @override
  void postInit() {
    setLoading(false);
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    print(error);
    onMybookingError(error);
  }
}
