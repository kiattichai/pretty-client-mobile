import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/mybooking_base_screen.dart';

class MybookingBaseScreenRoute extends BaseRoute {
  static String buildPath = '/mybooking';

  @override
  String get path => buildPath;

  @override
  bool get clearStack => true;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => MybookingBaseScreen());
  }
}
