import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/orders_response_entity.dart';


class MybookingHistoryViewModel extends BaseViewModel {
  Function onMybookingError;
  List<OrdersResponseDataRecord> ordersRecord = [];
  ScrollController scrollController;
  int limit = 6;
  int page = 1;
  String orderStatus;
  bool itemEmpty = false;
  bool is_complete = false;
  bool load_more = false;
  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;

  MybookingHistoryViewModel() {
    refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    scrollController = ScrollController();
  }

  @override
  void postInit() {
    getOrdersList(limit, page, "all");
  }

  Future<void> getOrdersList(limit, page, orderStatus) async {
    if (page == 1) {
      setLoading(true);
    } else {
      setLoadMore(true);
    }

    catchError(() async {
      OrdersResponseEntity tmp = null;
      if (is_complete == false) {
        if (orderStatus == "all") {
          tmp = await di.myBookingRepository.ordersList(limit, page);
        }
//        else if (orderStatus == "job-accepted") {
//          print(orderStatus);
//          tmp = await di.myBookingRepository.ordersListSuccess(limit, page, orderStatus);
//        }
        else {
          tmp = await di.myBookingRepository.ordersListByStutus(limit, page, orderStatus);
        }
      }
      if (tmp !=null && tmp.data.record.length > 0 && tmp.data.pagination.currentPage <= tmp.data.pagination.lastPage) {
        ordersRecord..addAll(tmp.data.record);
        if (ordersRecord.length == 0) {
          itemEmpty = true;
        } else {
          itemEmpty = false;
        }
      } else {
        if (ordersRecord.length == 0) {
          itemEmpty = true;
        } else {
          itemEmpty = false;
        }
        setIsComplete(true);
      }
      setLoading(false);
      setLoadMore(false);
    });
  }

  void setPage(int value) {
    page = value;
  }

  void setIsComplete(bool value) {
    is_complete = value;
  }

  void setLoadMore(bool value) {
    load_more = value;
  }

  void resetOrdersRecord() {
    setPage(1);
    setIsComplete(false);
    ordersRecord.clear();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    print(error);
    onMybookingError(error);
  }
}
