import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/history/mybooking_history_view_model.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/orders_response_entity.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/order_detail_route.dart';
import 'package:intl/intl.dart';
import 'package:flutter_svg/svg.dart';
//import 'package:material_segmented_control/material_segmented_control.dart';
import 'package:pretty_client_mobile/widgets/history_filter_segment.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';

class MybookingHistoryScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return MybookingHistoryState();
  }
}

class MybookingHistoryState extends BaseStateProvider<MybookingHistoryScreen, MybookingHistoryViewModel> {
  final formatter = new NumberFormat("#,###,###");
  int currentSelected = 0;
  
  String filter = "all";
  @override
  void initState() {
    super.initState();
    viewModel = MybookingHistoryViewModel();
    viewModel
      ..onMybookingError = handleError
      ..scrollController.addListener(() {
        if (viewModel.scrollController.position.extentAfter == 0) {
          viewModel.setPage(viewModel.page + 1);
          viewModel.getOrdersList(viewModel.limit, viewModel.page, filter);
        }
      });
  }

  @override

  Widget build(BuildContext context) {
    // TODO: implement build
    appLocal = AppLocalizations.of(context);

    List<String> menus = [
      "${appLocal.mybookingMenusAll}", 
      "${appLocal.mybookingMenusSuccess}", 
      "${appLocal.mybookingMenusCancel}"
    ];

    return BaseWidget<MybookingHistoryViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        color: Design.theme.backgroundColor,
        progressIndicator: CircleLoading(),
        inAsyncCall: viewModel.loading,
        child: Container(
          color: Design.theme.backgroundColor,
          child: SafeArea(
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 0,
                  child: Container(
                    margin: EdgeInsets.only(top: 16, left: 16, bottom: 16, right: 16),
                    height: 42.0,
                    child: MyCustomMenu(
                      currentSelected: currentSelected,
                      menus: menus,
                      onMenuTap: (int newValue) {
                        _handleSelectedMenu(newValue, model);
                      },
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Builder(builder: (context) {
                    return RefreshIndicator(
                      color: Design.theme.colorMainBackground,
                      backgroundColor: Design.theme.colorPrimary,
                      key: model.refreshIndicatorKey,
                      onRefresh: () {
                        model.resetOrdersRecord();
                        return model.getOrdersList(model.limit, model.page, filter);
                      },
                      child: model.itemEmpty
                      ? ListView(
                        physics: const AlwaysScrollableScrollPhysics(),
                        controller: model.scrollController,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 150.0),
                            child: Center(
                              child: Column(
                                children: <Widget> [
                                  SvgPicture.asset(
                                    'images/inbox_fill.svg',
                                    
                                  ),
                                  SizedBox(
                                    height: Screen.convertHeightSize(10),
                                  ),
                                  Text(
                                    appLocal.mybookingEmptyText,
                                    style: TextStyle(color: Colors.white),
                                  )
                                ]
                              )
                            )
                          )
                        ]
                      )
                      
                      : ListView.builder(
                        padding: EdgeInsets.only(bottom: 10.0),
                        physics: const AlwaysScrollableScrollPhysics(),
                        itemCount: model.ordersRecord.length,
                        controller: model.scrollController,
                        itemBuilder: (context, i) {
                          int n = i;
                          if (model.load_more && n >= 0) {
                            return Container(
                              margin: EdgeInsets.only(top: 22),
                              child: CupertinoActivityIndicator(),
                            );
                          }
                      
                          return InkWell(
                            child: itemOrdersRecord(model.ordersRecord, i),
                            onTap: () {
                              RouterService.instance.navigateTo(OrderDetailRoute.buildPath, 
                                data: {"order_id":model.ordersRecord[n].id, "ispop": true});
                            }
                          );
                        }
                      )
                    );
                  }),
                )
              ],
            )
          ),
        )
      ),
    );
  }

  Widget itemOrdersRecord(List<OrdersResponseDataRecord> ordersRecord, i) {
    int index = i;
    var order = ordersRecord[i];
    //print(ordersRecord[index].orderNo);
    return Container(
      margin: EdgeInsets.only(top: i == 0 ? 0 : 20, left: 12, right: 12),
      
      decoration: BoxDecoration(
        color: Design.theme.backgroundColorSecondary,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            color: Color(0x35000000),
            offset: Offset(0,2),
            blurRadius: 5,
            spreadRadius: 0
          )
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8), 
                bottomLeft: Radius.circular(8)
              ),
              child: Hero(
                child: CachedNetworkImage(
                  fit: BoxFit.fitWidth,
                  imageUrl: order.products.first.image.url,
                  width: 63,
                  height: 77,
                ),
                tag: index,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 8.0, left: 12.0),
            width: Screen.width - 100,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Expanded(
                      flex: 0,
                      child: Container(                        
                        child: AutoSizeText.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: "${appLocal.mybookingNo} : ",
                                style: TextStyle(
                                  fontFamily: 'Sarabun',
                                  color: Design.theme.fontColorSecondary,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                ) 
                              ),
                              TextSpan(
                                text: order.orderNo,
                                style: TextStyle(
                                  fontFamily: 'Sarabun',
                                  color: Design.theme.fontColorInactive,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                )  
                              )
                            ]
                          ),
                          minFontSize: 10,
                          maxLines: 2,
                        )
                      )
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 2.0, left: 8.0, bottom: 4.0, right: 8.0),
                      // height: 20,
                      child: AutoSizeText(
                        order.products.first.status.text,
                        style: TextStyle(
                          fontFamily: 'Sarabun',
                          color: handleOrderStatusDisplay(order.products.first.status.id),//Color(0xff2d8206),
                          fontSize: 10,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        ),
                        maxLines: 2
                      ),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: handleOrderStatusDisplay(order.products.first.status.id),//Color(0xff2d8206),
                          width: 1
                        ),
                        borderRadius: BorderRadius.circular(7)
                      )
                    )
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                        text: TextSpan(
                            children: [
                              TextSpan(
                                  text: "${appLocal.mybookingDate} ",
                                  style: TextStyle(
                                    fontFamily: 'Sarabun',
                                    color: Design.theme.fontColorSecondary,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                  )
                              ),
                              TextSpan(
                                  text: "${order.shippingAt.date} ${order.shippingAt.time}",
                                  style: TextStyle(
                                    fontFamily: 'Sarabun',
                                    color: Design.theme.fontColorInactive,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                  )
                              ),
                            ]
                        )
                    ),
                    RichText(
                        text: TextSpan(
                            children: [
                              TextSpan(
                                  text: order.products.first.total == 0 ? "" : formatter.format(order.products.first.total),
                                  style: TextStyle(
                                    fontFamily: 'Sarabun',
                                    color: Design.theme.primaryColor,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                  )
                              ),
                              TextSpan(
                                  text: order.products.first.total == 0 ? "" : " ${appLocal.mybookingCoins}",
                                  style: TextStyle(
                                    fontFamily: 'Sarabun',
                                    color: Design.theme.fontColorSecondary,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                  )
                              ),
                            ]
                        )
                    )
                  ],
                ),

              ],
            )
          )
        ],
      )
    );
  }

  void handleError(BaseError error) {
    print(error.toJson());
  }

  String convertDateFromString(String strDate) {
    DateTime aDate = DateTime.parse(strDate);
    String dateStr = DateFormat('dd/MM/yyyy').format(aDate);
    print(dateStr);
    return "${dateStr}";
  }
  
  void _handleSelectedMenu(int value, MybookingHistoryViewModel model) {
    setState(() {
      currentSelected = value;
      switch (value) {
        case 0:
          setState(() {
            filter = "all";
          });
          model.resetOrdersRecord();
          model.getOrdersList(model.limit, model.page, "all");

          break;
        case 1:
          setState(() {
            filter = "complete";
          });
          model.resetOrdersRecord();
          model.getOrdersList(model.limit, model.page, "complete");
          break;
        case 2:
          setState(() {
            filter = "cancel";
          });
          model.resetOrdersRecord();
          model.getOrdersList(model.limit, model.page, "cancel");
          break;
        default:
      }
      print(value);
    });
  }

  Color handleOrderStatusDisplay(int statusId) {
    print(statusId);
    switch (statusId) {
     
      case 1:
        return Design.theme.primaryColor;
        break;
      case 2:
        return Color(0xff2d8206);
        break;
      case 3:
        return Design.theme.colorText;
        break;
      case 4:
        return Design.theme.colorText;
        break;
      case 5:
        return Design.theme.colorText;
        break;
      case 6:
        return Design.theme.colorText;
        break;
      default:
        return Design.theme.colorText;
    }
  }
}
