import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/order_detail_screen.dart';

class OrderDetailRoute extends BaseRoute {
  static String buildPath = '/order_detail';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
      builder: (context) => OrderDetailScreen(
        data: data,
      ),
    );
  }
}
