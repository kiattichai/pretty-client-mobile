import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/modules/favorite_module/model/favorite_response_entity.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/product_response_entity.dart';

class MybookingFavouritesViewModel extends BaseViewModel {
  Function onMybookingError;
  List<Favorite> favoriteRecord = [];
  ScrollController scrollController;
  Product friendDetail;
  int limit = 6;
  int page = 1;
  bool itemEmpty = false;
  bool is_complete = false;
  bool load_more = false;
  bool isFavorite = true;
  int id;
  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;
  FavoritePagination pagination;

  MybookingFavouritesViewModel() {
    refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    scrollController = ScrollController();
  }

  @override
  void postInit() {
    super.postInit();
    getFavoritesList(limit, page);
  }

  Future<void> getFavoritesList(limit, page) async {
    if (page == 1) {
      favoriteRecord.clear();
      setLoading(true);
    } else {
      setLoadMore(true);
    }

    catchError(() async {
      FavoriteResponseEntity tmp = null;

      if (is_complete == false) {
        tmp = await di.favoriteRepository.getFavoritesListWithPage(limit, page);
        if (pagination != null) {
          pagination.currentPage = tmp.data.pagination.lastPage;
          return;
        } else {
          pagination = tmp.data.pagination;
        }
      }

      if (tmp != null &&
          tmp.data.record.length > 0 &&
          tmp.data.pagination.currentPage <= tmp.data.pagination.lastPage) {

        tmp.data.record.forEach((model) {
          getDataFirebase(model.product);
        });

        favoriteRecord..addAll(tmp.data.record);
        if (favoriteRecord.length == 0) {
          itemEmpty = true;
        } else {
          itemEmpty = false;
        }
      } else {
        if (favoriteRecord.length == 0) {
          itemEmpty = true;
        } else {
          itemEmpty = false;
        }
        setIsComplete(true);
      }
      setLoading(false);
      setLoadMore(false);
    });
  }

  void getFriendDetail(String friendId, Function onSuccess) {
    print(friendId);
    catchError(() async {
      friendDetail = await di.friendsRepository.getFriendById(friendId);
      onSuccess();
    });
  }

  void clickFavorite(int id) {
    notifyListeners();
    catchError(() async {
      setLoading(true);

      await di.favoriteRepository.removeFavorite(id);
      resetFavouritesRecord();
      page=1;
      getFavoritesList(limit, page);

      setLoading(false);
    });
  }

  getDataFirebase(FavoriteProduct friend) async {
    final itemRef = FirebaseDatabase.instance.reference().child('product/${friend.id}');
    if (itemRef != null) {
      itemRef.onValue.listen((Event e) {
        var snapshot = e.snapshot;
        if (snapshot.value != null) {
          final data = Map<String, dynamic>.from(snapshot.value);
          if (data['status'] == 'on') {
            friend..onFloor = true;
            notifyListeners();
          }
          if (data['status'] == 'off') {
            friend..onFloor = false;
            notifyListeners();
          }
        }
      });
    }
  }

  void setPage(int value) {
    page = value;
  }

  void setIsComplete(bool value) {
    is_complete = value;
  }

  void setLoadMore(bool value) {
    load_more = value;
  }

  void resetFavouritesRecord() {
    setPage(1);
    setIsComplete(false);
    favoriteRecord.clear();
    pagination = null;
  }

  bool shouldLogin() {
    return di.sharePrefInterface.shouldLogin();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    print(error);
    onMybookingError(error);
  }
}
