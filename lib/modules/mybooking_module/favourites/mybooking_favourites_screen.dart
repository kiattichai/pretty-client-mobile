import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/favorite_module/model/favorite_response_entity.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/favourites/mybooking_favourites_view_model.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/orders_response_entity.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/order_detail_route.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pretty_client_mobile/modules/login_module/login_route.dart';
import 'package:pretty_client_mobile/modules/booking_module/booking_route.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/friends_response_entity.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_detail_route.dart';

class MybookingFavouritesScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MybookingFavouritesState();
  }
}

class MybookingFavouritesState
    extends BaseStateProvider<MybookingFavouritesScreen, MybookingFavouritesViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = MybookingFavouritesViewModel();
    viewModel
      ..onMybookingError = handleError
      ..scrollController.addListener(() {
        if (viewModel.scrollController.position.extentAfter == 0) {
          viewModel.setPage(viewModel.page + 1);
          viewModel.getFavoritesList(viewModel.limit, viewModel.page);
        }
      });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    appLocal = AppLocalizations.of(context);
    return BaseWidget<MybookingFavouritesViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
          color: Design.theme.backgroundColor,
          progressIndicator: CircleLoading(),
          inAsyncCall: viewModel.loading,
          child: Container(
              color: Design.theme.backgroundColor,
              child: SafeArea(
                child: Builder(builder: (context) {
                  return RefreshIndicator(
                      color: Design.theme.colorMainBackground,
                      backgroundColor: Design.theme.colorPrimary,
                      key: model.refreshIndicatorKey,
                      onRefresh: () {
                        model.resetFavouritesRecord();
                        return model.getFavoritesList(model.limit, model.page);
                      },
                      child: model.itemEmpty
                          ? ListView(
                              physics: const AlwaysScrollableScrollPhysics(),
                              controller: model.scrollController,
                              children: <Widget>[
                                  Padding(
                                      padding: const EdgeInsets.only(top: 150.0),
                                      child: Center(
                                          child: Column(children: <Widget>[
                                        SvgPicture.asset(
                                          'images/inbox_fill.svg',
                                        ),
                                        SizedBox(
                                          height: Screen.convertHeightSize(10),
                                        ),
                                        Text(
                                          appLocal.mybookingEmptyText,
                                          style: TextStyle(color: Colors.white),
                                        )
                                      ])))
                                ])
                          : ListView.builder(
                              padding: EdgeInsets.only(bottom: 10.0),
                              physics: const AlwaysScrollableScrollPhysics(),
                              itemCount: model.favoriteRecord.length,
                              controller: model.scrollController,
                              itemBuilder: (context, i) {
                                int n = i;
                                if (model.load_more && n >= 0) {
                                  return Container(
                                    margin: EdgeInsets.only(top: 22),
                                    child: CupertinoActivityIndicator(),
                                  );
                                }
                                return InkWell(
                                    child: itemOrdersRecord(model, i),
                                    //return itemOrdersRecord(model, i);
                                    onTap: () {
                                      viewModel.getFriendDetail(
                                          model.favoriteRecord[i].product.id.toString(), () {
                                        RouterService.instance.navigateTo(
                                            FriendsDetailRoute.buildPath,
                                            data: viewModel.friendDetail);
                                      });
                                    });
                              }));
                }),
              ))),
    );
  }

  Widget itemOrdersRecord(MybookingFavouritesViewModel model, int i) {
    int index = i;
    //print(ordersRecord[index].orderNo);
    var favorite = model.favoriteRecord[i];
    return Container(
        margin: EdgeInsets.only(top: 20, left: 12, right: 12),
        decoration: new BoxDecoration(
          color: Design.theme.backgroundColorSecondary,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
                color: Color(0x35000000), offset: Offset(0, 2), blurRadius: 5, spreadRadius: 0)
          ],
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              // width: 87,
              // height: 107,
              child: ClipRRect(
                borderRadius:
                    BorderRadius.only(topLeft: Radius.circular(8), bottomLeft: Radius.circular(8)),
                child: Hero(
                  child: CachedNetworkImage(
                    fit: BoxFit.fitWidth,
                    // imageUrl: favorite.prettyI order.products.first.image.url,
                    imageUrl: favorite.product.avatar.url,
                    width: 87,
                    height: 107,
                  ),
                  tag: index,
                ),
              ),
            ),
            Container(
                padding: EdgeInsets.only(left: 12.0),
                width: Screen.width - 120,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                            flex: 0,
                            child: Container(
                                child: Text(favorite.product.name,
                                    style: TextStyle(
                                      fontFamily: 'Sarabun',
                                      color: Design.theme.fontColorInactive,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                    )))),
                        SizedBox(width: 10),
                        Expanded(
                          flex: 0,
                          child: Container(
                            child: SvgPicture.asset(favorite.product.displayOnFloor()
                                ? 'images/status_online.svg'
                                : 'images/status_offline.svg'),
                          ),
                          // child: Center(
                          //   child: Container(
                          //     padding: EdgeInsets.only(top: 1, left: 4, bottom: 1, right: 4),
                          //     decoration: BoxDecoration(
                          //       color: Color(0xff3a3a3a),
                          //       borderRadius: BorderRadius.circular(6)
                          //     ),
                          //     height: 16,
                          //     child: Text(
                          //       "Online",
                          //       style: TextStyle(
                          //         fontFamily: 'Sarabun',
                          //         color: Color(0xffd5d5d5),
                          //         fontSize: 10,
                          //         fontWeight: FontWeight.w700,
                          //         fontStyle: FontStyle.normal,
                          //       )
                          //     )
                          //   ),
                          // )
                        ),
                        SizedBox(width: 10),
                        Expanded(
                          flex: 1,
                          child: Container(),
                        ),

                        // Expanded(
                        //   flex: 0,
                        //   child: Container(
                        //     width: 49,
                        //     height: 18,
                        //     color: Colors.green,
                        //     // decoration: new BoxDecoration(
                        //     //   border: Border.all(
                        //     //     color: Color(0xffe7c260),
                        //     //     width: 1
                        //     //   ),
                        //     //   borderRadius: BorderRadius.circular(7)
                        //     // )
                        //   )
                        // )
                        Expanded(
                            flex: 0,
                            child: Container(child: iconFavorite(favorite.product.id, model)))

                        // Container(
                        //   padding: EdgeInsets.only(top: 2.0, left: 8.0, bottom: 4.0, right: 8.0),
                        //   height: 20,
                        //   child: Text(
                        //     order.products.first.status.text,
                        //     style: TextStyle(
                        //       fontFamily: 'Sarabun',
                        //       color: handleOrderStatusDisplay(order.products.first.status.id),//Color(0xff2d8206),
                        //       fontSize: 10,
                        //       fontWeight: FontWeight.w400,
                        //       fontStyle: FontStyle.normal,
                        //     )
                        //   ),
                        //   decoration: new BoxDecoration(
                        //     border: Border.all(
                        //       color: handleOrderStatusDisplay(order.products.first.status.id),//Color(0xff2d8206),
                        //       width: 1
                        //     ),
                        //     borderRadius: BorderRadius.circular(7)
                        //   )
                        // )
                      ],
                    ),
                    Text("${appLocal.mybookingMenusId} : ${favorite.product.code}",
                        style: TextStyle(
                          fontFamily: 'Sarabun',
                          color: Design.theme.fontColorSecondary,
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        )),
                    Container(
                      padding: EdgeInsets.only(top: 3),
                      height: 20,
                      child: ratings(favorite),
                    ),

                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //   mainAxisSize: MainAxisSize.max,
                    //   children: <Widget>[
                    //     Expanded(
                    //       flex: 1,
                    //       child: Container()
                    //     ),
                    //     SizedBox(width: 10),
                    //     Expanded(
                    //       flex: 0,
                    //       child: Container(
                    //         child: InkWell(
                    //           onTap: () async {
                    //             if (viewModel.shouldLogin()) {
                    //               await RouterService.instance.navigateTo(LoginRoute.buildPath);

                    //             } else {
                    //               RouterService.instance.navigateTo(BookingRoute.buildPath, data: favorite.pretty);
                    //             }
                    //           },
                    //           child: Text(appLocal.mybookingMenusBookAgainButton,
                    //             textAlign: TextAlign.right,
                    //             style: TextStyle(
                    //               fontFamily: 'Sarabun',
                    //               color: Design.theme.primaryColor,
                    //               fontSize: 14,
                    //               fontWeight: FontWeight.w400,
                    //               fontStyle: FontStyle.normal,
                    //             )
                    //           ),
                    //         ),
                    //       ),
                    //     )
                    //   ],
                    // ),

                    //   RichText(
                    //     text: TextSpan(
                    //     children: [
                    //       TextSpan(
                    //         text: "Date : ",
                    //     style: TextStyle(
                    //     fontFamily: 'Sarabun',
                    //     color: Color(0xffa4a4a4),
                    //     fontSize: 14,
                    //     fontWeight: FontWeight.w400,
                    //     fontStyle: FontStyle.normal,

                    //     )
                    //     ),
                    //   TextSpan(
                    //     text: convertDateFromString(order.products.first.jobAt.value),
                    //     style: TextStyle(
                    //     fontFamily: 'Sarabun',
                    //     color: Color(0xffd5d5d5),
                    //     fontSize: 14,
                    //     fontWeight: FontWeight.w400,
                    //     fontStyle: FontStyle.normal,

                    //     )
                    //     ),
                    //     ]
                    //   )
                    // ),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //   mainAxisSize: MainAxisSize.max,
                    //   children: <Widget>[
                    //     Expanded(
                    //       flex: 0,
                    //       child: Container(
                    //         child: Text(order.products.first.jobAt.time,
                    //           textAlign: TextAlign.left,
                    //           style: TextStyle(
                    //             fontFamily: 'Sarabun',
                    //             color: Design.theme.fontColorSecondary,
                    //             fontSize: 16,
                    //             fontWeight: FontWeight.w400,
                    //             fontStyle: FontStyle.normal,
                    //           )
                    //         ),
                    //       )
                    //     ),
                    //     SizedBox(width: 10),
                    //     Expanded(
                    //       flex: 0,
                    //       child: Container(

                    //       )
                    //     )
                    //   ],
                    // ),
                    //   RichText(
                    //     text: TextSpan(
                    //     children: [
                    //       TextSpan(
                    //         text: formatter.format(order.products.first.price),
                    //         style: TextStyle(
                    //           fontFamily: 'Sarabun',
                    //           color: Color(0xffe7c260),
                    //           fontSize: 14,
                    //           fontWeight: FontWeight.w400,
                    //           fontStyle: FontStyle.normal,
                    //         )
                    //       ),
                    //       TextSpan(
                    //         text: " coins",
                    //         style: TextStyle(
                    //           fontFamily: 'Sarabun',
                    //           color: Color(0xffa4a4a4),
                    //           fontSize: 14,
                    //           fontWeight: FontWeight.w400,
                    //           fontStyle: FontStyle.normal,
                    //         )
                    //       ),
                    //     ]
                    //   )
                    // )
                    // Container(
                    //   child: Text(convertDateFromString(order.products.first.jobAt.value),
                    //     textAlign: TextAlign.left,
                    //     style: TextStyle(
                    //       fontFamily: 'Sarabun',
                    //       color: Design.theme.colorText,
                    //       fontSize: 14,
                    //       fontWeight: FontWeight.w400,
                    //       fontStyle: FontStyle.normal,
                    //     )
                    //   )
                    // ),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //   mainAxisSize: MainAxisSize.max,
                    //   children: <Widget>[
                    //     Expanded(
                    //       flex: 1,
                    //       child: Container(
                    //         child: Text(order.orderNo,
                    //           // textDirection: TextDirection.ltr,
                    //           textAlign: TextAlign.left,
                    //           style: TextStyle(
                    //             fontFamily: 'Sarabun',
                    //             color: Design.theme.colorInactive,
                    //             fontSize: 14,
                    //             fontWeight: FontWeight.w400,
                    //             fontStyle: FontStyle.normal,
                    //           ),
                    //         )
                    //       ),
                    //     ),
                    //     Expanded(
                    //       flex: 0,
                    //       child: Container(
                    //         // color: Colors.green,
                    //         child: FlatButton(
                    //           child: Text("Book again",
                    //             textAlign: TextAlign.right,
                    //             style: TextStyle(
                    //               fontFamily: 'Sarabun',
                    //               color: Design.theme.primaryColor,
                    //               fontSize: 14,
                    //               fontWeight: FontWeight.w400,
                    //               fontStyle: FontStyle.normal,
                    //             )
                    //           ),
                    //           onPressed: () => Navigator.of(context).pop(),
                    //         ),
                    //       )
                    //     )
                    //   ],
                    // )
                  ],
                ))
          ],
        ));
  }

  Widget iconFavorite(int friendId, MybookingFavouritesViewModel model) {
    return Container(
        width: 40,
        height: 40,
        child: InkWell(
          onTap: () async {
            if (viewModel.shouldLogin()) {
              RouterService.instance.navigateTo(LoginRoute.buildPath);
            } else {
              viewModel.clickFavorite(friendId);
              // viewModel.resetFavouritesRecord();
              // viewModel.getFavoritesList(model.limit, model.page);
            }
          },
          child: SvgPicture.asset(
            viewModel.isFavorite ? 'images/icon_heart_fill.svg' : 'images/icon_heart_empty.svg',
            fit: BoxFit.none,
            // width: 17,
            height: 16,
            color: Design.theme.primaryColor,
          ),
        ));
  }

  Widget ratings(Favorite favorite) {
    if(favorite.product.feedbacks.summary>0){
      return RatingBarIndicator(
        rating: favorite.product.feedbacks.summary,
        itemCount: 5,
        itemSize: 17.0,
        unratedColor: Design.theme.colorText,
        itemBuilder: (context, _) => Icon(
          Icons.star,
          color: Design.theme.primaryColor,
        ),
        // allowHalfRating: true,

        // onRatingUpdate: (rating) => print('rating')
      );
    } else {
      return Container();
    }

    // return Container(
    //   // padding: EdgeInsets.fromLTRB(0.0, 0.0, 4.0, 0.0),
    //   child: Row(
    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //     children: <Widget>[
    //       Row(
    //         mainAxisSize: MainAxisSize.min,
    //         children: <Widget>[
    //           SvgPicture.asset(
    //             'images/icon_star.svg',
    //             fit: BoxFit.none,
    //             width: 17,
    //             height: 17,
    //             color: Design.theme.primaryColor,
    //           ),
    //           SizedBox(
    //             height: 17,
    //             width: 4,
    //           ),
    //           SvgPicture.asset(
    //             'images/icon_star.svg',
    //             fit: BoxFit.none,
    //             width: 17,
    //             height: 17,
    //             color: Design.theme.primaryColor,
    //           ),
    //           SizedBox(
    //             height: 17,
    //             width: 4,
    //           ),
    //           SvgPicture.asset(
    //             'images/icon_star.svg',
    //             fit: BoxFit.none,
    //             width: 17,
    //             height: 17,
    //             color: Design.theme.primaryColor,
    //           ),
    //           SizedBox(
    //             height: 17,
    //             width: 4,
    //           ),
    //           SvgPicture.asset(
    //             'images/icon_star.svg',
    //             fit: BoxFit.none,
    //             width: 17,
    //             height: 17,
    //             color: Design.theme.primaryColor,
    //           ),
    //           SizedBox(
    //             height: 17,
    //             width: 4,
    //           ),
    //           SvgPicture.asset(
    //             'images/icon_star.svg',
    //             fit: BoxFit.none,
    //             width: 17,
    //             height: 17,
    //             color: Design.theme.primaryColor,
    //           ),
    //         ]
    //       ),
    //     ],
    //   )
    // );
  }

  void handleError(BaseError error) {
    print(error.toJson());
  }
}
