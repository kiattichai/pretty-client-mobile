import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/modules/profile_module/edit_profile/edit_profile_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/profile_view_model.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_response_entity.dart';

typedef ProfileTitleHeaderCallback = void Function();

class ProfileTitleHeader extends StatelessWidget {
  final UserProfileResponseData userModel;
  final String username;
  final String birthDate;
  final String userAvatar;
  final ProfileViewModel viewModel;

  ProfileTitleHeader({Key key, this.username, this.birthDate, this.userAvatar, this.userModel, this.viewModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        print("tap");
        print("${this.userModel.username}");
        await RouterService.instance.navigateTo(EditProfileRoute.buildPath, data: this.userModel);
        this.viewModel.getMyProfile();
      },
      child: Row(children: <Widget>[
        Padding(padding: EdgeInsets.only(left: 24.0)),
        Container(
          width: 55,
          height: 55,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                fit: BoxFit.fill,
                image: userAvatar == null
                    ? AssetImage('images/account_circle_black.png')
                    : NetworkImage(userAvatar)),
            border: Border.all(color: Design.theme.colorInactive, width: 1),
          ),
        ),
        Padding(padding: EdgeInsets.only(left: 12.0)),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Text(
                      username,
                      style: TextStyle(
                        color: Design.theme.colorActive,
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0,
                      ),
                    ),
                  ),
                  SvgPicture.asset(
                    "images/icons_settings.svg",
                    width: 18,
                    height: 18,
                    color: Design.theme.colorText2,
                  )
                ],
              ),
              Text(
                birthDate,
                style: TextStyle(
                  color: Design.theme.colorText2,
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0,
                ),
              ),
            ],
          ),
        ),
        Padding(padding: EdgeInsets.only(left: 12.0)),
      ]),
    );
  }
}
