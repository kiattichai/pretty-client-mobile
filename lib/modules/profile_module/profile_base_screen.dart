import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/login_module/login_route.dart';
import 'package:pretty_client_mobile/modules/pre_register_module/pre_register_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/profile_title_header.dart';
import 'package:pretty_client_mobile/modules/profile_module/profile_view_model.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custome_header.dart';
import 'package:pretty_client_mobile/widgets/primary_button_widget.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/profile_menu_model.dart';
import 'package:pretty_client_mobile/modules/profile_module/profile_menu_list.dart';
import 'package:pretty_client_mobile/modules/main_module/main_screen_route.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:intl/intl.dart';
import 'package:pretty_client_mobile/services/router_service.dart';

class ProfileScreen extends StatefulWidget {
  @override
  ProfileScreenState createState() {
    return ProfileScreenState();
  }
}

class ProfileScreenState extends BaseStateProvider<ProfileScreen, ProfileViewModel> {
  List<Menu> menus() => [
//        Menu(title: appLocal.myCoins, iconName: "icon_coins2.svg"),
        Menu(title: appLocal.profileInfo, iconName: "icon_member.svg"),
        Menu(title: appLocal.profileSetting, iconName: "icons_settings_gold.svg"),
        Menu(title: appLocal.profileHelpCenter, iconName: "icon_info.svg"),
      ];

  List<Menu> menusGuest() => [
        Menu(title: appLocal.profileSetting, iconName: "icons_settings_gold.svg"),
        Menu(title: appLocal.profileHelpCenter, iconName: "icon_info.svg"),
      ];

  void initState() {
    super.initState();
    viewModel = ProfileViewModel();
    viewModel.onErrorProfile = handleError;
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    Screen(context);
//    rebuildAllChildren(context);
    return BaseWidget<ProfileViewModel>(
        model: viewModel,
        builder: (context, model, child) => ProgressHUD(
            progressIndicator: CircleLoading(),
            inAsyncCall: viewModel.state == ProfileState.Init || viewModel.loading,
            color: Colors.transparent,
            child: Scaffold(
              appBar:
                  CustomHeader(height: Screen.convertHeightSize(55), title: appLocal.profileTitle),
              body: Builder(
                builder: (context) {
                  switch (viewModel.state) {
                    case ProfileState.Init:
                      return Container();
                      break;
                    case ProfileState.Member:
                      return Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(top: 16.0),
                                    child: ProfileTitleHeader(
                                      username: viewModel.firstname == null
                                          ? "--"
                                          : "${viewModel.firstname} ${viewModel.lastname}",
                                      birthDate: viewModel.birthday == null
                                          ? ""
                                          : convertDateFromString(viewModel.birthday),
                                      userAvatar:
                                          viewModel.avatar == null ? null : viewModel.avatar,
                                      userModel:
                                          viewModel.userModel == null ? null : viewModel.userModel,
                                        viewModel: viewModel
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Flexible(
                                child: Container(
                              child: MenuList(
                                menus: menus(),
                                viewModel: viewModel,
                                userModel: viewModel.userModel == null ? null : viewModel.userModel,
                              ),
                            )),
                            Container(
                                child: Align(
                                    alignment: Alignment.topCenter,
                                    child: FlatButton(
                                        child: Text(appLocal.settingLogout,
                                            style: TextStyle(
                                              color: Design.theme.colorInactive,
                                              fontSize: 18,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            )),
                                        onPressed: () => logout())))
                          ],
                        ),
                      );
                      break;
                    case ProfileState.Guest:
                      return Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              appLocal.profileGuestDesc,
                              style: TextStyle(color: Design.theme.colorInactive),
                            ),
                            Padding(
                              child: PrimaryButtonWidget(
                                buttonTitle: appLocal.loginBtnSubmit,
                                onBtnPressed: () async {
                                  final result =
                                      await RouterService.instance.navigateTo(LoginRoute.buildPath);
                                  if (result == true) {
                                    viewModel.state = ProfileState.Member;
                                    viewModel.getMyProfile();
                                  }
                                },
                              ),
                              padding: EdgeInsets.only(left: 15, right: 15, top: 8, bottom: 20),
                            ),
                            InkWell(
                              onTap: () {
                                RouterService.instance.navigateTo(PreRegisterRoute.buildPath);
                              },
                              child: Text(appLocal.loginBtnRegister,
                                  style: TextStyle(color: Design.theme.colorActive)),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Flexible(
                                child: Container(
                              child: MenuList(
                                menus: menusGuest(),
                                viewModel: viewModel,
                                userModel: viewModel.userModel == null ? null : viewModel.userModel,
                              ),
                            )),
                          ],
                        ),
                      );
                      break;
                    default:
                      return Container();
                  }
                },
              ),
            )));
  }

  void handleError(BaseError error) {
    print(error.toJson());
  }

  void logout() {
    viewModel.logout();
    RouterService.instance.navigateTo(MainScreenRoute.buildPath, clearStack: true);
  }

  String convertDateFromString(String strDate) {
    DateTime birthDate = DateTime.parse(strDate);
    String dateStr = DateFormat('dd MMMM yyyy').format(birthDate);
    print(dateStr);
    return dateStr;
  }

  void rebuildAllChildren(BuildContext context) {
    void rebuild(Element el) {
      el.markNeedsBuild();
      el.visitChildren(rebuild);
    }

    (context as Element).visitChildren(rebuild);
  }
}
