import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/model/language_entity.dart';

class PhoneNumberSection extends StatelessWidget {
  final String title;
  final LanguageEntity appLocal;
  PhoneNumberSection({Key key, this.title, @required this.appLocal}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(padding: EdgeInsets.only(top: 28.0)),
        Text(
          appLocal.editProfilePhoneNo,
          style: TextStyle(
            color: Design.theme.colorInactive,
            fontSize: 16,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
          ),
        ),
        Container(
          child: Text(
            title,
            style: TextStyle(
                color: Design.theme.colorPrimary,
                fontSize: 18,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
                letterSpacing: 0),
          ),
        ),
        Padding(padding: EdgeInsets.only(bottom: 28.0)),
      ],
    );
  }
}
