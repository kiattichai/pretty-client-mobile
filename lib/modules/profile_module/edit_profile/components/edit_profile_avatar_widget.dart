import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';

typedef CameraCallback = void Function();

class AvatarSection extends StatelessWidget {
  final String title;
  final String birthDate;
  final String userAvatar;
  final File imageFile;
  final CameraCallback onOpenCamera;
  final CameraCallback onPickImage;

  AvatarSection(
      {Key key,
      this.title,
      this.birthDate,
      this.userAvatar,
      this.onOpenCamera,
      this.onPickImage,
      this.imageFile})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    //rebuildAllChildren(context);
    print('imageFile : ${imageFile}');
    return Row(
      children: <Widget>[
        GestureDetector(
          child: imageFile == null ? avatarFromNetwork(context) : defaultAvatar(context),
          onTap: () async {
            print('tap');
            containerForSheet<String>(
                context: context,
                child: CupertinoActionSheet(
                  actions: <Widget>[
                    CupertinoActionSheetAction(
                      child: Text('Camera'),
                      onPressed: () {
                        onOpenCamera();
                      },
                    ),
                    CupertinoActionSheetAction(
                      child: Text('Photo Library'),
                      onPressed: () {
                        onPickImage();
                      },
                    )
                  ],
                  cancelButton: CupertinoActionSheetAction(
                    child: Text('Cancel'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ));
          },
        ),
        Padding(
          padding: EdgeInsets.only(left: 8.0),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(padding: EdgeInsets.only(top: 28.0)),
            Text(
              title,
              style: TextStyle(
                color: Design.theme.colorActive,
                fontSize: 18,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            ),
            Container(
              child: Text(
                birthDate,
                style: TextStyle(
                  color: Design.theme.colorText2,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0,
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 28.0)),
          ],
        )
      ],
    );
  }

  void containerForSheet<T>({BuildContext context, Widget child}) {
    showCupertinoModalPopup<T>(
      context: context,
      builder: (BuildContext context) => child,
    ).then<void>((T value) {
      // Scaffold.of(context).showSnackBar(new SnackBar(
      //   content: new Text('You clicked $value'),
      //   duration: Duration(milliseconds: 800),
      // ));
    });
  }

  Widget avatarFromNetwork(BuildContext context) {
    return Stack(alignment: Alignment.center, children: <Widget>[
      Row(
        children: <Widget>[
          Container(
            width: 92,
            height: 92,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                  fit: BoxFit.fill,
                  image: userAvatar == null
                      ? AssetImage('images/account_circle_black.png')
                      : NetworkImage(userAvatar)),
              border: Border.all(color: Design.theme.colorInactive, width: 1),
            ),
          ),
          Container(width: 10.0, height: 92.0)
        ],
      ),
      editAvatarButton(context)
    ]);
  }

  Widget defaultAvatar(BuildContext context) {
    return Stack(children: <Widget>[
      Row(children: <Widget>[
        Container(
          width: 92,
          height: 92,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                fit: BoxFit.fill,
                image: imageFile == null
                    ? AssetImage('images/account_circle_black.png')
                    : FileImage(imageFile)),
            border: Border.all(color: Design.theme.colorInactive, width: 1),
          ),
        ),
        Container(width: 10.0, height: 92.0)
      ]),
      editAvatarButton(context)
    ]);
  }

  Widget editAvatarButton(BuildContext context) {
    return Positioned(
        right: 5.0,
        bottom: 10.0,
        child: Stack(
          children: <Widget>[
            Container(
                width: 28,
                height: 28,
                decoration: new BoxDecoration(
                    border: Border.all(color: Color(0xffffffff), width: 1),
                    gradient: LinearGradient(
                        colors: [Color(0xfffad961), Color(0xfff76b1c)], stops: [0, 1]),
                    shape: BoxShape.circle)),
            Positioned.fill(
              child: Align(
                alignment: Alignment.center,
                child: SvgPicture.asset('images/icons_edit.svg'),
              ),
            )
          ],
        ));
  }
}
