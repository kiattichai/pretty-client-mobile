import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/edit_profile/edit_profile_screen.dart';

class EditProfileRoute extends BaseRoute {
  static String buildPath = '/edit_profile';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
      builder: (context) => EditProfileScreen(
        userModel: data,
      ),
    );
  }
}
