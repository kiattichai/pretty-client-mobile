import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_response_entity.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_update_request_entity.dart';

class EditProfileViewModel extends BaseViewModel {
  Function onErrorEditProfile;
  String firstname;
  String lastname;
  String phoneNumber;
  String birthday;
  String gender;
  String avatar;

  EditProfileViewModel();

  @override
  void postInit() {
    //getMyProfile();
  }

  void getMyProfile() {
    catchError(() async {
      final result = await di.profileRepository.getProfile();
      print('result ${result.firstName}');
      if (result == null) {
        print("firstName null");
      } else {
        print("firstName ${result.firstName}");
      }

      displayProfile(result);
      setLoading(false);
    });
  }

  void displayProfile(UserProfileResponseData data) {
    firstname = data.firstName;
    lastname = data.lastName;
    phoneNumber = data.username;
    birthday = data.birthday;
    avatar = data.avatar.url;
    gender = data.gender;
  }

  void updateProfile(UserProfileUpdateRequestEntity request, Function updateProfileSuccess) {
    catchError(() async {
      setLoading(true);
      final result = await di.profileRepository.updateProfile(request);
      updateProfileSuccess(result);
      setLoading(false);
    });
  }

  void uploadAvatar(String base64, String mime, Function updateProfileSuccess) {
    catchError(() async {
      setLoading(true);
      final result = await di.profileRepository.updateAvatar(base64, mime);
      updateProfileSuccess(result);
      setLoading(false);
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    print(error);
    onErrorEditProfile(error);
  }

  void logout() {
    di.favoriteRepository.favoriteList.clear();
    di.sharePrefInterface.clearToken();
  }
}
