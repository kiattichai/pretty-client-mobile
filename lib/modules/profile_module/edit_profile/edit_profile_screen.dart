import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/input_birthday_picker_widget.dart';
import 'package:pretty_client_mobile/widgets/input_gender_picker_widget.dart';
import 'package:pretty_client_mobile/widgets/input_text_field_form_widget.dart';
import 'package:pretty_client_mobile/widgets/primary_button_widget.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:intl/intl.dart';
import 'package:pretty_client_mobile/modules/profile_module/edit_profile/edit_profile_view_model.dart';

import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:pretty_client_mobile/widgets/modal_select_gender.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_response_entity.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_update_request_entity.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/widgets/custom_modal.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

import 'package:pretty_client_mobile/modules/profile_module/edit_profile/components/edit_profile_phone_number_widget.dart';
import 'package:pretty_client_mobile/modules/profile_module/edit_profile/components/edit_profile_avatar_widget.dart';
import 'package:mime_type/mime_type.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:flutter/services.dart';

const String MIN_DATETIME = '1960-01-01';
const String MAX_DATETIME = '2021-11-25';
const String INIT_DATETIME = '2019-05-17';

typedef CameraCallback = void Function();

class EditProfileScreen extends StatefulWidget {
  const EditProfileScreen({Key key, this.userModel}) : super(key: key);
  final UserProfileResponseData userModel;

  @override
  EditProfileScreenState createState() {
    return EditProfileScreenState();
  }
}

class EditProfileScreenState extends BaseStateProvider<EditProfileScreen, EditProfileViewModel> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController controllerFirstName = TextEditingController();
  TextEditingController controllerLastName = TextEditingController();
  TextEditingController controllerBirthday = TextEditingController();
  TextEditingController controllerGender = TextEditingController();

  final focusFirstName = FocusNode();
  final focusLastName = FocusNode();
  bool _showTitle = true;
  bool autoFocus = true;
  bool _validate = false;
  String birthDay = "";
  String gender = "";
  String _format = 'yyyy-MMM-dd';
  DateTime _dateTime;
  DateTimePickerLocale _locale = DateTimePickerLocale.en_us;
  List<DateTimePickerLocale> _locales = DateTimePickerLocale.values;
  bool isEnableButton = false;
  File _image;

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
        keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
        keyboardBarColor: Colors.grey[200],
        nextFocus: true,
        actions: [
          KeyboardAction(
            focusNode: focusFirstName,
          ),
          KeyboardAction(
            focusNode: focusLastName,
          ),
        ]);
  }

  void initState() {
    super.initState();
    viewModel = EditProfileViewModel();
    viewModel.displayProfile(widget.userModel);
    viewModel.onErrorEditProfile = handleError;
    _dateTime = DateTime.parse(INIT_DATETIME);
    controllerFirstName.text = viewModel.firstname == null ? "" : viewModel.firstname;
    controllerLastName.text = viewModel.lastname == null ? "" : viewModel.lastname;
    controllerBirthday.text =
        viewModel.birthday == null ? "" : convertDateFromString(viewModel.birthday, 'yyyy-MM-dd');
    isEnableButton = true;
    birthDay =
        viewModel.birthday == null ? "" : convertDateFromString(viewModel.birthday, 'yyyy-MM-dd');
    gender = viewModel.gender == null ? "" : viewModel.gender;
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    controllerGender.text = viewModel.gender == null ? "" : showGenderText(viewModel.gender);

    Screen(context);
    return BaseWidget<EditProfileViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        color: Colors.transparent,
        progressIndicator: CircularProgressIndicator(),
        inAsyncCall: viewModel.loading,
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          appBar: CustomHeaderDetail(
            height: Screen.convertHeightSize(55),
            title: appLocal.settingEditProfile,
          ),
          body: KeyboardActions(
            isDialog: true,
            config: _buildConfig(context),
            child: Container(
              alignment: FractionalOffset.topCenter,
              padding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              child: SafeArea(
                child: SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        SizedBox(
                          height: 20,
                        ),
                        AvatarSection(
                            title: "${controllerFirstName.text} ${controllerLastName.text}",
                            birthDate: convertDateFromString(controllerBirthday.text, 'dd MMMM yyyy'),
                            userAvatar: viewModel.avatar == null ? null : viewModel.avatar,
                            onPickImage: () => openPicker(),
                            onOpenCamera: () => openCamera(),
                            imageFile: _image),
                        PhoneNumberSection(
                          title: viewModel.phoneNumber == null ? "" : viewModel.phoneNumber,
                          appLocal: appLocal,
                        ),
                        InputTextFieldFormWidget(
                            autoFocus: false,
                            keyboardType: TextInputType.text,
                            title: appLocal.editProfileName,
                            hintText: appLocal.editProfileNameHint,
                            validate: _validate,
                            textEditingController: controllerFirstName,
                            onInputChanged: (val) {
                              setState(() {
                                updateEnableButton();
                              });
                            },
                            inputFormatter: BlacklistingTextInputFormatter(RegExp("[/\\\\]")),
                            onInputValidator: (value) {
                              var regExpResult = _regExpCheckString(value);

                              if (value.isEmpty) {
                                return "กรุณากรอกข้อมูล";
                              } else if (value.length > 20) {
                                return "ชื่อจริงไม่ควรเกิน 20 ตัวอักษร";
                              } else if (!regExpResult) {
                                return "ชื่อจริงต้องมีเฉพาะ a-z A-Z และ ตัวเลข";
                              } else {
                                return null;
                              }
                            },
                            onFieldSubmitted: (val) {
                              
                              FocusScope.of(context).requestFocus(focusFirstName);
                            },
                            focusInput: focusFirstName),
                        SizedBox(
                          height: 12,
                        ),
                        InputTextFieldFormWidget(
                            autoFocus: false,
                            title: appLocal.editProfileLastName,
                            hintText: appLocal.editProfileLastNameHint,
                            validate: _validate,
                            textEditingController: controllerLastName,
                            onInputChanged: (val) {
                              setState(() {
                                updateEnableButton();
                              });
                            },
                            inputFormatter: BlacklistingTextInputFormatter(RegExp("[/\\\\]")),
                            onInputValidator: (value) {
                              var regExpResult = _regExpCheckString(value);

                              if (value.isEmpty) {
                                return "กรุณากรอกข้อมูล";
                              } else if (value.length > 20) {
                                return "ชื่อจริงไม่ควรเกิน 20 ตัวอักษร";
                              } else if (!regExpResult) {
                                return "ชื่อจริงต้องมีเฉพาะ a-z A-Z และ ตัวเลข";
                              } else {
                                return null;
                              }
                            },
                            onFieldSubmitted: (val) {
                              FocusScope.of(context).requestFocus(focusLastName);
                            },
                            focusInput: focusLastName),
                        InputBirthDayPickerWidget(
                            appLocal: appLocal,
                            controllerBirthday: controllerBirthday,
                            onBirthDayFieldTap: () {
                              FocusScope.of(context).requestFocus(new FocusNode());
                              _showDatePicker();
                            }),
                        InputGenderPickerWidget(
                            appLocal: appLocal,
                            controllerGender: controllerGender,
                            onGenderFieldTap: () {
                              FocusScope.of(context).requestFocus(new FocusNode());
                              showModalGender();
                            }),
                        SizedBox(
                          height: 20,
                        ),
                        PrimaryButtonWidget(
                            buttonTitle: appLocal.editProfileSave,
                            onBtnPressed: isEnableButton ? () => updateProfile() : null)
  //
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  /// Display date picker.
  void _showDatePicker() {
    DatePicker.showDatePicker(
      context,
      pickerTheme: DateTimePickerTheme(
        showTitle: _showTitle,
        confirm: Text(appLocal.commonConfirm, style: TextStyle(color: Color(0xff3a3a3a))),
        cancel: Text(appLocal.commonCancel, style: TextStyle(color: Color(0xff848484))),
      ),
      minDateTime: DateTime.parse(MIN_DATETIME),
      maxDateTime: DateTime.parse(MAX_DATETIME),
      initialDateTime: _dateTime,
      dateFormat: _format,
      locale: _locale,
      onClose: () => print("----- onClose -----"),
      onCancel: () => print('onCancel'),
      onChange: (dateTime, List<int> index) {
        setState(() {
          _dateTime = dateTime;
          controllerBirthday.text = DateFormat("yyyy-MM-dd").format(_dateTime);
          birthDay = DateFormat("yyyy-MM-dd").format(_dateTime);
          updateEnableButton();
        });
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          FocusScope.of(context).requestFocus(FocusNode());
          _dateTime = dateTime;
          controllerBirthday.text = DateFormat("yyyy-MM-dd").format(_dateTime);
          birthDay = DateFormat("yyyy-MM-dd").format(_dateTime);
          updateEnableButton();
        });
      },
    );
  }

  void showModalGender() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return ModalSelectGender(
            appLocale: appLocal,
            onSelectGender: handleDisplayGender,
            initialGender: viewModel.gender,
          );
        });
  }

  handleDisplayGender(String value) {
    FocusScope.of(context).requestFocus(FocusNode());
    setState(() {
      controllerGender.text = showGenderText(value); //value;
      if (value != null) {
        gender = value;
      } else {
        gender = "";
      }
      updateEnableButton();
    });
  }

  String showGenderText(String value) {
    if (value == "male") {
      return appLocal.genderDialogMale;
    } else if (value == "female") {
      return appLocal.genderDialogFemale;
    } else if (value == "unknown") {
      return appLocal.genderDialogNotSpecified;
    } else {
      return null;
    }
  }

  String convertGenderText(String value) {
    if (value == "เพศชาย") {
      return "male";
    } else if (value == "เพศหญิง") {
      return "female";
    } else if (value == "ไม่ระบุ") {
      return "unknown";
    } else {
      return null;
    }
  }

  bool _regExpCheckString(String value) {
    RegExp regExp = new RegExp(
      r"^[a-zA-Z0-9ก-๙]*$",
      caseSensitive: false,
      multiLine: false,
    );
    print('regExp : ${regExp.hasMatch(value)}');
    if (regExp.hasMatch(value)) {
      return true;
    }
    return false;
  }

  void updateEnableButton() {
    if (controllerFirstName.text.length < 3 ||
        controllerLastName.text.length < 3 ||
        birthDay.isEmpty ||
        gender.isEmpty) {
      isEnableButton = false;
      //isEnableButton = true;
    } else {
      isEnableButton = true;
    }
  }

  updateProfile() {
    if (_formKey.currentState.validate()) {
      // No any error in validation
      _formKey.currentState.save();
      viewModel.updateProfile(
          UserProfileUpdateRequestEntity()
            ..firstName = controllerFirstName.text
            ..lastName = controllerLastName.text
            ..birthday = controllerBirthday.text
            ..gender = convertGenderText(controllerGender.text),
          onUpdateProfileSuccess);
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }

  void onUpdateProfileSuccess(BaseResponseEntity baseResponse) {
    _showAlertSuccess(baseResponse.data.message);
  }

  void _showAlertSuccess(String message) {
    // AlertDialog alertDialog = AlertDialog(
    //   content: Text(message),
    //   actions: <Widget>[
    //     FlatButton(
    //       child: Text("Ok"),
    //       onPressed: () {
    //         Navigator.of(context).pop(
    //           RouterService.instance.pop(),
    //         );
    //       },
    //     ),
    //   ],
    //   elevation: 24.0,
    // );

    // showDialog(
    //     context: context,
    //     builder: (BuildContext context) {
    //       return alertDialog;
    //     });

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomModal(
            type: ModalType.NORMAL,
            title: appLocal.confirmation,
            desc: message,
            appLocal: appLocal,
            onTapConfirm: () {
              Navigator.of(context).pop();
            },
          );
        });
  }

  void handleError(BaseError error) {
    print(error.toJson());
  }

  String convertDateFromString(String strDate, String displayFormat) {
    DateTime birthDate = DateTime.parse(strDate);
    String dateStr = DateFormat(displayFormat).format(birthDate); //'yyyy-MM-dd'
    print(dateStr);
    return dateStr;
  }

  Future openPicker() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxWidth: 450, maxHeight: 450, imageQuality: 80);
    setState(() {
      _image = image;
    });

    List<int> imageBytes = await image.readAsBytes();
    print("imageBytes : ${imageBytes}");
    print("${image.path}");
    print("path : ${image.path}");
    String mimeType = mime(image.path);
    print("mimeType : ${mimeType}");
    String base64Image = base64Encode(imageBytes);
    print("${getFileFormat(mimeType)}");
    String fileFormat = getFileFormat(mimeType);
    uploadImage(base64Image, fileFormat);
  }

  Future openCamera() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.camera, maxWidth: 450, maxHeight: 450, imageQuality: 80);
    setState(() {
      _image = image;
    });

    List<int> imageBytes = await image.readAsBytes();
    print(imageBytes);
    print("path : ${image.path}");
    String mimeType = mime(image.path);
    print("mimeType : ${mimeType}");
    String base64Image = base64Encode(imageBytes);

    String fileFormat = getFileFormat(mimeType);
    uploadImage(base64Image, fileFormat);
  }

  void uploadImage(String base64Image, String mime) {
    if (base64Image == null) {
      _showAlertSuccess("Can't get image");
    } else {
      viewModel.uploadAvatar(base64Image, mime, onUpdateProfileSuccess);
    }
  }

  String getFileFormat(String fileName) {
    int lastDot = fileName.lastIndexOf('/', fileName.length - 1);
    if (lastDot != -1) {
      String result = fileName.substring(lastDot + 1);
      if (result == "jpeg") {
        return "jpg";
      } else {
        return result;
      }
    } else
      return null;
  }
}
