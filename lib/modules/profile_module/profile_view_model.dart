import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_response_entity.dart';

enum ProfileState { Member, Guest, Init }

class ProfileViewModel extends BaseViewModel {
  Function onErrorProfile;
  String firstname;
  String lastname;
  String birthday;
  String avatar;
  UserProfileResponseData userModel;
  ProfileState state;

  ProfileViewModel() {
    state = ProfileState.Init;
  }

  @override
  void postInit() {
    super.postInit();
    getMyProfile();
  }

  void getMyProfile() {
    if (shouldLogin()) {
      state = ProfileState.Guest;
      notifyListeners();
    } else {
      catchError(() async {
        final result = await di.profileRepository.getProfile();
        await di.favoriteRepository.getFavoriteList();
        print('result ${result.firstName}');
        if (result == null) {
          print("firstName null");
        } else {
          print("firstName ${result.firstName}");
        }
        state = ProfileState.Member;
        displayProfile(result);
        setLoading(false);
      });
    }
  }

  void displayProfile(UserProfileResponseData data) {
    firstname = data.firstName;
    lastname = data.lastName;
    birthday = data.birthday;
    avatar = data.avatar.url;
    userModel = data;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    onErrorProfile(error);
  }

  bool shouldLogin() {
    return di.sharePrefInterface.shouldLogin();
  }

  void logout() {
    catchError(() async {
//      await di.profileRepository.logout();
      di.favoriteRepository.favoriteList.clear();
      di.sharePrefInterface.clearToken();
    });
  }
}
