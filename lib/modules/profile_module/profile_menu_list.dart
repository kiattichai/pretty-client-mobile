import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/modules/coins_module/coins_transaction_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/profile_menu_model.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pretty_client_mobile/modules/profile_module/profile_view_model.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/modules/profile_module/edit_profile/edit_profile_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_response_entity.dart';
import 'package:pretty_client_mobile/modules/profile_module/setting_base_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/static_web_view_menus_route.dart';

typedef MenuListCallback = void Function();

class MenuList extends StatelessWidget {
  MenuList({@required this.menus, this.onClickMenu, this.viewModel, this.userModel});
  final UserProfileResponseData userModel;
  final List<Menu> menus;
  final MenuListCallback onClickMenu;
  final ProfileViewModel viewModel;
  final numberFormat = new NumberFormat("#,###.##", "en_US");

  Widget _buildItem(BuildContext context, int index) {
    final menu = menus[index];

    return InkWell(
        child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                height: 60.0,
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 16.0, right: 8.0),
                      child: SvgPicture.asset(
                        "images/${menu.iconName}",
                        color: Design.theme.colorPrimary,
                        width: 25,
                      ),
                    ),
                    Expanded(
                        child: Text(menu.title,
                            style: TextStyle(
                              color: Design.theme.colorInactive,
                              fontSize: 18,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ))),
//                    index == 0 && viewModel.userModel != null
//                        ? Container(
//                            child: Text(
//                              "${numberFormat.format(this.userModel.wallet)} Coins",
//                              style: TextStyle(
//                                color: Design.theme.colorInactive,
//                              ),
//                            ),
//                          )
//                        : Container(),
                    Padding(
                      padding: EdgeInsets.only(left: 16.0, right: 12.0),
                      child: SvgPicture.asset(
                        "images/icons_right.svg",
                        width: 8,
                        height: 16,
                        color: Design.theme.colorInactive,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 8.0, right: 8.0),
                child: Container(
                  height: 1,
                  color: Design.theme.colorLine.withOpacity(0.45),
                ),
              )
            ]),
        onTap: () => {
              if (menus.length == 3) {handleMenu(index)} else {handleMenuGuest(index)}
            });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        itemBuilder: _buildItem,
        itemCount: menus.length,
        physics: NeverScrollableScrollPhysics());
  }

  void handleMenu(int index) async {
    switch (index) {
//      case 0:
//        await RouterService.instance
//            .navigateTo(CoinsTransactionRoute.buildPath, data: this.userModel.wallet);
//        viewModel.getMyProfile();
//        break;
      case 0:
        await RouterService.instance.navigateTo(EditProfileRoute.buildPath, data: this.userModel);
        viewModel.getMyProfile();
        break;
      case 1:
        RouterService.instance.navigateTo(SettingBaseRoute.buildPath, data: this.userModel);
        break;
      case 2:
        RouterService.instance.navigateTo(StaticWebViewMenusRoute.buildPath);
        break;
      default:
        break;
    }
  }

  void handleMenuGuest(int index) async {
    switch (index) {
      case 0:
        RouterService.instance.navigateTo(SettingBaseRoute.buildPath, data: null);
        break;
      case 1:
        RouterService.instance.navigateTo(StaticWebViewMenusRoute.buildPath);
        break;
      default:
        break;
    }
  }
}
