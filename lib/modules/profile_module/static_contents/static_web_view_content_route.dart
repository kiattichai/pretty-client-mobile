import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/static_contents/static_web_view_content.screen.dart';

class StaticWebViewRoute extends BaseRoute {
  static String buildPath = '/static_content';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => StaticWebViewContentScreen(
              urlString: data['url'],
              title: data['title'],
            ));
  }
}
