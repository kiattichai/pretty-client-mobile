import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:webview_flutter/webview_flutter.dart';

class StaticWebViewContentScreen extends StatefulWidget {
  final String urlString;
  final String title;

  const StaticWebViewContentScreen({Key key, this.urlString, this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return StaticWebViewContentState();
  }
}

class StaticWebViewContentState extends State<StaticWebViewContentScreen> {
  double heightWebView;
  WebViewController webViewController;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    heightWebView = Screen.height - Screen.convertHeightSize(55);
    isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomHeaderDetail(
        height: Screen.convertHeightSize(55),
        title: widget.title,
        onTap: () {
          RouterService.instance.pop();
        },
      ),
      body: WebView(
        onWebViewCreated: (WebViewController controller) {
          webViewController = controller;
        },
        javascriptMode: JavascriptMode.unrestricted,
        initialUrl: widget.urlString,
        onPageFinished: (_) async {
          final result =
              await webViewController.evaluateJavascript("document.documentElement.scrollHeight;");
          print(result);
        },
      ),
    );
  }
}
