import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/profile_module/notification_setting_view_model.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';

class NotificationSettingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NotificationSettingState();
  }
}

class NotificationSettingState
    extends BaseStateProvider<NotificationSettingScreen, NotificationSettingViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = NotificationSettingViewModel();
  }

  Widget lineBottom() {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: Design.theme.colorLine.withOpacity(0.45),
          ),
        ),
      ),
    );
  }

  Widget buildItem(int index) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                width: Screen.convertWidthSize(270),
                child: Text(
                  viewModel.notificationSettingRecord[index].name,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    color: viewModel.notificationSettingRecord[index].status == true
                        ? Design.theme.colorActive
                        : Design.theme.colorActive,
                  ),
                ),
              ),
              Container(
                width: 55,
                height: 20,
                child: Switch(
                  inactiveThumbColor: Color(0xffffffff),
                  inactiveTrackColor: Color(0xffc5d0de),
                  activeColor: Design.theme.colorPrimary,
                  value: viewModel.notificationSettingRecord[index].status,
                  onChanged: (val) async {
                    await viewModel.setSettingStatus(index, val);
                    await viewModel.setNotificationSetting(
                        viewModel.notificationSettingRecord[index].id, val);
                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 14, right: 14),
          child: lineBottom(),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    return Scaffold(
      appBar: CustomHeaderDetail(
        height: Screen.convertHeightSize(55),
        title: appLocal.settingNotification,
        onTap: () {
          RouterService.instance.pop();
        },
      ),
      body: BaseWidget<NotificationSettingViewModel>(
        model: viewModel,
        builder: (context, model, child) => ProgressHUD(
          color: Colors.transparent,
          progressIndicator: CircleLoading(),
          inAsyncCall: viewModel.loading,
          child: Container(
            child: ListView.builder(
              physics: const AlwaysScrollableScrollPhysics(),
              itemCount: model.notificationSettingRecord.length,
              itemBuilder: (context, i) {
                return buildItem(i);
              },
            ),
          ),
        ),
      ),
    );
  }

  void handleError(BaseError error) {
    print(error.toJson());
  }
}
