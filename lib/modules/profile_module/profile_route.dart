import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/profile_base_screen.dart';


class ProfileRoute extends BaseRoute {
  static String buildPath = '/profile';

  @override
  String get path => buildPath;

  @override
  Future<bool> hasPermission(params) async {
    return await Future.delayed(Duration(seconds: 1), () => true);
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => ProfileScreen());
  }
}
