import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/modules/profile_module/setting_base_view_model.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/profile_menu_model.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pretty_client_mobile/modules/profile_module/static_contents/static_web_view_content_route.dart';
import 'package:pretty_client_mobile/env_config.dart';

class StaticWebViewMenusScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StaticWebViewMenusState();
  }
}

class StaticWebViewMenusState
    extends BaseStateProvider<StaticWebViewMenusScreen, SettingBaseViewModel> {
  List<Menu> menus() => [
        Menu(title: appLocal.profileContactUs, iconName: "icons_communication.svg"),
        Menu(title: appLocal.profileTermsOfService, iconName: "icon_policy.svg"),
        Menu(title: appLocal.profilePrivacyPolicy, iconName: "icons_privacy.svg"),
      ];

  @override
  void initState() {
    super.initState();
    viewModel = SettingBaseViewModel();
  }

  Widget buildItem(int index) {
    final menu = menus()[index];
    return GestureDetector(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 24, right: 12, top: 20, bottom: 20),
              child: Row(
                // crossAxisAlignment: CrossAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SvgPicture.asset(
                    "images/${menu.iconName}",
                    width: 20,
                    height: 20,
                    color: Design.theme.colorPrimary,
                  ),
                  Container(
                    width: Screen.convertWidthSize(270),
                    child: Text(
                      menu.title,
                      style: TextStyle(
                        color: Design.theme.colorInactive,
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                  SvgPicture.asset(
                    "images/icons_right.svg",
                    width: 8,
                    height: 16,
                    color: Design.theme.colorInactive,
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 14, right: 14),
              child: lineBottom(),
            )
          ],
        ),
        onTap: () => handleMenu(index));
  }

  Widget lineBottom() {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: Design.theme.colorLine.withOpacity(0.45),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    return Scaffold(
      appBar: CustomHeaderDetail(
        height: Screen.convertHeightSize(55),
        title: appLocal.profileHelpCenter,
        onTap: () {
          RouterService.instance.pop();
        },
      ),
      body: BaseWidget<SettingBaseViewModel>(
        model: viewModel,
        builder: (context, model, child) => ProgressHUD(
          color: Design.theme.colorMainBackground,
          progressIndicator: CircleLoading(),
          inAsyncCall: model.loading,
          child: Container(
            color: Design.theme.colorMainBackground,
            child: ListView.builder(
              physics: const AlwaysScrollableScrollPhysics(),
              itemCount: menus().length,
              itemBuilder: (context, i) {
                return buildItem(i);
              },
            ),
          ),
        ),
      ),
    );
  }

  void handleMenu(int index) async {
    switch (index) {
      case 0:
        RouterService.instance.navigateTo(StaticWebViewRoute.buildPath, data: {
          'url': "${EnvConfig.instance.frontUrl}/webview/c/contact?lang=th",
          'title': menus()[index].title
        });
        break;
      case 1:
        RouterService.instance.navigateTo(StaticWebViewRoute.buildPath, data: {
          'url': "${EnvConfig.instance.frontUrl}/webview/c/privacy?lang=th",
          'title': menus()[index].title
        });
        break;
      case 2:
        RouterService.instance.navigateTo(StaticWebViewRoute.buildPath, data: {
          'url': "${EnvConfig.instance.frontUrl}/webview/c/term-and-conditions?lang=th",
          'title': menus()[index].title
        });
        break;
      default:
        break;
    }
  }
}
