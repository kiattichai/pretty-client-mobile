import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ListsTitle extends StatelessWidget {

  final String title; 
  ListsTitle({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(padding: EdgeInsets.only(top: 28.0)),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          
          children: <Widget>[
            Padding(padding: EdgeInsets.only(left: 12.0)),
            SvgPicture.asset(
              "images/friends.svg",
              width: 18,
              height: 18
            ),
            Padding(padding: EdgeInsets.only(right: 12.0)),
            Container(
              child: Text(
                title,
                style: TextStyle(
                  fontFamily: 'SFUIText',
                  color: Color(0xff1d1d1d),
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(right: 12.0)),
          ],
        ),
        Padding(padding: EdgeInsets.only(bottom: 28.0)),
      ],
    );
  }
}