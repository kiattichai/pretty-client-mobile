import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/notification_setting_screen.dart';

class NotificationSettingRoute extends BaseRoute {
  static String buildPath = '/notification_setting';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
      builder: (context) => NotificationSettingScreen(),
    );
  }
}
