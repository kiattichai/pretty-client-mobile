import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class UserProfileResponseEntity with JsonConvert<UserProfileResponseEntity> {
  UserProfileResponseData data;
}

class UserProfileResponseData with JsonConvert<UserProfileResponseData> {
  int id;
  String username;
  @JSONField(name: "first_name")
  String firstName;
  @JSONField(name: "last_name")
  String lastName;
  String idcard;
  @JSONField(name: "display_name")
  String displayName;
  UserProfileAvatar avatar;
  String gender;
  String birthday;
  @JSONField(name: "terms_accepted")
  bool termsAccepted;
  bool activated;
  @JSONField(name: "activated_at")
  String activatedAt;
  bool blocked;
  @JSONField(name: "profile_score")
  int profileScore;
  @JSONField(name: "last_login_at")
  String lastLoginAt;
  @JSONField(name: "last_login_ip")
  String lastLoginIp;
  @JSONField(name: "created_at")
  String createdAt;
  @JSONField(name: "updated_at")
  String updatedAt;
  String referral;
  String lang;
  UserProfileGroup group;
  UserProfileAgent agent;
  @JSONField(name: "wallet")
  double wallet;
}

class UserProfileAvatar with JsonConvert<UserProfileAvatar> {
  String id;
  String name;
  int width;
  int height;
  String mime;
  String size;
  String url;
  @JSONField(name: "resize_url")
  String resizeUrl;
}

class UserProfileWallet with JsonConvert<UserProfileWallet> {
  int id;
  double amount;
}

class UserProfileGroup with JsonConvert<UserProfileGroup> {
  int id;
  String name;
}

class UserProfileAgent with JsonConvert<UserProfileAgent> {
  int id;
}
