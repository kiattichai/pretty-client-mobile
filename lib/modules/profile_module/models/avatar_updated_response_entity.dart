import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class AvatarUpdatedResponseEntity with JsonConvert<AvatarUpdatedResponseEntity> {
  AvatarUpdatedResponseData data;
}

class AvatarUpdatedResponseData with JsonConvert<AvatarUpdatedResponseData> {
  String message;
  String image;
}
