import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class NotificationSettingResponseEntity with JsonConvert<NotificationSettingResponseEntity> {
  NotificationSettingData data;
  NotificationSettingBench bench;
}

class NotificationSettingData with JsonConvert<NotificationSettingData> {
  bool summary;
  List<NotificationSettingRecord> record;
}

class NotificationSettingRecord with JsonConvert<NotificationSettingRecord> {
  int id;
  String name;
  bool status;
}

class NotificationSettingBench with JsonConvert<NotificationSettingBench> {
  int second;
  double millisecond;
  String format;
}
