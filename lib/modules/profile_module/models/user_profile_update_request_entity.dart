import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class UserProfileUpdateRequestEntity with JsonConvert<UserProfileUpdateRequestEntity> {
  @JSONField(name: "first_name")
  String firstName;
  @JSONField(name: "last_name")
  String lastName;
  String idcard;
  @JSONField(name: "display_name")
  String displayName;
  String gender;
  String birthday;
  @JSONField(name: "terms_accepted")
  bool termsAccepted;
}
