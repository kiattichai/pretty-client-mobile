import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class PasswordUpdateRequestEntity with JsonConvert<PasswordUpdateRequestEntity> {
  @JSONField(name: "password_old")
  String passwordOld;
  String password;
  @JSONField(name: "password_confirmation")
  String passwordConfirmation;
}
