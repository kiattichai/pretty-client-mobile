import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class AvatarResponseEntity with JsonConvert<AvatarResponseEntity> {
  AvatarResponseData data;
}

class AvatarResponseData with JsonConvert<AvatarResponseData> {
  String id;
  String name;
  int width;
  int height;
  String mime;
  String size;
  String url;
  @JSONField(name: "resize_url")
  String resizeUrl;
}
