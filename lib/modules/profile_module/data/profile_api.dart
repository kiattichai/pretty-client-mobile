import 'package:dio/dio.dart';
import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/avatar_response_entity.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/password_update_request_entity.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_response_entity.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_update_request_entity.dart';

class ProfileApi {
  final CoreApi coreApi;
  final String pathGetAvatart = "/users/avatar";
  final String pathUserProfile = '/users/profile';
  final String pathUserPassword = '/users/password';

  @provide
  @singleton
  ProfileApi(this.coreApi);

  Future<AvatarResponseEntity> getAvatar(Function badRequestToModelError) async {
    Response response = await coreApi
        .get(pathGetAvatart, {}, BaseErrorEntity.badRequestToModelError, hasPermission: true);
    return AvatarResponseEntity().fromJson(response.data);
  }

  Future<BaseResponseEntity> updateAvatar(String base64, String mime, Function badRequestToModelError) async {
    final data = {"file_name": "my_picture.${mime}", "file_data": base64};
    Response response =
        await coreApi.put(pathGetAvatart, data, BaseErrorEntity.badRequestToModelError);
    return BaseResponseEntity().fromJson(response.data);
  }

  Future<UserProfileResponseEntity> getUserProfile(Function badRequestToModelError) async {
    Response response = await coreApi
        .get(pathUserProfile, null, BaseErrorEntity.badRequestToModelError, hasPermission: true);
    return UserProfileResponseEntity().fromJson(response.data);
  }

  Future<BaseResponseEntity> updateUserProfile(
      UserProfileUpdateRequestEntity request, Function badRequestToModelError) async {
    Response response = await coreApi.put(
        pathUserProfile, request.toJson(), BaseErrorEntity.badRequestToModelError);
    return BaseResponseEntity().fromJson(response.data);
  }

  Future<BaseResponseEntity> updatePassword(
      PasswordUpdateRequestEntity request, Function badRequestToModelError) async {
    Response response = await coreApi.put(
        pathUserPassword, request.toJson(), BaseErrorEntity.badRequestToModelError);
    return BaseResponseEntity().fromJson(response.data);
  }

  Future<BaseResponseEntity> changePassword(
      String oldPassword, String password, String confirmPassword) async {
    var queryParameters = {"password_old": oldPassword, "password": password, "password_confirmation": confirmPassword};
    final response = await coreApi.put(pathUserPassword, queryParameters,
        BaseErrorEntity.badRequestToModelError);
    return BaseResponseEntity().fromJson(response.data);
  }

  Future<BaseResponseEntity> logout(Function badRequestToModelError) async {
    final response = await coreApi.delete("/users/authen",
        BaseErrorEntity.badRequestToModelError, hasPermission: true);
    return BaseResponseEntity().fromJson(response.data);
  }


}
