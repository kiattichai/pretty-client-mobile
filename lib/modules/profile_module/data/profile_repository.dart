import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/modules/profile_module/data/profile_api.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/avatar_response_entity.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_response_entity.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_update_request_entity.dart';

class ProfileRepository extends BaseRepository {
  final ProfileApi api;
  AvatarResponseData avatarData;
  UserProfileResponseData profileData;

  @provide
  ProfileRepository(this.api);

  Future<UserProfileResponseData> getProfile() async {
    final model = await api.getUserProfile(BaseErrorEntity.badRequestToModelError);
    return model.data;
  }

  Future<BaseResponseEntity> changePassword(
      String oldPassword, String password, String confirmPassword) async {
    final response = await api.changePassword(oldPassword, password, confirmPassword);
    return response;
  }

  Future<BaseResponseEntity> updateProfile(
    UserProfileUpdateRequestEntity requestUpdateProfileEntity) async {
      final response = await api.updateUserProfile(requestUpdateProfileEntity, BaseErrorEntity.badRequestToModelError);
      return response;
  }

  Future<BaseResponseEntity> updateAvatar(String base64, String mime) async {
    final response = await api.updateAvatar(base64, mime, BaseErrorEntity.badRequestToModelError);
    return response;
  }

  Future<BaseResponseEntity> logout() async {
    final response = await api.logout(BaseErrorEntity.badRequestToModelError);
    return response;
  }

}
