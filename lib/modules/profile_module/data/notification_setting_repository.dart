import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/profile_module/data/notification_setting_api.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/notification_setting_response_entity.dart';
import 'package:inject/inject.dart';

class NotificationSettingRepository extends BaseRepository {
  final NotificationSettingApi notificationSettingApi;

  List<NotificationSettingRecord> notificationSettingRecord = [];

  @provide
  NotificationSettingRepository(this.notificationSettingApi);

  Future<List<NotificationSettingRecord>> notificationSetting() async {

    final response = await notificationSettingApi.notificationSetting(BaseErrorEntity.badRequestToModelError);

    if (response.data != null) {
      notificationSettingRecord.clear();
      notificationSettingRecord.addAll(response.data.record);
      return notificationSettingRecord;
    } else {
      return [];
    }
  }

  Future<BaseResponseEntity> setNotificationSetting(int id,bool status) async {

    final response = await notificationSettingApi.setNotificationSetting(id, status ,BaseErrorEntity.badRequestToModelError);

    return response;
  }
}