import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/notification_setting_response_entity.dart';
import 'package:inject/inject.dart';

class NotificationSettingApi {
  final CoreApi coreApi;
  final String path = '/users/notifications';

  @provide
  @singleton
  NotificationSettingApi(this.coreApi);

  Future<NotificationSettingResponseEntity> notificationSetting(Function badRequestToModelError) async {
    final response = await coreApi.get(path, null, badRequestToModelError, hasPermission: true);
    return NotificationSettingResponseEntity().fromJson(response.data);
  }

  Future<BaseResponseEntity> setNotificationSetting(int id, bool status,Function badRequestToModelError) async {
    final response = await coreApi.put(path+"/"+id.toString(), {"status": status}, badRequestToModelError, hasPermission: true);
    return BaseResponseEntity().fromJson(response.data);
  }

}