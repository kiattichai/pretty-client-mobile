class RequestUpdateProfileEntity {
  String firstName;
  String lastName;
  String displayName;
  String gender;
  String birthday;
  String termsAccepted;
  

  RequestUpdateProfileEntity(
      {this.firstName,
      this.lastName,
      this.displayName,
      this.gender,
      this.birthday,
      this.termsAccepted});

  RequestUpdateProfileEntity.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name'];
    lastName = json['last_name'];
    displayName = json['display_name'];
    gender = json['gender'];
    birthday = json['birthday'];
    termsAccepted = json['terms_accepted'];
    
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['display_name'] = this.displayName;
    data['gender'] = this.gender;
    data['birthday'] = this.birthday;
    data['terms_accepted'] = this.termsAccepted;
    return data;
  }
}
