import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/notification_setting_response_entity.dart';

class NotificationSettingViewModel extends BaseViewModel {
  Function onErrorSetting;
  List<NotificationSettingRecord>  notificationSettingRecord = [];
  ScrollController scrollController;

  @override
  void postInit() {
    notificationSetting();
  }

  Future<void> notificationSetting() async {
    setLoading(true);
    catchError(() async {
      notificationSettingRecord = await di.notificationSettingRepository.notificationSetting();
      setLoading(false);
    });
  }

  Future<void> setSettingStatus(int index, status){
    notificationSettingRecord[index].status = status;
    notifyListeners();
  }

  Future<void> setNotificationSetting(int id, bool status) async {
    catchError(() async {
      await di.notificationSettingRepository.setNotificationSetting(id, status);
    });
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    print(error);
    onErrorSetting(error);
  }
}