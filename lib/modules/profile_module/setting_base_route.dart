import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/setting_base_screen.dart';

class SettingBaseRoute extends BaseRoute {
  static String buildPath = '/setting';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
      builder: (context) => SettingBaseScreen(
        userModel: data,
      ),
    );
  }
}
