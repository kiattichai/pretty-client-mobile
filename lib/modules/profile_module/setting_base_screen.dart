import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/app/app_state.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/env_config.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/modules/profile_module/setting_base_view_model.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/profile_menu_model.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pretty_client_mobile/modules/profile_module/edit_profile/edit_profile_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_response_entity.dart';
import 'package:pretty_client_mobile/modules/change_password_module/change_password_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/notification_setting_route.dart';
import 'package:provider/provider.dart';

class SettingBaseScreen extends StatefulWidget {
  SettingBaseScreen({this.userModel});
  final UserProfileResponseData userModel;

  @override
  State<StatefulWidget> createState() {
    return SettingBaseScreenState();
  }
}

class SettingBaseScreenState extends BaseStateProvider<SettingBaseScreen, SettingBaseViewModel> {
  List<Menu> menus() => [
        Menu(title: appLocal.settingEditProfile, iconName: ""),
        Menu(title: appLocal.settingNotification, iconName: ""),
        Menu(title: appLocal.settingChangeLanguage),
        Menu(title: appLocal.settingChangePassword, iconName: ""),
        Menu(title: appLocal.settingCurrentVersion)
      ];

  List<Menu> menusGuest() =>
      [Menu(title: appLocal.settingChangeLanguage), Menu(title: appLocal.settingCurrentVersion)];

  @override
  void initState() {
    super.initState();
    viewModel = SettingBaseViewModel();
  }

  Widget buildItem(int index, String title) {
    return InkWell(
      onTap: () => handleMenu(index),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 24, right: 12, top: 20, bottom: 20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: Screen.convertWidthSize(270),
                  child: Text(
                    title,
                    style: TextStyle(
                      color: Design.theme.colorInactive,
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
                SvgPicture.asset(
                  "images/icons_right.svg",
                  width: 8,
                  height: 16,
                  color: Design.theme.colorInactive,
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 14, right: 14),
            child: lineBottom(),
          )
        ],
      ),
    );
  }

  Widget buildItemChangeLanguage(int index, BuildContext context, String title) {
    return InkWell(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 24, right: 12, top: 20, bottom: 20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: Screen.convertWidthSize(270),
                    child: Text(
                      title,
                      style: TextStyle(
                        color: Design.theme.colorInactive,
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                  Text(
                    appLocal.currentLanguage,
                    style: TextStyle(
                        color: Design.theme.colorPrimary,
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 14, right: 14),
              child: lineBottom(),
            )
          ],
        ),
        onTap: () => widget.userModel != null ? handleMenu(index) : handleMenuGuest(index));
  }

  Widget buildItemVersion(int index, BuildContext context, String title) {
    return InkWell(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 24, right: 12, top: 20, bottom: 20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: Screen.convertWidthSize(270),
                    child: Text(
                      title,
                      style: TextStyle(
                        color: Design.theme.colorInactive,
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                  Text(
                    EnvConfig.instance.versionApp,
                    style: TextStyle(
                        color: Design.theme.colorPrimary,
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 14, right: 14),
              child: lineBottom(),
            )
          ],
        ),
        onTap: () => widget.userModel != null ? handleMenu(index) : handleMenuGuest(index));
  }

  Widget lineBottom() {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: Design.theme.colorLine.withOpacity(0.45),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    appState = Provider.of<AppState>(context);
    return Scaffold(
      appBar: CustomHeaderDetail(
        height: Screen.convertHeightSize(55),
        title: appLocal.settingTitle,
        onTap: () {
          RouterService.instance.pop();
        },
      ),
      body: BaseWidget<SettingBaseViewModel>(
        model: viewModel,
        builder: (context, model, child) => ProgressHUD(
          color: Colors.transparent,
          progressIndicator: CircleLoading(),
          inAsyncCall: viewModel.loading,
          child: Builder(
            builder: (context) {
              if (widget.userModel != null) {
                return Container(
                  child: ListView.builder(
                    physics: const AlwaysScrollableScrollPhysics(),
                    itemCount: menus().length,
                    itemBuilder: (context, i) {
                      switch (i) {
                        case 2:
                          return buildItemChangeLanguage(i, context, menus()[i].title);
                        case 4:
                          return buildItemVersion(i, context, menus()[i].title);
                        default:
                          return buildItem(i, menus()[i].title);
                      }
                    },
                  ),
                );
              } else {
                return Container(
                  child: ListView.builder(
                    physics: const AlwaysScrollableScrollPhysics(),
                    itemCount: menusGuest().length,
                    itemBuilder: (context, i) {
                      switch (i) {
                        case 0:
                          return buildItemChangeLanguage(i, context, menusGuest()[i].title);
                        case 1:
                          return buildItemVersion(i, context, menusGuest()[i].title);
                        default:
                          return Container();
                      }
                    },
                  ),
                );
              }
            },
          ),
        ),
      ),
    );
  }

  void handleMenu(int index) async {
    switch (index) {
      case 0:
        RouterService.instance.navigateTo(EditProfileRoute.buildPath, data: widget.userModel);
        break;
      case 1:
        RouterService.instance.navigateTo(NotificationSettingRoute.buildPath);
        break;
      case 2:
        if (appState.appLocal == Locale('th')) {
          appState.changeLanguage(Locale("en"));
        } else {
          appState.changeLanguage(Locale("th"));
        }
        break;
      case 3:
        RouterService.instance.navigateTo(ChangePasswordRoute.buildPath);
        break;
      default:
        break;
    }
  }

  void handleMenuGuest(int index) async {
    switch (index) {
      case 0:
        if (appState.appLocal == Locale('th')) {
          appState.changeLanguage(Locale("en"));
        } else {
          appState.changeLanguage(Locale("th"));
        }
        break;
      default:
        break;
    }
  }
}
