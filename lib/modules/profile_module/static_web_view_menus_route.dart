import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/static_web_view_menus_screen.dart';

class StaticWebViewMenusRoute extends BaseRoute {
  static String buildPath = '/static_web_menus';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
      builder: (context) => StaticWebViewMenusScreen(),
    );
  }
}
