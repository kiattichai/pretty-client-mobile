import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';

class SettingBaseViewModel extends BaseViewModel {
  Function onErrorSetting;
  ScrollController scrollController;

  @override
  void postInit() {
    
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    print(error);
    onErrorSetting(error);
  }
}