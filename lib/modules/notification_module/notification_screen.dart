import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/env_config.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/coins_module/coins_transaction_route.dart';
import 'package:pretty_client_mobile/modules/feedback_module/feedback_route.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/order_detail_route.dart';
import 'package:pretty_client_mobile/modules/notification_module/models/notification_response_entity.dart';
import 'package:pretty_client_mobile/modules/notification_module/notification_list_route.dart';
import 'package:pretty_client_mobile/modules/notification_module/notification_view_model.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custome_header.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';

class NotificationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NotificationScreenState();
  }
}

class NotificationScreenState
    extends BaseStateProvider<NotificationScreen, NotificationViewModel> {
  final int title_count = 4;

  @override
  void initState() {
    super.initState();
    viewModel = NotificationViewModel();
    viewModel.setMessageType("");
    viewModel
      ..onErrorNotificationList = handleError
      ..scrollController.addListener(() {
        if (viewModel.scrollController.position.extentAfter == 0) {
          viewModel.setPage(viewModel.page + 1);
          viewModel.getMessageList(viewModel.limit, viewModel.page);
        }
      });
  }

  Widget sizeBox(double data) => SizedBox(
        height: data,
      );

  Widget iconTabNotification(String img_url) {
    return Container(
      margin: EdgeInsets.only(right: 12, top: 2),
      child: SvgPicture.asset(
        img_url,
        width: 34,
        height: 34,
        color: Design.theme.colorPrimary,
      ),
    );
  }

  Widget imageItem(String img_url) {
    if (img_url != "") {
      return Container(
        margin: EdgeInsets.only(right: 12, top: 2),
        child: Image.network(
          img_url,
          width: 55,
          height: 55,
        ),
      );
    } else {
      return Container(
        margin: EdgeInsets.only(right: 12, top: 2),
        child: Icon(
          Icons.replay,
          color: Design.theme.fontColorSecondary,
          size: 55,
          semanticLabel: 'Text to announce in accessibility modes',
        ),
      );
    }
  }

  Widget lineBottom() {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: Design.theme.colorLine.withOpacity(0.6),
          ),
        ),
      ),
    );
  }

  Widget notificationTabPromotion() {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: 25, bottom: 25, left: 20, right: 20),
          color: Design.theme.colorBox,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              iconTabNotification("images/icon_noti_promotion.svg"),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        appLocal.notificationPromotionTitle,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          color: Design.theme.colorActive.withOpacity(0.8),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Container(
                      child: Text(
                        viewModel.lastPromotionDesc == ""
                            ? appLocal.notificationActivityDesc
                            : viewModel.lastPromotionDesc,
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          color: Design.theme.colorText2.withOpacity(0.6),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        lineBottom(),
      ],
    );
  }

  Widget notificationTabActivity() {
    return Column(
      children: <Widget>[
        Container(
          color: Design.theme.colorBox,
          padding: EdgeInsets.only(top: 25, bottom: 25, left: 20, right: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              iconTabNotification("images/icon_noti_bell.svg"),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        appLocal.notificationActivityTitle,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          color: Design.theme.colorActive.withOpacity(0.8),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Container(
                      child: Text(
                        viewModel.lastActivityDesc == ""
                            ? appLocal.notificationActivityDesc
                            : viewModel.lastActivityDesc,
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          color: Design.theme.colorText2.withOpacity(0.6),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget titleMessageList() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        sizeBox(15),
        Container(
          padding: EdgeInsets.only(left: 20, right: 15, top: 10, bottom: 12),
          color: Color(0xfd92a2b2b),
          child: Text(
            appLocal.notificationSectionTitle,
            style: TextStyle(
                color: Design.theme.colorPrimary,
                fontSize: 14,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal),
          ),
        )
      ],
    );
  }

  Widget buildItem(List<NotificationRecord> notificationRecord, int list_index,
      int recordIndex) {
    if (list_index == 0) {
      return sizeBox(20);
    } else if (list_index == 1) {
      return InkWell(
        child: notificationTabPromotion(),
        onTap: () async {
          await RouterService.instance
              .navigateTo(NotificationListRoute.buildPath, data: {
            "type": "promotion",
            "title": appLocal.notificationPromotionTitle
          });
          viewModel.resetRecord();
          return viewModel.getMessageList(viewModel.limit, viewModel.page);
        },
      );
    } else if (list_index == 2) {
      return InkWell(
        child: notificationTabActivity(),
        onTap: () async {
          await RouterService.instance
              .navigateTo(NotificationListRoute.buildPath, data: {
            "type": "activity",
            "title": appLocal.notificationActivityTitle
          });
          viewModel.resetRecord();
          return viewModel.getMessageList(viewModel.limit, viewModel.page);
        },
      );
    } else if (list_index == 3) {
      return titleMessageList();
    } else if (list_index >= 4) {
      NotificationRecord record = notificationRecord[recordIndex];
      return InkWell(
        onTap: () {
          switch (record.refPath) {
            case "wallet":
              viewModel.readMessage(record.type.text);
              RouterService.instance
                  .navigateTo(CoinsTransactionRoute.buildPath, data: 0.00);
              break;
            case "order":
              viewModel.readMessage(record.type.text);
              dynamic dataOrder = {
                "order_id": int.parse(record.refCode),
                "ispop": true
              };
              RouterService.instance
                  .navigateTo(OrderDetailRoute.buildPath, data: dataOrder);
              break;
            case "feedback":
              dynamic dataOrder = {
                "order_id": int.parse(record.refCode),
                "ispop": true
              };
              RouterService.instance
                  .navigateTo(FeedbackRoute.buildPath, data: dataOrder);
              break;
            default:
          }
        },
        child: Column(
          children: <Widget>[
            Container(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 15),
              color: !record.read
                  ? Design.theme.colorBox
                  : Design.theme.colorMainBackground,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  imageItem(getImgUrl(record.images, 100, 100)),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Text(
                            record.title,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 16,
                              color: !record.read
                                  ? Design.theme.colorActive.withOpacity(0.7)
                                  : Design.theme.colorActive,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 2,
                        ),
                        Container(
                          child: Text(
                            record.description,
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              color: Design.theme.colorText2.withOpacity(0.6),
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "${record.createdAt.date} ${record.createdAt.time}",
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              color: Design.theme.colorText2.withOpacity(0.6),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            lineBottom(),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return BaseWidget<NotificationViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        progressIndicator: CircleLoading(),
        inAsyncCall: viewModel.loading,
        color: Colors.transparent,
        child: Scaffold(
          appBar: CustomHeader(
            height: Screen.convertHeightSize(55),
            title: appLocal.notificationHeader,
          ),
          body: Builder(
            builder: (context) {
              return RefreshIndicator(
                color: Design.theme.colorMainBackground,
                backgroundColor: Design.theme.colorPrimary,
                key: model.refreshIndicatorKey,
                onRefresh: () {
                  model.resetRecord();
                  return model.getMessageList(model.limit, model.page);
                },
                child: Container(
                  child: Builder(builder: (context) {
                    if (model.notificationRecord.length == 0 &&
                        !model.is_complete) {
                      return ListView(
                        children: <Widget>[
                          sizeBox(20),
                          notificationTabPromotion(),
                          notificationTabActivity(),
                          titleMessageList(),
                        ],
                      );
                    } else if (model.notificationRecord.length == 0 &&
                        model.is_complete) {
                      return ListView(
                        physics: const AlwaysScrollableScrollPhysics(),
                        controller: viewModel.scrollController,
                        children: <Widget>[
                          sizeBox(20),
                          notificationTabPromotion(),
                          notificationTabActivity(),
                          titleMessageList(),
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              color: Design.theme.colorMainBackground,
                              margin: EdgeInsets.only(top: 15),
                              child: Text(
                                appLocal.notificationActivityDesc,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  color:
                                      Design.theme.colorActive.withOpacity(0.8),
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    } else {
                      return ListView.builder(
                        physics: const AlwaysScrollableScrollPhysics(),
                        itemCount:
                            model.notificationRecord.length + title_count,
                        controller: model.scrollController,
                        itemBuilder: (context, i) {
                          int n = i - title_count;
                          return buildItem(model.notificationRecord, i, n);
                        },
                      );
                    }
                  }),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void handleError(BaseError error) {
    print(error.toJson());
  }

  String getImgUrl(List<NotificationImage> img, double w, double h) {
    String result = "";
    if (img.length > 0) {
      result =
          "${EnvConfig.instance.resUrl}/w_${w.round()},h_${h.round()},c_crop/${img[0].id}/${img[0].name}";
    }
    return result;
  }
}
