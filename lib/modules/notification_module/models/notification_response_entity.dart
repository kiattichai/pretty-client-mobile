import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class NotificationResponseEntity with JsonConvert<NotificationResponseEntity> {
  NotificationResponseData data;
  NotificationResponseBench bench;
}

class NotificationResponseData with JsonConvert<NotificationResponseData> {
  NotificationResponseDataPagination pagination;
  List<NotificationRecord> record;
  bool cache;
}

class NotificationResponseDataPagination with JsonConvert<NotificationResponseDataPagination> {
  @JSONField(name: "current_page")
  int currentPage;
  @JSONField(name: "last_page")
  int lastPage;
  int limit;
  int total;
}

class NotificationRecord with JsonConvert<NotificationRecord> {
  int id;
  NotificationType type;
  String title;
  String description;
  NotificationUser user;
  bool read;
  bool status;
  @JSONField(name: "ref_path")
  String refPath;
  @JSONField(name: "ref_code")
  String refCode;
  List<NotificationImage> images;
  @JSONField(name: "created_at")
  NotificationResponseDataRecordCreatedAt createdAt;
  @JSONField(name: "updated_at")
  NotificationResponseDataRecordUpdatedAt updatedAt;
  dynamic meta;
}

class NotificationType with JsonConvert<NotificationType> {
  int id;
  String text;
}

class NotificationUser with JsonConvert<NotificationUser> {
  int id;
  String username;
  @JSONField(name: "country_code")
  String countryCode;
  @JSONField(name: "first_name")
  String firstName;
  @JSONField(name: "last_name")
  String lastName;
}

class NotificationImage with JsonConvert<NotificationImage> {
  String id;
  String name;
  int width;
  int height;
  String mime;
  int size;
  String url;
}

class NotificationResponseDataRecordCreatedAt
    with JsonConvert<NotificationResponseDataRecordCreatedAt> {
  String value;
  String date;
  String time;
}

class NotificationResponseDataRecordUpdatedAt
    with JsonConvert<NotificationResponseDataRecordUpdatedAt> {
  String value;
  String date;
  String time;
}

class NotificationResponseBench with JsonConvert<NotificationResponseBench> {
  int second;
  double millisecond;
  String format;
}
