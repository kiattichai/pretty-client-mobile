import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/modules/notification_module/models/notification_response_entity.dart';

class NotificationViewModel extends BaseViewModel {
  Function onErrorNotificationList;
  List<NotificationRecord> notificationRecord = [];
  ScrollController scrollController;
  int limit = 10;
  int page = 1;
  String lastActivityDesc = "";
  String lastPromotionDesc = "";
  int lastActivityIndex = -1;
  int lastPromotionIndex = -1;
  bool is_complete = false;
  bool load_more = false;
  String message_type = "";

  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;

  NotificationViewModel() {
    refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    scrollController = ScrollController();
  }

  @override
  void postInit() {
    lastActivityDesc = "";
    lastPromotionDesc = "";
    getMessageList(limit, page);
  }

  Future<void> getMessageList(limit, page) async {
    if (page == 1) {
      setLoading(true);
    } else {
      setLoadMore(true);
    }

    catchError(() async {
      NotificationResponseEntity tmp = null;
      if (is_complete == false) {
        tmp = await di.notificationRepository.messageList(limit, page, message_type);
        if(page == 1){
          setNotificationLastDesc(tmp.data.record);
        }
        if(!message_type.isEmpty){
          di.notificationRepository.readMessage(message_type);
        }
      }
      if (tmp !=null && tmp.data.record.length > 0 && tmp.data.pagination.currentPage <= tmp.data.pagination.lastPage) {
        notificationRecord..addAll(tmp.data.record);
      } else {
        setIsComplete(true);
      }
      setLoading(false);
      setLoadMore(false);
    });
  }

  void readMessage(String type) async {
    catchError(() async {
      di.notificationRepository.readMessage(type);
    });
  }

  void setPage(int value) {
    page = value;
  }

  void setIsComplete(bool value) {
    is_complete = value;
  }

  void setLoadMore(bool value) {
    load_more = value;
  }

  void setMessageType(String value) {
    message_type = value;
  }

  void resetRecord() {
    setPage(1);
    setIsComplete(false);
    notificationRecord.clear();
  }

  void setNotificationLastDesc(List<NotificationRecord> record){
    String a="";
    String b="";
    for(int i=0;i<record.length; i++){
      if(record[i].type.text == "activity" && a.isEmpty){
        a = record[i].description;
        lastActivityIndex = i;
      }
      if(record[i].type.text == "promotion" && b.isEmpty){
        b = record[i].description;
        lastPromotionIndex = i;
      }
    }
    lastActivityDesc = a.isEmpty? "": a;
    lastPromotionDesc = b.isEmpty? "": b;
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    print(error);
    onErrorNotificationList(error);
  }
}
