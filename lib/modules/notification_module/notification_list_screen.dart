import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/env_config.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/coins_module/coins_transaction_route.dart';
import 'package:pretty_client_mobile/modules/feedback_module/feedback_route.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/order_detail_route.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'models/notification_response_entity.dart';
import 'notification_view_model.dart';

class NotificationListScreen extends StatefulWidget {
  final dynamic notificationData;

  const NotificationListScreen({Key key, this.notificationData})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return NotificationListScreenState();
  }
}

class NotificationListScreenState
    extends BaseStateProvider<NotificationListScreen, NotificationViewModel> {
  void initState() {
    super.initState();
    viewModel = NotificationViewModel();
    viewModel.setMessageType(super.widget.notificationData["type"]);
    viewModel
      ..onErrorNotificationList = handleError
      ..scrollController.addListener(() {
        if (viewModel.scrollController.position.extentAfter == 0) {
          viewModel.setPage(viewModel.page + 1);
          viewModel.getMessageList(viewModel.limit, viewModel.page);
        }
      });
  }

  Widget lineBottom() {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: Design.theme.colorLine.withOpacity(0.6),
          ),
        ),
      ),
    );
  }

  Widget imageItem(String img_url) {
    if (img_url != "") {
      return Container(
        margin: EdgeInsets.only(right: 12, top: 2),
        child: Image.network(
          img_url,
          width: 55,
          height: 55,
        ),
      );
    } else {
      return Container(
        margin: EdgeInsets.only(right: 12, top: 2),
        child: Icon(
          Icons.replay,
          color: Design.theme.fontColorSecondary,
          size: 55,
          semanticLabel: 'Text to announce in accessibility modes',
        ),
      );
    }
  }

  Widget buildItem(NotificationRecord record) {
    return Column(
      children: <Widget>[
        InkWell(
          onTap: () {
            switch (record.refPath) {
              case "wallet":
                viewModel.readMessage(record.type.text);
                RouterService.instance
                    .navigateTo(CoinsTransactionRoute.buildPath, data: 0.00);
                break;
              case "order":
                viewModel.readMessage(record.type.text);
                dynamic dataOrder = {
                  "order_id": int.parse(record.refCode),
                  "ispop": true
                };
                RouterService.instance
                    .navigateTo(OrderDetailRoute.buildPath, data: dataOrder);
                break;
              case "feedback":
                dynamic dataOrder = {
                  "order_id": int.parse(record.refCode),
                  "ispop": true
                };
                RouterService.instance
                    .navigateTo(FeedbackRoute.buildPath, data: dataOrder);
                break;
              default:
            }
          },
          child: Container(
            padding: EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 15),
            color: !record.read
                ? Design.theme.colorBox
                : Design.theme.colorMainBackground,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                imageItem(getImgUrl(record.images, 100, 100)),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          record.title,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 16,
                            color: !record.read
                                ? Design.theme.colorActive.withOpacity(0.7)
                                : Design.theme.colorActive,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Container(
                        child: Text(
                          record.description,
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            color: Design.theme.colorText2.withOpacity(0.6),
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          "${record.createdAt.date} ${record.createdAt.time}",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            color: Design.theme.colorText2.withOpacity(0.6),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        lineBottom(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);
    appLocal = AppLocalizations.of(context);
    return Scaffold(
      appBar: CustomHeaderDetail(
        height: Screen.convertHeightSize(55),
        title: widget.notificationData["title"],
        onTap: () {
          RouterService.instance.pop();
        },
      ),
      body: BaseWidget<NotificationViewModel>(
        model: viewModel,
        builder: (context, model, child) => ProgressHUD(
          color: Colors.transparent,
          progressIndicator: CircleLoading(),
          inAsyncCall: viewModel.loading,
          child: Builder(
            builder: (context) {
              return RefreshIndicator(
                color: Design.theme.colorMainBackground,
                backgroundColor: Design.theme.colorPrimary,
                key: model.refreshIndicatorKey,
                onRefresh: () {
                  model.resetRecord();
                  return model.getMessageList(model.limit, model.page);
                },
                child: Builder(
                  builder: (context) {
                    if (model.notificationRecord.length > 0) {
                      return Container(
                        child: ListView.builder(
                          physics: const AlwaysScrollableScrollPhysics(),
                          itemCount: model.notificationRecord.length,
                          controller: model.scrollController,
                          itemBuilder: (context, i) {
                            return buildItem(model.notificationRecord[i]);
                          },
                        ),
                      );
                    } else if (model.notificationRecord.length == 0 &&
                        model.is_complete) {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: Center(
                              child: Column(
                                children: <Widget>[
                                  SvgPicture.asset(
                                    'images/inbox_fill.svg',
                                  ),
                                  SizedBox(
                                    height: Screen.convertHeightSize(10),
                                  ),
                                  Text(
                                    appLocal.mybookingEmptyText,
                                    style: TextStyle(color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      );
                    } else {
                      return Container();
                    }
                  },
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void handleError(BaseError error) {
    print(error.toJson());
  }

  String getImgUrl(List<NotificationImage> img, double w, double h) {
    String result = "";
    if (img.length > 0) {
      result =
          "${EnvConfig.instance.resUrl}/w_${w.round()},h_${h.round()},c_crop/${img[0].id}/${img[0].name}";
    }
    return result;
  }
}
