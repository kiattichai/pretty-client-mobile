import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/notification_module/models/notification_response_entity.dart';
import 'package:inject/inject.dart';

class NotificationApi {
  final CoreApi coreApi;
  final String path = '/users/messages';

  @provide
  @singleton
  NotificationApi(this.coreApi);

  Future<NotificationResponseEntity> messageList(int limit, int page, String type, Function badRequestToModelError) async {
    Map<String, String> query = {
      "limit": limit.toString(),
      "page": page.toString(),
      "app": "user",
      "order": "created_at",
      "sort": "desc"
//      "fields": "fields,images,webview_url,share_url",
    };
    if(!type.isEmpty){
      query.addAll({"type": type});
    }

    final response = await coreApi.get(path, query, badRequestToModelError, hasPermission: true);
    return NotificationResponseEntity().fromJson(response.data);
  }


  Future<BaseResponseEntity> readMessage(String type, Function badRequestToModelError) async {
    Map<String, String> query = {
      "type": type
    };
    final response = await coreApi.put(path+"/read", query, badRequestToModelError, hasPermission: true);
    return BaseResponseEntity().fromJson(response.data);
  }

}