import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/notification_module/data/notification_api.dart';
import 'package:pretty_client_mobile/modules/notification_module/models/notification_response_entity.dart';
import 'package:inject/inject.dart';

class NotificationRepository extends BaseRepository {
  final NotificationApi notificationApi;
  List<NotificationRecord> notificationRecord = [];

  @provide
  NotificationRepository(this.notificationApi);

  Future<NotificationResponseEntity> messageList(limit, page, String type) async {
    final response = await notificationApi.messageList(limit, page, type, BaseErrorEntity.badRequestToModelError);
    if (response.data != null) {
//      notificationRecord.clear();
//      notificationRecord.addAll(response.data.record);
      return response;
    } else {
      return null;
    }
  }

  Future<BaseResponseEntity> readMessage(String type) async {
    final response = await notificationApi.readMessage(type, BaseErrorEntity.badRequestToModelError);
    return response;
  }

}
