import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/notification_module/notification_list_screen.dart';

class NotificationListRoute extends BaseRoute {
  static String buildPath = '/notification_list';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
      builder: (context) => NotificationListScreen(
        notificationData: data,
      ),
    );
  }
}
