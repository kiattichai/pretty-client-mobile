import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/share_preference.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/utils/jwt_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainViewModel extends BaseViewModel {
  var currentIndex = 0;
  int notificationBadge = 0;

  PageController pageController;
  FirebaseDatabase firebaseDatabase = FirebaseDatabase();
  DatabaseReference itemRef = null;

  MainViewModel(int index) {
    if (index == 6) {
      currentIndex = 1;
    } else {
      currentIndex = index == null ? 0 : index;
    }
    pageController = PageController(
      initialPage: index == null ? 0 : index,
      keepPage: true,
    );
  }

  @override
  Future<void> postInit() {
    // TODO: implement postInit
    super.postInit();
    getDataFirebase();
  }

  void selectTab(int index) {
    print("select $index");
    if (currentIndex == index) {
      return;
    }
    currentIndex = index;
    pageController.jumpToPage(index);
    notifyListeners();
  }

  void goToMemberAuthen() {
    final share = di.sharePrefInterface;
    if (share.shouldLogin()) {
      currentIndex = 1;
      pageController.jumpToPage(5);
      notifyListeners();
    } else {
      if (JwtUtils().isMember(share.accessToken())) {
        selectTab(1);
      } else {
        currentIndex = 1;
        pageController.jumpToPage(5);
        notifyListeners();
      }
    }
  }

  void setNotificationBadge(int n) {
    notificationBadge = n;
    notifyListeners();
  }

  Future<void> getProfile() async {
    catchError(() async {
      await di.profileRepository.getProfile();
    });
  }

  SharePrefInterface share() {
    return di.sharePrefInterface;
  }

  getFavourite() async {
    try {
      await di.favoriteRepository.getFavoriteList();
    } catch (e) {}
  }

  Future<void> getDataFirebase() async {
    SharedPreferences share = await SharedPreferences.getInstance();
    if (itemRef == null && share.get("access_token") != null) {
      getFavourite();
      String uid = JwtUtils().getUid(share.get("access_token").toString());
      itemRef = FirebaseDatabase.instance.reference().child("user/" + uid);
    } else {
      itemRef = null;
      print("No login!");
    }
    if (itemRef != null) {
      itemRef.onValue.listen((Event e) {
        var snapshot = e.snapshot;
        if (snapshot.value == null) {
          setNotificationBadge(0);
        } else {
          Map<dynamic, dynamic> values = snapshot.value;
          final promotion = values['promotion'] ?? 0;
          final activity = values['activity'] ?? 0;
          int sum = promotion + activity;
          setNotificationBadge((sum > 99 ? 99 : sum));
        }
      });
    }
  }
}
