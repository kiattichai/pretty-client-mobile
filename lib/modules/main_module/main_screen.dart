import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/modules/feedback_module/feedback_route.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_landing_route.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_screen.dart';
import 'package:pretty_client_mobile/modules/login_module/login_route.dart';
import 'package:pretty_client_mobile/modules/main_module/main_view_model.dart';
import 'package:pretty_client_mobile/modules/news_module/news_list_screen.dart';
import 'package:pretty_client_mobile/modules/notification_module/notification_screen.dart';
import 'package:pretty_client_mobile/modules/profile_module/profile_base_screen.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/device_info.dart';
import 'package:pretty_client_mobile/utils/jwt_utils.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/mybooking_base_screen.dart';
import 'package:pretty_client_mobile/modules/home_tab_module/home_tab_screen.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_landing_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class MainScreen extends StatefulWidget {
  final index;

  const MainScreen({Key key, this.index}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MainScreenState();
  }
}

class MainScreenState extends BaseStateProvider<MainScreen, MainViewModel> {
  final List<Map<String, String>> pathImageList = [
    {"default": "images/icon_home.svg", "active": "images/icon_home_active.svg"},
    {"default": "images/icon_friends.svg", "active": "images/icon_friends_active.svg"},
    {"default": "images/icon_booking.svg", "active": "images/icon_booking_active.svg"},
    {"default": "images/icon_notification.svg", "active": "images/icon_notification_active.svg"},
    {"default": "images/icon_user.svg", "active": "images/icon_user_active.svg"}
  ];
  final List<Widget> viewContainer = [
    HomeTabScreen(),
    FriendsScreen(),
    MybookingBaseScreen(),
    NotificationScreen(),
    ProfileScreen(),
    FriendsLandingScreen(),
  ];

  Widget hideText() => Padding(padding: EdgeInsets.all(0));
  PanelController panelController;

  final FirebaseMessaging fcm = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    viewModel = MainViewModel(widget.index);

    fcm.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    fcm.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    handleNotification();
  }

  void handleNotification() {
    fcm.configure(
      onResume: (Map<String, dynamic> message) async {
        dynamic data = message['data'] ?? message;
        handleOpenPageFeedback(data);
      },
    );
  }

  void handleOpenPageFeedback(dynamic data) {
    final String id = data['id'] ?? '';
    final String type = data['type'] ?? '';
    if (type == 'feedback') {
      dynamic dataOrder = {"order_id": int.parse(id), "ispop": false};
      RouterService.instance.navigateTo(FeedbackRoute.buildPath, clearStack: true, data: dataOrder);
    }
  }

  Widget buildPageView() {
    return PageView(
      physics: NeverScrollableScrollPhysics(),
      controller: viewModel.pageController,
      children: viewContainer,
    );
  }

  Widget customBottomNavBar(int selected) {
    return new Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: buildIconList(),
        ),
        height: Screen.convertHeightSize(66),
        decoration: new BoxDecoration(
          color: Color(0xff0d0d0d),
          boxShadow: [
            BoxShadow(
                color: Color(0x80000000), offset: Offset(0, 2), blurRadius: 6, spreadRadius: 0)
          ],
        ));
  }

  List<Widget> buildIconList() {
    List<Widget> list = [];
    pathImageList.asMap().forEach((index, value) {
      if (index == 3) {
        list.add(iconBottomNav(index, value, badge: viewModel.notificationBadge));
      } else {
        list.add(iconBottomNav(index, value));
      }
    });
    return list;
  }

  Widget iconBottomNav(int index, Map<String, String> pathImages, {int badge = 0}) {
    return InkWell(
        key: Key(index.toString()),
        onTap: () {
          handleAuthen(index);
        },
        child: Container(
          height: Screen.convertHeightSize(66),
          width: Screen.width / 5,
          child: Stack(
            children: <Widget>[
              Align(
                child: Container(
                  child: viewModel.currentIndex == index
                      ? SvgPicture.asset(
                          pathImages['active'],
                          color: Design.theme.colorPrimary,
                        )
                      : SvgPicture.asset(pathImages['default']),
                ),
                alignment: Alignment(0.3, -0.3),
                // alignment: Alignment.center,
              ),
              badge != 0
                  ? Align(
                      alignment: Alignment(0.6, -0.6),
                      child: new Container(
                          child: Center(
                            child: Text(
                              '$badge',
                              style: TextStyle(color: Colors.white, fontSize: 12),
                            ),
                          ),
                          width: 18,
                          height: 18,
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              gradient: LinearGradient(
                                  colors: [Color(0xffdc0000), Color(0xffb60000)], stops: [0, 1]))),
                    )
                  : SizedBox.shrink()
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return BaseWidget<MainViewModel>(
      model: viewModel,
      builder: (context, model, child) => Container(
        color: Design.theme.colorMainBackground,
        child: SafeArea(
          bottom: false,
          child: Scaffold(
            body: buildPageView(),
            bottomNavigationBar: SizedBox(
              height: Screen.convertHeightSize(Device.get().isIphoneX
                  ? Screen.convertHeightSize(66)
                  : Screen.convertHeightSize(45)),
              child: customBottomNavBar(model.currentIndex),
            ),
          ),
        ),
      ),
    );
  }

  void handleAuthen(int index) async {
    final share = viewModel.share();
    switch (index) {
      case 1:
        if (share.shouldLogin()) {
          await RouterService.instance.navigateTo(LoginRoute.buildPath);
          await viewModel.getDataFirebase();
        } else {
          viewModel.goToMemberAuthen();
        }

        break;
      case 2:
        if (share.shouldLogin()) {
          await RouterService.instance.navigateTo(LoginRoute.buildPath);
          await viewModel.getDataFirebase();
        } else {
          viewModel.selectTab(index);
        }
        break;
      case 3:
        if (share.shouldLogin()) {
          await RouterService.instance.navigateTo(LoginRoute.buildPath);
          await viewModel.getDataFirebase();
        } else {
          viewModel.selectTab(index);
        }
        break;
      case 4:
        viewModel.selectTab(index);
        break;
      default:
        viewModel.selectTab(index);
        break;
    }
  }
}
