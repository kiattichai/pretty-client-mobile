import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/main_module/main_screen.dart';

class MainScreenRoute extends BaseRoute {
  static String buildPath = '/main';

  @override
  String get path => buildPath;

  @override
  bool get clearStack => true;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => MainScreen(
              index: data,
            ));
  }
}
