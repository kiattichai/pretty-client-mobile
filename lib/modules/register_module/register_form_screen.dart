import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/main_module/main_screen_route.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_flow_type.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_request_screen.dart';
import 'package:pretty_client_mobile/modules/register_module/models/request_register_entity.dart';
import 'package:pretty_client_mobile/modules/register_module/models/request_register_social_entity.dart';
import 'package:pretty_client_mobile/modules/register_module/register_view_model.dart';
import 'package:pretty_client_mobile/modules/register_module/routes/register_create_password_route.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/modal_select_gender.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:intl/intl.dart';
import 'package:pretty_client_mobile/widgets/input_gender_picker_widget.dart';
import 'package:pretty_client_mobile/widgets/input_birthday_picker_widget.dart';
import 'package:pretty_client_mobile/widgets/primary_button_widget.dart';
import 'package:pretty_client_mobile/widgets/input_text_field_form_widget.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:flutter/services.dart';

class RegisterFormScreen extends StatefulWidget {
  final OtpRequestScreen otpRequestScreen;

  const RegisterFormScreen({Key key, this.otpRequestScreen}) : super(key: key);
  @override
  RegisterFormState createState() {
    return RegisterFormState();
  }
}

const String MIN_DATETIME = '1960-01-01';
const String MAX_DATETIME = '2021-11-25';
const String INIT_DATETIME = '2019-05-17';

class RegisterFormState extends BaseStateProvider<RegisterFormScreen, RegisterMobileViewModel> {
  bool _showTitle = true;
  DateTimePickerLocale _locale = DateTimePickerLocale.en_us;
  List<DateTimePickerLocale> _locales = DateTimePickerLocale.values;
  String _format = 'yyyy-MMM-dd';
  DateTime _dateTime;

  TextEditingController controllerBirthday = TextEditingController();
  TextEditingController controllerGender = TextEditingController();
  TextEditingController controllerFirstName = TextEditingController();
  TextEditingController controllerLastName = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  final focusFirstName = FocusNode();
  final focusLastName = FocusNode();
  bool _validate = false;
  bool autoFocus = true;
  String countryCode = "TH";
  String phoneCode = "+66";
  bool _isLoading = false;
  String birthDay = "";
  String gender = "";
  bool isEnableButton = false;

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
        keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
        keyboardBarColor: Colors.grey[200],
        nextFocus: true,
        actions: [
          KeyboardAction(
            focusNode: focusFirstName,
          ),
          KeyboardAction(
            focusNode: focusLastName,
          ),
        ]);
  }

  @override
  void initState() {
    super.initState();
    viewModel = RegisterMobileViewModel();
    viewModel.handleError = handleError;
    _dateTime = DateTime.parse(INIT_DATETIME);
    checkRegisterSocial();
  }

  void checkRegisterSocial() {
    if (widget.otpRequestScreen.otpFlowType == OTPFlowType.REGISTER_SOCIAL &&
        widget.otpRequestScreen.facebookInfoEntity != null) {
      final info = widget.otpRequestScreen.facebookInfoEntity;
      setState(() {
        controllerFirstName.text = info.firstName ?? '';
        controllerLastName.text = info.lastName ?? '';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return ProgressHUD(
      progressIndicator: CircleLoading(),
      inAsyncCall: _isLoading,
      child: Scaffold(
          resizeToAvoidBottomPadding: false,
          appBar: CustomHeaderDetail(
            height: Screen.convertHeightSize(55),
            title: appLocal.userInfoHeader,
          ),
          body: KeyboardActions(
            isDialog: true,
            config: _buildConfig(context),
            child: Container(
              alignment: FractionalOffset.topCenter,
              padding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              child: SafeArea(
                child: SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    autovalidate: _validate,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 0, top: 30, right: 0),
                          child: InputTextFieldFormWidget(
                              title: appLocal.userInfoFirstname,
                              hintText: appLocal.userInfoFirstnameHint,
                              autoFocus: autoFocus,
                              validate: _validate,
                              keyboardType: TextInputType.text,
                              textEditingController: controllerFirstName,
                              onInputChanged: (String val) {
                                setState(() {
                                  updateEnableButton();
                                });
                              },
                              inputFormatter: BlacklistingTextInputFormatter(RegExp("[/\\\\]")),
                              onInputValidator: (String value) {
                                var regExpResult = _regExpCheckString(value);

                                if (value.isEmpty) {
                                  return "กรุณากรอกข้อมูล";
                                } else if (value.length > 20) {
                                  return "ชื่อจริงไม่ควรเกิน 20 ตัวอักษร";
                                } else if (!regExpResult) {
                                  return "ชื่อจริงต้องมีเฉพาะ a-z A-Z และ ตัวเลข";
                                } else {
                                  return null;
                                }
                              },
                              onFieldSubmitted: (v) {
                                focusFirstName.unfocus();
                              },
                              focusInput: focusFirstName),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 0, top: 12, right: 0),
                          child: InputTextFieldFormWidget(
                              title: appLocal.userInfoLastname,
                              hintText: appLocal.userInfoLastnameHint,
                              autoFocus: false,
                              validate: _validate,
                              textEditingController: controllerLastName,
                              onInputChanged: (String val) {
                                setState(() {
                                  updateEnableButton();
                                });
                              },
                              inputFormatter: BlacklistingTextInputFormatter(RegExp("[/\\\\]")),
                              onInputValidator: (String value) {
                                var regExpResult = _regExpCheckString(value);
                                if (value.isEmpty) {
                                  return "กรุณากรอกข้อมูล";
                                } else if (value.length > 20) {
                                  return "นามสกุลไม่ควรเกิน 20 ตัวอักษร";
                                } else if (!regExpResult) {
                                  return "นามสกุลต้องมีเฉพาะ a-z A-Z และ ตัวเลข";
                                } else {
                                  return null;
                                }
                              },
                              onFieldSubmitted: (v) {
                                focusLastName.unfocus();
                              },
                              focusInput: focusLastName),
                        ),
                        InputBirthDayPickerWidget(
                          appLocal: appLocal,
                          controllerBirthday: controllerBirthday,
                          onBirthDayFieldTap: showDatePicker,
                        ),
                        InputGenderPickerWidget(
                          appLocal: appLocal,
                          controllerGender: controllerGender,
                          onGenderFieldTap: showModalGender,
                        ),
                        PrimaryButtonWidget(
                          buttonTitle: appLocal.commonNext,
                          onBtnPressed: isEnableButton ? () => checkRegisterType() : null,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          )),
    );
  }

  checkRegisterType() {
    if (_formKey.currentState.validate()) {
      // No any error in validation
      _formKey.currentState.save();
      if (widget.otpRequestScreen.otpFlowType == OTPFlowType.REGISTER) {
        gotoCreatePassword();
      }
      if (widget.otpRequestScreen.otpFlowType == OTPFlowType.REGISTER_SOCIAL) {
        registerSocial();
      }
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }

  gotoCreatePassword() {
    focusLastName.unfocus(focusPrevious: false);
    RouterService.instance.navigateTo(RegisterCreatePasswordRoute.buildPath,
        data: RequestRegisterEntity(
            countryCode: widget.otpRequestScreen.countryCode,
            username: widget.otpRequestScreen.mobile,
            birthday: controllerBirthday.text,
            gender: convertGenderText(controllerGender.text),
            firstName: controllerFirstName.text,
            lastName: controllerLastName.text));
  }

  registerSocial() {
    viewModel.registerSocial(
        RequestRegisterSocialEntity()
          ..service = 'facebook'
          ..token = widget.otpRequestScreen.token
          ..username = widget.otpRequestScreen.mobile
          ..countryCode = widget.otpRequestScreen.countryCode
          ..birthday = controllerBirthday.text
          ..gender = convertGenderText(controllerGender.text)
          ..firstName = controllerFirstName.text
          ..lastName = controllerLastName.text,
        onRegisterSocialSuccess);
  }

  void onRegisterSocialSuccess() {
    var token = widget.otpRequestScreen.token;
    viewModel.loginSocial(token, "facebook", onLoginSuccess);
  }

  void onLoginSuccess() {
    RouterService.instance.navigateTo(MainScreenRoute.buildPath, clearStack: true);
  }

  bool _regExpCheckString(String value) {
    RegExp regExp = new RegExp(
      r"^[a-zA-Z0-9ก-๙]*$",
      caseSensitive: false,
      multiLine: false,
    );
    print('regExp : ${regExp.hasMatch(value)}');
    if (regExp.hasMatch(value)) {
      return true;
    }
    return false;
  }

  handleError(BaseError error) {
    _showAlert(error.message);
  }

  void updateEnableButton() {
    if (controllerFirstName.text.length < 3 ||
        controllerLastName.text.length < 3 ||
        birthDay.isEmpty ||
        gender.isEmpty) {
      isEnableButton = false;
    } else {
      isEnableButton = true;
    }
  }

  void showModalGender() {
    FocusScope.of(context).requestFocus(FocusNode());
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return ModalSelectGender(
            onSelectGender: handleDisplayGender,
            appLocale: appLocal,
          );
        });
  }

  handleDisplayGender(String value) {
    FocusScope.of(context).requestFocus(FocusNode());
    setState(() {
      controllerGender.text = showGenderText(value);
      if (value != null) {
        gender = value;
      } else {
        gender = "";
      }
      updateEnableButton();
    });
  }

  String showGenderText(String value) {
    if (value == "male") {
      return appLocal.genderDialogMale;
    } else if (value == "female") {
      return appLocal.genderDialogFemale;
    } else if (value == "unknown") {
      return appLocal.genderDialogNotSpecified;
    } else {
      return null;
    }
  }

  String convertGenderText(String value) {
    if (value == "เพศชาย") {
      return "male";
    } else if (value == "เพศหญิง") {
      return "female";
    } else if (value == "ไม่ระบุ") {
      return "unknown";
    } else {
      return null;
    }
  }

  void _showAlert(String message) {
    AlertDialog alertDialog = AlertDialog(
      content: Text(message),
      actions: <Widget>[],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alertDialog;
        });
  }

  /// Display date picker.
  void showDatePicker() {
    FocusScope.of(context).requestFocus(FocusNode());
    DatePicker.showDatePicker(
      context,
      pickerTheme: DateTimePickerTheme(
        itemTextStyle: TextStyle(
          color: Design.theme.colorLine,
          fontSize: 22,
          fontWeight: FontWeight.w400,
          fontStyle: FontStyle.normal,
        ),
        showTitle: _showTitle,
        confirm: Text(appLocal.commonConfirm,
            style: TextStyle(
              color: Design.theme.colorLine,
              fontSize: 16,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
            )),
        cancel: Text(appLocal.commonCancel,
            style: TextStyle(
              color: Design.theme.colorText2,
              fontSize: 16,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
            )),
      ),
      minDateTime: DateTime.parse(MIN_DATETIME),
      maxDateTime: DateTime.parse(MAX_DATETIME),
      initialDateTime: _dateTime,
      dateFormat: _format,
      locale: _locale,
      onClose: () => print("----- onClose -----"),
      onCancel: () => print('onCancel'),
      onChange: (dateTime, List<int> index) {
        setState(() {
          _dateTime = dateTime;
          controllerBirthday.text = DateFormat("yyyy-MM-dd").format(_dateTime);
          birthDay = DateFormat("yyyy-MM-dd").format(_dateTime);
          updateEnableButton();
        });
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          FocusScope.of(context).requestFocus(FocusNode());
          _dateTime = dateTime;
          controllerBirthday.text = DateFormat("yyyy-MM-dd").format(_dateTime);
          birthDay = DateFormat("yyyy-MM-dd").format(_dateTime);
          updateEnableButton();
        });
      },
    );
  }
}
