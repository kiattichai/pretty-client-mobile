import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/modules/otp_module/models/otp_response_entity.dart';
import 'package:pretty_client_mobile/modules/otp_module/models/request_otp_entity.dart';
import 'package:pretty_client_mobile/modules/register_module/models/check_user_entity.dart';
import 'package:pretty_client_mobile/modules/register_module/models/facebook_info_entity.dart';
import 'package:pretty_client_mobile/modules/register_module/models/register_response_entity.dart';
import 'package:pretty_client_mobile/modules/register_module/models/request_register_entity.dart';
import 'package:pretty_client_mobile/modules/register_module/models/request_register_social_entity.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class RegisterMobileViewModel extends BaseViewModel {
  bool isEnableButton = false;
  RequestRegisterEntity requestRegisterEntity;
  RegisterResponseData registerResponseData;
  OtpResponseData otpResponseData;
  CheckUserData checkUserData;
  FacebookInfoEntity facebookInfoEntity;
  Function handleError;
  FacebookLoginResult result;

  RegisterMobileViewModel({this.requestRegisterEntity});

  @override
  void postInit() {
    super.postInit();
    if (result != null) {
      callGraphApiFacebook(result);
    }
  }

  void checkRegister(String username, String countryCode, String type, String channel,
      Function checkRegisterSuccess,
      {bool verifyMobile = true}) {
    //verify mobile number = false case register with social
    catchError(() async {
      setLoading(true);
      if (verifyMobile) {
        checkUserData = await di.registerRepository.checkRegister(username, countryCode, type);
      }
      otpResponseData = await di.otpRepository.requestOtp(
          RequestOtpEntity(countryCode: countryCode, mobile: username, channel: channel));
      checkRegisterSuccess(otpResponseData.ref);
      setLoading(false);
    });
  }

  void register(String password, Function registerSuccess) {
    catchError(() async {
      requestRegisterEntity.password = password;
      setLoading(true);
      await di.registerRepository.registerUser(requestRegisterEntity);
      registerSuccess();
      setLoading(false);
    });
  }

  void loginSocial(String token, String service, Function onLoginSocialSuccess) {
    setLoading(true);
    catchError(() async {
      await di.loginRepository.loginSocial(token, service);
      onLoginSocialSuccess();
    });
  }

  void enableButton(bool value) {
    isEnableButton = value;
    notifyListeners();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    handleError(error);
  }

  void registerSocial(RequestRegisterSocialEntity request, Function registerSocialSuccess) {
    catchError(() async {
      setLoading(true);
      await di.registerRepository.registerSocial(request);
      registerSocialSuccess();
      setLoading(false);
    });
  }

  void callGraphApiFacebook(FacebookLoginResult result) async {
    final token = result.accessToken.token;

    final graphResponse = await http
        .get('https://graph.facebook.com/v2.12/me?fields=picture.type(large),name,first_name,'
            'last_name,email&access_token=$token');
    facebookInfoEntity = FacebookInfoEntity().fromJson(jsonDecode(graphResponse.body));
    notifyListeners();
  }

  void login(String username, String password, String countryCode, Function onLoginSuccess) {
    setLoading(true);
    catchError(() async {
      await di.loginRepository.login(username, password, countryCode);
      onLoginSuccess();
    });
  }

  void updatePassword(String text, String text2) {
    if (text.isEmpty || text2.isEmpty) {
      isEnableButton = false;
      notifyListeners();
      return;
    }
    if (text != text2) {
      isEnableButton = false;
      notifyListeners();
      return;
    }
    if (text == text2) {
      isEnableButton = true;
      notifyListeners();
      return;
    }
  }
}
