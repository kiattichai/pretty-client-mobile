import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/modules/register_module/models/request_register_entity.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/main_module/main_screen_route.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/modules/register_module/register_view_model.dart';

class RegisterSuccessScreen extends StatefulWidget {
  final RequestRegisterEntity requestRegisterEntity;

  const RegisterSuccessScreen({Key key, this.requestRegisterEntity}) : super(key: key);
  @override
  RegisterSuccessState createState() {
    return RegisterSuccessState();
  }
}

class RegisterSuccessState
    extends BaseStateProvider<RegisterSuccessScreen, RegisterMobileViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = RegisterMobileViewModel(requestRegisterEntity: widget.requestRegisterEntity);
    viewModel.handleError = handleError;
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return Container(
      child: SafeArea(
        child: Scaffold(
            body: Container(
                alignment: FractionalOffset.topCenter,
                padding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Expanded(flex: 2, child: _logo()),
                    Expanded(
                      flex: 1,
                      child: Container(),
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Text(appLocal.createUserSuccessTitle,
                              style: TextStyle(
                                color: Design.theme.colorActive,
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )),
                          primaryButton(context)
                        ],
                      ),
                    )
                  ],
                ))),
      ),
    );
  }

  Widget _logo() {
    return Container(
        alignment: FractionalOffset.center,
        padding: EdgeInsets.only(
          top: Screen.convertHeightSize(80),
        ),
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 10),
              child: SvgPicture.asset(
                "images/logo_night2.svg",
                width: 120,
              ),
            ),
          ],
        ));
  }

  Widget primaryButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 24, right: 0, bottom: 48),
      child: SizedBox(
        width: double.infinity,
        child: MaterialButton(
          height: 48.0,
          minWidth: 70.0,
          disabledColor: Color(0xff6d7278).withOpacity(0.7021019345238095),
          disabledTextColor: Color(0xff100e12),
          color: Design.theme.colorPrimary,
          textColor: Color(0xff000000),
          child: Text(
            appLocal.createUserSuccessStart,
            style: Design.text.textButton,
          ),
          onPressed: () => handleClickSuccess(),
          splashColor: Design.theme.colorPrimary,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24.0),
          ),
        ),
      ),
    );
  }

  void handleClickSuccess() async {
    var username = widget.requestRegisterEntity.username;
    var password = widget.requestRegisterEntity.password;
    var countryCode = widget.requestRegisterEntity.countryCode;
    //print("${username} ${password} ${countryCode}");
    viewModel.login(username, password, countryCode, onLoginSuccess);
  }

  void onLoginSuccess() {
    viewModel.setLoading(false);
    RouterService.instance.navigateTo(MainScreenRoute.buildPath, clearStack: true);
  }

  void handleError(BaseError baseError) {
    _showAlert(baseError.message);
  }

  void _showAlert(String message) {
    AlertDialog alertDialog = AlertDialog(
      content: Text(message),
      actions: <Widget>[],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alertDialog;
        });
  }
}
