import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/register_module/register_create_password.dart';

class RegisterCreatePasswordRoute extends BaseRoute {
  static String buildPath = '/register/create_password';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => RegisterCreatePasswordScreen(
              requestRegisterEntity: data,
            ));
  }
}
