import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/register_module/register_mobile_screen.dart';

class RegisterMobileRoute extends BaseRoute {
  static String buildPath = '/register/mobile';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => RegisterMobileScreen());
  }
}
