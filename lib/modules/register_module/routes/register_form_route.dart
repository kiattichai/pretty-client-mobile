import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/register_module/register_form_screen.dart';

class RegisterFormRoute extends BaseRoute {
  static String buildPath = '/register/form';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => RegisterFormScreen(
              otpRequestScreen: data,
            ));
  }
}
