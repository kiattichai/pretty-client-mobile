import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/register_module/register_success_screen.dart';

class RegisterSuccessRoute extends BaseRoute {
  static String buildPath = '/register/success';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
      builder: (context) => RegisterSuccessScreen(
        requestRegisterEntity: data,
      )
    ); 
  }
}
