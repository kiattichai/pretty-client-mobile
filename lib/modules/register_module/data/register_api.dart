import 'package:dio/dio.dart';
import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:pretty_client_mobile/modules/register_module/models/check_user_entity.dart';
import 'package:pretty_client_mobile/modules/register_module/models/register_response_entity.dart';
import 'package:pretty_client_mobile/modules/register_module/models/request_register_entity.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/modules/register_module/models/request_register_social_entity.dart';

class RegisterApi {
  final CoreApi coreApi;
  final String pathRegister = "/users/register";
  final String pathRegisterSocial = "/users/register/social";
  final String pathCheckRegister = "/users/check";

  @provide
  @singleton
  RegisterApi(this.coreApi);

  Future<CheckUserEntity> checkRegister(
      String username, String countryCode, String type, Function badRequestToModelError) async {
    var queryParameters = {'username': username, 'country_code': countryCode, 'type': type};
    Response response =
        await coreApi.get(pathCheckRegister, queryParameters, badRequestToModelError);
    return CheckUserEntity.fromJson(response.data);
  }

  Future<RegisterResponseEntity> registerUser(
      RequestRegisterEntity registerEntity, Function badRequestToModelError) async {
    Response response =
        await coreApi.post(pathRegister, registerEntity.toJson(), badRequestToModelError);
    return RegisterResponseEntity.fromJson(response.data);
  }

  Future<RegisterResponseEntity> registerSocial(
      RequestRegisterSocialEntity registerSocialEntity, Function badRequestToModelError) async {
    Response response = await coreApi.post(
        pathRegisterSocial, registerSocialEntity.toJson(), badRequestToModelError);
    return RegisterResponseEntity.fromJson(response.data);
  }
}
