import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/register_module/data/register_api.dart';
import 'package:pretty_client_mobile/modules/register_module/models/check_user_entity.dart';
import 'package:pretty_client_mobile/modules/register_module/models/register_response_entity.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/modules/register_module/models/request_register_entity.dart';
import 'package:pretty_client_mobile/modules/register_module/models/request_register_social_entity.dart';

class RegisterRepository extends BaseRepository {
  final RegisterApi registerApi;

  @provide
  RegisterRepository(this.registerApi);

  Future<CheckUserData> checkRegister(String username, String countryCode, String type) async {
    final response = await registerApi.checkRegister(
        username, countryCode, type, BaseErrorEntity.badRequestToModelError);
    return response != null ? response.data : null;
  }

  Future<RegisterResponseData> registerUser(RequestRegisterEntity registerEntity) async {
    final response =
        await registerApi.registerUser(registerEntity, BaseErrorEntity.badRequestToModelError);
    return response != null ? response.data : null;
  }

  Future<RegisterResponseData> registerSocial(
      RequestRegisterSocialEntity registerSocialEntity) async {
    final response = await registerApi.registerSocial(
        registerSocialEntity, BaseErrorEntity.badRequestToModelError);
    return response != null ? response.data : null;
  }
}
