import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class RequestRegisterSocialEntity with JsonConvert<RequestRegisterSocialEntity> {
  String service;
  String token;
  String username;
  @JSONField(name: "country_code")
  String countryCode;
  @JSONField(name: "first_name")
  String firstName;
  @JSONField(name: "last_name")
  String lastName;
  String gender;
  String birthday;
}
