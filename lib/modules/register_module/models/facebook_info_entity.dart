import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class FacebookInfoEntity with JsonConvert<FacebookInfoEntity> {
  FacebookInfoPicture picture;
  String name;
  @JSONField(name: "first_name")
  String firstName;
  @JSONField(name: "last_name")
  String lastName;
  String id;

  FacebookInfoEntity() {
    picture = FacebookInfoPicture();
    name = '';
    firstName = '';
    lastName = '';
    id = '';
  }
}

class FacebookInfoPicture with JsonConvert<FacebookInfoPicture> {
  FacebookInfoPictureData data;

  FacebookInfoPicture() {
    data = FacebookInfoPictureData();
  }
}

class FacebookInfoPictureData with JsonConvert<FacebookInfoPictureData> {
  int height;
  @JSONField(name: "is_silhouette")
  bool isSilhouette;
  String url;
  int width;

  FacebookInfoPictureData() {
    height = 0;
    isSilhouette = false;
    url = '';
    width = 0;
  }
}
