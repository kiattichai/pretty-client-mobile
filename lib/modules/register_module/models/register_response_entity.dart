import 'package:pretty_client_mobile/model/error_entity.dart';

class RegisterResponseEntity {
  RegisterResponseData data;
  ErrorEntity error;

  RegisterResponseEntity({this.data, this.error});

  RegisterResponseEntity.fromJson(Map<String, dynamic> json) {
    try {
      error = json['error'] != null ? new ErrorEntity().fromJson(json['error']) : null;
      data = json['data'] != null ? new RegisterResponseData.fromJson(json['data']) : null;
    } catch (e) {
      print(e);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class RegisterResponseData {
  int id;
  String message;
  String username;

  RegisterResponseData({this.id, this.message, this.username});

  RegisterResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    message = json['message'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['message'] = this.message;
    data['username'] = this.username;
    return data;
  }
}
