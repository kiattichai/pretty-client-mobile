import 'package:pretty_client_mobile/model/error_entity.dart';

class CheckUserEntity {
  CheckUserData data;
  ErrorEntity error;

  CheckUserEntity({this.data, this.error});

  CheckUserEntity.fromJson(Map<String, dynamic> json) {
    try {
      error = json['error'] != null ? new ErrorEntity().fromJson(json['error']) : null;
      data = json['data'] != null ? new CheckUserData.fromJson(json['data']) : null;
    } catch (e) {
      print(e);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class CheckUserData {
  int userId;
  String message;

  CheckUserData({this.userId, this.message});

  CheckUserData.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['message'] = this.message;
    return data;
  }
}
