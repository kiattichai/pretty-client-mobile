class RequestRegisterEntity {
  String birthday;
  String countryCode;
  String password;
  String gender;
  String lastName;
  String firstName;
  String username;

  RequestRegisterEntity(
      {this.birthday,
      this.countryCode,
      this.password,
      this.gender,
      this.lastName,
      this.firstName,
      this.username});

  RequestRegisterEntity.fromJson(Map<String, dynamic> json) {
    birthday = json['birthday'];
    countryCode = json['country_code'];
    password = json['password'];
    gender = json['gender'];
    lastName = json['last_name'];
    firstName = json['first_name'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['birthday'] = this.birthday;
    data['country_code'] = this.countryCode;
    data['password'] = this.password;
    data['gender'] = this.gender;
    data['last_name'] = this.lastName;
    data['first_name'] = this.firstName;
    data['username'] = this.username;
    return data;
  }
}
