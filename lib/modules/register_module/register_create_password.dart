import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/register_module/models/request_register_entity.dart';
import 'package:pretty_client_mobile/modules/register_module/register_view_model.dart';
import 'package:pretty_client_mobile/modules/register_module/routes/register_success_route.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/primary_button_widget.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

class RegisterCreatePasswordScreen extends StatefulWidget {
  final RequestRegisterEntity requestRegisterEntity;

  const RegisterCreatePasswordScreen({Key key, this.requestRegisterEntity}) : super(key: key);
  @override
  RegisterCreatePasswordState createState() {
    return RegisterCreatePasswordState();
  }
}

class RegisterCreatePasswordState
    extends BaseStateProvider<RegisterCreatePasswordScreen, RegisterMobileViewModel> {
  final _formKey = GlobalKey<FormState>();
  final focusPassword = FocusNode();
  final focusConfirmPassword = FocusNode();
  final TextEditingController controllerPassword = TextEditingController();
  final TextEditingController controllerConfirmPassword = TextEditingController();
  bool _validate = false;
  bool autoFocus = true;
  bool _obscureText = true;
  bool _obscureConfirmText = true;

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
        keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
        keyboardBarColor: Colors.grey[200],
        nextFocus: true,
        actions: [
          KeyboardAction(
            focusNode: focusPassword,
          ),
          KeyboardAction(
            focusNode: focusConfirmPassword,
          ),
        ]);
  }

  @override
  void initState() {
    super.initState();
    viewModel = RegisterMobileViewModel(requestRegisterEntity: widget.requestRegisterEntity);
    viewModel.handleError = handleErrorCreatePassword;
  }

  Widget titlePasswordInfo(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 36, right: 0),
      child: SizedBox(
        width: double.infinity,
        child: Container(
          child: Text(appLocal.createPasswordTitle,
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Design.theme.colorInactive,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              )),
        ),
      ),
    );
  }

  Widget inputPassword(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: 8,
      ),
      child: TextFormField(
        onChanged: (values) {
          viewModel.updatePassword(controllerPassword.text, controllerConfirmPassword.text);
        },
        controller: controllerPassword,
        keyboardType: TextInputType.text,
        autofocus: autoFocus,
        obscureText: _obscureText,
        style: TextStyle(
            color: Design.theme.colorActive,
            fontSize: 16,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal),
        textInputAction: TextInputAction.next,
        focusNode: focusPassword,
        onFieldSubmitted: (v) {
          focusPassword.unfocus();
        },
        autovalidate: _validate,
        validator: (String value) {
          var regExpResult = validatePasswordString(value);

          if (value.isEmpty) {
            return "กรุณากรอกข้อมูล";
          } else if (value.length < 8) {
            return "รหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวอักษร";
          } else if (!regExpResult) {
            return "รหัสผ่านต้องมีตัวอักษร และ ตัวเลขอย่างน้อย 1 ตัว";
          } else {
            return null;
          }
        },
        decoration: InputDecoration(
          suffixIcon: GestureDetector(
            onTap: () {
              setState(() {
                _obscureText = !_obscureText;
              });
            },
            child: Icon(
              _obscureText ? Icons.visibility_off : Icons.visibility,
              semanticLabel: _obscureConfirmText ? 'hide password' : 'show password',
              color: Design.theme.colorInactive,
            ),
          ),
          hintText: appLocal.createPasswordTitle,
          filled: true,
          hintStyle: TextStyle(
            color: Design.theme.colorInactive.withOpacity(0.57),
            fontSize: 16,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
            letterSpacing: -0.2,
          ),
          fillColor: Design.theme.colorLine,
          contentPadding: new EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Design.theme.colorActive, width: 1.0),
              borderRadius: BorderRadius.circular(8.0)),
          border: OutlineInputBorder(
              borderSide: BorderSide(color: Design.theme.colorLine, width: 1.0),
              borderRadius: BorderRadius.circular(8.0)),
        ),
      ),
    );
  }

  Widget descriptionText(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 5, right: 0),
      child: SizedBox(
        width: double.infinity,
        child: Container(
          child: Text(appLocal.createPasswordHelp,
              style: TextStyle(
                color: Design.theme.colorInactive.withOpacity(0.8),
                fontSize: 12,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              )),
        ),
      ),
    );
  }

  Widget titleConfirmPasswordInfo(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 15, right: 0),
      child: SizedBox(
        width: double.infinity,
        child: Container(
          child: Text(appLocal.createPasswordAgainTitle,
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Design.theme.colorInactive,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              )),
        ),
      ),
    );
  }

  Widget inputConfirmPassword(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: 8,
      ),
      child: TextFormField(
        onChanged: (values) {
          viewModel.updatePassword(controllerPassword.text, controllerConfirmPassword.text);
        },
        maxLengthEnforced: true,
        controller: controllerConfirmPassword,
        keyboardType: TextInputType.text,
        autofocus: false,
        obscureText: _obscureConfirmText,
        style: TextStyle(
          color: Design.theme.colorActive,
          fontSize: 16,
          fontWeight: FontWeight.w400,
          fontStyle: FontStyle.normal,
        ),
        textInputAction: TextInputAction.next,
        focusNode: focusConfirmPassword,
        onFieldSubmitted: (v) {
          focusConfirmPassword.unfocus();
        },
        autovalidate: _validate,
        validator: (String value) {
          var regExpResult = validatePasswordString(value);

          if (value.isEmpty) {
            return "กรุณากรอกข้อมูล";
          } else if (value.length < 8) {
            return "รหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวอักษร";
          } else if (!regExpResult) {
            return "รหัสผ่านต้องมีตัวอักษร และ ตัวเลขอย่างน้อย 1 ตัว";
          } else {
            return null;
          }
        },
        decoration: InputDecoration(
          suffixIcon: GestureDetector(
            onTap: () {
              setState(() {
                _obscureConfirmText = !_obscureConfirmText;
              });
            },
            child: Icon(
              _obscureConfirmText ? Icons.visibility_off : Icons.visibility,
              semanticLabel: _obscureConfirmText ? 'hide password' : 'show password',
              color: Design.theme.colorInactive,
            ),
          ),
          hintText: appLocal.createPasswordAgainTitle,
          filled: true,
          hintStyle: TextStyle(
            color: Design.theme.colorInactive.withOpacity(0.57),
            fontSize: 16,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
            letterSpacing: -0.2,
          ),
          fillColor: Design.theme.colorLine,
          contentPadding: new EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Design.theme.colorActive, width: 1.0),
              borderRadius: BorderRadius.circular(8.0)),
          border: OutlineInputBorder(
              borderSide: BorderSide(color: Design.theme.colorLine, width: 1.0),
              borderRadius: BorderRadius.circular(8.0)),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return BaseWidget<RegisterMobileViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        color: Colors.transparent,
        progressIndicator: CircleLoading(),
        inAsyncCall: viewModel.loading,
        child: Scaffold(
            resizeToAvoidBottomPadding: false,
            appBar: CustomHeaderDetail(
              height: Screen.convertHeightSize(55),
              title: appLocal.createPasswordHeader,
            ),
            body: KeyboardActions(
              isDialog: true,
              config: _buildConfig(context),
              child: Container(
                alignment: FractionalOffset.topCenter,
                padding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                child: SafeArea(
                  child: SingleChildScrollView(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          this.titlePasswordInfo(context),
                          this.inputPassword(context),
                          this.descriptionText(context),
                          this.titleConfirmPasswordInfo(context),
                          this.inputConfirmPassword(context),
                          SizedBox(
                            height: Screen.convertHeightSize(96),
                          ),
                          PrimaryButtonWidget(
                            buttonTitle: appLocal.commonNext,
                            onBtnPressed: !viewModel.isEnableButton
                                ? null
                                : () {
                                    focusConfirmPassword.unfocus(focusPrevious: false);
                                    if (_formKey.currentState.validate()) {
                                      // No any error in validation
                                      _formKey.currentState.save();
                                      _gotoFinish(model);
                                    } else {
                                      // validation error
                                      setState(() {
                                        _validate = true;
                                      });
                                    }
                                  },
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )),
      ),
    );
  }

  void _gotoFinish(RegisterMobileViewModel model) {
    //call api register
    model.register(controllerConfirmPassword.text, registerSuccess);
  }

  void registerSuccess() {
    RouterService.instance
        .navigateTo(RegisterSuccessRoute.buildPath, data: widget.requestRegisterEntity);
  }

  void _showAlert(String message) {
    AlertDialog alertDialog = AlertDialog(
      content: Text(message),
      actions: <Widget>[],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alertDialog;
        });
  }

  bool validatePassword(String value) {
    print(value);
    if (value.isEmpty || value.length < 8) {
      return false;
    } else {
      return true;
    }
  }

  bool validatePasswordString(String value) {
    RegExp regExp = new RegExp(
      r"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$",
      caseSensitive: false,
      multiLine: false,
    );

    print(value);
    if (regExp.hasMatch(value)) {
      return true;
    }
    return false;
  }

  void handleErrorCreatePassword(BaseError baseError) {
    _showAlert(baseError.message);
  }
}
