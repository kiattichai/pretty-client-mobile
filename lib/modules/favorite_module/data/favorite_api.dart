import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/favorite_module/model/favorite_add_request.dart';
import 'package:pretty_client_mobile/modules/favorite_module/model/favorite_remove_request.dart';
import 'package:pretty_client_mobile/modules/favorite_module/model/favorite_response_entity.dart';

class FavoriteApi {
  final String pathFavorite = "/users/favorites";
  final CoreApi coreApi;

  @provide
  @singleton
  FavoriteApi(this.coreApi);

  Future<FavoriteResponseEntity> getFavoriteList() async {
    final response = await coreApi.get(pathFavorite, null, BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return FavoriteResponseEntity().fromJson(response.data);
  }

  Future<FavoriteResponseEntity> getFavoriteListWithPage(int limit, int page) async {
    Map<String, String> query = {
      "limit": limit.toString(),
      "page": page.toString(),
    };
    final response = await coreApi.get(pathFavorite, query, BaseErrorEntity.badRequestToModelError,
        hasPermission: true);
    return FavoriteResponseEntity().fromJson(response.data);
  }

  Future<BaseResponseEntity> addFavorite(FavoriteAddRequest request) async {
    final response =
        await coreApi.post(pathFavorite, request.toJson(), BaseErrorEntity.badRequestToModelError);
    return BaseResponseEntity().fromJson(response.data);
  }

  Future<BaseResponseEntity> removeFavorite(FavoriteRemoveRequest request) async {
    final response =
        await coreApi.post(pathFavorite, request.toJson(), BaseErrorEntity.badRequestToModelError);
    return BaseResponseEntity().fromJson(response.data);
  }
}
