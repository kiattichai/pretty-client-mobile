import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:pretty_client_mobile/app/share_preference.dart';
import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/favorite_module/data/favorite_api.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/modules/favorite_module/model/favorite_add_request.dart';
import 'package:pretty_client_mobile/modules/favorite_module/model/favorite_remove_request.dart';
import 'package:pretty_client_mobile/modules/favorite_module/model/favorite_response_entity.dart';

class FavoriteRepository extends BaseRepository {
  final FavoriteApi api;
  final SharePrefInterface share;
  final List<Favorite> favoriteList = [];

  @provide
  @singleton
  FavoriteRepository(this.api, this.share);

  List<Favorite> loadFavoriteList() {
    if (favoriteList.isEmpty) {
      try {
        final Map<String, dynamic> result = jsonDecode(share.getFavorite());
        final data = FavoriteData().fromJson(result);
        favoriteList.addAll(data.record);
      } catch (e) {
        print(e);
      }
    }
    return favoriteList;
  }

  Future<FavoriteData> getFavoriteList() async {
    favoriteList.clear();
    final response = await api.getFavoriteList();
    await share.setFavorite(jsonEncode(response.data));
    return response.data;
  }

  Future<FavoriteResponseEntity> getFavoritesList() async {
    final response = await api.getFavoriteList();
    if (response.data != null) {
      return response;
    } else {
      return null;
    }
  }

  Future<FavoriteResponseEntity> getFavoritesListWithPage(limit, page) async {
    final response = await api.getFavoriteListWithPage(limit, page);
    if (response.data != null) {
      return response;
    } else {
      return null;
    }
  }

  Future<BaseResponseData> addFavorite(int id) async {
    final product = new FavoriteProduct();
    product.id = id;
    final model = Favorite();
    model.product = product;
    favoriteList.add(model);
    final response = await api.addFavorite(FavoriteAddRequest(id));
    return response.data;
  }

  Future<BaseResponseData> removeFavorite(int id) async {
    try {
      if (favoriteList.isNotEmpty) {
        final model = favoriteList.firstWhere((model) {
          return model.product.id == id;
        });
        favoriteList.remove(model);
      }
    } catch (e) {}
    final response = await api.removeFavorite(FavoriteRemoveRequest(id));
    return response.data;
  }
}
