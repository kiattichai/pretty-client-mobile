import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class FavoriteListResponseEntity with JsonConvert<FavoriteListResponseEntity> {
	FavoriteListResponseData data;
	FavoriteListResponseBench bench;
}

class FavoriteListResponseData with JsonConvert<FavoriteListResponseData> {
	List<FavoriteListResponseDataRecord> record;
}

class FavoriteListResponseDataRecord with JsonConvert<FavoriteListResponseDataRecord> {
	int id;
	FavoriteListResponseDataRecordPretty pretty;
	@JSONField(name: "created_at")
	String createdAt;
	@JSONField(name: "updated_at")
	String updatedAt;
}

class FavoriteListResponseDataRecordPretty with JsonConvert<FavoriteListResponseDataRecordPretty> {
	int id;
	String code;
	FavoriteListResponseDataRecordPrettyGroup group;
	@JSONField(name: "first_name")
	String firstName;
	@JSONField(name: "last_name")
	String lastName;
	@JSONField(name: "nick_name")
	String nickName;
	String birthday;
	int age;
	String gender;
	int weight;
	int height;
	int chest;
	int waistline;
	int hip;
	FavoriteListResponseDataRecordPrettyAvatar avatar;
}

class FavoriteListResponseDataRecordPrettyGroup with JsonConvert<FavoriteListResponseDataRecordPrettyGroup> {
	int id;
	String name;
	@JSONField(name: "start_drink")
	int startDrink;
}

class FavoriteListResponseDataRecordPrettyAvatar with JsonConvert<FavoriteListResponseDataRecordPrettyAvatar> {
	String id;
	String tag;
	String name;
	int width;
	int height;
	String mime;
	int size;
	String url;
	int position;
}

class FavoriteListResponseBench with JsonConvert<FavoriteListResponseBench> {
	int second;
	double millisecond;
	String format;
}
