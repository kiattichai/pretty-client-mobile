import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class FavoriteResponseEntity with JsonConvert<FavoriteResponseEntity> {
  FavoriteData data;
  FavoriteResponseBench bench;
}

class FavoriteData with JsonConvert<FavoriteData> {
  FavoritePagination pagination;
  List<Favorite> record;
}

class FavoritePagination with JsonConvert<FavoritePagination> {
  @JSONField(name: "current_page")
  int currentPage;
  @JSONField(name: "last_page")
  int lastPage;
  int limit;
  int total;
}

class Favorite with JsonConvert<Favorite> {
  int id;
  FavoriteProduct product;
  @JSONField(name: "created_at")
  String createdAt;
  @JSONField(name: "updated_at")
  String updatedAt;
}

class FavoriteProduct with JsonConvert<FavoriteProduct> {
  int id;
  String code;
  String name;
  List<dynamic> attributes;
  FavoriteProductAvatar avatar;
  bool onFloor;
  FavoriteProductFeedback feedbacks;

  displayOnFloor() {
    return onFloor == null ? false : onFloor;
  }
}

class FavoriteProductAvatar with JsonConvert<FavoriteProductAvatar> {
  String id;
  @JSONField(name: "store_id")
  int storeId;
  String tag;
  @JSONField(name: "album_id")
  int albumId;
  String name;
  int width;
  int height;
  String mime;
  int size;
  String url;
  int position;
}

class FavoriteProductFeedback with JsonConvert<FavoriteProductFeedback> {
  double summary;
  int total;
  List<FeedbackRecord> record;
}

class FeedbackRecord with JsonConvert<FeedbackRecord> {
  int score;
  int count;
}



class FavoriteResponseBench with JsonConvert<FavoriteResponseBench> {
  int second;
  double millisecond;
  String format;
}
