class FavoriteRemoveRequest {
  String action = 'remove';
  int id;

  FavoriteRemoveRequest(this.id);

  toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['action'] = action;
    data['product_id'] = id;
    return data;
  }
}
