import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/shared_module/data/refresh_token_api.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/modules/shared_module/models/refresh_token_response_entity.dart';

class RefreshTokenRepository extends BaseRepository {
  final RefreshTokenApi api;

  @provide
  RefreshTokenRepository(this.api);

  Future<RefreshTokenResponseData> refreshToken(String token) async {
    final response = await api.refreshToken(token, BaseErrorEntity.badRequestToModelError);
    return response != null ? response.data : null;
  }
}
