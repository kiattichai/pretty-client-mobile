import 'package:dio/dio.dart';
import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/modules/shared_module/models/refresh_token_response_entity.dart';

class RefreshTokenApi {
  final CoreApi coreApi;
  final String pathRefreshToken = "/users/register";

  @provide
  @singleton
  RefreshTokenApi(this.coreApi);

  Future<RefreshTokenResponseEntity> refreshToken(
      String token, Function badRequestToModelError) async {
    Response response =
        await coreApi.putRefreshToken(pathRefreshToken, token, badRequestToModelError);
    return RefreshTokenResponseEntity().fromJson(response.data);
  }
}
