import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/generated/json/product_response_entity_helper.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/BookingTime.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/checkout_response_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/get_checkout_response_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/jobs_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/checkout_request_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/order_request_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/payment_request_entity.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_response_entity.dart';

class BookingViewModel extends BaseViewModel {
  Function onErrorBooking;
  TabController tabController;
  ScrollController scrollController;
  int tabIndex = -1;
  bool tabClick = false;
  List<JobsDataRecord> jobsRecord = [];
  List<Widget> tabLists = [];
  CheckoutResponseEntity checkoutResponseEntity;
  BaseResponseEntity orderResponse;
  OrderRequestEntity orderRequest;
  int jobs_id;
  int productId;
  int productVariantId;
  String currentMonth;
  int variantIndex;

  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;

  BookingViewModel() {
    scrollController = new ScrollController();
    refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  }

  @override
  void postInit() {
    getJobs(productId);
  }

  Map<int, String> mapWeekDay = {
    1: "MON",
    2: "TUE",
    3: "WED",
    4: "THU",
    5: "FRI",
    6: "SAT",
    7: "SUN",
  };

  Widget titleTabBar(String month, String day, String strStatus, bool isAvailable,
      {bool isActive = false}) {
    return Padding(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Text(
              month,
              style: TextStyle(fontSize: 14),
            ),
          ),
          Container(
            child: Text(
              day,
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 1, bottom: 1, left: 8, right: 8),
            margin: EdgeInsets.only(top: 2),
            decoration: BoxDecoration(
                color:
                    isActive ? Design.theme.primaryColor : Colors.transparent,
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: Text(
              strStatus,
              style: TextStyle(
                  fontSize: 10,
                  color: isActive ? Colors.black : Design.theme.colorActive),
            ),
          ),
        ],
      ),
    );
  }

  Widget setTabBarActive(int prevIndex, int index) {
    if(prevIndex != index) {
      JobsDataRecord data = jobsRecord[index];
      DateTime dateTime = DateTime.parse(data.date);
      tabLists[index] = titleTabBar(mapWeekDay[dateTime.weekday],
          dateTime.day.toString(), data.status.jobText, data.status.isAvailable(), isActive: true);

      data = jobsRecord[prevIndex];
      dateTime = DateTime.parse(data.date);
      tabLists[prevIndex] = titleTabBar(mapWeekDay[dateTime.weekday],
          dateTime.day.toString(), data.status.jobText, data.status.isAvailable(), isActive: false);
    }
  }

  List<BookingTime> genBookingTime(String date) {
    DateTime dateTime;
    int dateNow = DateTime.now().millisecondsSinceEpoch;
    List<BookingTime> result = [];
    BookingTime bookingTime;
    bookingTime = new BookingTime();

    dateTime = DateTime.parse('$date 18:00:00');
    if (dateNow <= dateTime.millisecondsSinceEpoch) {
      bookingTime
        ..setTextTime("18:00 น.")
        ..setSelected(false)
        ..setTimeValue('18:00');
      result.add(bookingTime);
    }

    dateTime = DateTime.parse('$date 18:30:00');
    if (dateNow <= dateTime.millisecondsSinceEpoch) {
      bookingTime = new BookingTime();
      bookingTime
        ..setTextTime("18:30 น.")
        ..setSelected(false)
        ..setTimeValue('18:30');
      result.add(bookingTime);
    }

    dateTime = DateTime.parse('$date 19:00:00');
    if (dateNow <= dateTime.millisecondsSinceEpoch) {
      bookingTime = new BookingTime();
      bookingTime
        ..setTextTime("19:00 น.")
        ..setSelected(false)
        ..setTimeValue('19:00');
      result.add(bookingTime);
    }

    dateTime = DateTime.parse('$date 19:30:00');
    if (dateNow <= dateTime.millisecondsSinceEpoch) {
      bookingTime = new BookingTime();
      bookingTime
        ..setTextTime("19:30 น.")
        ..setSelected(false)
        ..setTimeValue('19:30');
      result.add(bookingTime);
    }

    dateTime = DateTime.parse('$date 20:00:00');
    if (dateNow <= dateTime.millisecondsSinceEpoch) {
      bookingTime = new BookingTime();
      bookingTime
        ..setTextTime("20:00 น.")
        ..setSelected(false)
        ..setTimeValue('20:00');
      result.add(bookingTime);
    }

    dateTime = DateTime.parse('$date 20:30:00');
    if (dateNow <= dateTime.millisecondsSinceEpoch) {
      bookingTime = new BookingTime();
      bookingTime
        ..setTextTime("20:30 น.")
        ..setSelected(false)
        ..setTimeValue('20:30');
      result.add(bookingTime);
    }

    dateTime = DateTime.parse('$date 21:00:00');
    if (dateNow <= dateTime.millisecondsSinceEpoch) {
      bookingTime = new BookingTime();
      bookingTime
        ..setTextTime("21:00 น.")
        ..setSelected(false)
        ..setTimeValue('21:00');
      result.add(bookingTime);
    }

    dateTime = DateTime.parse('$date 21:30:00');
    if (dateNow <= dateTime.millisecondsSinceEpoch) {
      bookingTime = new BookingTime();
      bookingTime
        ..setTextTime("21:30 น.")
        ..setSelected(false)
        ..setTimeValue('21:30');
      result.add(bookingTime);
    }

    dateTime = DateTime.parse('$date 22:00:00');
    if (dateNow <= dateTime.millisecondsSinceEpoch) {
      bookingTime = new BookingTime();
      bookingTime
        ..setTextTime("22:00 น.")
        ..setSelected(false)
        ..setTimeValue('22:00');
      result.add(bookingTime);
    }

    dateTime = DateTime.parse('$date 22:30:00');
    if (dateNow <= dateTime.millisecondsSinceEpoch) {
      bookingTime = new BookingTime();
      bookingTime
        ..setTextTime("22:30 น.")
        ..setSelected(false)
        ..setTimeValue('22:30');
      result.add(bookingTime);
    }

    dateTime = DateTime.parse('$date 23:00:00');
    if (dateNow <= dateTime.millisecondsSinceEpoch) {
      bookingTime = new BookingTime();
      bookingTime
        ..setTextTime("23:00 น.")
        ..setSelected(false)
        ..setTimeValue('23:00');
      result.add(bookingTime);
    }

    dateTime = DateTime.parse('$date 23:30:00');
    if (dateNow <= dateTime.millisecondsSinceEpoch) {
      bookingTime = new BookingTime();
      bookingTime
        ..setTextTime("23:30 น.")
        ..setSelected(false)
        ..setTimeValue('23:30');
      result.add(bookingTime);
    }

    return result;
  }

  setTabIndex(int val) {
    tabIndex = val;
    notifyListeners();
  }

  setTabClick(bool val) {
    tabClick = val;
    notifyListeners();
  }

  setJobsId(int val) {
    jobs_id = val;
  }

  setProductId(int val) {
    productId = val;
  }
  setProductVariantId(int val) {
    productVariantId = val;
  }

  void setBookingTimeList(int recordIndex, int index) {
    JobsDataRecord data = jobsRecord[recordIndex];
    bool selected = data.bookingTime[index].selected;
    for (int i = 0; i < data.bookingTime.length; i++) {
      data.bookingTime[i].selected = false;
      data.canBooking = false;
    }
    data.bookingTime[index].selected = !selected;
    data.canBooking = !selected;
    notifyListeners();
  }

  Future<void> getJobs(int id) async {
    setLoading(true);
    catchError(() async {
      List<JobsDataRecord> response = [];
      List<JobsDataRecord> tmp = await di.bookingRepository.getJobs(id);
      if (tmp.length > 0) {
        for (int i = 0; i < tmp.length; i++) {
          JobsDataRecord data = tmp[i];

          int dateNow = DateTime.now().millisecondsSinceEpoch;
          int dateCurrent =
              DateTime.parse("${data.date} 23:30:00").millisecondsSinceEpoch;
          if (dateNow <= dateCurrent) {
            JobsDataRecordVenue venue = JobsDataRecordVenue();
            venue.id = 1;
            venue.name = '35 DARA SHOW';
            venue.address = '35 DARA SHOW';
            venue.lat = 13.9066423;
            venue.long = 100.4508948;

            tmp[i].bookingTime = genBookingTime(data.date);
            tmp[i].venue = venue;
            DateTime dateTime = DateTime.parse(data.date);

            response.add(tmp[i]);

            if (tabIndex == -1 && (data.status.isAvailable())) {
              setTabIndex(response.length - 1);
              tabLists.add(titleTabBar(mapWeekDay[dateTime.weekday],
                  dateTime.day.toString(), data.status.jobText, data.status.isAvailable(),
                  isActive: true));
            } else {
              tabLists.add(titleTabBar(mapWeekDay[dateTime.weekday],
                  dateTime.day.toString(), data.status.jobText, data.status.isAvailable()));
            }
          }
        }

        if (tabIndex > -1) {
          jobsRecord.addAll(response);
          setMonth(tabIndex);
        } else {
          jobsRecord = null;
        }
      } else {
        jobsRecord = null;
      }
      setLoading(false);
    });
  }

  void resetData() {
    tabIndex = -1;
    tabClick = false;
    jobsRecord = [];
    tabLists = [];
  }

  setMonth(int index) {
    String date = jobsRecord[index].date;
    DateTime dateTime = DateTime.parse(date);
    currentMonth = DateFormat('MMMM').format(dateTime).toUpperCase();
    notifyListeners();
  }

  String getBookingTimeList(int recordIndex) {
    int n = -1;
    JobsDataRecord data = jobsRecord[recordIndex];
    for (int i = 0; i < data.bookingTime.length; i++) {
      if (data.bookingTime[i].selected == true) {
        n = i;
        break;
      }
    }
    return data.bookingTime[n].timeValue;
  }

  bool hasSelected() {
    bool result = false;
    for (int i = 0; i < jobsRecord.length; i++) {
      if (jobsRecord[i].canBooking == true) {
        result = true;
        break;
      }
    }
    return result;
  }

  void buildOrderRequest(
      CheckoutRequestEntity requestEntity, Function onSuccess) {
    setLoading(true);
    catchError(() async {
      int dateNow = DateTime.now().millisecondsSinceEpoch;

//      for (int a = 0; a < requestEntity.jobs.length; a++) {
//        int dateCurrent =
//            DateTime.parse(requestEntity.jobs[a].jobAt).millisecondsSinceEpoch;
//        if (dateNow >= dateCurrent) {
//          BaseError e = BaseError();
//          e.message = "เวลาในการจองไม่ถูกต้อง";
//          throw e;
//        }
//      }
      int dateCurrent =
          DateTime.parse(requestEntity.shipping.time).millisecondsSinceEpoch;
      if (dateNow >= dateCurrent) {
        BaseError e = BaseError();
        e.message = "เวลาในการจองไม่ถูกต้อง";
        throw e;
      }
      checkoutResponseEntity =
          await di.bookingRepository.createCheckout(requestEntity);
      GetCheckoutResponseEntity response = await di.bookingRepository
          .getCheckout(checkoutResponseEntity.data.id);
      GetCheckoutResponseData checkoutData = response.data;
      print("============== checkoutData ==============");
      print(checkoutData.toJson());
      final result = await di.profileRepository.getProfile();
      orderRequest = wrapRequestOrder(checkoutData, result);

      print("============== Order Request ==============");
      print(orderRequest.toJson());
      setLoading(false);
      onSuccess();
    });
  }

  OrderRequestEntity wrapRequestOrder(
      GetCheckoutResponseData checkoutData, UserProfileResponseData profile) {
    OrderRequestEntity order = OrderRequestEntity();
    order.storeId = checkoutData.record[0].store.id;
    order.storeName = checkoutData.record[0].store.name;
    order.firstname = profile.firstName;
    order.lastname = profile.lastName;
    order.userWallet = profile.wallet;
    order.telephone = profile.username;
    order.price = checkoutData.totalCost.price;
    order.discount = checkoutData.totalCost.discount;
    order.paymentMethod = checkoutData.totalCost.total > 0 ? "wallet" : "free";
    order.paymentProvider = "pret";
    order.shippingAddress = checkoutData.shipping.address;
    order.shippingLat = checkoutData.shipping.lat;
    order.shippingLong = checkoutData.shipping.long;
    order.shippingAt = checkoutData.shipping.time;
    order.total = checkoutData.totalCost.total;
    order.orderProducts = wrapOrderProduct(checkoutData);

    return order;
  }

  List<OrderRequestOrderProduct> wrapOrderProduct(
      GetCheckoutResponseData checkoutData) {
    List<OrderRequestOrderProduct> result = [];
    for (int i = 0; i < checkoutData.record.length; i++) {
      var record = checkoutData.record[i];
      OrderRequestOrderProduct tmp = OrderRequestOrderProduct();
      tmp.productId = record.id;
      tmp.productVariantId = record.variants[0].id;
      tmp.productCode = record.code;
      tmp.productName = record.name;
      tmp.productOption = record.variants[0].textOption();
      if(record.option){
        tmp.imageId = record.variants[0].image.id;
        tmp.imageName = record.variants[0].image.name;
      } else {
        tmp.imageId = record.images[0].id;
        tmp.imageName = record.images[0].name;
      }
      tmp.quantity = record.variants[0].quantity;
      tmp.price = record.variants[0].price;
      tmp.discount = record.variants[0].discount;
      tmp.total = record.variants[0].total;
      result.add(tmp);
    }
    return result;
  }

  String wrapOption(List<GetCheckoutResponseDataRecordVariantsOption> options){
    String result;
    for (int i = 0; i < options.length; i++) {
      if(options[i].name != null){
        result = options[i].name+ ": " + options[i].value.name;
        if(i < options.length-1){
          result +=', ';
        }
      }

    }
    return result;
  }

  PaymentRequestEntity wrapPayment(
      BaseResponseEntity responseOrder, GetCheckoutResponseData checkoutData) {
    PaymentRequestEntity payment = PaymentRequestEntity();
    payment.orderId = responseOrder.data.orderId;
    payment.paymentMethod =
        checkoutData.totalCost.total > 0 ? "wallet" : "free";
    payment.paymentProvider = "pret";

    return payment;
  }

  setScrollToBottom() {
    scrollController.animateTo(
      scrollController.position.maxScrollExtent,
      curve: Curves.easeOut,
      duration: const Duration(milliseconds: 450),
    );
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    print(error);
    onErrorBooking(error);
  }
}
