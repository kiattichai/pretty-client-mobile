import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/env_config.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/model/language_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/booking_view_model.dart';
import 'package:pretty_client_mobile/modules/booking_module/confirmation_route.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/BookingTime.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/checkout_request_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/jobs_entity.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/product_response_entity.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/custom_modal.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/widgets/status_bar.dart';

class BookingScreen extends StatefulWidget {
  final dynamic data;

  const BookingScreen({
    Key key,
    this.data,
  }) : super(key: key);

//  bookingData
  @override
  State<StatefulWidget> createState() {
    return BookingScreenState();
  }
}

class BookingScreenState
    extends BaseStateProvider<BookingScreen, BookingViewModel>
    with TickerProviderStateMixin {
  PageController mPageController;
  final int animationDuration = 300;
  Product product = Product();
  int variantIndex = 0;

  @override
  void initState() {
    product = widget.data['product'];
    variantIndex = widget.data['variant_index'];
    super.initState();
    viewModel = BookingViewModel();
    viewModel
      ..setProductId(product.id)
      ..variantIndex = variantIndex
      ..onErrorBooking = handleError;
  }

  Widget lineBottom() {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: Design.theme.colorLine.withOpacity(0.45),
          ),
        ),
      ),
    );
  }

  Widget buildStepBar(LanguageEntity appLocal) {
    return Container(
      color: Design.theme.colorMainBackground,
      padding: EdgeInsets.only(
        top: 30,
        bottom: 30,
//        left: Screen.convertWidthSize(20),
//        right: Screen.convertWidthSize(20),
      ),
      child: StatusBar(1, 0, [], [], appLocal),
    );
  }

  Widget buildHeader() {
    return Container(
      padding: EdgeInsets.all(Screen.convertWidthSize(15)),
      decoration: BoxDecoration(
        color: Design.theme.colorLine.withOpacity(0.2),
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 10),
            child: Image.network(
              product.option
                  ? product.variants[variantIndex].image
                      .getImgUrlResize(EnvConfig.instance.resUrl, 140, 160)
                  : product.images[0]
                      .getImgUrlResize(EnvConfig.instance.resUrl, 140, 160),
              width: Screen.convertWidthSize(90),
              height: Screen.convertHeightSize(110),
            ),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(bottom: 8),
                  child: RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: product.name,
                          style: TextStyle(
                              color: Design.theme.colorActive,
                              fontSize: 18,
                              fontWeight: FontWeight.w700),
                        ),
                        product.option&&!product.variants[variantIndex].textOption().isEmpty?TextSpan(
                          text: " (${product.variants[variantIndex].textOption()})",
                          style: TextStyle(
                              color: Design.theme.colorActive.withOpacity(0.8),
                              fontSize: 14,
                              fontWeight: FontWeight.w500),
                        ):TextSpan(),
                      ],
                    ),
                  ),

//                  Text(
//                    product.name +" "+ product.variants[variantIndex].textOption(),
//                    style: TextStyle(
//                        color: Design.theme.colorActive,
//                        fontSize: 18,
//                        fontWeight: FontWeight.w700),
//                  ),
                ),
                Container(
                  width: Screen.width - Screen.convertWidthSize(175),
                  height: Screen.convertHeightSize(80),
                  child: GridView.count(
                    physics: NeverScrollableScrollPhysics(),
                    crossAxisCount: 2,
                    childAspectRatio: 3.7,
                    children: <Widget>[
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          mainAxisAlignment: MainAxisAlignment.start,
                          textBaseline: TextBaseline.alphabetic,
                          children: <Widget>[
                            Container(
                              child: Text(
                                "${appLocal.friendInfoAge} : ",
                                style:
                                    TextStyle(color: Design.theme.colorText2),
                              ),
                              width: 60,
                            ),
                            Container(
                              child: Text(
                                "${getAttribute("birthdate")}",
                                style: TextStyle(
                                    color: Design.theme.colorActive,
                                    fontSize: 15),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 5),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          mainAxisAlignment: MainAxisAlignment.start,
                          textBaseline: TextBaseline.alphabetic,
                          children: <Widget>[
                            Container(
                              child: Text(
                                "${appLocal.friendInfoWaist} : ",
                                style:
                                    TextStyle(color: Design.theme.colorText2),
                              ),
                              width: 65,
                            ),
                            Container(
                              child: Text(
                                "${getAttribute("weight")}",
                                style: TextStyle(
                                    color: Design.theme.colorActive,
                                    fontSize: 15),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          mainAxisAlignment: MainAxisAlignment.start,
                          textBaseline: TextBaseline.alphabetic,
                          children: <Widget>[
                            Container(
                              child: Text(
                                "${appLocal.friendInfoHeight} : ",
                                style:
                                    TextStyle(color: Design.theme.colorText2),
                              ),
                              width: 60,
                            ),
                            Container(
                              child: Text(
                                "${getAttribute("height")}",
                                style: TextStyle(
                                    color: Design.theme.colorActive,
                                    fontSize: 15),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 5),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          mainAxisAlignment: MainAxisAlignment.start,
                          textBaseline: TextBaseline.alphabetic,
                          children: <Widget>[
                            Container(
                              child: Text(
                                "${appLocal.friendInfoChest} : ",
                                style:
                                    TextStyle(color: Design.theme.colorText2),
                              ),
                              width: 65,
                            ),
                            Container(
                              child: Text(
                                "${getAttribute("chest")}",
                                style: TextStyle(
                                    color: Design.theme.colorActive,
                                    fontSize: 15),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          mainAxisAlignment: MainAxisAlignment.start,
                          textBaseline: TextBaseline.alphabetic,
                          children: <Widget>[
                            Container(
                              child: Text(
                                "${appLocal.friendInfoWaist} : ",
                                style:
                                    TextStyle(color: Design.theme.colorText2),
                              ),
                              width: 60,
                            ),
                            Container(
                              child: Text(
                                "${getAttribute("waistline")}",
                                style: TextStyle(
                                    color: Design.theme.colorActive,
                                    fontSize: 15),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 5),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          mainAxisAlignment: MainAxisAlignment.start,
                          textBaseline: TextBaseline.alphabetic,
                          children: <Widget>[
                            Container(
                              child: Text(
                                "${appLocal.friendInfoHip} :   ",
                                style:
                                    TextStyle(color: Design.theme.colorText2),
                              ),
                              width: 65,
                            ),
                            Container(
                              child: Text(
                                "${getAttribute("hip")}",
                                style: TextStyle(
                                    color: Design.theme.colorActive,
                                    fontSize: 15),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildVenue(int index) {
    JobsDataRecord record = viewModel.jobsRecord[index];
    return Container(
      padding: EdgeInsets.only(top: 20, left: 10, right: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 10),
            child: Text(
              appLocal.venue,
              style: TextStyle(color: Design.theme.colorActive, fontSize: 18),
            ),
          ),
          Container(
            width: Screen.convertWidthSize(230),
            child: Text(
              record.venue.name.toString(),
              style: TextStyle(color: Design.theme.colorText2, fontSize: 18),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 2),
            child: SvgPicture.asset("images/map_pin_line.svg"),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Screen(context);
    appLocal = AppLocalizations.of(context);
    return BaseWidget<BookingViewModel>(
      model: viewModel,
      builder: (context, model, child) {
        return Scaffold(
          appBar: CustomHeaderDetail(
            height: Screen.convertHeightSize(55),
            title: appLocal.appointment,
            onTap: () {
              RouterService.instance.pop();
            },
          ),
          body: ProgressHUD(
            color: Colors.transparent,
            progressIndicator: CircleLoading(),
            inAsyncCall: model.loading,
//            child: RefreshIndicator(
//              color: Design.theme.colorMainBackground,
//              backgroundColor: Design.theme.colorPrimary,
//              key: model.refreshIndicatorKey,
//              onRefresh: () {
//                model.resetData();
//                return model.getJobs(model.jobs_id);
//              },
            child: Builder(
              builder: (context) {
                if (viewModel.jobsRecord != null &&
                    viewModel.jobsRecord.length == 0 &&
                    viewModel.tabIndex == -1) {
                  return Container(
                    color: Design.theme.backgroundColor,
                    padding: EdgeInsets.only(
                      left: 15,
                      right: 15,
                      bottom: Screen.convertHeightSize(10),
                    ),
                    child: Column(
                      children: <Widget>[
                        buildStepBar(appLocal),
                        buildHeader(),
//                          lineBottom(),
                      ],
                    ),
                  );
                } else if (viewModel.jobsRecord == null) {
                  return Container(
                    color: Design.theme.backgroundColor,
                    padding: EdgeInsets.only(
                      left: 15,
                      right: 15,
                      bottom: Screen.convertHeightSize(10),
                    ),
                    child: Column(
                      children: <Widget>[
                        buildStepBar(appLocal),
                        buildHeader(),
//                          lineBottom(),
                        Align(
                          alignment: Alignment.center,
                          child: Container(
                              padding: EdgeInsets.only(
                                  left: 15,
                                  right: 15,
                                  bottom: Screen.convertHeightSize(10)),
                              margin: EdgeInsets.only(
                                  top: Screen.convertHeightSize(30)),
                              child: Text(
                                appLocal.errorDataNotFound,
                                style: TextStyle(
                                    color: Design.theme.colorInactive),
                              )),
                        ),
                      ],
                    ),
                  );
                } else {
                  viewModel
                    ..tabController = new TabController(
                        vsync: this,
                        length: viewModel.jobsRecord.length,
                        initialIndex: viewModel.tabIndex);
                  mPageController =
                      PageController(initialPage: viewModel.tabIndex);
                  return Container(
//                      color: Design.theme.colorLine.withOpacity(0.2),
//                      color: Colors.green,
                    child: ListView.builder(
                      padding: EdgeInsets.only(
                        left: 15,
                        right: 15,
//                            bottom: Screen.convertHeightSize(10),
                      ),
//                        padding: EdgeInsets.only(
//                          bottom: Screen.convertHeightSize(10),
//                        ),
                      physics: const AlwaysScrollableScrollPhysics(),
                      itemCount: 5,
                      controller: model.scrollController,
                      itemBuilder: (context, i) {
                        if (i == 0) {
                          return buildStepBar(appLocal);
                        } else if (i == 1) {
                          return Column(
                            children: <Widget>[
                              buildHeader(),
//                                lineBottom(),
                            ],
                          );
                        } else if (i == 2) {
                          return Column(
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                  color:
                                      Design.theme.colorLine.withOpacity(0.2),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                  ),
                                ),
                                margin: EdgeInsets.only(top: 20),
                                padding: EdgeInsets.only(top: 15, bottom: 15),
                                alignment: Alignment.center,
                                child: Text(
                                  viewModel.currentMonth,
                                  style: TextStyle(
                                    color: Design.theme.colorActive,
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                              lineBottom(),
                            ],
                          );
                        } else if (i == 3) {
                          return Column(
                            children: <Widget>[
                              Container(
                                color: Design.theme.colorLine.withOpacity(0.2),
                                alignment: Alignment.topLeft,
                                child: TabBar(
                                  controller: viewModel.tabController,
                                  isScrollable: true,
                                  unselectedLabelColor:
                                      Design.theme.colorActive,
                                  labelColor: Design.theme.colorPrimary,
                                  indicatorColor: Colors.transparent,
                                  labelPadding: EdgeInsets.only(
                                    left: Screen.convertWidthSize(8),
                                    right: Screen.convertWidthSize(8),
                                  ),
                                  tabs: viewModel.tabLists,
                                  onTap: (index) async {
                                    int prevIndex = viewModel.tabIndex;
                                    print("========== onTabbb ======== " +
                                        index.toString());
                                    viewModel.setScrollToBottom();
                                    viewModel.setTabClick(true);
                                    if (!viewModel.jobsRecord[index].status
                                        .isAvailable()) {
                                      viewModel.tabController.index =
                                          viewModel.tabIndex;
                                      viewModel.setTabClick(false);
                                    } else {
                                      viewModel.setTabBarActive(
                                          prevIndex, index);
                                      viewModel.setTabIndex(index);
                                      await mPageController.animateToPage(index,
                                          duration: Duration(
                                              milliseconds: animationDuration),
                                          curve: Curves.ease);
                                      viewModel.setMonth(index);
                                      viewModel.setTabClick(false);
                                    }
                                  },
                                ),
                              ),
                              lineBottom(),
                            ],
                          );
                        } else if (i == 4) {
                          return Container(
//                              height: Screen.convertHeightSize(
//                                  Device.get().isIos ? 345 : 360),
                            height: Screen.convertHeightSize(300),
                            decoration: BoxDecoration(
                              color: Design.theme.colorLine.withOpacity(0.2),
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(10),
                                bottomRight: Radius.circular(10),
                              ),
                            ),
                            child: PageView.builder(
                              controller: mPageController,
                              scrollDirection: Axis.horizontal,
                              onPageChanged: (index) async {
                                int prevIndex = viewModel.tabIndex;
                                int next = index;
                                if (!viewModel.tabClick) {
                                  if (!viewModel.jobsRecord[index].status
                                      .isAvailable()) {
                                    if (next <= 0 ||
                                        next >=
                                            viewModel.jobsRecord.length - 1) {
                                      next = viewModel.tabIndex;
                                    } else if (viewModel.tabIndex < index) {
                                      int tmp = next;
                                      for (;
                                          tmp < viewModel.jobsRecord.length;) {
                                        if (viewModel.jobsRecord[tmp].status
                                            .isAvailable()) {
                                          next = tmp;
                                          break;
                                        } else {
                                          tmp++;
                                          if (tmp ==
                                              viewModel.jobsRecord.length)
                                            next--;
                                        }

                                        print(tmp);
                                      }
                                    } else {
                                      int tmp = next;
                                      for (; tmp >= 0;) {
                                        if (viewModel.jobsRecord[tmp].status
                                            .isAvailable()) {
                                          next = tmp;
                                          break;
                                        } else {
                                          tmp--;
                                          if (tmp < 0) next++;
                                        }
                                        print(tmp);
                                      }
                                    }
                                    viewModel.setTabBarActive(prevIndex, next);
                                    await mPageController.animateToPage(next,
                                        duration: Duration(milliseconds: 180),
                                        curve: Curves.ease);
                                    viewModel.tabController.index = next;
                                    viewModel.setTabIndex(next);
                                    viewModel.setTabClick(false);
                                  } else {
                                    viewModel.tabController.index = next;
                                    viewModel.setTabBarActive(prevIndex, next);
                                    viewModel.setTabIndex(next);
                                    viewModel.setMonth(next);
                                    viewModel.setTabClick(false);
                                  }
                                }
                                viewModel.setScrollToBottom();
                              },
                              itemCount: viewModel.jobsRecord.length,
                              itemBuilder: (context, i) {
                                return Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: <Widget>[
                                      InkWell(
                                        onTap: () async {
                                          print("Open map");
                                          final JobsDataRecordVenue venue =
                                              viewModel.jobsRecord[i].venue;
                                          final availableMaps =
                                              await MapLauncher.installedMaps;
                                          if (await MapLauncher.isMapAvailable(
                                              MapType.google)) {
                                            await MapLauncher.launchMap(
                                              mapType: MapType.google,
                                              coords:
                                                  Coords(venue.lat, venue.long),
                                              title: venue.name,
                                              description: venue.address,
                                            );
                                          } else if (await MapLauncher
                                              .isMapAvailable(MapType.apple)) {
                                            await MapLauncher.launchMap(
                                              mapType: MapType.apple,
                                              coords:
                                                  Coords(venue.lat, venue.long),
                                              title: venue.name,
                                              description: venue.address,
                                            );
                                          }
                                        },
                                        child: buildVenue(i),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(
                                            top: 10, left: 10, right: 10),
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          appLocal.chooseTime,
                                          style: TextStyle(
                                              color: Design.theme.colorActive,
                                              fontSize: 18),
                                        ),
                                      ),
                                      Expanded(
                                        child: GridView.count(
                                          physics:
                                              NeverScrollableScrollPhysics(),
                                          childAspectRatio:
                                              Screen.convertWidthSize(12) /
                                                  Screen.convertWidthSize(4.6),
                                          primary: false,
                                          padding: EdgeInsets.only(
                                              top: 20,
                                              bottom: 10,
                                              left: 10,
                                              right: 10),
                                          crossAxisSpacing:
                                              Screen.convertHeightSize(10),
                                          mainAxisSpacing:
                                              Screen.convertHeightSize(10),
                                          crossAxisCount: 3,
                                          children: buildBookingTime(i),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                          );
                        } else {
                          return Container();
                        }
                      },
                    ),
                  );
                }
              },
            ),
//            ),
          ),
          bottomNavigationBar: Builder(
            builder: (context) {
              if (viewModel.jobsRecord != null &&
                  viewModel.jobsRecord.length > 0 &&
                  viewModel.hasSelected()) {
                if (!viewModel.loading && viewModel.orderRequest == null) {
                  Future.delayed(Duration(milliseconds: 10)).then((value) {
                    viewModel.setScrollToBottom();
                  });
                }

                return Container(
                  height: Screen.convertHeightSize(80),
                  alignment: Alignment.bottomCenter,
                  padding:
                      EdgeInsets.only(left: 15, right: 15, top: 0, bottom: 15),
                  child: MaterialButton(
                      disabledColor: Color(0xff636362),
                      disabledTextColor: Color(0xffbfbfbf),
                      textColor: Design.theme.colorMainBackground,
                      color: Design.theme.colorPrimary,
                      height: 48,
                      minWidth: Screen.width - 30,
                      child: Text(
                        appLocal.commonNext,
                        style: TextStyle(
                          fontFamily: 'Sarabun',
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(26),
                      ),
                      onPressed: viewModel.jobsRecord.length > 0 &&
                              viewModel
                                  .jobsRecord[viewModel.tabIndex].canBooking
                          ? _confirmBooking
                          : null),
                );
              } else {
                return Container(
                  height: 1,
                );
              }
            },
          ),
        );
      },
    );
  }

  List<Widget> buildBookingTime(index) {
    List<Widget> result = [];
    for (int i = 0; i < viewModel.jobsRecord[index].bookingTime.length; i++) {
      BookingTime bookingTime = viewModel.jobsRecord[index].bookingTime[i];
      Widget widget = InkWell(
          onTap: () => _onTileClicked(i),
          child: Container(
            child: Center(
              child: Text(
                bookingTime.textTime,
                style: TextStyle(
                  fontSize: 16,
                  color: bookingTime.selected
                      ? Design.theme.colorMainBackground
                      : Design.theme.colorInactive,
                ),
              ),
            ),
            decoration: new BoxDecoration(
              color: bookingTime.selected
                  ? Design.theme.colorPrimary
                  : Color(0xff636362),
              borderRadius: BorderRadius.circular(8),
            ),
          ));
      result.add(widget);
    }
    return result;
  }

  void _onTileClicked(int index) {
    debugPrint("You tapped on item $index  ${viewModel.tabController.index}");
    viewModel.setBookingTimeList(viewModel.tabController.index, index);
  }

  String getImgUrl(List<ProductImage> img, double w, double h) {
    String result = "https://admin.prettyhub.me/images/image-empty.png";
    if (img.length > 0) {
      result =
          "${EnvConfig.instance.resUrl}/w_${w.round()},h_${h.round()},c_crop/${img[0].id}/${img[0].name}";
    }
    return result;
  }

  String getMonth(JobsDataRecord jobsDataRecord) {
    DateTime dateTime = DateTime.parse(jobsDataRecord.date);
    String month = DateFormat('MMMM').format(dateTime);
    return month;
  }

  String getAttribute(String code) {
    try {
      List<ProductAttribute> attr = product.attributes;
      String result = "";
      for (int i = 0; i < product.attributes.length; i++) {
        if (attr[i].code == code) {
          if (code == "birthdate") {
            var berlinWallFell = new DateTime.now();
            var dDay = DateTime.parse(attr[i].valueText);
            Duration difference = berlinWallFell.difference(dDay);
            var birthdate = (difference.inDays / 365);
            result = birthdate.floor().toString();
          } else {
            result = attr[i].valueText;
          }
          break;
        }
      }
      return result;
    } catch (e) {
      return '';
    }
  }

  _confirmBooking() async {
    CheckoutRequestEntity requestEntity = CheckoutRequestEntity();
//    CheckoutRequestJob job = CheckoutRequestJob();
//    List<CheckoutRequestJob> jobs = [];
    JobsDataRecord data = viewModel.jobsRecord[viewModel.tabController.index];
    print(data.venue.address);
    String selectTime =
        viewModel.getBookingTimeList(viewModel.tabController.index);
//    job.id = data.id;
//    job.prettyId = data.pretty.id;
//    job.quantity = 1;
//    job.jobAt = "${data.date} $selectTime:00";
//    jobs.add(job);
//    requestEntity.storeId = data.store.id;
//    requestEntity.venueId = data.venue.id;
//    requestEntity.jobs = jobs;

    CheckoutRequestProduct products = new CheckoutRequestProduct();
    CheckoutRequestShipping shipping = new CheckoutRequestShipping();
    products.id = product.id;
    products.variantId = product.variants[variantIndex].id;
//    products.jobId = data.id;
    products.quantity = 1;

    shipping.address = data.venue.address;
    shipping.lat = data.venue.lat;
    shipping.long = data.venue.long;
    shipping.time = "${data.date} $selectTime:00";

    requestEntity.storeId = 1;
    requestEntity.products = [];
    requestEntity.products.add(products);
    requestEntity.shipping = shipping;
    print(requestEntity.toJson());
    viewModel.buildOrderRequest(requestEntity, onSuccess);
  }

  void onSuccess() async {
    viewModel.setLoading(false);
    await RouterService.instance
        .navigateTo(ConfirmationRoute.buildPath, data: viewModel.orderRequest);
    viewModel.orderRequest = null;
    viewModel.resetData();
    viewModel.getJobs(viewModel.productId);
  }

  void handleError(BaseError error) {
    viewModel.setLoading(false);
    _showAlert(error.message.toString());
    print(error.toJson());
  }

  void _showAlert(String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomModal(
            type: ModalType.CLOSE,
            desc: message,
            appLocal: appLocal,
            onTapConfirm: () {
              viewModel.resetData();
              viewModel.getJobs(viewModel.productId);
            },
          );
        });
  }
}
