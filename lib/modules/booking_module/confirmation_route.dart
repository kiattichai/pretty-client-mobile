import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/booking_module/confirmation_screen.dart';

class ConfirmationRoute extends BaseRoute {
  static String buildPath = '/order_confirmation';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
      builder: (context) => ConfirmationScreen(
        order: data,
      ),
    );
  }
}
