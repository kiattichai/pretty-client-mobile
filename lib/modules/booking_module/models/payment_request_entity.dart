import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class PaymentRequestEntity with JsonConvert<PaymentRequestEntity> {
	@JSONField(name: "order_id")
	int orderId;
	@JSONField(name: "payment_method")
	String paymentMethod;
	@JSONField(name: "payment_provider")
	String paymentProvider;
}
