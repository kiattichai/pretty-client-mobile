import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class CheckoutResponseEntity with JsonConvert<CheckoutResponseEntity> {
	CheckoutResponseData data;
	CheckoutResponseBench bench;
}

class CheckoutResponseData with JsonConvert<CheckoutResponseData> {
	String message;
	String id;
	@JSONField(name: "expired_time")
	int expiredTime;
	@JSONField(name: "expired_at")
	int expiredAt;
}

class CheckoutResponseBench with JsonConvert<CheckoutResponseBench> {
	int second;
	double millisecond;
	String format;
}
