import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class OrderRequestEntity with JsonConvert<OrderRequestEntity> {
	@JSONField(name: "store_id")
	int storeId;
	@JSONField(name: "store_name")
	String storeName;
	String firstname;
	String lastname;
	String email;
	String telephone;
	double price;
	double discount;
	@JSONField(name: "payment_method")
	String paymentMethod;
	@JSONField(name: "payment_provider")
	String paymentProvider;
	double vat;
	double fee;
	@JSONField(name: "service_fee")
	double serviceFee;
	@JSONField(name: "payment_fee")
	double paymentFee;
	double tax;
	@JSONField(name: "organizer_fee")
	double organizerFee;
	@JSONField(name: "organizer_service_fee")
	double organizerServiceFee;
	@JSONField(name: "organizer_payment_fee")
	double organizerPaymentFee;
	@JSONField(name: "organizer_tax")
	double organizerTax;
	double total;
	double income;
	@JSONField(name: "user_wallet")
	double userWallet;

	@JSONField(name: "shipping_address")
	String shippingAddress;
	@JSONField(name: "shipping_lat")
	double shippingLat;
	@JSONField(name: "shipping_long")
	double shippingLong;
	@JSONField(name: "shipping_at")
	String shippingAt;

	@JSONField(name: "order_products")
	List<OrderRequestOrderProduct> orderProducts;
}

class OrderRequestOrderProduct with JsonConvert<OrderRequestOrderProduct> {
	@JSONField(name: "job_id")
	int jobId;
	@JSONField(name: "pretty_id")
	int prettyId;
	@JSONField(name: "pretty_code")
	String prettyCode;
	@JSONField(name: "pretty_name")
	String prettyName;
	@JSONField(name: "pretty_nick_name")
	String prettyNickName;

	@JSONField(name: "product_id")
	int productId;
	@JSONField(name: "product_variant_id")
	int productVariantId;
	@JSONField(name: "product_code")
	String productCode;
	@JSONField(name: "product_name")
	String productName;
	@JSONField(name: "product_option")
	String productOption;


	@JSONField(name: "pretty_group_name")
	String prettyGroupName;
	@JSONField(name: "image_id")
	String imageId;
	@JSONField(name: "image_name")
	String imageName;

	@JSONField(name: "venue_id")
	int venueId;
	@JSONField(name: "venue_name")
	String venueName;
	double lat;
	double long;
	int quantity;
	double price;
	double discount;
	double fee;
	@JSONField(name: "service_fee")
	double serviceFee;
	@JSONField(name: "payment_fee")
	double paymentFee;
	double tax;
	@JSONField(name: "organizer_fee")
	double organizerFee;
	@JSONField(name: "organizer_service_fee")
	double organizerServiceFee;
	@JSONField(name: "organizer_payment_fee")
	double organizerPaymentFee;
	@JSONField(name: "organizer_tax")
	double organizerTax;
	double total;
	double income;
	@JSONField(name: "job_at")
	String jobAt;
	@JSONField(name: "venue_phone")
	String venuePhone;
}
