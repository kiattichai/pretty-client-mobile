import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class OrdersResponseEntity with JsonConvert<OrdersResponseEntity> {
	OrdersResponseData data;
	OrdersResponseBench bench;
}

class OrdersResponseData with JsonConvert<OrdersResponseData> {
	OrdersResponseDataPagination pagination;
	List<OrdersResponseDataRecord> record;
}

class OrdersResponseDataPagination with JsonConvert<OrdersResponseDataPagination> {
	@JSONField(name: "current_page")
	int currentPage;
	@JSONField(name: "last_page")
	int lastPage;
	int limit;
	int total;
}

class OrdersResponseDataRecord with JsonConvert<OrdersResponseDataRecord> {
	int id;
	@JSONField(name: "order_no")
	String orderNo;
	@JSONField(name: "store_id")
	int storeId;
	@JSONField(name: "store_name")
	String storeName;
	@JSONField(name: "user_id")
	int userId;
	String firstname;
	String lastname;
	dynamic email;
	String telephone;
	@JSONField(name: "payment_provider")
	String paymentProvider;
	@JSONField(name: "payment_method")
	String paymentMethod;
	@JSONField(name: "payment_method_text")
	String paymentMethodText;
	@JSONField(name: "payment_ref")
	String paymentRef;
	@JSONField(name: "payment_ref_no1")
	dynamic paymentRefNo1;
	@JSONField(name: "payment_ref_no2")
	dynamic paymentRefNo2;
	@JSONField(name: "payment_message")
	String paymentMessage;
	@JSONField(name: "shipping_provider")
	String shippingProvider;
	@JSONField(name: "shipping_method")
	String shippingMethod;
	@JSONField(name: "shipping_address")
	String shippingAddress;
	@JSONField(name: "shipping_lat")
	double shippingLat;
	@JSONField(name: "shipping_long")
	double shippingLong;
	@JSONField(name: "shipping_at")
	OrdersResponseDataRecordShippingAt shippingAt;
	int price;
	int discount;
	int summary;
	int total;
	@JSONField(name: "currency_code")
	String currencyCode;
	@JSONField(name: "currency_symbol")
	String currencySymbol;
	@JSONField(name: "price_text")
	String priceText;
	@JSONField(name: "discount_text")
	String discountText;
	@JSONField(name: "summary_text")
	String summaryText;
	@JSONField(name: "total_text")
	String totalText;
	dynamic comment;
	String ip;
	OrdersResponseDataRecordUser user;
	List<OrdersResponseDataRecordProduct> products;
	int quantity;
	@JSONField(name: "order_total")
	List<OrdersResponseDataRecordOrderTotal> orderTotal;
	@JSONField(name: "payment_status")
	OrdersResponseDataRecordPaymentStatus paymentStatus;
	@JSONField(name: "order_status")
	OrdersResponseDataRecordOrderStatus orderStatus;
	@JSONField(name: "order_expired_at")
	OrdersResponseDataRecordOrderExpiredAt orderExpiredAt;
	@JSONField(name: "payment_at")
	OrdersResponseDataRecordPaymentAt paymentAt;
	@JSONField(name: "created_at")
	OrdersResponseDataRecordCreatedAt createdAt;
	@JSONField(name: "updated_at")
	OrdersResponseDataRecordUpdatedAt updatedAt;
}

class OrdersResponseDataRecordShippingAt with JsonConvert<OrdersResponseDataRecordShippingAt> {
	String value;
	String date;
	String time;
}

class OrdersResponseDataRecordUser with JsonConvert<OrdersResponseDataRecordUser> {
	int id;
	String username;
	@JSONField(name: "country_code")
	String countryCode;
	String gender;
	@JSONField(name: "first_name")
	String firstName;
	@JSONField(name: "last_name")
	String lastName;
	OrdersResponseDataRecordUserAvatar avatar;
}

class OrdersResponseDataRecordUserAvatar with JsonConvert<OrdersResponseDataRecordUserAvatar> {
	String id;
	String name;
	int width;
	int height;
	String mime;
	int size;
	String url;
	@JSONField(name: "resize_url")
	String resizeUrl;
}

class OrdersResponseDataRecordProduct with JsonConvert<OrdersResponseDataRecordProduct> {
	int id;
	@JSONField(name: "product_id")
	int productId;
	@JSONField(name: "product_variant_id")
	int productVariantId;
	@JSONField(name: "product_code")
	String productCode;
	@JSONField(name: "product_name")
	String productName;
	@JSONField(name: "product_option")
	String productOption;
	int quantity;
	int price;
	@JSONField(name: "sum_price")
	int sumPrice;
	int discount;
	int summary;
	@JSONField(name: "service_fee")
	int serviceFee;
	@JSONField(name: "payment_fee")
	int paymentFee;
	int tax;
	@JSONField(name: "exclude_tax")
	int excludeTax;
	int total;
	@JSONField(name: "price_text")
	String priceText;
	@JSONField(name: "sum_price_text")
	String sumPriceText;
	@JSONField(name: "discount_text")
	String discountText;
	@JSONField(name: "total_text")
	String totalText;
	OrdersResponseDataRecordProductsStatus status;
	OrdersResponseDataRecordProductsImage image;
}

class OrdersResponseDataRecordProductsStatus with JsonConvert<OrdersResponseDataRecordProductsStatus> {
	int id;
	String text;
}

class OrdersResponseDataRecordProductsImage with JsonConvert<OrdersResponseDataRecordProductsImage> {
	String id;
	dynamic tag;
	String name;
	int width;
	int height;
	String mime;
	int size;
	String url;
}

class OrdersResponseDataRecordOrderTotal with JsonConvert<OrdersResponseDataRecordOrderTotal> {
	String code;
	String title;
	int value;
	@JSONField(name: "value_text")
	String valueText;
}

class OrdersResponseDataRecordPaymentStatus with JsonConvert<OrdersResponseDataRecordPaymentStatus> {
	int id;
	String name;
}

class OrdersResponseDataRecordOrderStatus with JsonConvert<OrdersResponseDataRecordOrderStatus> {
	int id;
	String name;
}

class OrdersResponseDataRecordOrderExpiredAt with JsonConvert<OrdersResponseDataRecordOrderExpiredAt> {
	String value;
	String date;
	String time;
}

class OrdersResponseDataRecordPaymentAt with JsonConvert<OrdersResponseDataRecordPaymentAt> {
	String value;
	String date;
	String time;
}

class OrdersResponseDataRecordCreatedAt with JsonConvert<OrdersResponseDataRecordCreatedAt> {
	String value;
	String date;
	String time;
}

class OrdersResponseDataRecordUpdatedAt with JsonConvert<OrdersResponseDataRecordUpdatedAt> {
	String value;
	String date;
	String time;
}

class OrdersResponseBench with JsonConvert<OrdersResponseBench> {
	int second;
	double millisecond;
	String format;
}
