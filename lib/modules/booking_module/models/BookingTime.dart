import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';

class BookingTime with JsonConvert<BookingTime> {
  String textTime;
  bool selected;
  String timeValue;

  setTextTime(String val) {
    textTime = val;
  }

  setSelected(bool val) {
    selected = val;
  }

  setTimeValue(String val) {
    timeValue = val;
  }
}