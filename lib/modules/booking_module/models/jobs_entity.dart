import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/BookingTime.dart';

class JobsEntity with JsonConvert<JobsEntity> {
	JobsData data;
	JobsBench bench;
}

class JobsData with JsonConvert<JobsData> {
	JobsDataPagination pagination;
	List<JobsDataRecord> record;
}

class JobsDataPagination with JsonConvert<JobsDataPagination> {
	@JSONField(name: "current_page")
	int currentPage;
	@JSONField(name: "last_page")
	int lastPage;
	int limit;
	int total;
}

class JobsDataRecord with JsonConvert<JobsDataRecord> {
	int id;
	String date;
	int track;
	JobsDataRecordStore store;
	JobsDataRecordPretty pretty;
	JobsDataRecordVenue venue;
	JobsDataRecordBooking booking;
	JobsDataRecordStatus status;
	List<BookingTime> bookingTime;
	bool canBooking = false;
}

class JobsDataRecordStore with JsonConvert<JobsDataRecordStore> {
	int id;
}

class JobsDataRecordPretty with JsonConvert<JobsDataRecordPretty> {
	int id;
	String code;
	JobsDataRecordPrettyGroup group;
	@JSONField(name: "first_name")
	String firstName;
	@JSONField(name: "last_name")
	String lastName;
	@JSONField(name: "nick_name")
	String nickName;
	String gender;
	bool status;
	JobsDataRecordPrettyAvatar avatar;
}

class JobsDataRecordPrettyGroup with JsonConvert<JobsDataRecordPrettyGroup> {
	int id;
	String name;
}

class JobsDataRecordPrettyAvatar with JsonConvert<JobsDataRecordPrettyAvatar> {
	String id;
	dynamic tag;
	String name;
	int width;
	int height;
	String mime;
	int size;
	String url;
	int position;
}

class JobsDataRecordVenue with JsonConvert<JobsDataRecordVenue> {
	int id;
	double lat;
	double long;
	String name;
	String address;
}

class JobsDataRecordBooking with JsonConvert<JobsDataRecordBooking> {
	int id;
	@JSONField(name: "order_id")
	int orderId;
	@JSONField(name: "store_id")
	int storeId;
	@JSONField(name: "pretty_id")
	int prettyId;
	@JSONField(name: "pretty_code")
	String prettyCode;
	@JSONField(name: "pretty_group_name")
	String prettyGroupName;
	@JSONField(name: "pretty_name")
	String prettyName;
	@JSONField(name: "pretty_nick_name")
	String prettyNickName;
	@JSONField(name: "venue_id")
	int venueId;
	@JSONField(name: "venue_name")
	String venueName;
	double lat;
	double long;
	int price;
	int discount;
	int total;
	@JSONField(name: "job_start")
	String jobStart;
	@JSONField(name: "job_end")
	String jobEnd;
	@JSONField(name: "created_at")
	String createdAt;
	@JSONField(name: "updated_at")
	String updatedAt;
}

class JobsDataRecordStatus with JsonConvert<JobsDataRecordStatus> {
	int id;
	String text;
	@JSONField(name: "job_text")
	String jobText;
	bool isAvailable(){
		return id==0||id==3;
	}
}

class JobsBench with JsonConvert<JobsBench> {
	int second;
	double millisecond;
	String format;
}
