import 'package:pretty_client_mobile/env_config.dart';
import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class OrderDetailResponseEntity with JsonConvert<OrderDetailResponseEntity> {
	OrderDetailResponseData data;
	OrderDetailResponseBench bench;
}

class OrderDetailResponseData with JsonConvert<OrderDetailResponseData> {
	int id;
	@JSONField(name: "order_no")
	String orderNo;
	@JSONField(name: "store_id")
	int storeId;
	@JSONField(name: "store_name")
	String storeName;
	@JSONField(name: "user_id")
	int userId;
	String firstname;
	String lastname;
	dynamic email;
	String telephone;
	@JSONField(name: "payment_provider")
	String paymentProvider;
	@JSONField(name: "payment_method")
	String paymentMethod;
	@JSONField(name: "payment_method_text")
	String paymentMethodText;
	@JSONField(name: "payment_ref")
	String paymentRef;
	@JSONField(name: "payment_ref_no1")
	dynamic paymentRefNo1;
	@JSONField(name: "payment_ref_no2")
	dynamic paymentRefNo2;
	@JSONField(name: "payment_message")
	String paymentMessage;
	@JSONField(name: "shipping_provider")
	String shippingProvider;
	@JSONField(name: "shipping_method")
	String shippingMethod;
	@JSONField(name: "shipping_address")
	String shippingAddress;
	@JSONField(name: "shipping_lat")
	double shippingLat;
	@JSONField(name: "shipping_long")
	double shippingLong;
	@JSONField(name: "shipping_at")
	OrderDetailResponseDataShippingAt shippingAt;
	int price;
	int discount;
	int summary;
	int total;
	@JSONField(name: "currency_code")
	String currencyCode;
	@JSONField(name: "currency_symbol")
	String currencySymbol;
	@JSONField(name: "price_text")
	String priceText;
	@JSONField(name: "discount_text")
	String discountText;
	@JSONField(name: "summary_text")
	String summaryText;
	@JSONField(name: "total_text")
	String totalText;
	dynamic comment;
	String ip;
	OrderDetailResponseDataUser user;
	List<OrderDetailResponseDataProduct> products;
	int quantity;
	@JSONField(name: "order_total")
	List<OrderDetailResponseDataOrderTotal> orderTotal;
	@JSONField(name: "payment_status")
	OrderDetailResponseDataPaymentStatus paymentStatus;
	@JSONField(name: "order_status")
	OrderDetailResponseDataOrderStatus orderStatus;
	@JSONField(name: "order_expired_at")
	OrderDetailResponseDataOrderExpiredAt orderExpiredAt;
	@JSONField(name: "payment_at")
	OrderDetailResponseDataPaymentAt paymentAt;
	@JSONField(name: "created_at")
	OrderDetailResponseDataCreatedAt createdAt;
	@JSONField(name: "updated_at")
	OrderDetailResponseDataUpdatedAt updatedAt;
}

class OrderDetailResponseDataShippingAt with JsonConvert<OrderDetailResponseDataShippingAt> {
	String value;
	String date;
	String time;
}

class OrderDetailResponseDataUser with JsonConvert<OrderDetailResponseDataUser> {
	int id;
	String username;
	@JSONField(name: "country_code")
	String countryCode;
	String gender;
	@JSONField(name: "first_name")
	String firstName;
	@JSONField(name: "last_name")
	String lastName;
	OrderDetailResponseDataUserAvatar avatar;
}

class OrderDetailResponseDataUserAvatar with JsonConvert<OrderDetailResponseDataUserAvatar> {
	String id;
	String name;
	int width;
	int height;
	String mime;
	int size;
	String url;
	@JSONField(name: "resize_url")
	String resizeUrl;
}

class OrderDetailResponseDataProduct with JsonConvert<OrderDetailResponseDataProduct> {
	int id;
	@JSONField(name: "product_id")
	int productId;
	@JSONField(name: "product_variant_id")
	int productVariantId;
	@JSONField(name: "product_code")
	String productCode;
	@JSONField(name: "product_name")
	String productName;
	@JSONField(name: "product_option")
	String productOption;
	int quantity;
	int price;
	@JSONField(name: "sum_price")
	int sumPrice;
	int discount;
	int summary;
	@JSONField(name: "service_fee")
	int serviceFee;
	@JSONField(name: "payment_fee")
	int paymentFee;
	int tax;
	@JSONField(name: "exclude_tax")
	int excludeTax;
	int total;
	@JSONField(name: "price_text")
	String priceText;
	@JSONField(name: "sum_price_text")
	String sumPriceText;
	@JSONField(name: "discount_text")
	String discountText;
	@JSONField(name: "total_text")
	String totalText;
	OrderDetailResponseDataProductsStatus status;
	OrderDetailResponseDataProductsImage image;
}

class OrderDetailResponseDataProductsStatus with JsonConvert<OrderDetailResponseDataProductsStatus> {
	int id;
	String text;
}

class OrderDetailResponseDataProductsImage with JsonConvert<OrderDetailResponseDataProductsImage> {
	String id;
	dynamic tag;
	String name;
	int width;
	int height;
	String mime;
	int size;
	String url;

	String getImgUrlResize(double w, double h) {
		return "${EnvConfig.instance.resUrl}/w_${w.round()},h_${h.round()},c_crop/$id/$name";
	}
}

class OrderDetailResponseDataOrderTotal with JsonConvert<OrderDetailResponseDataOrderTotal> {
	String code;
	String title;
	int value;
	@JSONField(name: "value_text")
	String valueText;
}

class OrderDetailResponseDataPaymentStatus with JsonConvert<OrderDetailResponseDataPaymentStatus> {
	int id;
	String name;
}

class OrderDetailResponseDataOrderStatus with JsonConvert<OrderDetailResponseDataOrderStatus> {
	int id;
	String name;
}

class OrderDetailResponseDataOrderExpiredAt with JsonConvert<OrderDetailResponseDataOrderExpiredAt> {
	String value;
	String date;
	String time;
}

class OrderDetailResponseDataPaymentAt with JsonConvert<OrderDetailResponseDataPaymentAt> {
	String value;
	String date;
	String time;
}

class OrderDetailResponseDataCreatedAt with JsonConvert<OrderDetailResponseDataCreatedAt> {
	String value;
	String date;
	String time;
}

class OrderDetailResponseDataUpdatedAt with JsonConvert<OrderDetailResponseDataUpdatedAt> {
	String value;
	String date;
	String time;
}

class OrderDetailResponseBench with JsonConvert<OrderDetailResponseBench> {
	int second;
	double millisecond;
	String format;
}
