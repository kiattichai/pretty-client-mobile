import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class CheckoutRequestEntity with JsonConvert<CheckoutRequestEntity> {
	@JSONField(name: "store_id")
	int storeId;
	List<CheckoutRequestProduct> products;
	CheckoutRequestShipping shipping;
}

class CheckoutRequestProduct with JsonConvert<CheckoutRequestProduct> {
	int id;
	@JSONField(name: "job_id")
	int jobId;
	@JSONField(name: "variant_id")
	int variantId;
	int quantity;
}

class CheckoutRequestShipping with JsonConvert<CheckoutRequestShipping> {
	String address;
	double lat;
	double long;
	String time;
}
