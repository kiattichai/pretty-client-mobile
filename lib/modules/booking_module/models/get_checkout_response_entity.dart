import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class GetCheckoutResponseEntity with JsonConvert<GetCheckoutResponseEntity> {
	GetCheckoutResponseData data;
	GetCheckoutResponseBench bench;
}

class GetCheckoutResponseData with JsonConvert<GetCheckoutResponseData> {
	@JSONField(name: "total_cost")
	GetCheckoutResponseDataTotalCost totalCost;
	GetCheckoutResponseDataShipping shipping;
	@JSONField(name: "expired_time")
	int expiredTime;
	@JSONField(name: "expired_at")
	int expiredAt;
	List<GetCheckoutResponseDataRecord> record;
}

class GetCheckoutResponseDataTotalCost with JsonConvert<GetCheckoutResponseDataTotalCost> {
	int quantity;
	double price;
	double discount;
	double vat;
	double fee;
	@JSONField(name: "service_fee")
	int serviceFee;
	@JSONField(name: "payment_fee")
	double paymentFee;
	double tax;
	@JSONField(name: "organizer_fee")
	double organizerFee;
	@JSONField(name: "organizer_service_fee")
	double organizerServiceFee;
	@JSONField(name: "organizer_payment_fee")
	double organizerPaymentFee;
	@JSONField(name: "organizer_tax")
	double organizerTax;
	@JSONField(name: "exclude_tax")
	double excludeTax;
	double total;
	double income;
	@JSONField(name: "currency_code")
	String currencyCode;
	@JSONField(name: "currency_symbol")
	String currencySymbol;
	@JSONField(name: "vat_text")
	String vatText;
	@JSONField(name: "quantity_text")
	String quantityText;
	@JSONField(name: "price_text")
	String priceText;
	@JSONField(name: "discount_text")
	String discountText;
	@JSONField(name: "fee_text")
	String feeText;
	@JSONField(name: "service_fee_text")
	String serviceFeeText;
	@JSONField(name: "payment_fee_text")
	String paymentFeeText;
	@JSONField(name: "tax_text")
	String taxText;
	@JSONField(name: "organizer_fee_text")
	String organizerFeeText;
	@JSONField(name: "organizer_service_fee_text")
	String organizerServiceFeeText;
	@JSONField(name: "organizer_payment_fee_text")
	String organizerPaymentFeeText;
	@JSONField(name: "organizer_tax_text")
	String organizerTaxText;
	@JSONField(name: "exclude_tax_text")
	String excludeTaxText;
	@JSONField(name: "total_text")
	String totalText;
	@JSONField(name: "income_text")
	String incomeText;
	@JSONField(name: "server_time")
	String serverTime;
	@JSONField(name: "payment_channel")
	GetCheckoutResponseDataTotalCostPaymentChannel paymentChannel;
}

class GetCheckoutResponseDataTotalCostPaymentChannel with JsonConvert<GetCheckoutResponseDataTotalCostPaymentChannel> {
	@JSONField(name: "FREE")
	GetCheckoutResponseDataTotalCostPaymentChannelFREE fREE;
	@JSONField(name: "WALLET")
	GetCheckoutResponseDataTotalCostPaymentChannelWALLET wALLET;
}

class GetCheckoutResponseDataTotalCostPaymentChannelFREE with JsonConvert<GetCheckoutResponseDataTotalCostPaymentChannelFREE> {
	bool status;
	@JSONField(name: "PRET")
	GetCheckoutResponseDataTotalCostPaymentChannelFREEPRET pRET;
}

class GetCheckoutResponseDataTotalCostPaymentChannelFREEPRET with JsonConvert<GetCheckoutResponseDataTotalCostPaymentChannelFREEPRET> {
	bool status;
	@JSONField(name: "expire_time")
	int expireTime;
	@JSONField(name: "payment_fee")
	bool paymentFee;
}

class GetCheckoutResponseDataTotalCostPaymentChannelWALLET with JsonConvert<GetCheckoutResponseDataTotalCostPaymentChannelWALLET> {
	bool status;
	@JSONField(name: "PRET")
	GetCheckoutResponseDataTotalCostPaymentChannelWALLETPRET pRET;
	@JSONField(name: "WIZDOM")
	GetCheckoutResponseDataTotalCostPaymentChannelWALLETWIZDOM wIZDOM;
}

class GetCheckoutResponseDataTotalCostPaymentChannelWALLETPRET with JsonConvert<GetCheckoutResponseDataTotalCostPaymentChannelWALLETPRET> {
	bool status;
	@JSONField(name: "expire_time")
	int expireTime;
	@JSONField(name: "payment_fee")
	bool paymentFee;
}

class GetCheckoutResponseDataTotalCostPaymentChannelWALLETWIZDOM with JsonConvert<GetCheckoutResponseDataTotalCostPaymentChannelWALLETWIZDOM> {
	bool status;
	@JSONField(name: "expire_time")
	int expireTime;
	@JSONField(name: "payment_fee")
	bool paymentFee;
}

class GetCheckoutResponseDataShipping with JsonConvert<GetCheckoutResponseDataShipping> {
	String address;
	double lat;
	double long;
	String time;
}

class GetCheckoutResponseDataRecord with JsonConvert<GetCheckoutResponseDataRecord> {
	int id;
	String code;
	int position;
	bool option;
	GetCheckoutResponseDataRecordPrice price;
	String name;
	dynamic description;
	GetCheckoutResponseDataRecordStore store;
	List<GetCheckoutResponseDataRecordCategory> categories;
	List<GetCheckoutResponseDataRecordVariant> variants;
	List<GetCheckoutResponseDataRecordAttribute> attributes;
	List<GetCheckoutResponseDataRecordImage> images;
	bool status;
	@JSONField(name: "publish_status")
	bool publishStatus;
	@JSONField(name: "created_at")
	String createdAt;
	@JSONField(name: "updated_at")
	String updatedAt;
}

class GetCheckoutResponseDataRecordPrice with JsonConvert<GetCheckoutResponseDataRecordPrice> {
	int min;
	int max;
	@JSONField(name: "min_text")
	String minText;
	@JSONField(name: "max_text")
	String maxText;
}

class GetCheckoutResponseDataRecordStore with JsonConvert<GetCheckoutResponseDataRecordStore> {
	int id;
	String name;
	dynamic image;
}

class GetCheckoutResponseDataRecordCategory with JsonConvert<GetCheckoutResponseDataRecordCategory> {
	int id;
	int position;
	String name;
	String description;
}

class GetCheckoutResponseDataRecordVariant with JsonConvert<GetCheckoutResponseDataRecordVariant> {
	int id;
	String sku;
	String name;
	String description;
	@JSONField(name: "cost_price")
	int costPrice;
	@JSONField(name: "cost_price_text")
	String costPriceText;
	double price;
	@JSONField(name: "price_text")
	String priceText;
	@JSONField(name: "compare_at_price")
	int compareAtPrice;
	@JSONField(name: "compare_at_price_text")
	String compareAtPriceText;
	int quantity;
	int hold;
	int position;
	int weight;
	bool status;
	List<GetCheckoutResponseDataRecordVariantsOption> options;
	GetCheckoutResponseDataRecordVariantsImage image;
	double discount;
	double total;
	int fee;
	@JSONField(name: "service_fee")

	double serviceFee;
	@JSONField(name: "payment_fee")
	double paymentFee;
	double tax;
	@JSONField(name: "organizer_fee")
	double organizerFee;
	@JSONField(name: "organizer_service_fee")
	double organizerServiceFee;
	@JSONField(name: "organizer_payment_fee")
	double organizerPaymentFee;
	@JSONField(name: "organizer_tax")
	double organizerTax;
	double income;

	String textOption(){
		String result = '';
		for (int i = 0; i < options.length; i++) {
			if(options[i].name != null){
				result = options[i].name+ ": " + options[i].value.name;
				if(i < options.length-1){
					result +=', ';
				}
			}

		}
		return result;
	}
}

class GetCheckoutResponseDataRecordVariantsOption with JsonConvert<GetCheckoutResponseDataRecordVariantsOption> {
	GetCheckoutResponseDataRecordVariantsOptionsValue value;
	int id;
	String code;
	String name;
}

class GetCheckoutResponseDataRecordVariantsOptionsValue with JsonConvert<GetCheckoutResponseDataRecordVariantsOptionsValue> {
	int id;
	String code;
	String name;
}

class GetCheckoutResponseDataRecordVariantsImage with JsonConvert<GetCheckoutResponseDataRecordVariantsImage> {
	String id;
	@JSONField(name: "store_id")
	int storeId;
	@JSONField(name: "album_id")
	int albumId;
	String name;
	int width;
	int height;
	String mime;
	int size;
	String url;
}

class GetCheckoutResponseDataRecordAttribute with JsonConvert<GetCheckoutResponseDataRecordAttribute> {
	int id;
	String code;
	String name;
	GetCheckoutResponseDataRecordAttributesType type;
	bool require;
	GetCheckoutResponseDataRecordAttributesMetafield metafield;
	GetCheckoutResponseDataRecordAttributesValue value;
	@JSONField(name: "value_text")
	String valueText;
}

class GetCheckoutResponseDataRecordAttributesType with JsonConvert<GetCheckoutResponseDataRecordAttributesType> {
	int id;
	String name;
}

class GetCheckoutResponseDataRecordAttributesMetafield with JsonConvert<GetCheckoutResponseDataRecordAttributesMetafield> {

}

class GetCheckoutResponseDataRecordAttributesValue with JsonConvert<GetCheckoutResponseDataRecordAttributesValue> {

}

class GetCheckoutResponseDataRecordImage with JsonConvert<GetCheckoutResponseDataRecordImage> {
	String id;
	@JSONField(name: "store_id")
	int storeId;
	dynamic tag;
	@JSONField(name: "album_id")
	int albumId;
	String name;
	int width;
	int height;
	String mime;
	int size;
	String url;
	int position;
}

class GetCheckoutResponseBench with JsonConvert<GetCheckoutResponseBench> {
	int second;
	double millisecond;
	String format;
}
