import 'package:inject/inject.dart';
import 'package:intl/intl.dart';
import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/data/jobs_api.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/checkout_request_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/checkout_response_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/get_checkout_response_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/jobs_entity.dart';

import 'package:pretty_client_mobile/modules/booking_module/models/order_request_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/payment_request_entity.dart';

class BookingRepository extends BaseRepository {
  final JobsApi jobsApi;
  List<JobsDataRecord> jobsDataRecord = [];

  @provide
  BookingRepository(this.jobsApi);

  Future<List<JobsDataRecord>> getJobs(int id) async {
    DateTime now = DateTime.now();
    String dateFormat = DateFormat('yyyy-MM-dd').format(now);
    String dateFormat2 = DateFormat('yyyy-MM-dd').format(now.add(Duration(days: 30)));
    String date = "${dateFormat}|${dateFormat2}";
    final response = await jobsApi.getJobs(id, date, BaseErrorEntity.badRequestToModelError);

    if (response.data != null) {
      jobsDataRecord.clear();
      jobsDataRecord.addAll(response.data.record);
      return jobsDataRecord;
    } else {
      return null;
    }
  }

  Future<CheckoutResponseEntity> createCheckout(CheckoutRequestEntity requestEntity) async {
    final response = await jobsApi.createCheckout(requestEntity, BaseErrorEntity.badRequestToModelError);
    return response;
  }

  Future<GetCheckoutResponseEntity> getCheckout(String request) async {
    final response = await jobsApi.getCheckout(request, BaseErrorEntity.badRequestToModelError);
    return response;
  }

  Future<BaseResponseEntity> placeOrder(OrderRequestEntity order) async {
    final response = await jobsApi.placeOrder(order, BaseErrorEntity.badRequestToModelError);
    return response;
  }

  Future<BaseResponseEntity> payment(PaymentRequestEntity payment) async {
    final response = await jobsApi.payment(payment, BaseErrorEntity.badRequestToModelError);
    return response;
  }

}