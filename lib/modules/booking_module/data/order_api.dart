import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/order_detail_response_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/orders_response_entity.dart';

class OrderApi {
  final CoreApi coreApi;
  final String path = '/users/orders';
  @provide
  @singleton
  OrderApi(this.coreApi);

  Future<OrderDetailResponseEntity> getOrderDetail(int id, Function badRequestToModelError) async {
    final response = await coreApi.get("$path/$id", null, badRequestToModelError, hasPermission: true);
    return OrderDetailResponseEntity().fromJson(response.data);
  }

  Future<OrdersResponseEntity> ordersList(int limit, int page, Function badRequestToModelError) async {
    Map<String, String> query = {
      "limit": limit.toString(),
      "page": page.toString(),
      "order": "created_at",
      "sort": "desc"
    };
    final response = await coreApi.get(path, query, badRequestToModelError, hasPermission: true);
    return OrdersResponseEntity().fromJson(response.data);
  }

  Future<OrdersResponseEntity> ordersListByStutus(int limit, int page, String orderStatus, Function badRequestToModelError) async {
    Map<String, String> query = {
      "limit": limit.toString(),
      "page": page.toString(),
      "order_statuses": orderStatus,
      "order": "updated_at",
      "sort": "desc"
    };
    final response = await coreApi.get(path, query, badRequestToModelError, hasPermission: true);
    return OrdersResponseEntity().fromJson(response.data);
  }

  Future<OrdersResponseEntity> ordersListSuccess(int limit, int page, String jobStatus, Function badRequestToModelError) async {
    Map<String, String> query = {
      "limit": limit.toString(),
      "page": page.toString(),
      "job_status": jobStatus,
      "order": "updated_at",
      "sort": "desc"
    };
    final response = await coreApi.get(path, query, badRequestToModelError, hasPermission: true);
    return OrdersResponseEntity().fromJson(response.data);
  }

  Future<BaseResponseEntity> cancelOrder(int id, Function badRequestToModelError) async {
    Map<String, String> query = {
      "order_status": "cancel",
    };
    final response = await coreApi.put("$path/$id", query, badRequestToModelError, hasPermission: true);
    return BaseResponseEntity().fromJson(response.data);
  }

}