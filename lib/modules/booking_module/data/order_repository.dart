import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/data/order_api.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/order_detail_response_entity.dart';

class OrderRepository extends BaseRepository {
  final OrderApi orderApi;

  @provide
  OrderRepository(this.orderApi);

  Future<OrderDetailResponseData> getOrderDetail(int id) async {
    final response = await orderApi.getOrderDetail(id, BaseErrorEntity.badRequestToModelError);
    if (response.data != null) {
      return response.data;
    } else {
      return null;
    }
  }

  Future<BaseResponseEntity> cancelOrder(int id) async {
    final response = await orderApi.cancelOrder(id, BaseErrorEntity.badRequestToModelError);
    if (response.data != null) {
      return response;
    } else {
      return null;
    }
  }

}