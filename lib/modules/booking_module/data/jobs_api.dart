import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/checkout_request_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/checkout_response_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/get_checkout_response_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/jobs_entity.dart';

import 'package:pretty_client_mobile/modules/booking_module/models/order_request_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/payment_request_entity.dart';

class JobsApi {
  final CoreApi coreApi;
  final String path = '/jobs';
  final String pathCheckout = '/users/checkout';
  final String pathPlaceOrder = '/users/orders';
  final String pathPayment = '/users/payments';
  final String pathProductsCheck = '/products/check';


  @provide
  @singleton
  JobsApi(this.coreApi);

  Future<JobsEntity> _getJobs(
      int id, String dateRage, Function badRequestToModelError) async {
    Map<String, String> query = {
      "pretty_id": id.toString(),
      "date_range": dateRage,
      "order": "date",
      "sort": "asc"
    };
    final response = await coreApi.get(path, query, badRequestToModelError);
    return JobsEntity().fromJson(response.data);
  }

  Future<JobsEntity> getJobs(
      int id, String dateRage, Function badRequestToModelError) async {
    Map<String, String> query = {
      "product_id": id.toString(),
      "store_id": "1",
      "date_range": dateRage,
      "order": "date",
      "sort": "asc"
    };
    final response = await coreApi.get(pathProductsCheck, query, badRequestToModelError);
    return JobsEntity().fromJson(response.data);
  }

  Future<CheckoutResponseEntity> createCheckout(CheckoutRequestEntity request, Function badRequestToModelError) async {
    final response = await coreApi.post(pathCheckout, request.toJson(), badRequestToModelError, hasPermission: true);
    return CheckoutResponseEntity().fromJson(response.data);
  }

  Future<GetCheckoutResponseEntity> getCheckout(String request, Function badRequestToModelError) async {
    final response = await coreApi.get("$pathCheckout/$request", null, badRequestToModelError, hasPermission: true);
    return GetCheckoutResponseEntity().fromJson(response.data);
  }

  Future<BaseResponseEntity> placeOrder(OrderRequestEntity order, Function badRequestToModelError) async {
    final response = await coreApi.post(pathPlaceOrder, order.toJson(), badRequestToModelError, hasPermission: true);
    return BaseResponseEntity().fromJson(response.data);
  }

  Future<BaseResponseEntity> payment(PaymentRequestEntity payment, Function badRequestToModelError) async {
    final response = await coreApi.post(pathPayment, payment.toJson(), badRequestToModelError, hasPermission: true);
    return BaseResponseEntity().fromJson(response.data);
  }


}
