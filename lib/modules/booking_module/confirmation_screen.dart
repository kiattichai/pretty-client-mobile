import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/env_config.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/order_request_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/order_view_model.dart';
import 'package:pretty_client_mobile/modules/coins_module/coins_transaction_route.dart';
import 'package:pretty_client_mobile/modules/main_module/main_screen_route.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/order_detail_route.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/custom_modal.dart';
import 'package:pretty_client_mobile/widgets/modal_booking_success.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/widgets/status_bar.dart';

class ConfirmationScreen extends StatefulWidget {
  final OrderRequestEntity order;

  const ConfirmationScreen({Key key, this.order}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ConfirmationState();
  }
}

class ConfirmationState
    extends BaseStateProvider<ConfirmationScreen, OrderViewModel> {
  final numberFormat = new NumberFormat("#,###.##", "en_US");

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    viewModel = OrderViewModel();
    viewModel..onErrorOrder = handleError;
  }

  Widget lineBottom() {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: Design.theme.colorLine.withOpacity(0.6),
          ),
        ),
      ),
    );
  }

  Widget lineVertical(double height) {
    return Container(
      padding: EdgeInsets.only(
          left: Screen.convertWidthSize(13),
          right: Screen.convertWidthSize(13),
          top: height,
          bottom: height),
      height: Screen.convertHeightSize(70),
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            right: BorderSide(
              width: 1,
              color: Color(0xff525252),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Screen(context);
    appLocal = AppLocalizations.of(context);
    String jobAt = widget.order.shippingAt;
    String jobDate = jobAt.substring(0, 10);
    String jobTime = jobAt.substring(11);
    DateTime dateTime = DateTime.parse(jobAt);
    jobDate = DateFormat('dd/MM/yyyy').format(dateTime);
    jobTime = DateFormat('HH:mm น.').format(dateTime);

    return Scaffold(
      appBar: CustomHeaderDetail(
        height: Screen.convertHeightSize(55),
        title: appLocal.bookingConfirmation,
        onTap: () {
          RouterService.instance.pop();
        },
      ),
      body: BaseWidget<OrderViewModel>(
        model: viewModel,
        builder: (context, model, child) => ProgressHUD(
          color: Design.theme.backgroundColor,
          progressIndicator: CircleLoading(),
          inAsyncCall: model.loading,
          child: Builder(
            builder: (context) {
              if (widget.order != null) {
                List<String> listDate = [];
                List<String> listTime = [];
//                    listDate.add(viewModel.orderDetail.createdAt.date);
//                    listDate.add(viewModel.orderDetail.paidAt.date);
//                    listTime.add(viewModel.orderDetail.createdAt.time);
//                    listTime.add(viewModel.orderDetail.paidAt.time);
                return Container(
                  child: ListView(
                    padding: EdgeInsets.only(
                      left: Screen.convertWidthSize(15),
                      right: Screen.convertWidthSize(15),
                    ),
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                          top: 30,
                          bottom: 30,
                        ),
                        child: StatusBar(2, 0, listDate, listTime, appLocal),
                      ),
                      // --------------------- Job At Box ----------------------------//
                      Container(
                        padding: EdgeInsets.all(Screen.convertWidthSize(15)),
                        decoration: BoxDecoration(
                          color: Design.theme.colorLine.withOpacity(0.2),
                          borderRadius: BorderRadius.all(
                            Radius.circular(8.0),
                          ),
                        ),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Container(
                                  width: (Screen.width / 2),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        appLocal.appointmentDate,
                                        style: TextStyle(
                                            color: Color(0xff636362),
                                            fontSize: 12),
                                      ),
                                      SizedBox(
                                        height: 2,
                                      ),
                                      Text(
                                        "$jobDate",
                                        style: TextStyle(
                                          color: Color(0xffa4a4a4),
                                          fontSize: 15,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        appLocal.time,
                                        style: TextStyle(
                                            color: Color(0xff636362),
                                            fontSize: 12),
                                      ),
                                      SizedBox(
                                        height: 2,
                                      ),
                                      Text(
                                        "$jobTime",
                                        style: TextStyle(
                                          color: Color(0xffa4a4a4),
                                          fontSize: 15,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: (Screen.width / 2),
                                  alignment: Alignment.centerLeft,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        appLocal.venue,
                                        style: TextStyle(
                                            color: Color(0xff636362), fontSize: 12),
                                      ),
                                      SizedBox(
                                        height: 2,
                                      ),
                                      Text(
                                        widget.order.shippingAddress,
                                        style: TextStyle(
                                          color: Color(0xffa4a4a4),
                                          fontSize: 15,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                widget.order.orderProducts[0].productOption!=""?Container(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        appLocal.theme,
                                        style: TextStyle(
                                            color: Color(0xff636362),
                                            fontSize: 12),
                                      ),
                                      SizedBox(
                                        height: 2,
                                      ),
                                      Text(
                                        widget.order.orderProducts[0].productOption,
                                        style: TextStyle(
                                          color: Color(0xffa4a4a4),
                                          fontSize: 15,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ],
                                  ),
                                ):Container(),
                              ],
                            ),

                          ],
                        ),
                      ),

                      // --------------------- Pretty Box ----------------------------//
                      Container(
                        margin: EdgeInsets.only(top: 25),
                        padding: EdgeInsets.all(Screen.convertWidthSize(15)),
                        decoration: BoxDecoration(
                            color: Design.theme.colorLine.withOpacity(0.2),
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0))),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Row(
//                                  crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(right: 6),
                                  child: Image.network(
                                    getImgUrl(
                                        widget.order.orderProducts[0].imageId,
                                        widget.order.orderProducts[0].imageName,
                                        105,
                                        120),
                                    width: Screen.convertWidthSize(65),
                                    height: Screen.convertHeightSize(70),
                                  ),
                                ),
                                Container(
                                  width: Screen.convertWidthSize(widget.order.total>0?60:100),
//                                      height: Screen.convertHeightSize(70),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        appLocal.editProfileName,
                                        style: TextStyle(
                                            color: Color(0xff636362),
                                            fontSize: 11),
                                      ),
                                      SizedBox(
                                        height: 2,
                                      ),
                                      Text(
                                        widget.order.orderProducts[0]
                                            .productName,
                                        style: TextStyle(
                                          color: Color(0xffa4a4a4),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                lineVertical(14),
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        appLocal.code,
                                        style: TextStyle(
                                            color: Color(0xff636362),
                                            fontSize: 11),
                                      ),
                                      SizedBox(
                                        height: 2,
                                      ),
                                      Text(
                                        widget
                                            .order.orderProducts[0].productCode,
                                        style: TextStyle(
                                          color: Color(0xffa4a4a4),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                widget.order.total>0?
                                lineVertical(14):
                                Container(),
                                widget.order.total>0?
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        appLocal.fee,
                                        style: TextStyle(
                                            color: Color(0xff636362),
                                            fontSize: 11),
                                      ),
                                      SizedBox(
                                        height: 2,
                                      ),
                                      Text(
                                        "${numberFormat.format(widget.order.orderProducts[0].price)} coins",
                                        style: TextStyle(
                                          color: Color(0xffa4a4a4),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ],
                                  ),
                                ):
                                Container(),
                              ],
                            ),
                          ],
                        ),
                      ),

                      // --------------------- Price Box ----------------------------//
                      widget.order.total>0?
                      Container(
                        margin: EdgeInsets.only(top: 25),
                        padding: EdgeInsets.all(Screen.convertWidthSize(15)),
                        decoration: BoxDecoration(
                            color: Design.theme.colorLine.withOpacity(0.2),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width: (Screen.width -
                                          Screen.convertWidthSize(96)) /
                                      2,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Align(
                                        alignment: Alignment.center,
                                        child: Text(
                                          appLocal.totalFee,
                                          style: TextStyle(
                                            color: Color(0xffa4a4a4),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Container(
                                        alignment: Alignment.center,
                                        child: RichText(
                                          overflow: TextOverflow.ellipsis,
                                          text: TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                text:
                                                    "${numberFormat.format(widget.order.total)} ",
                                                style: TextStyle(
                                                  color:
                                                      Design.theme.primaryColor,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w700,
                                                ),
                                              ),
                                              TextSpan(
                                                text: "coins",
                                                style: TextStyle(
                                                  color: Color(0xffa4a4a4),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                lineVertical(4),
                                SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              alignment: Alignment.center,
                                              child: Text(
                                                appLocal.yourCoins,
                                                style: TextStyle(
                                                  color: Color(0xffa4a4a4),
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 15,
                                            ),
                                            Container(
                                                child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Container(
                                                  width:
                                                      Screen.convertWidthSize(
                                                          110),
                                                  child: RichText(
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    text: TextSpan(
                                                      children: <TextSpan>[
                                                        TextSpan(
                                                          text:
                                                              "${numberFormat.format(widget.order.userWallet)} ",
                                                          style: TextStyle(
                                                            color: Color(
                                                                0xffd5d5d5),
                                                            fontSize: 18,
                                                            fontWeight:
                                                                FontWeight.w700,
                                                          ),
                                                        ),
                                                        TextSpan(
                                                          text: "coins",
                                                          style: TextStyle(
                                                            color: Color(
                                                                0xffd5d5d5),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            )),
                                          ],
                                        ),
                                      ),
                                      InkWell(
                                        onTap: (){
                                          RouterService.instance.navigateTo(CoinsTransactionRoute.buildPath, data: 0.00);
                                        },
                                        child: Container(
                                          child: SvgPicture.asset(
                                              "images/add_circle_line.svg"),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ):
                      Container(),


                    ],
                  ),
                );
              } else {
                return Container();
              }
            },
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: Screen.convertHeightSize(80),
        alignment: Alignment.bottomCenter,
        padding: EdgeInsets.only(left: 15, right: 15, top: 15, bottom: 15),
        child: MaterialButton(
            disabledColor: Color(0xff636362),
            disabledTextColor: Color(0xffbfbfbf),
            textColor: Design.theme.colorMainBackground,
            color: Design.theme.primaryColor,
            height: 48,
            minWidth: Screen.width - 30,
            child: Text(
              appLocal.commonConfirm,
              style: TextStyle(
                fontFamily: 'Sarabun',
                fontSize: 16,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
              ),
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(26),
            ),
            onPressed: () {
              if (widget.order.userWallet >= widget.order.total) {
                viewModel.placeOrder(widget.order, onSuccess);
              } else {
                _showAlert(appLocal.errorNotEnoughCoins);
              }
            }),
      ),
    );
  }

  String getImgUrl(String id, String name, double w, double h) {
    String result = "https://admin.prettyhub.me/images/image-empty.png";
    result =
        "${EnvConfig.instance.resUrl}/w_${w.round()},h_${h.round()},c_crop/${id}/${name}";
    return result;
  }

  void onSuccess() {
    viewModel.setLoading(false);
    _successDialog();
  }

  void handleError(BaseError error) {
    viewModel.setLoading(false);
    _showAlert(error.message.toString());
    print(error.toJson());
  }

  void _showAlert(String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomModal(
            type: ModalType.CLOSE,
            desc: message,
            appLocal: appLocal,
            onTapConfirm: (){
              RouterService.instance.pop();
            },
          );
        });
  }

  void _successDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return ModalBookingSuccess(
          openOrderDetail: onBtnPressed,
        );
      },
    );
  }

  void onBtnPressed() {
    RouterService.instance.navigateTo(OrderDetailRoute.buildPath,
        data: {"order_id": viewModel.orderId, "ispop": false});
  }
}
