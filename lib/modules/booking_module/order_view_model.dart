import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/order_detail_response_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/order_request_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/payment_request_entity.dart';

class OrderViewModel extends BaseViewModel {
  OrderDetailResponseData orderDetail;
  int orderId = -1;
  Function onErrorOrder;

  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;

  OrderViewModel() {
    refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  }

  @override
  void postInit() {
    if(orderId != -1){
      getOrderDetail(orderId);
    }
  }

  void setOrderId(int val){
    orderId = val;
  }

  Future<void> getOrderDetail(int id) async {
    setLoading(true);
    catchError(() async {
      orderDetail = await di.orderRepository.getOrderDetail(id);
      setLoading(false);
    });
  }

  void placeOrder(OrderRequestEntity order, Function onSuccess) {
    setLoading(true);
    catchError(() async {
      final orderResponse = await di.bookingRepository.placeOrder(order);
      orderId = orderResponse.data.orderId;
      PaymentRequestEntity payment = wrapPayment(orderResponse, order);
      final responsePayment = await di.bookingRepository.payment(payment);

      print("============== Payment Response ==============");
      print(responsePayment.toJson());
      onSuccess();
    });
  }

  Future<void> cancelOrder() async {
    setLoading(true);
    catchError(() async {
      await di.orderRepository.cancelOrder(orderId);
      await resetData();
      await getOrderDetail(orderId);
      setLoading(false);
    });
  }

  void resetData() {
    orderDetail = null;
  }

  PaymentRequestEntity wrapPayment(BaseResponseEntity responseOrder, OrderRequestEntity order){
    PaymentRequestEntity payment = PaymentRequestEntity();
    payment.orderId = responseOrder.data.orderId;
    payment.paymentMethod = order.total>0?"wallet":"free";
    payment.paymentProvider = "pret";

    return payment;
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    print(error);
    onErrorOrder(error);
  }

}