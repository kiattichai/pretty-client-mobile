import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/booking_module/booking_screen.dart';

class BookingRoute extends BaseRoute {
  static String buildPath = '/booking';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
      builder: (context) => BookingScreen(
        data: data,
      ),
    );
  }
}
