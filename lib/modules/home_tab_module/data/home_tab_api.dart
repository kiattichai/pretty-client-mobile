import 'package:dio/dio.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/home_tab_module/models/banner_entity.dart';

class ContentsApi {
  final CoreApi coreApi;
  final String path = '/contents';
  @provide
  @singleton
  ContentsApi(this.coreApi);

  Future<BannerEntity> getContentsBanner(Function badRequestToModelError) async {
    Map<String, String> query = {
      "group_code": "banner",
      "status": true.toString(),
    };
    Response response = await coreApi.get(path, query, badRequestToModelError, hasPermission: false);
    return BannerEntity().fromJson(response.data);
  }

  // Future<OrdersResponseEntity> ordersList(int limit, int page, Function badRequestToModelError) async {
  //   Map<String, String> query = {
  //     "limit": limit.toString(),
  //     "page": page.toString(),
  //     "order": "created_at",
  //     "sort": "desc"
  //   };
  //   final response = await coreApi.get(path, query, badRequestToModelError, hasPermission: true);
  //   return OrdersResponseEntity().fromJson(response.data);
  // }

  // Future<OrdersResponseEntity> ordersListByStutus(int limit, int page, String orderStatus, Function badRequestToModelError) async {
  //   Map<String, String> query = {
  //     "limit": limit.toString(),
  //     "page": page.toString(),
  //     "order_statuses": orderStatus,
  //     "order": "updated_at",
  //     "sort": "desc"
  //   };
  //   final response = await coreApi.get(path, query, badRequestToModelError, hasPermission: true);
  //   return OrdersResponseEntity().fromJson(response.data);
  // }

  // Future<OrdersResponseEntity> ordersListSuccess(int limit, int page, String jobStatus, Function badRequestToModelError) async {
  //   Map<String, String> query = {
  //     "limit": limit.toString(),
  //     "page": page.toString(),
  //     "job_status": jobStatus,
  //     "order": "updated_at",
  //     "sort": "desc"
  //   };
  //   final response = await coreApi.get(path, query, badRequestToModelError, hasPermission: true);
  //   return OrdersResponseEntity().fromJson(response.data);
  // }

  // Future<BaseResponseEntity> cancelOrder(int id, Function badRequestToModelError) async {
  //   Map<String, String> query = {
  //     "order_status": "cancel",
  //   };
  //   final response = await coreApi.put("$path/$id", query, badRequestToModelError, hasPermission: true);
  //   return BaseResponseEntity().fromJson(response.data);
  // }

}