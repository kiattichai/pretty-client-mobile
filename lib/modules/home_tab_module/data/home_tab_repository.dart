import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/modules/home_tab_module/data/home_tab_api.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/home_tab_module/models/banner_entity.dart';
import 'package:inject/inject.dart';

class HomeTabRepository extends BaseRepository {
  final ContentsApi contentsApi;
  @provide
  HomeTabRepository(this.contentsApi);

  Future<BannerData> getContentsBanner() async {
    final response = await contentsApi.getContentsBanner(BaseErrorEntity.badRequestToModelError);
    return response.data;
  }

  // Future<OrdersResponseEntity> ordersListByStutus(limit, page, orderStatus) async {
  //   final response = await orderApi.ordersListByStutus(limit, page, orderStatus, BaseErrorEntity.badRequestToModelError);
  //   if (response.data != null) {
  //     return response;
  //   } else {
  //     return null;
  //   }
  // }

  // Future<OrdersResponseEntity> ordersListSuccess(limit, page, orderStatus) async {
  //   final response = await orderApi.ordersListSuccess(limit, page, orderStatus, BaseErrorEntity.badRequestToModelError);
  //   if (response.data != null) {
  //     return response;
  //   } else {
  //     return null;
  //   }
  // }
  
}