import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/share_preference.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/product_response_entity.dart';
import 'package:pretty_client_mobile/modules/home_tab_module/models/banner_entity.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/friends_response_entity.dart';
import 'package:pretty_client_mobile/modules/news_module/models/news_response_entity.dart';

class HomeTabViewModel extends BaseViewModel {
  final List<BannerDataRecord> banners = [];
  final List<Product> products = [];
  final List<NewsRecord> newsRecord = [];
  NewsRecord newsDetail;
  int limit = 4;
  int page = 1;
  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;
  ScrollController scrollController;
  HomeTabViewModel() {
    refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  }

  @override
  void postInit() async {
    super.postInit();
    getContentsBanner();
    getFriends();
    getRecommendedEvents();
    await di.favoriteRepository.getFavoriteList();
  }

  void getContentsBanner() {
    setLoading(true);
    catchError(() async {
      final result = await di.homeTabRepository.getContentsBanner();
      banners.clear();
      banners.addAll(result.record);
      setLoading(false);
    });
  }

  void getFriends() {
    setLoading(true);
    catchError(() async {
      final result = await di.productRepository.getProductsPopular();
      products.clear();
      products.addAll(result.record);
      setLoading(false);
    });
  }

  void getRecommendedEvents() {
    setLoading(true);
    catchError(() async {
      final result = await di.newsRepository.newsList(limit, page);
      newsRecord.clear();
      newsRecord.addAll(result.data.record);
      setLoading(false);
    });
  }

  void getEventById(String newsId, Function onSuccess) {
    catchError(() async {
      newsDetail = await di.newsRepository.newsDetail(newsId);
      //newsDetail = result;
      onSuccess();
    });
  }

  SharePrefInterface share() {
    return di.sharePrefInterface;
  }

  bool shouldLogin() {
    return di.sharePrefInterface.shouldLogin();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
  }
}
