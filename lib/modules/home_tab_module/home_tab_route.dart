import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/home_tab_module/home_tab_screen.dart';

class HomeTabRoute extends BaseRoute {
  static String buildPath = '/home_tab';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
      builder: (context) => HomeTabScreen()
    );
  }
}
