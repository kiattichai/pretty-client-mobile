import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/env_config.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_landing_route.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/modules/home_tab_module/home_tab_view_model.dart';
import 'package:pretty_client_mobile/widgets/custome_header.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/widgets/custom_header_center_icon.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/modules/news_module/models/news_response_entity.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_detail_route.dart';
import 'package:pretty_client_mobile/modules/news_module/news_detail_route.dart';
import 'package:pretty_client_mobile/modules/main_module/main_screen_route.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_landing_route.dart';
import 'package:pretty_client_mobile/modules/login_module/login_route.dart';
import 'package:pretty_client_mobile/modules/news_module/news_list_route.dart';
import 'package:pretty_client_mobile/modules/main_module/main_view_model.dart';
import 'package:provider/provider.dart';

enum SectionType { friend, news, unknown }

class HomeTabScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeTabScreenState();
  }
}

class HomeTabScreenState extends BaseStateProvider<HomeTabScreen, HomeTabViewModel> {
  MainViewModel mainViewModel;
  @override
  void initState() {
    super.initState();
    viewModel = HomeTabViewModel();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget buildPhotoSlider(BuildContext context, HomeTabViewModel viewModel) {
    print(Screen.width);
    print((Screen.width / 1.785).roundToDouble());
    return CarouselSlider.builder(
      height: (Screen.width / 1.785).roundToDouble(),
      enableInfiniteScroll: viewModel.banners.length > 1,
      viewportFraction: 1.0,
      autoPlay: true,
      onPageChanged: (index) {
        //viewModel.updatePosition(index + 1);
      },
      itemCount: viewModel.banners.length,
      itemBuilder: (BuildContext context, int itemIndex) {
        var banner = viewModel.banners[itemIndex];
        return InkWell(
          child: Container(
            width: Screen.width,
            height: (Screen.width / 1.785).roundToDouble(),
            child: Hero(
              child: CachedNetworkImage(
                fit: BoxFit.cover,
                filterQuality: FilterQuality.high,
                imageUrl: viewModel.banners[itemIndex].images.first.url,
              ),
              tag: banner.id,
            ),
          ),
          onTap: () async {
            var contents = banner.fields.where((item) => item.lang == "content");
            var dataIds = banner.fields.where((item) => item.name == "data_id");
            if (contents.length > 0) {
              viewModel.getEventById(dataIds.first.lang, () {
                RouterService.instance.navigateTo(NewsDetailRoute.buildPath,
                    data: {'news': viewModel.newsDetail, 'index': banner.id});
              });
            }

            //var news = viewModel.newsRecord.where((item) => item.id == 270);
            //print(news.first);
            // RouterService.instance.navigateTo(NewsDetailRoute.buildPath,
            //     data: {'news': news.first, 'index': 0}
            // );
          },
        );
      },
    );
  }

  Widget customScrollView(BuildContext context, HomeTabViewModel viewModel) {
    return ListView(
      scrollDirection: Axis.vertical,
      children: <Widget>[
        buildPhotoSlider(context, viewModel),
        viewModel.products.length != 0 
        ? highlightSection(context, viewModel)
        : Container(),
        headerSectionTitle(context, 'news'),
        recommendedEvents(context, viewModel),
      ],
    );
  }

  Widget highlightSection(BuildContext context, HomeTabViewModel viewModel) {
    return Column(
      children: <Widget>[
        headerSectionTitle(context, 'friend'),
        popularSection(context, viewModel)
      ]
    );
  }

  Widget headerSectionTitle(BuildContext context, String title) {
    return Container(
        padding: EdgeInsets.only(left: 8.0, right: 8.0),
        width: Screen.width,
        height: 56.0,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                  flex: 0,
                  child: Container(
                    width: (Screen.width / 2) - 8.0,
                    child: Text(title == 'friend' ? 'Popular' : 'Recommended Events',
                        style: TextStyle(
                          fontFamily: 'Sarabun',
                          color: Color(0xffd5d5d5),
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.normal,
                          letterSpacing: -0.225,
                        )),
                  )),
              Expanded(
                  flex: 0,
                  child: InkWell(
                    child: Container(
                        width: (Screen.width / 2) - 8.0,
                        child: Text("View all",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontFamily: 'Sarabun',
                              color: Color(0xffa4a4a4),
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              letterSpacing: -0.175,
                            ))),
                    onTap: () async {
                      if (title == 'friend') {
                        if (viewModel.shouldLogin()) {
                          final result =
                              await RouterService.instance.navigateTo(LoginRoute.buildPath);
                          if (result == true) {}
                        } else {
                          mainViewModel.goToMemberAuthen();
                        }
                      } else {
                        RouterService.instance.navigateTo(NewsListRoute.buildPath);
                      }
                    },
                  ))
            ]));
  }

  Widget popularSection(BuildContext context, HomeTabViewModel viewModel) {
    return Container(
        margin: EdgeInsets.only(left: 8.0),
        height: ((Screen.width / 3.5) / 0.724).roundToDouble() + 22,
        child: ListView.builder(
            controller: viewModel.scrollController,
            scrollDirection: Axis.horizontal,
            itemCount: viewModel.products.length,
            itemBuilder: (context, i) {
              var product = viewModel.products[i];
              return InkWell(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(right: 8.0),
                      width: (Screen.width / 3.5).roundToDouble(),
                      height: ((Screen.width / 3.5) / 0.724).roundToDouble(),
                      child: ClipRRect(
                          borderRadius: new BorderRadius.circular(0.0),
                          child: Hero(
                            child: CachedNetworkImage(
                              imageUrl: product.images.first.url ?? '',
                              placeholder: (context, _) {
                                return Container();
                              },
                            ),
                            tag: 'photo${product.id}',
                          )),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4.0, right: 8.0),
                      width: 100.0,
                      child: Text(product.name,
                          style: TextStyle(
                            fontFamily: 'Sarabun',
                            color: Color(0xffa4a4a4),
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          )),
                    ),
                  ],
                ),
                onTap: () {
                  print('friend : ${product}');

                  RouterService.instance.navigateTo(FriendsDetailRoute.buildPath, data: product);
                },
              );
            }));
  }

  Widget recommendedEvents(BuildContext context, HomeTabViewModel viewModel) {
    return Container(
        margin: EdgeInsets.only(left: 8.0),
        height: ((Screen.width / 1.422) / 1.919).roundToDouble() + 115,
        child: ListView.builder(
            controller: viewModel.scrollController,
            scrollDirection: Axis.horizontal,
            itemCount: viewModel.newsRecord.length,
            itemBuilder: (context, i) {
              var news = viewModel.newsRecord[i];
              return InkWell(
                child: Column(children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(right: 8.0),
                    width: (Screen.width / 1.422).roundToDouble(),
                    height: ((Screen.width / 1.422) / 1.919).roundToDouble(),
                    child: ClipRRect(
                        borderRadius: new BorderRadius.circular(0.0),
                        child: Hero(
                          child: CachedNetworkImage(
                            fit: BoxFit.cover,
                            imageUrl:
                                getImgUrl(news.images, 263.0, Screen.convertHeightSize(137.0)),
                            placeholder: (context, _) {
                              return Container();
                            },
                          ),
                          tag: 'news${i}',
                        )),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 4.0, right: 8.0),
                    width: 263.0,
                    child: Text(getFieldContent(news, 'content_date'),
                        style: TextStyle(
                          fontFamily: 'Sarabun',
                          color: Color(0xffe7c260),
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        )),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 4.0, right: 8.0),
                    width: 263.0,
                    child: Text(getFieldContent(news, 'title'),
                        style: TextStyle(
                          fontFamily: 'Sarabun',
                          color: Color(0xffffffff),
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0,
                        )),
                  ),
                ]),
                onTap: () {
                  RouterService.instance
                      .navigateTo(NewsDetailRoute.buildPath, data: {'news': news, 'index': i});
                },
              );
            }));
  }

  @override
  Widget build(BuildContext context) {
    mainViewModel = Provider.of(context);
    appLocal = AppLocalizations.of(context);
    return BaseWidget(
      model: viewModel,
      builder: (context, model, child) {
        return ProgressHUD(
          color: Colors.transparent,
          progressIndicator: CircleLoading(),
          inAsyncCall: viewModel.loading,
          child: Scaffold(
            appBar: CustomHeaderCenterIcon(
                height: Screen.convertHeightSize(55), title: appLocal.friendHeader),
            body: RefreshIndicator(
              color: Design.theme.colorMainBackground,
              backgroundColor: Design.theme.colorPrimary,
              key: viewModel.refreshIndicatorKey,
              onRefresh: () {
                viewModel.getContentsBanner();
                viewModel.getFriends();
                viewModel.getRecommendedEvents();
                return Future.value();
              },
              child: customScrollView(context, model),
            ),
          ),
        );
      },
    );
  }

  String getFieldContent(NewsRecord newsRecord, String field) {
    String result = "";
    for (var i = 0; i < newsRecord.fields.length; i++) {
      if (newsRecord.fields[i].name == field) {
        result = newsRecord.fields[i].lang;
        break;
      }
    }
    return result;
  }

  String getImgUrl(List<NewsImageEntity> img, double w, double h) {
    String result = "https://admin.prettyhub.me/images/image-empty.png";
    if (img.length > 0) {
      result =
          "${EnvConfig.instance.resUrl}/w_${w.round()},h_${h.round()},c_crop/${img[0].id}/${img[0].name}";
    }
    return result;
  }
}
