import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class BannerEntity with JsonConvert<BannerEntity> {
	BannerData data;
	BannerBench bench;
}

class BannerData with JsonConvert<BannerData> {
	BannerDataPagination pagination;
	List<BannerDataRecord> record;
	bool cache;
}

class BannerDataPagination with JsonConvert<BannerDataPagination> {
	@JSONField(name: "current_page")
	int currentPage;
	@JSONField(name: "last_page")
	int lastPage;
	int limit;
	int total;
}

class BannerDataRecord with JsonConvert<BannerDataRecord> {
	int id;
	dynamic slug;
	BannerDataRecordGroup group;
	BannerDataRecordCategory category;
	List<BannerDataRecordField> fields;
	List<BannerDataRecordImage> images;
	int viewed;
	bool status;
	@JSONField(name: "share_url")
	String shareUrl;
	@JSONField(name: "webview_url")
	String webviewUrl;
	@JSONField(name: "published_at")
	String publishedAt;
	@JSONField(name: "created_at")
	String createdAt;
	@JSONField(name: "updated_at")
	String updatedAt;
}

class BannerDataRecordGroup with JsonConvert<BannerDataRecordGroup> {
	int id;
	String code;
	String name;
	String description;
}

class BannerDataRecordCategory with JsonConvert<BannerDataRecordCategory> {
	BannerDataRecordCategoryCurrent current;
	List<BannerDataRecordCategoryItem> items;
}

class BannerDataRecordCategoryCurrent with JsonConvert<BannerDataRecordCategoryCurrent> {
	int id;
	String code;
	bool status;
	int position;
	String name;
	dynamic description;
	@JSONField(name: "meta_title")
	dynamic metaTitle;
	@JSONField(name: "meta_description")
	dynamic metaDescription;
	@JSONField(name: "meta_keyword")
	dynamic metaKeyword;
}

class BannerDataRecordCategoryItem with JsonConvert<BannerDataRecordCategoryItem> {
	int id;
	String code;
	bool status;
	int position;
	String name;
}

class BannerDataRecordField with JsonConvert<BannerDataRecordField> {
	String label;
	String name;
	BannerDataRecordFieldsType type;
	@JSONField(name: "has_lang")
	bool hasLang;
	bool require;
	dynamic options;
	String lang;
}

class BannerDataRecordFieldsType with JsonConvert<BannerDataRecordFieldsType> {
	int id;
	String name;
	String value;
}

class BannerDataRecordImage with JsonConvert<BannerDataRecordImage> {
	String id;
	String tag;
	String name;
	int width;
	int height;
	String mime;
	int size;
	String url;
	int position;

	String getImgUrlResize(String url, double w, double h) {
		return "$url/w_${w.round()},h_${h.round()},c_crop/$id/$name";
	}
}

class BannerBench with JsonConvert<BannerBench> {
	int second;
	double millisecond;
	String format;
}
