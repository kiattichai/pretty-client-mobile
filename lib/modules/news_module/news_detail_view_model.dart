import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/utils/screen.dart';

class NewsDetailViewModel extends BaseViewModelNoDi {
  double heightWebView = Screen.width;

  void updateWebview(double data) {
    heightWebView = data;
    notifyListeners();
    setLoading(true);
  }
}
