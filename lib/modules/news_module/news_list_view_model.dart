import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/modules/news_module/models/news_response_entity.dart';

class NewsListViewModel extends BaseViewModel {
  Function onErrorNewsList;
  List<NewsRecord> newsRecord = [];
  ScrollController scrollController;
  int limit = 6;
  int page = 1;
  bool is_complete = false;
  bool load_more = false;
  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;

  NewsListViewModel() {
    refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    scrollController = ScrollController();
  }

  @override
  void postInit() {
    getNewsList(limit, page);
  }

  Future<void> getNewsList(limit, page) async {
    if (page == 1) {
      setLoading(true);
    } else {
      setLoadMore(true);
    }

    catchError(() async {
//      List<NewsRecord> tmp = [];
      NewsResponseEntity tmp = null;
      if (is_complete == false) {
        tmp = await di.newsRepository.newsList(limit, page);
      }
      if (tmp !=null && tmp.data.record.length > 0 && tmp.data.pagination.currentPage <= tmp.data.pagination.lastPage) {
        newsRecord..addAll(tmp.data.record);
      } else {
        setIsComplete(true);
      }
      setLoading(false);
      setLoadMore(false);
    });
  }

  void setPage(int value) {
    page = value;
  }

  void setIsComplete(bool value) {
    is_complete = value;
  }

  void setLoadMore(bool value) {
    load_more = value;
  }

  void resetNewsRecord() {
    setPage(1);
    setIsComplete(false);
    newsRecord.clear();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    print(error);
    onErrorNewsList(error);
  }
}
