import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/news_module/news_list_screen.dart';

class NewsListRoute extends BaseRoute {
  static String buildPath = '/news_lists';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => NewsListScreen());
  }
}
