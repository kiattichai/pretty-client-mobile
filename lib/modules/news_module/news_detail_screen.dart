import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/modules/news_module/models/news_response_entity.dart';
import 'package:pretty_client_mobile/modules/news_module/news_detail_view_model.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:webview_flutter/webview_flutter.dart';

class NewsDetailScreen extends StatefulWidget {
  final NewsRecord newsRecord;
  final int index;

  const NewsDetailScreen({
    Key key,
    this.newsRecord,
    this.index,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return NewsDetailScreenState();
  }
}

class NewsDetailScreenState extends State<NewsDetailScreen> {
  WebViewController webViewController;
  NewsDetailViewModel viewModel;

  @override
  void initState() {
    super.initState();
    viewModel = NewsDetailViewModel();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      model: viewModel,
      builder: (context, model, child) => Container(
        color: Design.theme.colorMainBackground,
        child: SafeArea(
          top: false,
          child: Scaffold(
              extendBodyBehindAppBar: true,
              body: Stack(
                children: <Widget>[
                  customScrollView(),
                  Padding(
                    child: Container(
                      height: Screen.convertHeightSize(54),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              RouterService.instance.pop();
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: Color(0x7f1d1d1d),
                                shape: BoxShape.circle,
                              ),
                              height: 40,
                              width: 40,
                              child: Center(
                                child: SvgPicture.asset(
                                  'images/icon_arrow_left.svg',
                                  fit: BoxFit.none,
                                  height: 22,
                                  color: Design.theme.colorPrimary,
                                ),
                              ),
                            ),
                          ),
                          InkWell(
                            child: Container(
                              decoration: BoxDecoration(
                                color: Color(0x7f1d1d1d),
                                shape: BoxShape.circle,
                              ),
                              height: 40,
                              width: 40,
                              child: Center(
                                child: SvgPicture.asset(
                                  'images/icon_share.svg',
                                  fit: BoxFit.none,
                                  height: 22,
                                  color: Design.theme.colorPrimary,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    padding: EdgeInsets.only(top: 25, left: 10, right: 10),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Widget header() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        buildBanner(),
        Padding(
          padding: EdgeInsets.only(
              top: Screen.convertWidthSize(16),
              left: Screen.convertWidthSize(16),
              right: Screen.convertWidthSize(16)),
          child: Container(
            child: new Text(getFieldContent(widget.newsRecord, 'title'),
                style: Design.text.headerContent),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
              bottom: Screen.convertWidthSize(16),
              top: Screen.convertWidthSize(16),
              left: Screen.convertWidthSize(16),
              right: Screen.convertWidthSize(16)),
          child: Text(
            '${getFieldContent(widget.newsRecord, 'content_date')}',
            style: Design.text.textHighlight,
          ),
        )
      ],
    );
  }

  Widget buildBanner() {
    if (widget.newsRecord.images.isEmpty) {
      return Container(
        height: 200,
      );
    }
    return Hero(
      child: CachedNetworkImage(
        imageUrl: widget.newsRecord.images.first.url ?? '',
      ),
      tag: 'news${widget.index}',
    );
  }

  CustomScrollView customScrollView() {
    return CustomScrollView(
      physics: AlwaysScrollableScrollPhysics(),
      slivers: <Widget>[
        SliverList(
            delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
          switch (index) {
            case 0:
              {
                return header();
              }
              break;
            case 1:
              {
                return Container(
                  height: viewModel.heightWebView,
                  child: WebView(
                    onWebViewCreated: (controller) {
                      webViewController = controller;
                    },
                    javascriptMode: JavascriptMode.unrestricted,
                    initialUrl: widget.newsRecord.webviewUrl,
                  ),
                );
              }
              break;
            default:
              {
                return Container();
              }
          }
        }, childCount: 1)),
        SliverToBoxAdapter(
          child: Container(
              height: viewModel.heightWebView,
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  WebView(
                      onWebViewCreated: (controller) {
                        webViewController = controller;
                      },
                      javascriptMode: JavascriptMode.unrestricted,
                      initialUrl: widget.newsRecord.webviewUrl,
                      onPageFinished: (_) async {
                        await Future.delayed(Duration(milliseconds: 800));
                        final result = await webViewController
                            .evaluateJavascript("document.documentElement.offsetHeight;");
                        viewModel.updateWebview(double.parse(result.toString()));
                      }),
                  ProgressHUD(
                    color: Design.theme.colorMainBackground,
                    child: Container(),
                    opacity: 1,
                    inAsyncCall: !viewModel.loading,
                    progressIndicator: CircleLoading(),
                  )
                ],
              )),
        )
      ],
    );
  }

  String getFieldContent(NewsRecord newsRecord, String field) {
    String result = "";
    for (var i = 0; i < newsRecord.fields.length; i++) {
      if (newsRecord.fields[i].name == field) {
        result = newsRecord.fields[i].lang;
        break;
      }
    }
    return result;
  }
}
