import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:pretty_client_mobile/modules/news_module/models/news_response_entity.dart';
import 'package:inject/inject.dart';

class ContentApi {
  final CoreApi coreApi;
  final String pathContents = '/contents';

  @provide
  @singleton
  ContentApi(this.coreApi);

  Future<NewsResponseEntity> newsList(int limit, int page, Function badRequestToModelError) async {
    Map<String, String> query = {
      "group_code": "news",
      "limit": limit.toString(),
      "page": page.toString(),
      "fields": "fields,images,webview_url,share_url,id",
      'status': 'true'
    };
    final response = await coreApi.get(pathContents, query, badRequestToModelError);
    return NewsResponseEntity().fromJson(response.data);
  }

  Future<NewsDetail> newsDetail(String newsId, Function badRequestToModelError) async {
    final response = await coreApi.get('$pathContents/$newsId', null, badRequestToModelError);
    return NewsDetail().fromJson(response.data); 
  }
}
