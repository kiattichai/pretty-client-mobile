import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/modules/news_module/data/content_api.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/news_module/models/news_response_entity.dart';
import 'package:inject/inject.dart';

class NewsRepository extends BaseRepository {
  final ContentApi contentApi;
  NewsResponseEntity newsResponseEntity;
  @provide
  NewsRepository(this.contentApi);

  Future<NewsResponseEntity> newsList(limit, page) async {
    final response = await contentApi.newsList(limit, page, BaseErrorEntity.badRequestToModelError);
    if (response.data != null) {
      return response;
    } else {
      return null;
    }
  }

  Future<NewsRecord> newsDetail(newsId) async {
    final response = await contentApi.newsDetail(newsId, BaseErrorEntity.badRequestToModelError);
    return response.data; 
  }

}