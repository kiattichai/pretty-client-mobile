import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/env_config.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/news_module/models/news_response_entity.dart';
import 'package:pretty_client_mobile/modules/news_module/news_detail_route.dart';
import 'package:pretty_client_mobile/modules/news_module/news_list_view_model.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';

class NewsListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NewsListScreenState();
  }
}

class NewsListScreenState extends BaseStateProvider<NewsListScreen, NewsListViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = NewsListViewModel();
    viewModel
      ..onErrorNewsList = handleError
      ..scrollController.addListener(() {
        if (viewModel.scrollController.position.extentAfter == 0) {
          viewModel.setPage(viewModel.page + 1);
          viewModel.getNewsList(viewModel.limit, viewModel.page);
        }
      });
  }

  Widget itemNewsRecord(List<NewsRecord> newsRecord, i) {
    int index = i;
    return Container(
      margin: EdgeInsets.only(left: 15, right: 15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                color: Design.theme.colorBox,
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8), topRight: Radius.circular(8)),
                      child: Hero(
                        child: CachedNetworkImage(
                          fit: BoxFit.fitWidth,
                          imageUrl: getImgUrl(newsRecord[index].images, Screen.width - 30,
                              Screen.convertHeightSize(180)),
                          width: Screen.width - 30,
                          height: Screen.convertHeightSize(180),
                        ),
                        tag: 'news${index}',
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 15, left: 15, right: 15),
                    child: Text(getFieldContent(newsRecord[index], 'content_date'),
                        style: TextStyle(
                          fontSize: 14,
                          height: 1.35,
                          color: Design.theme.colorText2,
                        )),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, bottom: 20, left: 15, right: 15),
                    child: Text(
                      getFieldContent(newsRecord[index], 'title'),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Design.theme.colorActive,
                      ),
                    ),
                  ),
                ],
              )),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return BaseWidget<NewsListViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        color: Colors.transparent,
        progressIndicator: CircleLoading(),
        inAsyncCall: viewModel.loading,
        child: Scaffold(
            appBar: CustomHeaderDetail(
              height: Screen.convertHeightSize(55),
              title: appLocal.newsHeader,
              onTap: () {
                RouterService.instance.pop();
              },
            ),
            body: Builder(builder: (context) {
              return RefreshIndicator(
                color: Design.theme.colorMainBackground,
                backgroundColor: Design.theme.colorPrimary,
                key: model.refreshIndicatorKey,
                onRefresh: () {
                  model.resetNewsRecord();
                  return model.getNewsList(model.limit, model.page);
                },
                child: ListView.builder(
                  padding: EdgeInsets.only(top: 20),
                  physics: const AlwaysScrollableScrollPhysics(),
                  itemCount: model.newsRecord.length,
                  controller: model.scrollController,
                  itemBuilder: (context, i) {
                    int n = i;
                    if (model.load_more && n >= 0) {
                      return Container(
                        margin: EdgeInsets.only(top: 22),
                        child: CupertinoActivityIndicator(),
                      );
                    }
                    return InkWell(
                      child: itemNewsRecord(model.newsRecord, i),
                      onTap: () {
                        RouterService.instance.navigateTo(NewsDetailRoute.buildPath,
                            data: {'news': model.newsRecord[n], 'index': n});
                      },
                    );
                  },
                ),
              );
            })),
      ),
    );
  }

  String getFieldContent(NewsRecord newsRecord, String field) {
    String result = "";
    for (var i = 0; i < newsRecord.fields.length; i++) {
      if (newsRecord.fields[i].name == field) {
        result = newsRecord.fields[i].lang;
        break;
      }
    }
    return result;
  }

  String getImgUrl(List<NewsImageEntity> img, double w, double h) {
    String result = "https://admin.prettyhub.me/images/image-empty.png";
    if (img.length > 0) {
      result =
          "${EnvConfig.instance.resUrl}/w_${w.round()},h_${h.round()},c_crop/${img[0].id}/${img[0].name}";
    }
    return result;
  }

  void handleError(BaseError error) {
    print(error.toJson());
  }
}
