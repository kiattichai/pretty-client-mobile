import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/news_module/news_detail_screen.dart';
import 'package:pretty_client_mobile/modules/register_social_module/register_mobile_social_screen.dart';

class NewsDetailRoute extends BaseRoute {
  static String buildPath = '/news_detail';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => NewsDetailScreen(
              newsRecord: data['news'],
              index: data['index'],
            ));
  }
}
