import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class NewsResponseEntity with JsonConvert<NewsResponseEntity> {
  NewsResponseData data;
  NewsResponseBench bench;
}

class NewsResponseData with JsonConvert<NewsResponseData> {
  NewsPagination pagination;
  List<NewsRecord> record;
  bool cache;
}

class NewsDetail with JsonConvert<NewsDetail> {
  NewsRecord data;
  NewsResponseBench bench;
}

class NewsPagination with JsonConvert<NewsPagination> {
  @JSONField(name: "current_page")
  int currentPage;
  @JSONField(name: "last_page")
  int lastPage;
  int limit;
  int total;
}

class NewsImageEntity with JsonConvert<NewsImageEntity> {
  String id;
  String tag;
  String name;
  int width;
  int height;
  String mime;
  int size;
  String url;
  int position;
}

class NewsRecord with JsonConvert<NewsRecord> {
  int id;
  String slug;
  NewsGroup group;
  NewsCategory category;
  List<NewsRecordField> fields;
  List<NewsImageEntity> images;
  int viewed;
  bool status;
  @JSONField(name: "share_url")
  String shareUrl;
  @JSONField(name: "webview_url")
  String webviewUrl;
  @JSONField(name: "published_at")
  String publishedAt;
  @JSONField(name: "created_at")
  String createdAt;
  @JSONField(name: "updated_at")
  String updatedAt;
}

class NewsGroup with JsonConvert<NewsGroup> {
  int id;
  String code;
  String name;
  String description;
}

class NewsCategory with JsonConvert<NewsCategory> {
  NewsCategoryCurrent current;
  List<NewsCategoryItem> items;
}

class NewsCategoryCurrent with JsonConvert<NewsCategoryCurrent> {
  int id;
  String code;
  bool status;
  int position;
  String name;
  dynamic description;
  @JSONField(name: "meta_title")
  dynamic metaTitle;
  @JSONField(name: "meta_description")
  dynamic metaDescription;
  @JSONField(name: "meta_keyword")
  dynamic metaKeyword;
}

class NewsCategoryItem with JsonConvert<NewsCategoryItem> {
  int id;
  String code;
  bool status;
  int position;
  String name;
}

class NewsRecordField with JsonConvert<NewsRecordField> {
  String label;
  String name;
  NewsResponseDataRecordFieldsType type;
  @JSONField(name: "has_lang")
  bool hasLang;
  bool require;
  dynamic options;
  String lang;
}

class NewsResponseDataRecordFieldsType with JsonConvert<NewsResponseDataRecordFieldsType> {
  int id;
  String name;
  String value;
}

class NewsResponseBench with JsonConvert<NewsResponseBench> {
  int second;
  double millisecond;
  String format;
}
