import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/login_module/entitiies/authen_entity.dart';
import 'package:pretty_client_mobile/modules/login_module/data/login_api.dart';
import 'package:inject/inject.dart';

class LoginRepository extends BaseRepository {
  final LoginApi loginApi;
  AuthenData authenData;

  @provide
  LoginRepository(this.loginApi);

  Future<void> login(
    String username,
    String password,
    String countryCode,
  ) async {
    final response = await loginApi.logIn(
        username, password, countryCode, BaseErrorEntity.badRequestToModelError);
    await setToken(response.data);
  }

  Future<void> loginSocial(String token, String service) async {
    final response =
        await loginApi.loginSocial(token, service, BaseErrorEntity.badRequestToModelError);
    if (response.data != null) {
      await setToken(response.data);
    }
  }

  Future<AuthenData> getAuthenData() async {
    return Future.value(authenData ?? null);
  }

  Future<void> setToken(AuthenData data, {String fbToken = ''}) async {
    if (data != null) {
      authenData = data;
      final share = di.sharePrefInterface;
      share.setTokenType(data.tokenType);
      share.setAccessToken(data.accessToken);
      share.setRefreshToken(data.refreshToken);
      share.setFbToken(fbToken);
    }
  }
}
