import 'package:dio/dio.dart';
import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:pretty_client_mobile/app/share_preference.dart';
import 'package:pretty_client_mobile/modules/login_module/entitiies/authen_entity.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/utils/device_info.dart';
import 'package:device_id/device_id.dart';

class LoginApi {
  final CoreApi coreApi;
  final SharePrefInterface sharePrefInterface;
  final String pathRegister = '/users/authen';
  final String pathRegisterSocial = '/users/authen/social';

  @provide
  @singleton
  LoginApi(this.coreApi, this.sharePrefInterface);

  Future<AuthenEntity> logIn(
      String username, String password, String countryCode, Function badRequestToModelError) async {
    String deviceId = await DeviceId.getID;
    var loginData = {
      "username": username,
      "password": password,
      "country_code": countryCode,
      "device_type": deviceType(),
      "device_id": deviceId,
      "notification_token": sharePrefInterface.getDeviceToken(),
      "app": "user"
    };
    print(loginData);
    final response = await coreApi.post(pathRegister, loginData, badRequestToModelError);
    return AuthenEntity().fromJson(response.data);
  }

  Future<AuthenEntity> loginSocial(
      String token, String service, Function badRequestToModelError) async {
    String deviceId = await DeviceId.getID;
    var loginData = {
      "service": service,
      "token": token,
      "device_type": deviceType(),
      "device_id": deviceId,
      "notification_token": sharePrefInterface.getDeviceToken(),
      "app": "user"
    };

    Response response = await coreApi.post(pathRegisterSocial, loginData, badRequestToModelError,
        hasPermission: false);
    return AuthenEntity().fromJson(response.data);
  }

  String deviceType() {
    if (Device.get().isAndroid) {
      return "android";
    } else if (Device.get().isIos) {
      return "ios";
    } else {
      return "";
    }
  }
}
