import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/login_module/login_screen.dart';

class LoginRoute extends BaseRoute {
  static String buildPath = '/login';

  @override
  String get path => buildPath;

//  @override
//  Future<bool> hasPermission(params) async {
//    return await Future.delayed(Duration(seconds: 1), () => true);
//  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => LoginScreen());
  }
}
