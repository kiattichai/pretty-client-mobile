import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';

class LoginViewModel extends BaseViewModel {
  Function onErrorLogin;
  FirebaseMessaging firebaseMessaging;
  FocusNode inputMobileFocus;
  FocusNode inputPasswordFocus;
  bool inputMobileIsFocus;
  bool inputPasswordIsFocus;

  LoginViewModel() {
    inputMobileFocus = FocusNode();
    inputPasswordFocus = FocusNode();
  }

  String loginMode = "mobile";

  @override
  void postInit() {
    super.postInit();
    firebaseMessaging = FirebaseMessaging();
    firebaseMessaging.getToken().then((token) async {
      final share = di.sharePrefInterface;
      share.setDeviceToken(token);
      print('getFCM TOKEN $token');
    });
  }

  void setLoginMode(String value) {
    loginMode = value;
  }

  void login(String username, String password, String countryCode, Function onLoginSuccess) {
    setLoading(true);
    catchError(() async {
      await di.loginRepository.login(username, password, countryCode);
      await di.favoriteRepository.getFavoriteList();
      onLoginSuccess();
      setLoading(false);
    });
  }

  void loginSocial(String token, String service, Function onLoginSocialSuccess) {
    setLoading(true);
    catchError(() async {
      await di.loginRepository.loginSocial(token, service);
      onLoginSocialSuccess();
      setLoading(false);
    });
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    onErrorLogin(error);
  }
}
