import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class AuthenEntity with JsonConvert<AuthenEntity> {
  AuthenData data;
}

class AuthenData with JsonConvert<AuthenData> {
  @JSONField(name: "token_type")
  String tokenType;
  @JSONField(name: "access_token")
  String accessToken;
  @JSONField(name: "refresh_token")
  String refreshToken;
}
