import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/country_code_module/country_code_route.dart';
import 'package:pretty_client_mobile/modules/country_code_module/model/country_code_response_entity.dart';
import 'package:pretty_client_mobile/modules/forgot_password_module/routes/forgot_password_route.dart';
import 'package:pretty_client_mobile/modules/login_module/login_view_model.dart';
import 'package:pretty_client_mobile/modules/pre_register_module/pre_register_route.dart';
import 'package:pretty_client_mobile/modules/register_social_module/register_social_route.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_modal.dart';
import 'package:pretty_client_mobile/widgets/input_mobile_widget.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends BaseStateProvider<LoginScreen, LoginViewModel> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>(debugLabel: '_homeScreenkey');
  final facebookLogin = FacebookLogin();

  bool _validate = false;
  bool autoFocus = true;
  String username = "";
  String password = "";
  String countryCode = "TH";
  String phoneCode = "+66";
  FacebookLoginResult facebookLoginResult;
  TextEditingController controllerMobile = TextEditingController();
  final FocusNode inputMobileFocus = FocusNode();
  final FocusNode inputPasswordFocus = FocusNode();

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
        keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
        keyboardBarColor: Colors.grey[200],
        nextFocus: true,
        actions: [
          KeyboardAction(
            focusNode: inputMobileFocus,
          ),
          KeyboardAction(
            focusNode: inputPasswordFocus,
          ),
        ]);
  }

  @override
  void initState() {
    super.initState();
    viewModel = LoginViewModel();
    viewModel..onErrorLogin = handleError;
  }

  Widget _logo() {
    return Container(
        alignment: FractionalOffset.center,
        padding: EdgeInsets.only(
          top: Screen.convertHeightSize(80),
        ),
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 10),
              child: SvgPicture.asset(
                "images/logo_night2.svg",
                width: 120,
              ),
            ),
          ],
        ));
  }

  Widget buttonFBLogin(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Screen.convertHeightSize(60)),
      child: MaterialButton(
        color: Design.theme.colorButtonFacebook,
        height: 48,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 10),
              child: Image(
                image: AssetImage("images/fb-icon.png"),
                width: 32,
              ),
            ),
            Text(
              appLocal.loginBtnFbTitle,
              style: Design.text.textFacebookButton,
            ),
          ],
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(26),
        ),
        onPressed: () {
          FocusScope.of(context).requestFocus(new FocusNode());
          _loginWithFB();
        },
      ),
    );
  }

  Widget breakLineOr() {
    return Padding(
      padding: EdgeInsets.only(
        top: 20,
        left: 4,
        right: 4,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(width: Screen.convertWidthSize(135), height: 1, color: Design.theme.colorLine),
          SizedBox(
            width: 8,
          ),
          Text(
            appLocal.loginTitleOr,
            style: TextStyle(
              color: Design.theme.colorText2,
              fontSize: 16,
            ),
          ),
          SizedBox(
            width: 8,
          ),
          Container(width: Screen.convertWidthSize(135), height: 1, color: Design.theme.colorLine),
        ],
      ),
    );
  }

  Widget prefixCountryCode() {
    return GestureDetector(
      child: Container(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                left: 15,
                right: 15,
              ),
              child: Text(
                "$countryCode $phoneCode",
                style: TextStyle(
                  fontSize: 18,
                  color: Design.theme.colorInactive,
                ),
              ),
            ),
            Container(
              height: 30,
              width: 1,
              color: Color.fromRGBO(187, 187, 187, 1),
            ),
            SizedBox(
              width: 8,
            ),
          ],
        ),
      ),
      onTap: () async {
        final _countryCode =
            await RouterService.instance.navigateTo(CountryCodeRoute.buildPath) as CountryCode;
        print(_countryCode.toJson());
        countryCode = _countryCode.isoCode;
        phoneCode = _countryCode.phoneCodeText;
      },
    );
  }

  Widget inputMobile(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: 20,
      ),
      child: TextFormField(
        key: Key('mobile'),
        keyboardType: TextInputType.phone,
        autofocus: false,
        style:
            TextStyle(fontSize: 18, fontWeight: FontWeight.w500, color: Design.theme.colorActive),
        inputFormatters: <TextInputFormatter>[WhitelistingTextInputFormatter.digitsOnly],
        textInputAction: TextInputAction.next,
        focusNode: inputMobileFocus,
        onFieldSubmitted: (v) {
          inputMobileFocus.unfocus();
          FocusScope.of(context).requestFocus(inputPasswordFocus);
        },
        maxLength: 20,
        autovalidate: _validate,
        validator: (String val) {
          RegExp regExp = new RegExp(
            r"^[0-9]*$",
            caseSensitive: false,
            multiLine: false,
          );
          if (val.isEmpty) {
            _validate = true;
            return "กรุณากรอกข้อมูล";
          } else if (val.length < 10) {
            _validate = true;
            return "เบอร์โทรศัพท์ความยาวอย่างน้อย 10 ตัวอักษร";
          } else if (val.length > 20) {
            _validate = true;
            return "เบอร์โทรศัพท์ความยาวไม่เกิน 20 ตัวอักษร";
          } else if (!regExp.hasMatch(val)) {
            _validate = true;
            return "เบอร์โทรศัพท์ไม่ถูกต้อง";
          }
        },
        onSaved: (String val) {
          username = val;
        },
        decoration: InputDecoration(
          filled: true,
          fillColor: Design.theme.colorLine.withOpacity(0.2),
          prefixIcon: this.prefixCountryCode(),
          contentPadding: EdgeInsets.symmetric(
            vertical: 10.0,
            horizontal: 10,
          ),
          errorStyle: TextStyle(color: Design.theme.colorPrimary),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(8),
            ),
            borderSide: BorderSide(
              width: 1,
              color: Design.theme.colorPrimary,
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(8),
            ),
            borderSide: BorderSide(
              width: 1,
              color: Design.theme.colorPrimary,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(8),
            ),
            borderSide: BorderSide(
              width: 1,
              color: Design.theme.colorLine.withOpacity(0.2),
            ),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(width: 1, color: Design.theme.colorLine),
          ),
          hintStyle: TextStyle(
            color: Design.theme.colorInactive.withOpacity(0.6),
          ),
          hintText: appLocal.loginPhoneNoHint,
          counterText: '',
        ),
      ),
    );
  }

  Widget inputPassword(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: TextFormField(
        key: Key('password'),
        keyboardType: TextInputType.text,
        style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.w500,
          color: Design.theme.colorInactive,
        ),
        obscureText: true,
        focusNode: inputPasswordFocus,
        textInputAction: TextInputAction.done,
        onFieldSubmitted: (v) {
          inputPasswordFocus.unfocus();
        },
        autovalidate: _validate,
        validator: (String val) {
          RegExp regExp = new RegExp(
            r"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$",
            caseSensitive: false,
            multiLine: false,
          );
          if (val.isEmpty) {
            _validate = true;
            return "กรุณากรอกข้อมูล";
          } else if (val.length < 8) {
            _validate = true;
            return "รหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวอักษร";
          } else if (!regExp.hasMatch(val)) {
            _validate = true;
            return "รหัสผ่านต้องมีตัวอักษร และ ตัวเลขอย่างน้อย 1 ตัว";
          }
        },
        onSaved: (String val) {
          password = val;
        },
        decoration: InputDecoration(
            filled: true,
            fillColor: Design.theme.colorLine.withOpacity(0.2),
            contentPadding: EdgeInsets.symmetric(
              vertical: 10.0,
              horizontal: 15,
            ),
            errorStyle: TextStyle(color: Design.theme.colorPrimary),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
              borderSide: BorderSide(
                width: 1,
                color: Design.theme.colorPrimary,
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
              borderSide: BorderSide(
                width: 1,
                color: Design.theme.colorPrimary,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
              borderSide: BorderSide(
                width: 1,
                color: Design.theme.colorLine.withOpacity(0.2),
              ),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
              borderSide: BorderSide(
                width: 1,
                color: Design.theme.colorLine,
              ),
            ),
            hintStyle: TextStyle(
              color: Design.theme.colorInactive.withOpacity(0.6),
            ),
            hintText: appLocal.loginPasswordHint,
            counterText: ''
                ''),
      ),
    );
  }

  Widget buttonForgetPassword() {
    return Container(
        alignment: FractionalOffset.centerRight,
        child: FlatButton(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          child: Text(appLocal.loginBtnForgotPassword,
              style: TextStyle(
                color: Design.theme.colorInactive.withOpacity(0.6),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              )),
          onPressed: () {
            RouterService.instance.navigateTo(ForgotPasswordRoute.buildPath);
          },
        ));
  }

  Widget buttonLogin(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: MaterialButton(
        color: Design.theme.colorPrimary,
        height: 48,
        child: Text(
          appLocal.loginBtnSubmit,
          style: Design.text.textButton,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(26),
        ),
        onPressed: () {
          FocusScope.of(context).requestFocus(new FocusNode());
          _login();
        },
      ),
    );
  }

  Widget registerBox(BuildContext context) {
    return Container(
      alignment: FractionalOffset.center,
      padding: EdgeInsets.only(top: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            appLocal.loginBeforeMobileRegisterTitle,
            style: TextStyle(
                fontSize: 16,
                color: Design.theme.colorInactive.withOpacity(0.6),
                fontWeight: FontWeight.w500),
          ),
          FlatButton(
            padding: EdgeInsets.all(5),
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            child: Text(
              appLocal.loginBtnRegister,
              style: TextStyle(
                  fontSize: 16,
                  color: Design.theme.colorPrimary.withOpacity(0.6),
                  fontWeight: FontWeight.w500),
            ),
            onPressed: () {
              RouterService.instance.navigateTo(PreRegisterRoute.buildPath);
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return BaseWidget<LoginViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        color: Colors.transparent,
        progressIndicator: CircleLoading(),
        inAsyncCall: viewModel.loading,
        child: Container(
          child: Scaffold(
            body: SafeArea(
              child: KeyboardActions(
                isDialog: false,
                config: _buildConfig(context),
                child: SingleChildScrollView(
                  child: Stack(
                    fit: StackFit.loose,
                    children: <Widget>[
                      Container(
                        alignment: FractionalOffset.topCenter,
                        padding: EdgeInsets.only(
                          left: 20,
                          right: 20,
                        ),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              this._logo(),
                              this.buttonFBLogin(context),
                              this.breakLineOr(),
                              this.inputMobile(context),
                              this.inputPassword(context),
                              this.buttonForgetPassword(),
                              this.buttonLogin(context),
                              this.registerBox(context),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            InkWell(
                              child: Container(
                                height: Screen.convertHeightSize(54),
                                width: 50,
                                child: SvgPicture.asset(
                                  'images/icon_delete_gray.svg',
                                  height: 20,
                                  fit: BoxFit.none,
                                  color: Design.theme.colorPrimary,
                                ),
                              ),
                              onTap: () {
                                RouterService.instance.pop();
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void onLoginSuccess() {
    RouterService.instance.pop(data: true);
  }

  void handleError(BaseError error) {
    print(error.toJson());
    if (error.code == 404 && viewModel.loginMode == 'facebook') {
      RouterService.instance.navigateTo(RegisterSocialRoute.buildPath, data: facebookLoginResult);
    } else {
      _showAlert(error.message);
    }
  }

  void _login() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      viewModel.setLoginMode('mobile');
      viewModel.login(username, password, countryCode, onLoginSuccess);
    }
  }

  void _loginWithFB() async {
    await facebookLogin.logOut();
    final result = await facebookLogin.logIn(['email']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        viewModel.setLoginMode('facebook');
        viewModel.setLoading(true);
        final token = result.accessToken.token;
        facebookLoginResult = result;
        viewModel.loginSocial(token, "facebook", onLoginSuccess);
        break;
      case FacebookLoginStatus.cancelledByUser:
        viewModel.setLoading(false);
        break;
      case FacebookLoginStatus.error:
        viewModel.setLoading(false);
        break;
    }
  }

  void _showAlert(String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomModal(
            desc: message,
            appLocal: appLocal,
          );
        });
  }
}
