import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/modules/home_module/home_view_model.dart';
import 'package:pretty_client_mobile/widgets/custom_calendar.dart';
import 'package:webview_flutter/webview_flutter.dart';

class HomeScreen extends StatefulWidget {
  final String title;

  const HomeScreen({Key key, this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => HomeScreenState();
}

class HomeScreenState extends BaseStateProvider<HomeScreen, HomeViewModel> {
  double heightWebView = 1;
  WebViewController webViewController;
  DateTime startDate = DateTime.now();
  DateTime endDate = DateTime.now().add(Duration(days: 5));

  @override
  void initState() {
    super.initState();
    viewModel = HomeViewModel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: customScrollView(),
    );
  }

  CustomScrollView customScrollView() {
    return CustomScrollView(
      physics: AlwaysScrollableScrollPhysics(),
      slivers: <Widget>[
        SliverList(
            delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
          switch (index) {
            case 0:
              {
                return CustomCalendarView(
                  minimumDate: DateTime.now(),
                  initialEndDate: endDate,
                  initialStartDate: startDate,
                  startEndDateChange: (DateTime startDateData, DateTime endDateData) {
                    setState(() {
                      startDate = startDateData;
                      endDate = endDateData;
                    });
                  },
                );
              }
              break;
            case 1:
              {
                return Container(
                  height: heightWebView,
                  child: WebView(
                    onWebViewCreated: (controller) {
                      webViewController = controller;
                    },
                    javascriptMode: JavascriptMode.unrestricted,
                    initialUrl: 'https://alpha-www.theconcert.com/p/60?webview=1',
                    onPageFinished: (_) async {
                      final result = await webViewController
                          .evaluateJavascript("document.documentElement.scrollHeight;");
                      print(result);
                      setState(() {
                        heightWebView = double.parse(result.toString());
                      });
                    },
                  ),
                );
              }
              break;
            default:
              {
                return Container();
              }
          }
        }, childCount: 2)),
      ],
    );
  }
}
