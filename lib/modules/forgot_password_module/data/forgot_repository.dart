import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/modules/forgot_password_module/data/forgot_api.dart';

class ForgotRepository extends BaseRepository {
  final ForgotApi api;

  @provide
  ForgotRepository(this.api);

  Future<BaseResponseEntity> forgotPassword(
      int userId, String password, String confirmPassword) async {
    final response = await api.forgotPassword(userId, password, confirmPassword);
    return response;
  }
  
}
