import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';

class ForgotApi {
  final String pathForgotPassword = "/users";
  final CoreApi coreApi;

  @provide
  @singleton
  ForgotApi(this.coreApi);

  Future<BaseResponseEntity> forgotPassword(
      int userId, String password, String confirmPassword) async {
    var queryParameters = {"password": password, "password_confirmation": confirmPassword};
    final response = await coreApi.put(pathForgotPassword + "/$userId/password", queryParameters,
        BaseErrorEntity.badRequestToModelError);
    return BaseResponseEntity().fromJson(response.data);
  }
}
