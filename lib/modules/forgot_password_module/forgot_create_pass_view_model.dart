import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_request_screen.dart';

class ForgotCreatePassViewModel extends BaseViewModel {
  OtpRequestScreen otpRequestScreen;
  Function handleErrorForgotPassword;
  bool isEnableButton = false;

  ForgotCreatePassViewModel({this.otpRequestScreen});

  void forgotCreateNewPassword(
      String password, String confirmPassword, Function forgotPasswordSuccess) {
    setLoading(true);
    catchError(() async {
      final result = await di.forgotRepository
          .forgotPassword(otpRequestScreen.userId, password, confirmPassword);
      forgotPasswordSuccess(result);
      setLoading(false);
    });
  }

  void enableButton(bool value) {
    isEnableButton = value;
    notifyListeners();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    if (error is BaseError) {
      handleErrorForgotPassword(error);
    }
  }

  void updatePassword(String text, String text2) {
    if (text.isEmpty || text2.isEmpty) {
      isEnableButton = false;
      notifyListeners();
      return;
    }
    if (text != text2) {
      isEnableButton = false;
      notifyListeners();
      return;
    }
    if (text == text2) {
      isEnableButton = true;
      notifyListeners();
      return;
    }
  }
}
