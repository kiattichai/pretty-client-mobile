import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/forgot_password_module/forgot_password_screen.dart';

class ForgotPasswordRoute extends BaseRoute {
  static String buildPath = '/forgot_password';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => ForgotPasswordMobileScreen());
  }
}
