import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/otp_module/models/request_otp_entity.dart';
import 'package:pretty_client_mobile/modules/register_module/models/check_user_entity.dart';

class ForgotPasswordViewModel extends BaseViewModel {
  Function handleMobileNotFound;
  Map<String, String> result;

  ForgotPasswordViewModel();

  void checkMobile(
      String mobile, String countryCode, String channel, Function onCheckMobileSuccess) {
    catchError(() async {
      setLoading(true);
      final checkUserData =
          await di.registerRepository.checkRegister(mobile, countryCode, 'forget');
      final otpRef = await di.otpRepository
          .requestOtp(RequestOtpEntity(countryCode: countryCode, mobile: mobile, channel: channel));
      onCheckMobileSuccess({'id': checkUserData.userId.toString(), 'ref': otpRef.ref});
      setLoading(false);
    });
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    print(error);
    if (error is BaseError) {
      handleMobileNotFound(error);
    }
  }
}
