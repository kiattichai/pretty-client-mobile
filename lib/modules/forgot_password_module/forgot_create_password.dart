import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/forgot_password_module/forgot_create_pass_view_model.dart';
import 'package:pretty_client_mobile/modules/main_module/main_screen_route.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_request_screen.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/input_password_widget.dart';
import 'package:pretty_client_mobile/widgets/primary_button_widget.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

class ForgotCreatePasswordScreen extends StatefulWidget {
  final OtpRequestScreen otpRequestScreen;

  const ForgotCreatePasswordScreen({Key key, this.otpRequestScreen}) : super(key: key);

  @override
  ForgotCreatePasswordState createState() {
    return ForgotCreatePasswordState();
  }
}

class ForgotCreatePasswordState
    extends BaseStateProvider<ForgotCreatePasswordScreen, ForgotCreatePassViewModel> {
  final _formKey = GlobalKey<FormState>();
  final focusPassword = FocusNode();
  final focusConfirmPassword = FocusNode();
  final TextEditingController controllerPassword = TextEditingController();
  final TextEditingController controllerConfirmPassword = TextEditingController();
  bool _validate = false;
  bool autoFocus = true;

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
        keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
        keyboardBarColor: Colors.grey[200],
        nextFocus: true,
        actions: [
          KeyboardAction(
            focusNode: focusPassword,
          ),
          KeyboardAction(
            focusNode: focusConfirmPassword,
          ),
        ]);
  }

  @override
  void initState() {
    super.initState();
    viewModel = ForgotCreatePassViewModel(otpRequestScreen: widget.otpRequestScreen);
    viewModel.handleErrorForgotPassword = handleErrorForgotPassword;
  }

  void handleErrorForgotPassword(BaseError baseError) {
    _showAlert(baseError.message);
  }

  Widget titlePasswordInfo(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 36, right: 0),
      child: SizedBox(
        width: double.infinity,
        child: Container(
          child: Text(appLocal.createPasswordTitle,
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Design.theme.colorInactive,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              )),
        ),
      ),
    );
  }

  Widget descriptionText(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 5, right: 0),
      child: SizedBox(
        width: double.infinity,
        child: Container(
          child: Text(appLocal.createPasswordHelp,
              style: TextStyle(
                color: Design.theme.colorInactive.withOpacity(0.8),
                fontSize: 12,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              )),
        ),
      ),
    );
  }

  Widget titleConfirmPasswordInfo(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 15, right: 0),
      child: SizedBox(
        width: double.infinity,
        child: Container(
          child: Text(appLocal.createPasswordAgainTitle,
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Design.theme.colorInactive,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              )),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return BaseWidget<ForgotCreatePassViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        color: Colors.transparent,
        progressIndicator: CircleLoading(),
        inAsyncCall: viewModel.loading,
        child: Scaffold(
          backgroundColor: Design.theme.colorMainBackground,
          resizeToAvoidBottomPadding: false,
          appBar: CustomHeaderDetail(
            height: Screen.convertHeightSize(55),
            title: appLocal.createPasswordHeader,
          ),
          body: KeyboardActions(
            isDialog: true,
            config: _buildConfig(context),
            child: Container(
              alignment: FractionalOffset.topCenter,
              padding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              child: SafeArea(
                child: SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        titlePasswordInfo(context),
                        InputPasswordWidget(
                          hintText: appLocal.createPasswordTitle,
                          controllerPassword: controllerPassword,
                          focusPassword: focusPassword,
                          onChanged: (value) {
                            viewModel.updatePassword(
                                controllerPassword.text, controllerConfirmPassword.text);
                          },
                          onSummitted: (value) {
                            FocusScope.of(context).requestFocus(focusConfirmPassword);
                          },
                          autoFocus: autoFocus,
                          autoValidate: _validate,
                        ),
                        descriptionText(context),
                        titleConfirmPasswordInfo(context),
                        InputPasswordWidget(
                          hintText: appLocal.createPasswordAgainTitle,
                          controllerPassword: controllerConfirmPassword,
                          focusPassword: focusConfirmPassword,
                          onChanged: (value) {
                            viewModel.updatePassword(
                                controllerPassword.text, controllerConfirmPassword.text);
                          },
                          onSummitted: (value) {},
                          autoFocus: false,
                          autoValidate: _validate,
                        ),
                        SizedBox(
                          height: 80,
                        ),
                        PrimaryButtonWidget(
                          buttonTitle: appLocal.commonNext,
                          onBtnPressed: !viewModel.isEnableButton
                              ? null
                              : () {
                                  FocusScope.of(context).requestFocus(FocusNode());
                                  if (_formKey.currentState.validate()) {
                                    // No any error in validation
                                    _formKey.currentState.save();
                                    _gotoFinish(model);
                                  } else {
                                    // validation error
                                    setState(() {
                                      _validate = true;
                                    });
                                  }
                                },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _gotoFinish(ForgotCreatePassViewModel model) async {
    //call api register
    model.forgotCreateNewPassword(
        controllerPassword.text, controllerConfirmPassword.text, forgotPasswordSuccess);
  }

  void forgotPasswordSuccess(BaseResponseEntity baseResponseEntity) {
    _showAlert(baseResponseEntity.data.message, isSuccess: true);
  }

  void _showAlert(String message, {bool isSuccess = false}) {
    AlertDialog alertDialog = AlertDialog(
      content: Text(message),
      actions: <Widget>[
        FlatButton(
          child: Text("ยืนยัน"),
          onPressed: () {
            if (isSuccess) {
              RouterService.instance.navigateTo(MainScreenRoute.buildPath, clearStack: true);
            } else {
              RouterService.instance.pop();
            }
          },
        )
      ],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alertDialog;
        });
  }

  bool validatePasswordString(String value) {
    RegExp regExp = new RegExp(
      r"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$",
      caseSensitive: false,
      multiLine: false,
    );

    print(value);
    if (regExp.hasMatch(value)) {
      return true;
    }
    return false;
  }
}
