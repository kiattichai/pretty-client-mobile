import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/country_code_module/model/country_code_response_entity.dart';
import 'package:pretty_client_mobile/modules/forgot_password_module/forgot_password_view_model.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_flow_type.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_request_screen.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_route.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/modules/country_code_module/country_code_screen.dart';
import 'package:pretty_client_mobile/widgets/input_mobile_widget.dart';
import 'package:pretty_client_mobile/widgets/primary_button_widget.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/widgets/radio_button_select_verify_channel_widget.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

class ForgotPasswordMobileScreen extends StatefulWidget {
  @override
  ForgotPasswordMobileState createState() {
    return ForgotPasswordMobileState();
  }
}

class ForgotPasswordMobileState
    extends BaseStateProvider<ForgotPasswordMobileScreen, ForgotPasswordViewModel> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController controllerMobile = TextEditingController();
  final focusInputMobile = FocusNode();
  bool autoFocus = true;
  String countryCode = "TH";
  String phoneCode = "+66";
  String currentGroupRadioValue = "sms";
  bool acceptTerm = false;
  bool _btnEnabled = false;

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
        keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
        keyboardBarColor: Colors.grey[200],
        nextFocus: true,
        actions: [
          KeyboardAction(
            focusNode: focusInputMobile,
          ),
        ]);
  }

  @override
  void initState() {
    super.initState();
    viewModel = ForgotPasswordViewModel();
    viewModel.handleMobileNotFound = handleMobileNotFound;
  }

  void handleMobileNotFound(BaseError error) {
    _showAlert(error.message);
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return BaseWidget<ForgotPasswordViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        color: Colors.transparent,
        progressIndicator: CircleLoading(),
        inAsyncCall: viewModel.loading,
        child: Scaffold(
          backgroundColor: Design.theme.colorMainBackground,
          resizeToAvoidBottomPadding: false,
          appBar: CustomHeaderDetail(
            height: Screen.convertHeightSize(55),
            title: appLocal.forgotPasswordTitle,
          ),
          body: KeyboardActions(
            isDialog: true,
            config: _buildConfig(context),
            child: Container(
              alignment: FractionalOffset.topCenter,
              color: Design.theme.colorMainBackground,
              padding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              child: SafeArea(
                child: SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        InputMobileWidget(
                          onCountrySelected: () async {
                            final _countryCode = await Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => CountryCodeScreen()),
                            ) as CountryCode;
                            print(_countryCode.toJson());
                            countryCode = _countryCode.isoCode;
                            phoneCode = _countryCode.phoneCodeText;
                          },
                          countryCode: countryCode,
                          phoneCode: phoneCode,
                          onMobileInputChanged: (String val) {
                            if (val.length >= 10) {
                              setState(() {
                                _btnEnabled = true;
                              });
                            } else {
                              setState(() {
                                _btnEnabled = false;
                              });
                            }
                          },
                          onMobileInputValidator: (String val) {
                            if (val.length >= 10) {
                              setState(() {
                                _btnEnabled = true;
                              });
                            } else {
                              setState(() {
                                _btnEnabled = false;
                              });
                            }
                          },
                          controllerMobile: controllerMobile,
                          focusInputMobile: focusInputMobile,
                          onFieldSubmitted: onInputFieldSubmitted,
                          appLocale: appLocal,
                        ),
                        RadioButtonSelectVerifyChannelWidget(
                          currentGroupRadioValue: currentGroupRadioValue,
                          onChannelSelected: (String val) {
                            _handleRadioValueChange(val);
                          },
                          appLocale: appLocal,
                        ),
                        SizedBox(
                          height: 80,
                        ),
                        PrimaryButtonWidget(
                          buttonTitle: appLocal.commonNext,
                          onBtnPressed: _btnEnabled == true ? () => _requestOTP() : null,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void onInputFieldSubmitted(String value) {
    focusInputMobile.unfocus();
  }

  void _handleRadioValueChange(String value) {
    setState(() {
      currentGroupRadioValue = value;
    });
  }

  void _requestOTP() {
    focusInputMobile.unfocus(focusPrevious: false);
    viewModel.checkMobile(
        controllerMobile.text, countryCode, currentGroupRadioValue, checkMobileSuccess);
  }

  void checkMobileSuccess(Map<String, String> result) {
    RouterService.instance.navigateTo(OtpRoute.buildPath,
        data: OtpRequestScreen(
            userId: int.parse(result['id']),
            otpFlowType: OTPFlowType.FORGOT,
            mobile: controllerMobile.text,
            countryCode: countryCode,
            channel: currentGroupRadioValue,
            refCode: result['ref']));
  }

  void _showAlert(String message) {
    AlertDialog alertDialog = AlertDialog(
      content: Text(message),
      actions: <Widget>[],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alertDialog;
        });
  }
}
