import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/pre_register_module/pre_register_screen.dart';

class PreRegisterRoute extends BaseRoute {
  static String buildPath = '/pre_register';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => PreRegister());
  }
}
