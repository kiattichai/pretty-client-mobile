import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/login_module/login_view_model.dart';
import 'package:pretty_client_mobile/modules/main_module/main_screen_route.dart';
import 'package:pretty_client_mobile/modules/register_module/routes/register_mobile_route.dart';
import 'package:pretty_client_mobile/modules/register_social_module/register_social_route.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';

class PreRegister extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PreRegisterState();
  }
}

class PreRegisterState extends BaseStateProvider<PreRegister, LoginViewModel> {
  final facebookLogin = FacebookLogin();
  FacebookLoginResult facebookLoginResult;

  @override
  LoginViewModel viewModel = LoginViewModel();

  @override
  void initState() {
    super.initState();
    viewModel.onErrorLogin = handleError;
  }

  Widget _logo() {
    return Container(
        alignment: FractionalOffset.center,
        padding: EdgeInsets.only(
          top: 0,
          bottom: 100,
        ),
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 10),
              child: SvgPicture.asset(
                "images/logo_night2.svg",
                width: 120,
              ),
            ),
          ],
        ));
  }

  Widget buttonFBLogin() {
    return Container(
//      margin: EdgeInsets.only(top: 50),
      child: MaterialButton(
        color: Color(0xff1777F2),
        height: 48,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 10),
              child: Image(
                image: AssetImage("images/fb-icon.png"),
                width: 32,
              ),
            ),
            Text(
              appLocal.loginBtnFbTitle,
              style: Design.text.textFacebookButton,
            ),
          ],
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(26),
        ),
        onPressed: () {
          _loginWithFB(
            context,
          );
        },
      ),
    );
  }

  Widget breakLineOr() {
    return Padding(
      padding: EdgeInsets.only(
        top: 30,
        left: 4,
        right: 4,
        bottom: 30,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: Screen.convertWidthSize(142),
            height: 1,
            color: Design.theme.colorLine,
          ),
          Text(
            appLocal.loginTitleOr,
            style: TextStyle(
              color: Design.theme.colorText2,
              fontSize: 16,
              fontStyle: FontStyle.normal,
            ),
          ),
          Container(
            width: Screen.convertWidthSize(142),
            height: 1,
            color: Design.theme.colorLine,
          ),
        ],
      ),
    );
  }

  Widget buttonRegister(BuildContext context) {
    return Container(
      child: MaterialButton(
        color: Design.theme.colorPrimary,
        height: 48,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 15),
              child: Image(
                image: AssetImage("images/grip_horizontal.png"),
                width: 32,
              ),
            ),
            Text(
              appLocal.loginBeforeMobileRegisterTitle,
              style: Design.text.textButton,
            ),
          ],
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(26),
        ),
        onPressed: () {
          RouterService.instance.navigateTo(RegisterMobileRoute.buildPath);
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    return BaseWidget<LoginViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        progressIndicator: CircleLoading(),
        inAsyncCall: viewModel.loading,
        child: Container(
          child: Scaffold(
            body: SafeArea(
              child: Stack(
                fit: StackFit.loose,
                children: <Widget>[
                  Container(
                    alignment: FractionalOffset.topCenter,
                    padding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        this._logo(),
                        this.buttonFBLogin(),
                        this.breakLineOr(),
                        this.buttonRegister(context),
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        InkWell(
                          child: Container(
                            height: Screen.convertHeightSize(54),
                            width: 50,
                            child: SvgPicture.asset(
                              'images/icon_delete_gray.svg',
                              height: 20,
                              fit: BoxFit.none,
                              color: Design.theme.colorPrimary,
                            ),
                          ),
                          onTap: () {
                            RouterService.instance.pop();
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _loginWithFB(BuildContext context) async {
    final result = await facebookLogin.logIn(['email']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        viewModel.setLoading(true);
        final token = result.accessToken.token;
        facebookLoginResult = result;
        viewModel.loginSocial(token, "facebook", () {
          viewModel.setLoading(false);
          RouterService.instance.navigateTo(MainScreenRoute.buildPath, clearStack: true);
        });
        break;
      case FacebookLoginStatus.cancelledByUser:
        viewModel.setLoading(false);
        break;
      case FacebookLoginStatus.error:
        viewModel.setLoading(false);
        break;
    }
  }

  void handleError(BaseError error) {
    if (error.code == 404) {
      RouterService.instance.navigateTo(RegisterSocialRoute.buildPath, data: facebookLoginResult);
    } else {
      _showAlert(error.message);
    }
  }

  void _showAlert(String message) {
    AlertDialog alertDialog = AlertDialog(
      content: Text(message),
      actions: <Widget>[
        FlatButton(
          child: Text("Close"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alertDialog;
        });
  }
}
