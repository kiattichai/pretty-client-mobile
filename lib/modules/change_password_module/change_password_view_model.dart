import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';


class ChangePasswordViewModel extends BaseViewModel {
  
  Function handleErrorChangePassword;
  bool isEnableButton = false;

  
  ChangePasswordViewModel();

  void changePassword(String oldPassword, String password, String confirmPassword, Function changePasswordSuccess) {
    setLoading(true);
    catchError(() async {
      final result = await di.profileRepository.changePassword(oldPassword, password, confirmPassword);
      changePasswordSuccess(result);
      setLoading(false);
    });
  }

  void enableButton(bool value) {
    isEnableButton = value;
    notifyListeners();
  }

  @override
  void onError(error) {
    super.onError(error);
    print(error);
    if (error is BaseError) {
      handleErrorChangePassword(error);
    }
  }

  void updatePassword(String text, String text2, String text3) {
    if (text.isEmpty || text2.isEmpty || text3.isEmpty) {
      isEnableButton = false;
      notifyListeners();
      return;
    }
    if (text2 != text3) {
      isEnableButton = false;
      notifyListeners();
      return;
    }
    if (text2 == text3) {
      isEnableButton = true;
      notifyListeners();
      return;
    }
  }
}