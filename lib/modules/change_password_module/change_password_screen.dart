import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/change_password_module/change_password_view_model.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/input_password_widget.dart';
import 'package:pretty_client_mobile/widgets/primary_button_widget.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({Key key}) : super(key: key);
  @override
  ChangePasswordState createState() {
    return ChangePasswordState();
  }
}

class ChangePasswordState extends BaseStateProvider<ChangePasswordScreen, ChangePasswordViewModel> {
  final _formKey = GlobalKey<FormState>();
  final focusOldPassword = FocusNode();
  final focusPassword = FocusNode();
  final focusConfirmPassword = FocusNode();
  final TextEditingController controllerOldPassword = TextEditingController();
  final TextEditingController controllerPassword = TextEditingController();
  final TextEditingController controllerConfirmPassword = TextEditingController();
  bool _validate = false;
  bool autoFocus = true;

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
        keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
        keyboardBarColor: Colors.grey[200],
        nextFocus: true,
        actions: [
          KeyboardAction(
            focusNode: focusOldPassword,
          ),
          KeyboardAction(
            focusNode: focusPassword,
          ),
          KeyboardAction(
            focusNode: focusConfirmPassword,
          ),
        ]);
  }

  @override
  void initState() {
    super.initState();
    viewModel = ChangePasswordViewModel();
    viewModel.handleErrorChangePassword = handleErrorChangePassword;
  }

  Widget titlePasswordInfo(BuildContext context, String title) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 30, right: 0),
      child: SizedBox(
        width: double.infinity,
        child: Container(
          child: Text(title,
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Design.theme.colorInactive,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              )),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return BaseWidget<ChangePasswordViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        color: Colors.transparent,
        progressIndicator: CircleLoading(),
        inAsyncCall: viewModel.loading,
        child: Scaffold(
          backgroundColor: Design.theme.colorMainBackground,
          resizeToAvoidBottomPadding: false,
          appBar: CustomHeaderDetail(
            height: Screen.convertHeightSize(55),
            title: appLocal.changePasswordTitle,
          ),
          body: KeyboardActions(
            isDialog: false,
            config: _buildConfig(context),
            child: Container(
              alignment: FractionalOffset.topCenter,
              color: Design.theme.colorMainBackground,
              padding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              child: SafeArea(
                child: SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        titlePasswordInfo(context, appLocal.changePasswordOldPassword),
                        InputPasswordWidget(
                          hintText: appLocal.changePasswordOldPasswordHint,
                          controllerPassword: controllerOldPassword,
                          focusPassword: focusOldPassword,
                          onChanged: (value) {
                            viewModel.updatePassword(controllerOldPassword.text,
                                controllerPassword.text, controllerConfirmPassword.text);
                          },
                          onSummitted: (value) {
                            FocusScope.of(context).requestFocus(focusPassword);
                          },
                          autoFocus: autoFocus,
                          autoValidate: _validate,
                        ),
                        titlePasswordInfo(context, appLocal.changePasswordNewPassword),
                        InputPasswordWidget(
                          hintText: appLocal.changePasswordNewPasswordHint,
                          controllerPassword: controllerPassword,
                          focusPassword: focusPassword,
                          onChanged: (value) {
                            viewModel.updatePassword(controllerOldPassword.text,
                                controllerPassword.text, controllerConfirmPassword.text);
                          },
                          onSummitted: (value) {
                            FocusScope.of(context).requestFocus(focusConfirmPassword);
                          },
                          autoFocus: false,
                          autoValidate: _validate,
                        ),
                        Text(appLocal.changePasswordNewPasswordHelp,
                            style: TextStyle(
                              color: Design.theme.colorInactive.withOpacity(0.8),
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            )),
                        SizedBox(
                          height: 10,
                        ),
                        InputPasswordWidget(
                          hintText: appLocal.changePasswordNewPasswordAgain,
                          controllerPassword: controllerConfirmPassword,
                          focusPassword: focusConfirmPassword,
                          onChanged: (value) {
                            viewModel.updatePassword(controllerOldPassword.text,
                                controllerPassword.text, controllerConfirmPassword.text);
                          },
                          onSummitted: (value) {
                            FocusScope.of(context).requestFocus(FocusNode());
                          },
                          autoFocus: false,
                          autoValidate: _validate,
                        ),
                        SizedBox(
                          height: 80,
                        ),
                        PrimaryButtonWidget(
                            buttonTitle: appLocal.commonConfirm,
                            onBtnPressed: !viewModel.isEnableButton
                                ? null
                                : () {
                                    focusConfirmPassword.unfocus(focusPrevious: false);
                                    if (_formKey.currentState.validate()) {
                                      // No any error in validation
                                      _formKey.currentState.save();
                                      FocusScope.of(context).requestFocus(FocusNode());
                                      _confirmPassword(model);
                                    } else {
                                      // validation error
                                      setState(() {
                                        _validate = true;
                                      });
                                    }
                                  })
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _confirmPassword(ChangePasswordViewModel model) {
    model.changePassword(controllerOldPassword.text, controllerPassword.text,
        controllerConfirmPassword.text, changePasswordSuccess);
  }

  void changePasswordSuccess(BaseResponseEntity baseResponse) {
    _showAlertSuccess(baseResponse.data.message);
  }

  void _showAlert(String message) {
    AlertDialog alertDialog = AlertDialog(
      content: Text(message),
      actions: <Widget>[],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alertDialog;
        });
  }

  void _showAlertSuccess(String message) {
    AlertDialog alertDialog = AlertDialog(
      content: Text(message),
      actions: <Widget>[
        FlatButton(
          child: Text("Ok"),
          onPressed: () {
            Navigator.of(context).pop(
              RouterService.instance.pop(),
            );
          },
        ),
      ],
      elevation: 24.0,
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alertDialog;
        });
  }

  bool validatePassword(String value) {
    print(value);
    if (value.isEmpty || value.length < 8) {
      return false;
    } else {
      return true;
    }
  }

  void handleErrorChangePassword(BaseError baseError) {
    _showAlert(baseError.message);
  }
}
