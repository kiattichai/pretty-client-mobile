import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/change_password_module/change_password_screen.dart';


class ChangePasswordRoute extends BaseRoute {
  static String buildPath = '/change_password';

  @override
  String get path => buildPath;

  @override
  Future<bool> hasPermission(params) async {
    return await Future.delayed(Duration(seconds: 1), () => true);
  }

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => ChangePasswordScreen());
  }
}
