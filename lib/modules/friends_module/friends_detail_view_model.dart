import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/booking_route.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/product_response_entity.dart';
import 'package:pretty_client_mobile/modules/login_module/login_route.dart';
import 'package:pretty_client_mobile/modules/main_module/main_screen_route.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/jwt_utils.dart';

class FriendsDetailViewModel extends BaseViewModel {
  int position = 1;
  bool isFavorite = false;
  Function handleErrorFavorite;
  int id;
  int selectVariantIndex = -1;
  List<ProductImage> productImage = [];
  List<ProductImage> productImageGallary = [];
  Product product;

  @override
  void postInit() {
    super.postInit();
    loadFavoriteList();
  }

  void loadFavoriteList() {
    if (!shouldLogin()) {
      final result = di.favoriteRepository.loadFavoriteList().firstWhere((model) {
        return model.product.id == this.id;
      });
      if (result == null) {
        isFavorite = false;
      } else {
        isFavorite = true;
      }
      notifyListeners();
    }
  }

  void updatePosition(int index) {
    position = index;
    print('scroll to $position');
    notifyListeners();
  }

  bool shouldLogin() {
    return di.sharePrefInterface.shouldLogin();
  }

  void clickFavorite(int id) {
    final share = di.sharePrefInterface;
    final isMember = JwtUtils().isMember(share.accessToken());
    if (isMember != null) {
      if (isMember) {
        catchError(() async {
          setLoading(true);
          if (isFavorite) {
            await di.favoriteRepository.removeFavorite(id);
          } else {
            await di.favoriteRepository.addFavorite(id);
          }
          isFavorite = !isFavorite;
          setLoading(false);
        });
      } else {
        RouterService.instance.navigateTo(MainScreenRoute.buildPath, clearStack: true, data: 6);
      }
    } else {
      RouterService.instance.navigateTo(MainScreenRoute.buildPath, clearStack: true, data: 6);
    }
  }

  void onClickBooking(Product product) async {
    final share = di.sharePrefInterface;
    if (!share.shouldLogin()) {
      final isMember = JwtUtils().isMember(share.accessToken());
      if (isMember != null) {
        if (isMember) {
          dynamic data = {
            'product': product,
            'variant_index': selectVariantIndex==-1?0:selectVariantIndex
          };
          RouterService.instance.navigateTo(BookingRoute.buildPath, data: data);
        } else {
          RouterService.instance.navigateTo(MainScreenRoute.buildPath, clearStack: true, data: 6);
        }
      } else {
        RouterService.instance.navigateTo(MainScreenRoute.buildPath, clearStack: true, data: 6);
      }
    } else {
      await RouterService.instance.navigateTo(LoginRoute.buildPath);
    }
  }

  void setSelectVariantIndex(val) {
    selectVariantIndex = selectVariantIndex==val?-1:val;
  }

  void setProductImage(List<ProductImage> images){
    position=1;
    productImage.clear();
    productImage.addAll(images);
    notifyListeners();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    if (error is BaseError) {
      handleErrorFavorite(error);
    }
  }
}
