import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/share_preference.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/product_response_entity.dart';

class FriendsViewModel extends BaseViewModel {
  final List<Product> friends = [];
  Map<String, dynamic> filter;
  ScrollController scrollController;
  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;
  int limit = 12;
  int page = 1;
  bool isComplete = false;

  FriendsViewModel() {
    filter = Map();
    refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  }

  @override
  void postInit() async {
    super.postInit();
    getFriends();
  }

  getDataFirebase(List<Product> record) async {
    record.forEach((model) {
      final itemRef = FirebaseDatabase.instance.reference().child('product/${model.id}');
      if (itemRef != null) {
        itemRef.onValue.listen((Event e) {
          var snapshot = e.snapshot;
          if (snapshot.value != null) {
            final data = Map<String, dynamic>.from(snapshot.value);
            if (data['status'] == 'on') {
              record.firstWhere((friend) {
                return friend.id == model.id;
              })
                ..onFloor = true;
              notifyListeners();
            }
            if (data['status'] == 'off') {
              record.firstWhere((friend) {
                return friend.id == model.id;
              })
                ..onFloor = false;
              notifyListeners();
            }
          }
        });
      }
    });
  }

  void getFriends({String query = ''}) {
    if (page == 1) {
      setLoading(true);
    }
    catchError(() async {
      ProductResponseData result = null;
      if (isComplete == false) {
        result = await di.productRepository.getProducts(query: query, limit: limit, page: page);
      }
      if (result !=null && result.record.length > 0 && result.pagination.currentPage <= result.pagination.lastPage) {
        await getDataFirebase(result.record);
        if(page==1){
          friends.clear();
        }
        friends.addAll(result.record);
        page++;
      } else {
        isComplete = true;
      }
//      final result = await di.productRepository.getProducts(query: query, limit: limit, page: page);
//      await getDataFirebase(result.record);
//      friends.clear();
//      friends.addAll(result.record);
      setLoading(false);
    });
  }

  void getFriendsWithFilter() {
    setLoading(true);
    catchError(() async {
      final result = await di.productRepository.getProductsWithFilter(filter);
      await getDataFirebase(result.record);
      friends.clear();
      friends.addAll(result.record);
      setLoading(false);
    });
  }

  void checkFilterResult(dynamic result) {
    print(result);
    if (result != null) {
      if (result is String) {
        page = 1;
        isComplete = false;
        filter.clear();
        getFriends();
      }
      if (result is Map) {
        print(result);
        filter = result;
        getFriendsWithFilter();
      }
    }
  }

  SharePrefInterface share() {
    return di.sharePrefInterface;
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
  }
}
