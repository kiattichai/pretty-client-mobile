import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/modules/friends_module/component/friends_grid_item.dart';
import 'package:pretty_client_mobile/modules/friends_module/component/position_grid.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_detail_route.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/product_response_entity.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';

class FriendsGridView extends StatelessWidget {
  final List<Product> products;

  const FriendsGridView({Key key, this.products}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverGrid(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisSpacing: 13,
          mainAxisSpacing: 1,
          crossAxisCount: 3,
          childAspectRatio: Screen.findRatioPixelScreen(230)),
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return InkWell(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
              RouterService.instance
                  .navigateTo(FriendsDetailRoute.buildPath, data: products[index]);
            },
            child: FriendsGridItem(
              products[index],
              position: findPosition(index),
              url: products[index].images.first.url ?? '',
            ),
          );
        },
        childCount: products.length,
      ),
    );
  }

  PositionGrid findPosition(int index) {
    var currentSlot = ((index / 3) + 1).toInt();
    if ((currentSlot * 3) - 3 == index) {
      return PositionGrid.LEFT;
    }
    if ((currentSlot * 3) - 2 == index) {
      return PositionGrid.CENTER;
    }
    if ((currentSlot * 3) - 1 == index) {
      return PositionGrid.RIGHT;
    }
    return PositionGrid.CENTER;
  }
}
