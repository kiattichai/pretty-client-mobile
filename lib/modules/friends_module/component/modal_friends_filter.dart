import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/modules/friends_module/component/custom_range_shape.dart';
import 'package:pretty_client_mobile/modules/friends_module/component/modal_friends_filter_view_model.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/range_component.dart';

class ModalFriendFilter extends StatefulWidget {
  final Map<String, dynamic> filter;

  ModalFriendFilter(this.filter);

  @override
  _ModalFriendFilterState createState() => _ModalFriendFilterState();
}

class _ModalFriendFilterState
    extends BaseStateProvider<ModalFriendFilter, ModalFriendsFilterViewModel> {
  Widget buildTitleAndButtonClose() {
    return Padding(
        padding: EdgeInsets.only(top: Screen.convertHeightSize(15)),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              child: Text(
                appLocal.filters,
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
              padding: EdgeInsets.only(left: 20),
            ),
            Padding(
              padding: EdgeInsets.only(right: 20),
              child: InkWell(
                onTap: () {
                  RouterService.instance.pop();
                },
                child: SvgPicture.asset(
                  'images/icon_close.svg',
                  color: Design.theme.primaryColor,
                ),
              ),
            )
          ],
        ));
  }

  Widget buildStatus() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding:
              EdgeInsets.only(left: 38, bottom: Screen.convertHeightSize(10)),
          child: Container(
            child: Text(
              'Status',
              style: TextStyle(
                color: Color(0xffaaaaaa),
                fontSize: 18,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            InkWell(
              onTap: () {
                viewModel.updateStatus(true);
              },
              child: Container(
                  child: Center(
                    child: Text("Online",
                        style: viewModel.isOnline
                            ? Design.text.filterStatusActive
                            : Design.text.filterStatusInActive),
                  ),
                  width: Screen.convertWidthSize(151),
                  height: Screen.convertWidthSize(44),
                  decoration: viewModel.isOnline
                      ? Design.color.boxFilterActive
                      : Design.color.boxFilterInActive),
            ),
            SizedBox(
              width: 4,
            ),
            InkWell(
                onTap: () {
                  viewModel.updateStatus(false);
                },
                child: Container(
                    child: Center(
                      child: new Text("Offline",
                          style: !viewModel.isOnline
                              ? Design.text.filterStatusActive
                              : Design.text.filterStatusInActive),
                    ),
                    width: Screen.convertWidthSize(151),
                    height: Screen.convertWidthSize(44),
                    decoration: !viewModel.isOnline
                        ? Design.color.boxFilterActive
                        : Design.color.boxFilterInActive))
          ],
        )
      ],
    );
  }

  Widget buildAge() {
    return Padding(
      padding: EdgeInsets.only(
          left: 25, right: 25, top: Screen.convertHeightSize(15)),
      child: RangeComponent(
        unitTitle: 'ปี',
        rangeValues: viewModel.rangeValuesAge,
        rangeTitle: appLocal.friendInfoAge,
        startValue: viewModel.rangeValuesAge.start.round(),
        endValue: viewModel.rangeValuesAge.end.round(),
        maxValue: 60.0,
        minValue: 0.0,
        onValueChange: (values) {
          viewModel.updateRangeValueAge(values);
        },
      ),
    );
  }

  Widget buildHeight() {
    return Padding(
      padding: EdgeInsets.only(left: 25, right: 25),
      child: RangeComponent(
        unitTitle: 'cm',
        rangeValues: viewModel.rangeValuesHeight,
        rangeTitle: appLocal.friendInfoHeight,
        startValue: viewModel.rangeValuesHeight.start.round(),
        endValue: viewModel.rangeValuesHeight.end.round(),
        maxValue: 200.0,
        minValue: 0.0,
        onValueChange: (values) {
          viewModel.updateRangeValueHeight(values);
        },
      ),
    );
  }

  Widget buildWeight() {
    return Padding(
      padding: EdgeInsets.only(left: 25, right: 25),
      child: RangeComponent(
        unitTitle: 'kg',
        rangeValues: viewModel.rangeValuesWeight,
        rangeTitle: appLocal.friendInfoWeight,
        startValue: viewModel.rangeValuesWeight.start.round(),
        endValue: viewModel.rangeValuesWeight.end.round(),
        maxValue: 80.0,
        minValue: 0.0,
        onValueChange: (values) {
          viewModel.updateRangeValueWeight(values);
        },
      ),
    );
  }

  Widget buildChest() {
    return Padding(
      padding: EdgeInsets.only(left: 25, right: 25),
      child: RangeComponent(
        unitTitle: 'kg',
        rangeValues: viewModel.rangeValuesChest,
        rangeTitle: appLocal.friendInfoChest,
        startValue: viewModel.rangeValuesChest.start.round(),
        endValue: viewModel.rangeValuesChest.end.round(),
        maxValue: 50.0,
        minValue: 0.0,
        onValueChange: (values) {
          viewModel.updateRangeValueChest(values);
        },
      ),
    );
  }

  Widget buildWaist() {
    return Padding(
      padding: EdgeInsets.only(left: 25, right: 25),
      child: RangeComponent(
        unitTitle: 'kg',
        rangeValues: viewModel.rangeValuesWaist,
        rangeTitle: appLocal.friendInfoWaist,
        startValue: viewModel.rangeValuesWaist.start.round(),
        endValue: viewModel.rangeValuesWaist.end.round(),
        maxValue: 50.0,
        minValue: 0.0,
        onValueChange: (values) {
          viewModel.updateRangeValueWaist(values);
        },
      ),
    );
  }

  Widget buildHip() {
    return Padding(
      padding: EdgeInsets.only(left: 25, right: 25),
      child: RangeComponent(
        unitTitle: 'kg',
        rangeValues: viewModel.rangeValuesHip,
        rangeTitle: appLocal.friendInfoHip,
        startValue: viewModel.rangeValuesHip.start.round(),
        endValue: viewModel.rangeValuesHip.end.round(),
        maxValue: 50.0,
        minValue: 0.0,
        onValueChange: (values) {
          viewModel.updateRangeValueHip(values);
        },
      ),
    );
  }

//  Widget buildChest() {
//    return Padding(
//      padding: EdgeInsets.only(left: 25, right: 25, top: Screen.convertHeightSize(15)),
//      child: Column(
//        crossAxisAlignment: CrossAxisAlignment.start,
//        children: <Widget>[
//          Padding(
//            padding: EdgeInsets.only(left: 13, bottom: 10),
//            child: Text(
//              "Chest",
//              textAlign: TextAlign.left,
//              style: TextStyle(
//                color: Color(0xffaaaaaa),
//                fontSize: 18,
//                fontWeight: FontWeight.w400,
//                fontStyle: FontStyle.normal,
//              ),
//            ),
//          ),
//          Stack(
//            children: <Widget>[
//              Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: viewModel.rangeValuesChest.start.round(),
//                    child: SizedBox(),
//                  ),
//                  Container(
//                    width: 54,
//                    child: Text(
//                      "${viewModel.rangeValuesChest.start.round()} inc",
//                      textAlign: TextAlign.center,
//                      style: TextStyle(color: Colors.white),
//                    ),
//                  ),
//                  Expanded(
//                    flex: 60 - viewModel.rangeValuesChest.start.round(),
//                    child: SizedBox(),
//                  ),
//                ],
//              ),
//              Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: viewModel.rangeValuesChest.end.round(),
//                    child: SizedBox(),
//                  ),
//                  Container(
//                    width: 54,
//                    child: Text("${viewModel.rangeValuesChest.end.round()} inc",
//                        textAlign: TextAlign.center, style: TextStyle(color: Colors.white)),
//                  ),
//                  Expanded(
//                    flex: 60 - viewModel.rangeValuesChest.end.round(),
//                    child: SizedBox(),
//                  ),
//                ],
//              ),
//            ],
//          ),
//          SliderTheme(
//            data: SliderThemeData(
//              inactiveTrackColor: Colors.black,
//              activeTrackColor: Color(0xfff7b500),
//              thumbColor: Colors.white,
//            ),
//            child: RangeSlider(
//              values: viewModel.rangeValuesChest,
//              min: 0.0,
//              max: 60.0,
//              divisions: 1000,
//              onChanged: (RangeValues values) {
//                viewModel.updateRangeValueChest(values);
//              },
//            ),
//          ),
//          SizedBox(
//            height: 4,
//          )
//        ],
//      ),
//    );
//  }

  Widget buildButton() {
    return Container(
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.only(
          left: Screen.convertWidthSize(35),
          right: Screen.convertWidthSize(35),
          bottom: 25),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          InkWell(
            onTap: () {
              viewModel.reset();
            },
            child: Container(
                child: Center(
                  child: Text(appLocal.reset,
                      style: TextStyle(
                        color: Color(0xffbfbfbf),
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                      )),
                ),
                width: Screen.convertWidthSize(103),
                height: Screen.convertWidthSize(44),
                decoration: new BoxDecoration(
                    color: Color(0xff363a3f),
                    borderRadius: BorderRadius.circular(10))),
          ),
          InkWell(
              onTap: () {
                viewModel.apply();
              },
              child: Container(
                  child: Center(
                    child: Text(appLocal.apply,
                        style: TextStyle(
                          color: Color(0xff000000),
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          fontStyle: FontStyle.normal,
                        )),
                  ),
                  width: Screen.convertWidthSize(182),
                  height: Screen.convertWidthSize(44),
                  decoration: new BoxDecoration(
                      color: Design.theme.colorPrimary,
                      borderRadius: BorderRadius.circular(10))))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    viewModel = ModalFriendsFilterViewModel(widget.filter);
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    return BaseWidget(
      model: viewModel,
      builder: (context, model, child) {
        return Container(
          decoration: new BoxDecoration(
            color: Color(0xff1d1d1d),
            border: Border.all(color: Color(0xff343434), width: 1),
            borderRadius: BorderRadius.vertical(top: Radius.circular(32)),
            boxShadow: [
              BoxShadow(
                  color: Color(0x35000000),
                  offset: Offset(0, 2),
                  blurRadius: 5,
                  spreadRadius: 0)
            ],
          ),
          width: double.infinity,
          child: Column(
            children: <Widget>[
              buildTitleAndButtonClose(),
//              Padding(
//                padding: EdgeInsets.only(
//                    top: Screen.convertHeightSize(8), bottom: Screen.convertHeightSize(8)),
//                child: Container(
//                    height: 1,
//                    decoration:
//                        BoxDecoration(border: Border.all(color: Color(0xff101010), width: 1))),
//              ),
//              buildStatus(),
              buildAge(),
//              Padding(
//                padding: EdgeInsets.only(bottom: Screen.convertHeightSize(8)),
//                child: Container(
//                    height: 1,
//                    decoration:
//                        BoxDecoration(border: Border.all(color: Color(0xff313131), width: 1))),
//              ),
              buildHeight(),
//              Padding(
//                padding: EdgeInsets.only(bottom: Screen.convertHeightSize(8)),
//                child: Container(
//                    height: 1,
//                    decoration:
//                        BoxDecoration(border: Border.all(color: Color(0xff313131), width: 1))),
//              ),
              buildWeight(),
              Padding(
                padding: EdgeInsets.only(bottom: Screen.convertHeightSize(16)),
                child: Container(
                  height: 15,
                ),
              ),
              buildChest(),
              buildWaist(),
              buildHip(),

              Expanded(
                child: buildButton(),
              )
            ],
          ),
        );
      },
    );
  }
}
