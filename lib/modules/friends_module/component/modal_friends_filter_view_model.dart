import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/services/router_service.dart';

class ModalFriendsFilterViewModel extends BaseViewModel {
  bool isOnline;
  RangeValues rangeValuesAge;
  RangeValues rangeValuesHeight;
  RangeValues rangeValuesWeight;
  RangeValues rangeValuesChest;
  RangeValues rangeValuesWaist;
  RangeValues rangeValuesHip;

  ModalFriendsFilterViewModel(Map<String, dynamic> filter) {
    isOnline = true;
    if (filter == null || filter.isEmpty) {
      rangeValuesAge = RangeValues(18, 40);
      rangeValuesHeight = RangeValues(120, 160);
      rangeValuesWeight = RangeValues(20, 60);
      rangeValuesChest = RangeValues(20, 40);
      rangeValuesWaist = RangeValues(20, 40);
      rangeValuesHip = RangeValues(20, 40);
    } else {
      setUpFilter(filter);
    }
  }

  void updateStatus(bool _status) {
    isOnline = _status;
    notifyListeners();
  }

  void updateRangeValueAge(RangeValues values) {
    rangeValuesAge = values;
    notifyListeners();
  }

  void updateRangeValueHeight(RangeValues values) {
    rangeValuesHeight = values;
    notifyListeners();
  }

  void updateRangeValueWeight(RangeValues values) {
    rangeValuesWeight = values;
    notifyListeners();
  }

  void updateRangeValueChest(RangeValues values) {
    rangeValuesChest = values;
    notifyListeners();
  }

  void updateRangeValueWaist(RangeValues values) {
    rangeValuesWaist = values;
    notifyListeners();
  }

  void updateRangeValueHip(RangeValues values) {
    rangeValuesHip = values;
    notifyListeners();
  }

  void reset() {
    RouterService.instance.pop(data: 'reset');
  }

  void apply() {
    final result = {
      'age': convertToStringApi(rangeValuesAge),
      'status': isOnline,
      'weight': convertToStringApi(rangeValuesWeight),
      'height': convertToStringApi(rangeValuesHeight),
      'chest': convertToStringApi(rangeValuesChest),
      'waist': convertToStringApi(rangeValuesWaist),
      'hip': convertToStringApi(rangeValuesHip),
    };
    RouterService.instance.pop(data: result);
  }

  void setUpFilter(Map<String, dynamic> filter) {
    rangeValuesAge = convertStringToRangeValues(filter['age']);
    rangeValuesWeight = convertStringToRangeValues(filter['weight']);
    rangeValuesHeight = convertStringToRangeValues(filter['height']);
    rangeValuesChest = convertStringToRangeValues(filter['chest']);
    rangeValuesWaist = convertStringToRangeValues(filter['waist']);
    rangeValuesHip = convertStringToRangeValues(filter['hip']);
    isOnline = filter['status'];
    notifyListeners();
  }

  convertToStringApi(RangeValues rangeValues) {
    final start = rangeValues.start.round();
    final end = rangeValues.end.round();
    return '$start|$end';
  }

  convertStringToRangeValues(String data) {
    final list = data.split('|');
    if (list.isNotEmpty && list.length == 2) {
      final start = double.parse(list[0]);
      final end = double.parse(list[1]);
      return RangeValues(start, end);
    } else {
      return RangeValues(0, 0);
    }
  }
}
