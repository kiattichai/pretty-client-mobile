import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/modules/friends_module/component/position_grid.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/friends_response_entity.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/product_response_entity.dart';

class FriendsGridItem extends StatelessWidget {
  final String url;
  final PositionGrid position;
  final Product product;

  const FriendsGridItem(this.product, {Key key, @required this.url, this.position})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Stack(
          fit: StackFit.loose,
          children: <Widget>[
            Container(
                child: ClipRRect(
                    borderRadius: new BorderRadius.circular(0.0),
                    child: Hero(
                      child: CachedNetworkImage(
                        imageUrl: url,
                        placeholder: (context, _) {
                          return Container();
                        },
                      ),
                      tag: 'photo${product.id}',
                    )),
                decoration: new BoxDecoration(
                  color: Design.theme.colorMainBackground,
                  borderRadius: BorderRadius.circular(0.0),
                  boxShadow: [
                    BoxShadow(
                        color: Color(0x40000000),
                        offset: Offset(0, 9),
                        blurRadius: 10,
                        spreadRadius: 0)
                  ],
                )),
            Positioned(
              right: 5,
              bottom: 2,
              child: SvgPicture.asset(
                product.displayOnFloor() ? 'images/status_online.svg' : 'images/status_offline.svg',
              ),
            )
          ],
        ),
        SizedBox(height: 5),
        Text(
          product.name,
          style: TextStyle(
            color: Design.theme.colorInactive,
            fontSize: 14,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
            letterSpacing: 0,
          ),
          maxLines: 2,
        ),
      ],
    ));
  }
}
