import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/env_config.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_detail_view_model.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/product_response_entity.dart';
import 'package:pretty_client_mobile/modules/login_module/login_route.dart';
import 'package:pretty_client_mobile/services/router_service.dart';

import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'dart:math' as math;

import 'package:video_player/video_player.dart';

class FriendsDetailScreen extends StatefulWidget {
  final Product product;

  const FriendsDetailScreen({Key key, this.product}) : super(key: key);

  @override
  FriendsDetailScreenState createState() => FriendsDetailScreenState();
}

class FriendsDetailScreenState
    extends BaseStateProvider<FriendsDetailScreen, FriendsDetailViewModel> {
  PanelController panelController;
  ValueNotifier<double> notifier;
  double sildePosition = 0.0;
  var visible = false;
  final numberFormat = new NumberFormat("#,###.##", "en_US");
  VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    viewModel = FriendsDetailViewModel()
      ..handleErrorFavorite = handleErrorFavorite
      ..productImage.addAll(widget.product.images)
      ..productImageGallary.addAll(widget.product.images)
      ..id = widget.product.id;
    notifier = ValueNotifier<double>(0);
    panelController = PanelController();

    _controller = VideoPlayerController.network(
        'https://tcc-apidocs.s3-ap-southeast-1.amazonaws.com/video/35dara.mp4');
    _controller.addListener(() {
      setState(() {});
    });
    _controller.setLooping(true);
    _controller.initialize();
  }

  handleErrorFavorite(BaseError error) {
    print(error.message);
  }

  @override
  void dispose() {
    _controller.dispose();
    notifier?.dispose();
    super.dispose();
  }

  Widget iconBack() {
    return Align(
      alignment: Alignment(-0.97, -0.93),
      child: InkWell(
        onTap: () {
          RouterService.instance.pop();
        },
        child: Container(
          decoration: BoxDecoration(
            color: Color(0x7f1d1d1d),
            shape: BoxShape.circle,
          ),
          width: 40,
          height: 40,
          child: Center(
            child: Container(
              height: 21,
              child: SvgPicture.asset(
                'images/icon_arrow_left.svg',
                color: Design.theme.colorPrimary,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget mixIcon() {
    return Stack(
      children: <Widget>[iconIndicator(), iconFavorite()],
    );
  }

  Widget iconIndicator() {
    return Align(
      alignment: Alignment(0, -0.90),
      child: Container(
          child: Center(
            child:
                Text("${viewModel.position}/${viewModel.productImage.length}",
                    style: TextStyle(
                      color: Design.theme.colorInactive,
                      fontSize: 12,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                    )),
          ),
          width: 47,
          height: 23,
          decoration: new BoxDecoration(
              color: Color(0x7f1d1d1d),
              borderRadius: BorderRadius.circular(11.5))),
    );
  }

  Widget iconFavorite() {
    return Align(
      alignment: Alignment(0.93, -0.93),
      child: InkWell(
        onTap: () async {
          if (viewModel.shouldLogin()) {
            final result =
                await RouterService.instance.navigateTo(LoginRoute.buildPath);
            if (result == true) {
              viewModel.loadFavoriteList();
            }
          } else {
            viewModel.clickFavorite(widget.product.id);
          }
        },
        child: Container(
          decoration: BoxDecoration(
            color: Color(0x7f1d1d1d),
            shape: BoxShape.circle,
          ),
          width: 40,
          height: 40,
          child: Center(
            child: SvgPicture.asset(
              viewModel.isFavorite
                  ? 'images/icon_heart_fill.svg'
                  : 'images/icon_heart_empty.svg',
              fit: BoxFit.none,
              width: 21,
              color: Color(0xffe4c078),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildPhotoSlider() {
    return CarouselSlider.builder(
      height: Screen.height,
      enableInfiniteScroll: viewModel.productImage.length > 1,
      viewportFraction: 1.0,
      autoPlay: false,
      onPageChanged: (index) {
        viewModel.updatePosition(index + 1);
      },
      itemCount: viewModel.productImage.length,
      itemBuilder: (BuildContext context, int itemIndex) {
        return Container(
          width: Screen.width,
          child: CachedNetworkImage(
            filterQuality: FilterQuality.high,
            imageUrl: viewModel.productImage[itemIndex].getImgUrlResize(
                EnvConfig.instance.resUrl, Screen.width, Screen.height),
          ),
        );
      },
    );
  }

  Widget buildFieldInfo(String title, String data) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Stack(
        fit: StackFit.loose,
        children: <Widget>[
          Align(
            alignment: Alignment(-1, 0),
            child: Text(title,
                style: TextStyle(
                  fontFamily: 'Sarabun',
                  decoration: TextDecoration.none,
                  color: Design.theme.colorText2,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0,
                )),
          ),
          Align(
            alignment: Alignment(0, 0),
            child: Text(
              ':',
              style: TextStyle(
                fontFamily: 'Sarabun',
                decoration: TextDecoration.none,
                color: Design.theme.colorText2,
                fontSize: 14,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            ),
          ),
          Align(
            child: Text(data,
                style: TextStyle(
                  fontFamily: 'Sarabun',
                  decoration: TextDecoration.none,
                  color: Design.theme.colorInactive,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0,
                )),
            alignment: Alignment(0.7, 0),
          )
        ],
      ),
    );
  }

  Widget lineBottom() {
    return
      Padding(
          padding: EdgeInsets.only(left: 8, right: 8),
        child:
      Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: Design.theme.colorLine.withOpacity(0.45),
          ),
        ),
      ),
    ));
  }

  Widget buildFriendInfo() {
    return Padding(
      padding: EdgeInsets.only(
        left: 15,
        right: 15,
        top:
            widget.product.option ? 15 : widget.product.price.min > 0 ? 15 : 80,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: buildFieldInfo(
                    appLocal.friendInfoAge, widget.product.displayAge()),
              ),
              Expanded(
                flex: 1,
                child: buildFieldInfo(
                    appLocal.friendInfoWeight, widget.product.displayWeight()),
              ),
              Expanded(
                  flex: 1,
                  child: buildFieldInfo(appLocal.friendInfoHeight,
                      widget.product.displayHeight())),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: buildFieldInfo(
                    appLocal.friendInfoChest, widget.product.displayChest()),
              ),
              Expanded(
                flex: 1,
                child: buildFieldInfo(appLocal.friendInfoWaist,
                    widget.product.displayWaistline()),
              ),
              Expanded(
                  flex: 1,
                  child: buildFieldInfo(
                      appLocal.friendInfoHip, widget.product.displayHip())),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            child: widget.product.option
                ? buildProductVariants()
                : Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      widget.product.price.min > 0
                          ? Text(numberFormat.format(widget.product.price.min),
                              style: TextStyle(
                                fontFamily: 'Sarabun',
                                decoration: TextDecoration.none,
                                color: Design.theme.colorPrimary,
                                fontSize: 22,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              ))
                          : Text(""),
                      SizedBox(
                        width: 5,
                      ),
                      Container(
                        child: widget.product.price.min > 0
                            ? SvgPicture.asset('images/icon_coin.svg')
                            : null,
                      )
                    ],
                  ),
          ),
        ],
      ),
    );
  }

  Widget buildProductVariants() {
    return Column(
      children: <Widget>[
        lineBottom(),
        SizedBox(
          height: 8,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 10),
              alignment: Alignment.topLeft,
              child: Text(
                'เลือกชุด',
                style: TextStyle(
                  fontFamily: 'Sarabun',
                  decoration: TextDecoration.none,
                  color: Design.theme.colorActive,
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text(
                    viewModel.selectVariantIndex == -1
                        ? widget.product.price.min > 0
                            ? numberFormat.format(widget.product.price.min)
                            : ""
                        : widget.product.variants[viewModel.selectVariantIndex]
                                    .price >
                                0
                            ? numberFormat.format(widget.product
                                .variants[viewModel.selectVariantIndex].price)
                            : "",
                    style: TextStyle(
                      fontFamily: 'Sarabun',
                      decoration: TextDecoration.none,
                      color: Design.theme.colorPrimary,
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                    )),
                SizedBox(
                  width: 5,
                ),
                viewModel.selectVariantIndex == -1
                    ? widget.product.price.min > 0
                        ? Container(child: SvgPicture.asset('images/icon_coin.svg',width: 18))
                        : Container()
                    : widget.product.variants[viewModel.selectVariantIndex].price > 0
                        ? Container(child: SvgPicture.asset('images/icon_coin.svg',width: 18))
                        : Container()
              ],
            ),
          ],
        ),
        SizedBox(
          height: 8,
        ),
        Container(
          margin: EdgeInsets.only(bottom: 5),
          padding: EdgeInsets.only(left: 10),
          height: 120,
          child: ListView.builder(
            itemCount: widget.product.variants.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, i) {
              return GestureDetector(
                onTap: () {
                  viewModel.setSelectVariantIndex(i);
                  if (viewModel.selectVariantIndex != -1) {
                    ProductImage img = ProductImage();
                    List<ProductImage> images = [];
                    img.id = widget.product.variants[i].image.id;
                    img.storeId = widget.product.variants[i].image.storeId;
                    img.name = widget.product.variants[i].image.name;
                    img.albumId = widget.product.variants[i].image.albumId;
                    img.width = widget.product.variants[i].image.width;
                    img.height = widget.product.variants[i].image.height;
                    img.mime = widget.product.variants[i].image.mime;
                    img.size = widget.product.variants[i].image.size;
                    img.url = widget.product.variants[i].image.url;
                    images.add(img);
                    viewModel.setProductImage(images);
                  } else {
                    viewModel.setProductImage(viewModel.productImageGallary);
                  }
                },
                child: Container(
                  margin: EdgeInsets.only(right: 12),
                  decoration: BoxDecoration(
                      color: viewModel.selectVariantIndex == i
                          ? Design.theme.primaryColor
                          : Colors.transparent,
                      border: Border.all(
                        width: 2,
                        color: viewModel.selectVariantIndex == i
                            ? Design.theme.primaryColor
                            : Colors.transparent,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    child: CachedNetworkImage(
                      filterQuality: FilterQuality.high,
                      imageUrl: widget.product.variants[i].image
                          .getImgUrlResize(EnvConfig.instance.resUrl, 180, 200),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget buttonBookNow(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: MaterialButton(
          color: Design.theme.colorPrimary,
          disabledColor: Color(0xff636362),
          disabledTextColor: Color(0xffbfbfbf),
          minWidth: Screen.width - 40,
          height: 48,
          child: Text(
            appLocal.appointment2,
            style: Design.text.textButton,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(26),
          ),
          onPressed: widget.product.option
              ? (viewModel.selectVariantIndex == -1 ? null : onclickBooking)
              : onclickBooking
//
//
//        {
//          viewModel.onClickBooking(widget.product);
//        },
          ),
    );
  }

  void onclickBooking() {
    viewModel.onClickBooking(widget.product);
  }

  Widget rowStatus() {
    return Padding(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(widget.product.name,
                          style: TextStyle(
                            fontFamily: 'Sarabun',
                            decoration: TextDecoration.none,
                            color: Design.theme.colorActive,
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          )),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        child: SvgPicture.asset(widget.product.displayOnFloor()
                            ? 'images/status_online.svg'
                            : 'images/status_offline.svg'),
                      ),
                    ],
                  ),
                  Text('ID : ${widget.product.code}',
                      style: TextStyle(
                        fontFamily: 'Sarabun',
                        decoration: TextDecoration.none,
                        color: Design.theme.colorText2,
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0,
                      )),
                ],
              ),
              SizedBox(
                width: 10,
              ),
            ],
          ),
//          ratingStatus(4)
          ratings(widget.product)
        ],
      ),
      padding: EdgeInsets.only(left: 25, right: 25),
    );
  }

//  Widget ratingStatus(int rate) {
//    final result = List.generate(5, (value) {
//      final shouldFillColor = value < rate;
//      return SvgPicture.asset(
//        'images/icon_star.svg',
//        color: shouldFillColor
//            ? Design.theme.colorPrimary
//            : Design.theme.colorText,
//      );
//    });
//    return Container(
//      child: Row(
//        children: result,
//      ),
//    );
//  }

  Widget ratings(Product product) {
    if(product.feedbacks.summary > 0) {
      return RatingBarIndicator(
        rating: product.feedbacks.summary,
        itemCount: 5,
        itemSize: 22.0,
        unratedColor: Design.theme.colorText,
        itemBuilder: (context, _) =>
            Icon(
              Icons.star,
              color: Design.theme.primaryColor,
            ),
        // allowHalfRating: true,

        // onRatingUpdate: (rating) => print('rating')
      );
    } else {
      return Container(
        child: Text(
          appLocal.noRating,
          style: TextStyle(
          fontFamily: 'Sarabun',
          decoration: TextDecoration.none,
          color: Design.theme.colorActive.withOpacity(0.6),
          fontSize: 15,
          fontWeight: FontWeight.w600,
          fontStyle: FontStyle.normal,
          letterSpacing: 0,
        ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    return BaseWidget<FriendsDetailViewModel>(
      model: viewModel,
      builder: (context, model, child) => SlidingUpPanel(
        onPanelSlide: (value) {
          final result = 1 * math.pi * value;
          if (result.isInfinite) {
            notifier.value = 0.0;
          } else {
            notifier?.value = result;
          }
        },
        boxShadow: null,
        color: Colors.transparent,
        key: Key('panel'),
        controller: panelController,
        maxHeight: widget.product.option
            ? 430
            : widget.product.price.min > 0 ? 300 : 270,
        minHeight: 50,
        panel: Container(
          child: Stack(
            children: <Widget>[
              Align(
                child: FlatButton(
                  onPressed: () {
                    if (panelController.isPanelOpen) {
                      panelController.close();
                    } else {
                      panelController.open();
                    }
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Design.theme.colorBox,
                        border: Border.all(color: Design.theme.colorBox),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10.0),
                            topRight: Radius.circular(10.0))),
                    width: 90,
                    height: 45,
                  ),
                ),
                alignment: Alignment(0, -1),
              ),
              Align(
                alignment: Alignment(0, -1),
                child: FlatButton(
                  onPressed: () {
                    if (panelController.isPanelOpen) {
                      panelController.close();
                    } else {
                      panelController.open();
                    }
                  },
                  child: AnimatedBuilder(
                    animation: notifier,
                    builder: (context, _) {
                      return Transform.rotate(
                        angle: notifier.value,
                        child: SvgPicture.asset(
                          ('images/icon_arrow_top.svg'),
                          fit: BoxFit.none,
                          width: 50,
                        ),
                      );
                    },
                  ),
                ),
              ),
              Align(
                child: Container(
                  child: Stack(
                    children: <Widget>[
                      Align(
                        alignment: widget.product.option
                            ? Alignment(0, -0.86)
                            : Alignment(0, -0.75),
                        child: rowStatus(),
                      ),
                      Align(
                        child: buttonBookNow(context),
                        alignment: Alignment(0, 0.8),
                      )
                    ],
                  ),
                  decoration: BoxDecoration(
                      color: Design.theme.colorBox,
                      border: Border.all(color: Design.theme.colorBox),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          topRight: Radius.circular(30.0))),
                  height: widget.product.option
                      ? 400
                      : widget.product.price.min > 0 ? 268 : 240,
                  width: Screen.width,
                ),
                alignment: Alignment(0, 1),
              ),
              Align(
                child: buildFriendInfo(),
                alignment: Alignment(0, 0),
              ),
            ],
          ),
        ),
        body: Scaffold(
          backgroundColor: Colors.black,
          body: Builder(builder: (context) {
            if (visible) {
              return AspectRatio(
                aspectRatio: _controller.value.aspectRatio,
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: <Widget>[
                    VideoPlayer(_controller),
                    ClosedCaption(text: _controller.value.caption.text),
                    _PlayPauseOverlay(controller: _controller),
                    VideoProgressIndicator(_controller, allowScrubbing: true),
                  ],
                ),
              );
            } else {
              return Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment(0, 0),
                    child: Hero(
                      child: CachedNetworkImage(
                        imageUrl: widget.product.images.first.getImgUrlResize(
                            EnvConfig.instance.resUrl,
                            Screen.width,
                            Screen.height),
                        color: Colors.transparent,
                      ),
                      tag: 'photo${widget.product.id}',
                    ),
                  ),
                  InkWell(
                    child: buildPhotoSlider(),
                    onTap: () {
                      if (panelController.isPanelOpen) {
                        panelController.close();
                      }
                    },
                  ),
                  iconBack(),
                  mixIcon()
                ],
              );
            }
          }),
        ),
      ),
    );
  }
}

class _PlayPauseOverlay extends StatelessWidget {
  const _PlayPauseOverlay({Key key, this.controller}) : super(key: key);

  final VideoPlayerController controller;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: Duration(milliseconds: 50),
          reverseDuration: Duration(milliseconds: 200),
          child: controller.value.isPlaying
              ? SizedBox.shrink()
              : Container(
                  color: Colors.black26,
                  child: Center(
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 100.0,
                    ),
                  ),
                ),
        ),
        GestureDetector(
          onTap: () {
            controller.value.isPlaying ? controller.pause() : controller.play();
          },
        ),
      ],
    );
  }
}
