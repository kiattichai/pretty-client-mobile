import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_screen.dart';

class FriendsRoute extends BaseRoute {
  static String buildPath = '/friends';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
      builder: (context) => FriendsScreen()
    );
  }
}
