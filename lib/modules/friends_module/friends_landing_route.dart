import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_landing_screen.dart';

class FriendsLandingRoute extends BaseRoute {
  static String buildPath = '/friends_landing';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
      builder: (context) => FriendsLandingScreen()
    );
  }
}
