import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_view_model.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_filter.dart';
import 'package:pretty_client_mobile/widgets/custome_header.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FriendsLandingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FriendsLandingScreenState();
  }
}

class FriendsLandingScreenState extends BaseStateProvider<FriendsLandingScreen, FriendsViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = FriendsViewModel();
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return BaseWidget(
      model: viewModel,
      builder: (context, model, child) => Scaffold(
        appBar: CustomHeader(
          height: Screen.convertHeightSize(55),
          title: appLocal.friendHeader,
        ),
        body: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.only(top: 80.0, left: 64.0, right: 64.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SvgPicture.asset(
                "images/icons_lock_gold.svg",
                width: 64.0,
                height: 64.0,
              ),
              SizedBox(height: 28.0),
              Container(
                child: Text('สงวนสิทธิ์สำหรับลูกค้าที่เป็น Member',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Sarabun',
                      color: Color(0xffa4a4a4),
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    )),
              ),
              Container(
                  padding: EdgeInsets.only(top: 5.0),
                  child: RichText(
                    text: TextSpan(
                        text: 'ร้านค้า 35 ดาราโชว์',
                        style: TextStyle(
                          fontFamily: 'Sarabun',
                          color: Color(0xffffffff),
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.normal,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                              text: ' เท่านั้น',
                              style: TextStyle(
                                fontFamily: 'Sarabun',
                                color: Color(0xffa4a4a4),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ))
                        ]),
                  )),
              Container(
                padding: EdgeInsets.only(top: 30.0),
                child: Text("สนใจสมัคร Member",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: 'Sarabun',
                        color: Color(0xffa4a4a4),
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal)),
              ),
              Container(
                child: Text("โทร 088 533 3535",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: 'Sarabun',
                        color: Color(0xffa4a4a4),
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
