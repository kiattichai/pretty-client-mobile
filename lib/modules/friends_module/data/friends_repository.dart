import 'dart:async';

import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/friends_module/data/friends_api.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/friends_response_entity.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/product_response_entity.dart';

class FriendsRepository extends BaseRepository {
  final FriendsApi api;
  @provide
  FriendsRepository(this.api);

  Future<FriendsResponseData> getFriends({String query = ''}) async {
    final response = await api.getFriends(BaseErrorEntity.badRequestToModelError, query: query);
    return response.data;
  }

  Future<FriendsResponseData> getFriendsPopular() async {
    final response = await api.getFriendsPopular(BaseErrorEntity.badRequestToModelError);
    return response.data;
  }

  Future<FriendsResponseData> getFriendsWithFilter(Map<String, dynamic> parameter) async {
    final response =
        await api.getFriendsWithFilter(BaseErrorEntity.badRequestToModelError, parameter);
    return response.data;
  }

  Future<Product> getFriendById(String id) async {
    final response = await api.getFriendsDetail(id, BaseErrorEntity.badRequestToModelError);
    return response.data;
  }
}
