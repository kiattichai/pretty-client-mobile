import 'dart:async';

import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/friends_module/data/product_api.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/product_response_entity.dart';

class ProductRepository extends BaseRepository {
  final ProductApi api;
  @provide
  ProductRepository(this.api);

  Future<ProductResponseData> getProducts({String query = '', int limit=12, int page=1}) async {
    final response = await api.getProducts(BaseErrorEntity.badRequestToModelError, query: query, limit: limit, page: page);
    return response.data;
  }

  Future<ProductResponseData> getProductsPopular() async {
    final response = await api.getProductsPopular(BaseErrorEntity.badRequestToModelError);
    return response.data;
  }

  Future<ProductResponseData> getProductsWithFilter(Map<String, dynamic> parameter) async {
    final response =
        await api.getProductsWithFilter(BaseErrorEntity.badRequestToModelError, parameter);
    return response.data;
  }

  Future<Product> getProductById(String id) async {
    final response = await api.getProductsDetail(id, BaseErrorEntity.badRequestToModelError);
    return response;
  }
}
