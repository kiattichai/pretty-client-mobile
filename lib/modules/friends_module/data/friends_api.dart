import 'package:dio/dio.dart';
import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/friends_response_entity.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/product_response_entity.dart';

class FriendsApi {
  final CoreApi coreApi;
  final String pathFriends = '/pretties';
  final String pathProduct = '/products';

  @provide
  @singleton
  FriendsApi(this.coreApi);

  Future<FriendsResponseEntity> getFriends(Function badRequestToModelError,
      {String query = ''}) async {
    final parameter = {'q': query};
    Response response =
        await coreApi.get(pathFriends, query.isEmpty ? null : parameter, badRequestToModelError);
    return FriendsResponseEntity().fromJson(response.data);
  }

  Future<FriendsResponseEntity> getFriendsPopular(Function badRequestToModelError) async {
    Map<String, String> query = {
      "limit": "4".toString(),
      "page": "1".toString(),
    };
    Response response = await coreApi.get(pathFriends, query, badRequestToModelError);
    return FriendsResponseEntity().fromJson(response.data);
  }

  Future<FriendsResponseEntity> getFriendsWithFilter(
      Function badRequestToModelError, Map<String, dynamic> parameter) async {
    Response response = await coreApi.get(pathFriends, parameter, badRequestToModelError);
    return FriendsResponseEntity().fromJson(response.data);
  }

  Future<ProductDetailResponseEntity> getFriendsDetail(
      String id, Function badRequestToModelError) async {
    Map<String, String> query = {"store_id": "1"};
    Response response = await coreApi.get(pathProduct + '/$id', query, badRequestToModelError);
    return ProductDetailResponseEntity().fromJson(response.data);
  }
}
