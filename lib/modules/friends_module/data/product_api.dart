import 'package:dio/dio.dart';
import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/modules/friends_module/models/product_response_entity.dart';

class ProductApi {
  final CoreApi coreApi;
  final String pathProduct = '/products';

  @provide
  @singleton
  ProductApi(this.coreApi);

  Future<ProductResponseEntity> getProducts(Function badRequestToModelError,
      {String query = '', int limit = 12, int page = 1}) async {
    final parameter = {
      'fields': 'id,code,option,price,name,store,categories,variants,attributes,images,status,user,feedbacks',
      'q': query,
      'limit': limit,
      'page': page
    };
    Response response =
        await coreApi.get(pathProduct, parameter, badRequestToModelError);
    return ProductResponseEntity().fromJson(response.data);
  }

  Future<ProductResponseEntity> getProductsPopular(Function badRequestToModelError) async {
    Map<String, String> query = {
      "limit": "4",
      "page": "1",
    };
    Response response = await coreApi.get(pathProduct + '/highlight', query, badRequestToModelError);
    return ProductResponseEntity().fromJson(response.data);
  }

  Future<ProductResponseEntity> getProductsWithFilter(
      Function badRequestToModelError, Map<String, dynamic> parameter) async {
    Response response = await coreApi.get(pathProduct, parameter, badRequestToModelError);
    return ProductResponseEntity().fromJson(response.data);
  }

  Future<Product> getProductsDetail(String id, Function badRequestToModelError) async {
    Response response = await coreApi.get(pathProduct + '/$id', null, badRequestToModelError);
    final data = ProductResponseEntity().fromJson(response.data);
    return data.data.record.first;
  }
}
