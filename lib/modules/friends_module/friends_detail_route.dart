import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_detail_screen.dart';

class FriendsDetailRoute extends BaseRoute {
  static String buildPath = '/friends_detail';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => FriendsDetailScreen(
              product: data,
            ));
  }
}
