import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/modules/friends_module/component/friends_grid_view.dart';
import 'package:pretty_client_mobile/modules/friends_module/component/modal_friends_filter.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_view_model.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_filter.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

class FriendsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FriendsScreenState();
  }
}

class FriendsScreenState extends BaseStateProvider<FriendsScreen, FriendsViewModel> {
  TextEditingController controller;
  PanelController panelController;
  final focusNode = FocusNode();

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
        keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
        keyboardBarColor: Colors.grey[200],
        nextFocus: true,
        actions: [
          KeyboardAction(
            focusNode: focusNode,
          ),
        ]);
  }

  @override
  void initState() {
    super.initState();
    panelController = PanelController();
    viewModel = FriendsViewModel();
    controller = TextEditingController();
    viewModel.scrollController = ScrollController();

    viewModel.scrollController.addListener(() {
      if (viewModel.scrollController.position.extentAfter == 0) {
        viewModel.getFriends();
      }
    });
  }

  onSubmit(String data) {
    print('onSubmit $data');
    viewModel.getFriends(query: data);
  }

  void _showModalSheet() async {
    final result = await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (builder) {
          return FractionallySizedBox(
            heightFactor: 0.95,
            child: ModalFriendFilter(viewModel.filter),
          );
        });
    viewModel.checkFilterResult(result);
    clearFocus();
  }

  Widget searchFriend() {
    return Align(
      alignment: Alignment(-0.87, -0.80),
      child: Padding(
        padding: EdgeInsets.only(bottom: 20, top: 20),
        child: TextField(
          focusNode: focusNode,
          controller: controller,
          onSubmitted: onSubmit,
          keyboardType: TextInputType.text,
          autofocus: false,
          style: TextStyle(
            color: Design.theme.colorActive,
            fontSize: 16,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
          ),
          decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.search,
                color: Design.theme.colorText2,
              ),
              filled: true,
              fillColor: Colors.transparent,
              contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(25)),
                borderSide: BorderSide(width: 1, color: Design.theme.colorPrimary),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(25)),
                borderSide: BorderSide(width: 1, color: Design.theme.colorPrimary),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(25)),
                borderSide: BorderSide(width: 1, color: Design.theme.colorPrimary),
              ),
              hintStyle: TextStyle(
                color: Design.theme.colorText2,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
              hintText: appLocal.commonSearch,
              counterText: ''
                  ''),
        ),
      ),
    );
  }

  CustomScrollView customScrollView() {
    return CustomScrollView(
      controller: viewModel.scrollController,
      physics: const AlwaysScrollableScrollPhysics(),
      slivers: <Widget>[
        SliverList(
            delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
          switch (index) {
            case 0:
              {
                return SizedBox(
                  height: 10,
                );
              }
              break;
            default:
              {
                return Container();
              }
          }
        }, childCount: 2)),
        FriendsGridView(
          products: viewModel.friends,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    return BaseWidget(
      model: viewModel,
      builder: (context, model, child) {
        return ProgressHUD(
          color: Colors.transparent,
          progressIndicator: CircleLoading(),
          inAsyncCall: viewModel.loading,
          child: Scaffold(
            appBar: CustomHeaderFilter(
                isFiltered: viewModel.filter.isNotEmpty,
                onTap: () {
                  _showModalSheet();
                },
                height: Screen.convertHeightSize(55),
                title: appLocal.friendHeader),
            body: RefreshIndicator(
              color: Design.theme.colorMainBackground,
              backgroundColor: Design.theme.colorPrimary,
              key: viewModel.refreshIndicatorKey,
              onRefresh: () {
                controller.clear();
                viewModel.page = 1;
                viewModel.isComplete = false;
                viewModel.getFriends();
                return Future.value();
              },
              child: Padding(
                child: KeyboardActions(
                  isDialog: false,
                  config: _buildConfig(context),
                  child: customScrollView(),
                ),
                padding: EdgeInsets.only(left: 13, right: 13),
              ),
            ),
          ),
        );
      },
    );
  }

  void clearFocus() {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  @override
  void dispose() {
//    clearFocus();
    super.dispose();
  }
}
