import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class ProductResponseEntity with JsonConvert<ProductResponseEntity> {
  ProductResponseData data;
  ProductResponseBench bench;
}

class ProductDetailResponseEntity with JsonConvert<ProductDetailResponseEntity> {
  Product data;
  ProductResponseBench bench;
}

class ProductResponseData with JsonConvert<ProductResponseData> {
  ProductResponseDataPagination pagination;
  List<Product> record;
}

class ProductResponseDataPagination with JsonConvert<ProductResponseDataPagination> {
  @JSONField(name: "current_page")
  int currentPage;
  @JSONField(name: "last_page")
  int lastPage;
  int limit;
  int total;
}

class Product with JsonConvert<Product> {
  int id;
  String code;
  int position;
  bool option;
  ProductPrice price;
  String name;
  String description;
  ProductStore store;
  List<ProductCategory> categories;
  List<ProductVariant> variants;
  List<ProductAttribute> attributes;
  List<ProductImage> images;
  ProductUser user;
  bool status;
  ProductFeedback feedbacks;
  @JSONField(name: "publish_status")
  bool publishStatus;
  @JSONField(name: "created_at")
  String createdAt;
  @JSONField(name: "updated_at")
  String updatedAt;
  bool onFloor;

  displayOnFloor() {
    return onFloor == null ? false : onFloor;
  }

  displayFullName() {
    return name;
  }

  displayName() {
    return name;
  }

  displayAge() {
    return findValue('birthdate');
  }

  displayWeight() {
    return findValue('weight');
  }

  displayShape() {
    return findValue('shape');
  }

  displayHeight() {
    return findValue('height');
  }

  displayChest() {
    return findValue('chest');
  }

  displayWaistline() {
    return findValue('waistline');
  }

  displayHip() {
    return findValue('hip');
  }

  String findValue(String code) {
    try {
      final result = attributes.where((model) {
        return model.code == code;
      }).toList();
      if (code == "birthdate") {
        var berlinWallFell = new DateTime.now();
        var dDay = DateTime.parse(result.first.valueText);
        Duration difference = berlinWallFell.difference(dDay);
        var birthdate = (difference.inDays / 365);
        return birthdate.floor().toString();
      } else {
        return result.first.valueText;
      }
    } catch (e) {
      return '';
    }
  }
}

class ProductPrice with JsonConvert<ProductPrice> {
  int min;
  int max;
  @JSONField(name: "min_text")
  String minText;
  @JSONField(name: "max_text")
  String maxText;
}

class ProductStore with JsonConvert<ProductStore> {
  int id;
  String name;
  dynamic image;
}

class ProductCategory with JsonConvert<ProductCategory> {
  int id;
  int position;
  String name;
  String description;
}

class ProductVariant with JsonConvert<ProductVariant> {
  int id;
  String sku;
  String name;
  String description;
  @JSONField(name: "cost_price")
  int costPrice;
  @JSONField(name: "cost_price_text")
  String costPriceText;
  int price;
  @JSONField(name: "price_text")
  String priceText;
  @JSONField(name: "compare_at_price")
  int compareAtPrice;
  @JSONField(name: "compare_at_price_text")
  String compareAtPriceText;
  int quantity;
  int hold;
  int position;
  int weight;
  bool status;
  List<ProductVariantsOption> options;
  ProductVariantsImage image;

  String textOption(){
    String result = '';
    for (int i = 0; i < options.length; i++) {
      if(options[i].name != null){
        result = options[i].name.toString()+ ": " + options[i].value.name.toString();
        if(i < options.length-1){
          result +=', ';
        }
      }

    }
    return result;
  }
}

class ProductVariantsOption with JsonConvert<ProductVariantsOption> {
  ProductVariantsOptionsValue value;
  int id;
  String code;
  String name;
}

class ProductVariantsOptionsValue with JsonConvert<ProductVariantsOptionsValue> {
  int id;
  String code;
  String name;
}

class ProductVariantsImage with JsonConvert<ProductVariantsImage> {
  String id;
  @JSONField(name: "store_id")
  int storeId;
  @JSONField(name: "album_id")
  int albumId;
  String name;
  int width;
  int height;
  String mime;
  int size;
  String url;
  String getImgUrlResize(String url, double w, double h) {
    return "$url/w_${w.round()},h_${h.round()},c_crop/$id/$name";
  }
}

class ProductAttribute with JsonConvert<ProductAttribute> {
  int id;
  String code;
  String name;
  ProductAttributesType type;
  bool require;
  ProductAttributesMetafield metafield;
  ProductAttributesValue value;
  @JSONField(name: "value_text")
  String valueText;
}

class ProductAttributesType with JsonConvert<ProductAttributesType> {
  int id;
  String name;
}

class ProductAttributesMetafield with JsonConvert<ProductAttributesMetafield> {}

class ProductAttributesValue with JsonConvert<ProductAttributesValue> {}

class ProductImage with JsonConvert<ProductImage> {
  String id;
  @JSONField(name: "store_id")
  int storeId;
  dynamic tag;
  @JSONField(name: "album_id")
  int albumId;
  String name;
  int width;
  int height;
  String mime;
  int size;
  String url;
  int position;

  String getImgUrlResize(String url, double w, double h) {
    return "$url/w_${w.round()},h_${h.round()},c_crop/$id/$name";
  }
}

class ProductUser with JsonConvert<ProductUser> {
  int id;
  String username;
  @JSONField(name: "country_code")
  String countryCode;
  @JSONField(name: "first_name")
  String firstName;
  @JSONField(name: "last_name")
  String lastName;
}

class ProductFeedback with JsonConvert<ProductFeedback> {
  double summary;
  int total;
  List<ProductFeedbackRecord> record;
}

class ProductFeedbackRecord with JsonConvert<ProductFeedbackRecord> {
  int score;
  int count;
}

class ProductResponseBench with JsonConvert<ProductResponseBench> {
  int second;
  double millisecond;
  String format;
}
