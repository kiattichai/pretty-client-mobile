import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class FriendsResponseEntity with JsonConvert<FriendsResponseEntity> {
  FriendsResponseData data;
  FriendsResponseBench bench;
}

class FriendsDetailResponseEntity with JsonConvert<FriendsDetailResponseEntity> {
  Friend data;
  FriendsResponseBench bench;
}

class FriendsResponseData with JsonConvert<FriendsResponseData> {
  FriendsPagination pagination;
  List<Friend> record;
  bool cache;
}

class FriendsPagination with JsonConvert<FriendsPagination> {
  @JSONField(name: "current_page")
  int currentPage;
  @JSONField(name: "last_page")
  int lastPage;
  int limit;
  int total;
}

class Friend with JsonConvert<Friend> {
  int id;
  FriendsGroup group;
  @JSONField(name: "first_name")
  String firstName;
  @JSONField(name: "last_name")
  String lastName;
  @JSONField(name: "nick_name")
  String nickName;
  String birthday;
  int age;
  String gender;
  int weight;
  int height;
  int chest;
  int waistline;
  int hip;
  bool status;
  FriendsAvatar avatar;
  List<FriendsImage> images;
  @JSONField(name: "created_at")
  String createdAt;
  @JSONField(name: "updated_at")
  String updatedAt;
  int price;
  String code;
  bool onfloor;

  displayFullName() {
    return '$firstName  $lastName';
  }

  displayName() {
    return '$firstName  $lastName ($nickName)';
  }

  displayAge() {
    return age == null ? '' : '$age';
  }

  displayWeight() {
    return weight == null ? '' : '$weight';
  }

  displayShape() {
    return '$chest-$waistline-$hip';
  }

  displayHeight() {
    return height == null ? '' : '$height';
  }

  displayOnFloor() {
    return onfloor == null ? false : onfloor;
  }
}

class FriendsGroup with JsonConvert<FriendsGroup> {
  int id;
  String name;
}

class FriendsAvatar with JsonConvert<FriendsAvatar> {
  String id;
  dynamic tag;
  String name;
  int width;
  int height;
  String mime;
  int size;
  String url;
  int position;
}

class FriendsImage with JsonConvert<FriendsImage> {
  String id;
  dynamic tag;
  String name;
  int width;
  int height;
  String mime;
  int size;
  String url;
  int position;

  String getImgUrlResize(String url, double w, double h) {
    return "$url/w_${w.round()},h_${h.round()},c_crop/$id/$name";
  }
}

class FriendsResponseBench with JsonConvert<FriendsResponseBench> {
  int second;
  double millisecond;
  String format;
}
