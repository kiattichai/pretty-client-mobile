import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class FeedbackResponseEntity with JsonConvert<FeedbackResponseEntity> {
	FeedbackResponseData data;
	FeedbackResponseBench bench;
}

class FeedbackResponseData with JsonConvert<FeedbackResponseData> {
	FeedbackResponseDataPagination pagination;
	List<FeedbackResponseDataRecord> record;
}

class FeedbackResponseDataPagination with JsonConvert<FeedbackResponseDataPagination> {
	@JSONField(name: "current_page")
	int currentPage;
	@JSONField(name: "last_page")
	int lastPage;
	int limit;
	int total;
}

class FeedbackResponseDataRecord with JsonConvert<FeedbackResponseDataRecord> {
	int id;
	dynamic description;
	dynamic ip;
	@JSONField(name: "reason_id")
	int reasonId;
	int score;
	bool status;
	FeedbackResponseDataRecordUser user;
	@JSONField(name: "updated_by")
	int updatedBy;
	FeedbackResponseDataRecordStaff staff;
	@JSONField(name: "created_at")
	FeedbackResponseDataRecordCreatedAt createdAt;
	@JSONField(name: "updated_at")
	FeedbackResponseDataRecordUpdatedAt updatedAt;
}

class FeedbackResponseDataRecordUser with JsonConvert<FeedbackResponseDataRecordUser> {
	int id;
	String username;
	@JSONField(name: "country_code")
	String countryCode;
	String gender;
	@JSONField(name: "first_name")
	String firstName;
	@JSONField(name: "last_name")
	String lastName;
	FeedbackResponseDataRecordUserAvatar avatar;
}

class FeedbackResponseDataRecordUserAvatar with JsonConvert<FeedbackResponseDataRecordUserAvatar> {
	String id;
	String name;
	int width;
	int height;
	String mime;
	int size;
	String url;
	@JSONField(name: "resize_url")
	String resizeUrl;
}

class FeedbackResponseDataRecordStaff with JsonConvert<FeedbackResponseDataRecordStaff> {
	int id;
	String username;
	@JSONField(name: "first_name")
	String firstName;
	@JSONField(name: "last_name")
	String lastName;
}

class FeedbackResponseDataRecordCreatedAt with JsonConvert<FeedbackResponseDataRecordCreatedAt> {
	String value;
	String date;
	String time;
}

class FeedbackResponseDataRecordUpdatedAt with JsonConvert<FeedbackResponseDataRecordUpdatedAt> {
	String value;
	String date;
	String time;
}

class FeedbackResponseBench with JsonConvert<FeedbackResponseBench> {
	int second;
	double millisecond;
	String format;
}
