import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/feedback_module/models/feedback_response_entity.dart';

class FeedbackApi {
  final CoreApi coreApi;

  @provide
  @singleton
  FeedbackApi(this.coreApi);

  Future<FeedbackResponseEntity> getFeedbackByOrderId(
      int productId, int orderId, Function badRequestToModelError) async {
    Map<String, dynamic> query = {
      "order_id": orderId,
    };
    final response =
        await coreApi.get("/products/$productId/feedbacks", query, badRequestToModelError);
    return FeedbackResponseEntity().fromJson(response.data);
  }

  Future<BaseResponseEntity> postFeedback(
      int productId, dynamic data, Function badRequestToModelError) async {
    final response =
        await coreApi.post("/products/$productId/feedbacks", data, badRequestToModelError);
    return BaseResponseEntity().fromJson(response.data);
  }

  Future<BaseResponseEntity> editFeedback(
      int productId, int feedbackId, dynamic data, Function badRequestToModelError) async {
    final response = await coreApi.put(
        "/products/$productId/feedbacks/$feedbackId", data, badRequestToModelError);
    return BaseResponseEntity().fromJson(response.data);
  }
}
