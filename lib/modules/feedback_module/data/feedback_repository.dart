import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/feedback_module/data/feedback_api.dart';
import 'package:pretty_client_mobile/modules/feedback_module/models/feedback_response_entity.dart';

class FeedbackRepository extends BaseRepository {
  final FeedbackApi feedbackApi;

  @provide
  FeedbackRepository(this.feedbackApi);

  Future<FeedbackResponseEntity> getFeedbackByOrderId(int productId, int orderId) async {
    final response = await feedbackApi.getFeedbackByOrderId(
        productId, orderId, BaseErrorEntity.badRequestToModelError);
    return response;
  }

  Future<BaseResponseEntity> postFeedback(int productId, dynamic data) async {
    final response =
        await feedbackApi.postFeedback(productId, data, BaseErrorEntity.badRequestToModelError);
    return response;
  }

  Future<BaseResponseEntity> editFeedback(int productId, int feedbackId, dynamic data) async {
    final response = await feedbackApi.editFeedback(
        productId, feedbackId, data, BaseErrorEntity.badRequestToModelError);
    return response;
  }
}
