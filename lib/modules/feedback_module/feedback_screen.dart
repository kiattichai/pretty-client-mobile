import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/feedback_module/feedback_view_model.dart';
import 'package:pretty_client_mobile/modules/main_module/main_screen_route.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/custom_modal.dart';
import 'package:pretty_client_mobile/widgets/modal_booking_success.dart';
import 'package:pretty_client_mobile/widgets/modal_give_feedback.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';

class FeedbackScreen extends StatefulWidget {
  final dynamic data;

  const FeedbackScreen({Key key, this.data}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return FeedbackScreenState();
  }
}

class FeedbackScreenState extends BaseStateProvider<FeedbackScreen, FeedbackViewModel> {
  final FocusNode inputReview = FocusNode();
  final FocusNode inputReview2 = FocusNode();

  @override
  void initState() {
    super.initState();
    viewModel = FeedbackViewModel();
    viewModel.orderId = widget.data['order_id'];
    viewModel.onErrorFeedback = handleError;
  }

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
        keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
        keyboardBarColor: Colors.grey[200],
        nextFocus: true,
        actions: [
          KeyboardAction(
            focusNode: inputReview,
          ),
          KeyboardAction(
            focusNode: inputReview2,
          ),
        ]);
  }

  Widget ratingStatus(FeedbackMode type) {
    final result = List.generate(5, (value) {
      final shouldFillColor = value < viewModel.score;
      return Padding(
        padding: EdgeInsets.only(left: 8, right: 8),
        child: InkWell(
          onTap: () {
            if (type != FeedbackMode.SHOW) {
              if (value == 0 && value + 1 == viewModel.score) {
                viewModel.setScore(0);
              } else {
                viewModel.setScore(value + 1);
              }
            }
          },
          child: SvgPicture.asset(
            shouldFillColor ? "images/star_feedback.svg" : "images/star_feedback_empty.svg",
//          width: 30,
          ),
        ),
      );
    });
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: result,
      ),
    );
  }

  Widget showRatingBox() {
    return Container(
      margin: EdgeInsets.only(left: 30, right: 30),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: Design.theme.colorLine.withOpacity(0.45),
          ),
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          ratingStatus(viewModel.type),
          SizedBox(
            height: 30,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              viewModel.desc,
              style: TextStyle(
                fontFamily: 'Sarabun',
                color: Design.theme.colorInactive,
                fontSize: 14,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                viewModel.feedback.updatedAt.date,
                style: TextStyle(
                  fontFamily: 'Sarabun',
                  color: Design.theme.colorInactive.withOpacity(0.5),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0,
                ),
              ),
              InkWell(
                onTap: () {
                  viewModel.changeFeedbackMode(FeedbackMode.EDIT);
                },
                child: Text(
                  "แก้ไขรีวิว",
                  style: TextStyle(
                    fontFamily: 'Sarabun',
                    color: Design.theme.colorInactive.withOpacity(0.5),
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget giveRatingBox() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Text(
          "ให้คะแนน",
          style: TextStyle(
            fontFamily: 'Sarabun',
            color: Design.theme.fontColorSecondary,
            fontSize: 16,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
            letterSpacing: 0,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        ratingStatus(viewModel.type),
        SizedBox(
          height: 30,
        ),
        RichText(
          text: TextSpan(
              text: 'เขียนรีวิว',
              style: TextStyle(
                fontFamily: 'Sarabun',
                color: Design.theme.fontColorSecondary,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
              children: <TextSpan>[
                TextSpan(
                  text: ' (ไม่บังคับ)',
                  style: TextStyle(
                    fontFamily: 'Sarabun',
                    color: Design.theme.fontColorSecondary.withOpacity(0.6),
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                  ),
                )
              ]),
        ),
        Container(
          padding: EdgeInsets.only(top: 10, bottom: 6, left: 30, right: 30),
          child: TextField(
            controller: viewModel.textEditingController,
//            keyboardType: TextInputType.text,
            style: TextStyle(
                fontSize: 14, fontWeight: FontWeight.w500, color: Design.theme.colorInactive),
            maxLines: 3,
            focusNode: inputReview,
            textInputAction: TextInputAction.done,
            maxLength: 150,
            decoration: InputDecoration(
              filled: true,
              fillColor: Design.theme.colorLine.withOpacity(0.2),
              errorStyle: TextStyle(color: Design.theme.colorPrimary),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(8),
                ),
                borderSide: BorderSide(
                  width: 1,
                  color: Design.theme.colorText,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(8),
                ),
                borderSide: BorderSide(
                  width: 1,
                  color: Design.theme.colorText,
                ),
              ),
              hintStyle: TextStyle(
                color: Design.theme.colorInactive.withOpacity(0.4),
              ),
              hintText: "รีวิว (ไม่บังคับ)",
              counterText: '',
            ),
            onChanged: _onChanged,
          ),
        ),
        Container(
          padding: EdgeInsets.only(right: 30, bottom: 20),
          alignment: Alignment.centerRight,
          child: Text(
            "${viewModel.textCounter}/150",
            style: TextStyle(
              fontFamily: 'Sarabun',
              color: Design.theme.fontColorSecondary.withOpacity(0.8),
              fontSize: 16,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              letterSpacing: 0,
            ),
          ),
        ),
      ],
    );
  }

  _onChanged(String value) {
    viewModel.desc = value;
    viewModel.setTextCounter(value.length);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return BaseWidget<FeedbackViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        progressIndicator: CircleLoading(),
        inAsyncCall: viewModel.loading,
        color: Colors.transparent,
        child: Scaffold(
          appBar: CustomHeaderDetail(
            height: Screen.convertHeightSize(55),
            title: "ให้คะแนนและรีวิว",
            onTap: () {
              if (widget.data["ispop"] == true) {
                print("is pop()");
                RouterService.instance.pop();
              } else {
                RouterService.instance
                    .navigateTo(MainScreenRoute.buildPath, data: 1, clearStack: true);
              }
            },
          ),
          body: Builder(
            builder: (context) {
              if (viewModel.orderDetail != null) {
                return KeyboardActions(
                  isDialog: false,
                  config: _buildConfig(context),
                  child: SingleChildScrollView(
                    physics: NeverScrollableScrollPhysics(),
                    child: Container(
                      alignment: Alignment.center,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 50,
                          ),
                          Container(
                            width: 100,
                            height: 100,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                fit: BoxFit.fill,
                                image: NetworkImage(
                                  viewModel.orderDetail.products[0].image.getImgUrlResize(150, 150),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            viewModel.orderDetail.products[0].productName,
                            style: TextStyle(
                              fontFamily: 'Sarabun',
                              color: Design.theme.colorActive,
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                              letterSpacing: 0,
                            ),
                          ),
                          Text(
                            "ID : ${viewModel.orderDetail.products[0].productCode}",
                            style: TextStyle(
                              fontFamily: 'Sarabun',
                              color: Design.theme.fontColorSecondary.withOpacity(0.7),
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              letterSpacing: 0,
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          viewModel.type == FeedbackMode.SHOW ? showRatingBox() : giveRatingBox(),
                        ],
                      ),
                    ),
                  ),
                );
              } else {
                return Container();
              }
            },
          ),
          bottomNavigationBar: (viewModel.type == FeedbackMode.SHOW)
              ? Container(
                  height: 1,
                )
              : Container(
                  padding: EdgeInsets.all(15),
                  child: MaterialButton(
                    disabledColor: Color(0xff636362),
                    disabledTextColor: Color(0xffbfbfbf),
                    textColor: Design.theme.colorMainBackground,
                    color: Design.theme.colorPrimary,
                    height: 48,
                    minWidth: Screen.width - 30,
                    child: Text(
                      appLocal.commonConfirm,
                      style: TextStyle(
                        fontFamily: 'Sarabun',
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(26),
                    ),
                    onPressed: model.score == 0 ? null : _onSubmit,
                  ),
                ),
        ),
      ),
    );
  }

  void _onSubmit() {
    if (viewModel.type == FeedbackMode.CREATE) {
      viewModel.textEditingController = TextEditingController(text: viewModel.desc);
      viewModel.postFeedback(onSuccess);
    }
    if (viewModel.type == FeedbackMode.EDIT) {
      viewModel.textEditingController = TextEditingController(text: viewModel.desc);
      viewModel.editFeedback(onSuccess);
    }
  }

  void onSuccess() {
    viewModel.setLoading(false);
    _successDialog();
  }

  void handleError(BaseError error) {
    viewModel.setLoading(false);
    _showAlert(error.message.toString());
    print(error.toJson());
  }

  void _showAlert(String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return CustomModal(
          type: ModalType.CLOSE,
          desc: message,
          appLocal: appLocal,
          onTapConfirm: () {
            RouterService.instance.pop();
          },
        );
      },
    );
  }

  void _successDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return ModalGiveFeedback(
          closeModal: () {
            viewModel.resetData();
            viewModel.getFeedback(viewModel.orderId);
            RouterService.instance.pop();
          },
        );
      },
    );
  }
}
