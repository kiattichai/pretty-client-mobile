import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/feedback_module/feedback_screen.dart';

class FeedbackRoute extends BaseRoute {
  static String buildPath = '/feedback';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => FeedbackScreen(
              data: data,
            ));
  }
}
