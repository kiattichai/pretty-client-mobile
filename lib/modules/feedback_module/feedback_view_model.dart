import 'package:flutter/material.dart';
import 'package:get_ip/get_ip.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/order_detail_response_entity.dart';
import 'package:pretty_client_mobile/modules/feedback_module/models/feedback_response_entity.dart';

enum FeedbackMode { SHOW, CREATE, EDIT }

class FeedbackViewModel extends BaseViewModel {
  int score = 0;
  String desc = "";
  int textCounter = 0;
  int orderId;
  FeedbackMode type;
  TextEditingController textEditingController;
  Function onErrorFeedback;

  OrderDetailResponseData orderDetail = null;
  FeedbackResponseDataRecord feedback = null;

  @override
  void postInit() {
    type = FeedbackMode.SHOW;
    getFeedback(orderId);
  }

  void getFeedback(int id) async {
    setLoading(true);
    catchError(() async {
      orderDetail = await di.orderRepository.getOrderDetail(id);
      var res = await di.feedbackRepository
          .getFeedbackByOrderId(orderDetail.products[0].productId, orderDetail.id);
      if (res.data.record.length > 0) {
        feedback = res.data.record[0];
        score = feedback.score;
        desc = feedback.description == null ? '' : feedback.description;
        textEditingController = TextEditingController(text: desc);
        textCounter = desc.length;
      } else {
        type = FeedbackMode.CREATE;
      }
      setLoading(false);
    });
  }

  void postFeedback(Function onSuccess) async {
    setLoading(true);
    String ipAddress = await GetIp.ipAddress;
    catchError(() async {
      Map<String, dynamic> data = {
        "store_id": 1,
        "score": score,
        "order_id": orderDetail.id,
        "description": desc,
        "ip": ipAddress
      };
//      print(data.toString());
      await di.feedbackRepository.postFeedback(orderDetail.products[0].productId, data);
      onSuccess();
    });
  }

  void editFeedback(Function onSuccess) async {
    setLoading(true);
    String ipAddress = await GetIp.ipAddress;
    catchError(() async {
      Map<String, dynamic> data = {"score": score, "description": desc, "ip": ipAddress};
      if (feedback != null) {
        var res = await di.feedbackRepository
            .editFeedback(orderDetail.products[0].productId, feedback.id, data);
      }
      onSuccess();
    });
  }

  resetData() {
    type = FeedbackMode.SHOW;
    orderDetail = null;
    feedback = null;
  }

  setScore(int val) {
    score = val;
    notifyListeners();
  }

  setTextCounter(int val) {
    textCounter = val;
    notifyListeners();
  }

  changeFeedbackMode(FeedbackMode val) {
    type = val;
    notifyListeners();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    print(error);
    onErrorFeedback(error);
  }
}
