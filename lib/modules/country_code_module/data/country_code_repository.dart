import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/modules/country_code_module/data/country_code_api.dart';
import 'package:pretty_client_mobile/modules/country_code_module/model/country_code_response_entity.dart';
import 'package:inject/inject.dart';

class CountryCodeRepository extends BaseRepository {
  final CountryCodeApi countryCodeApi;
  List<CountryCode> countryCodeList = [];
  @provide
  CountryCodeRepository(this.countryCodeApi);

  Future<List<CountryCode>> getCountryCodeList() async {
    final response = await countryCodeApi.getCountryCodeList();
    if (response.data != null) {
      countryCodeList.clear();
      countryCodeList.addAll(response.data.record);
      return countryCodeList;
    } else {
      return [];
    }
  }

  List<CountryCode> getCountryList() {
    return countryCodeList;
  }
}
