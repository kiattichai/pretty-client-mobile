import 'package:dio/dio.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/modules/country_code_module/model/country_code_response_entity.dart';
import 'package:inject/inject.dart';

class CountryCodeApi {
  final String endpoint = "https://alpha-api.theconcert.co.th/countries";
  final CoreApi coreApi;

  @provide
  @singleton
  CountryCodeApi(this.coreApi);

  Future<CountryCodeResponseEntity> getCountryCodeList() async {
    final response =
        await coreApi.getWithoutBaseUrl(endpoint, BaseErrorEntity.badRequestToModelError);
    return CountryCodeResponseEntity().fromJson(response.data);
  }
}
