import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/modules/country_code_module/model/country_code_response_entity.dart';

class CountryCodeViewModel extends BaseViewModel {
  List<CountryCode> countryCodeList = [];
  ScrollController scrollController;
  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;

  CountryCodeViewModel() {
    refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    scrollController = ScrollController();
  }

  @override
  void postInit() {
    getCountryCodeList();
  }

  Future<void> getCountryCodeList() async {
    setLoading(true);
    catchError(() async {
      countryCodeList = await di.countryCodeRepository.getCountryCodeList();
      setLoading(false);
    });
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    print(error);
  }
}
