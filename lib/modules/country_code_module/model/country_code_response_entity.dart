import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class CountryCodeResponseEntity with JsonConvert<CountryCodeResponseEntity> {
  CountryCodeResponseData data;
}

class CountryCodeResponseData with JsonConvert<CountryCodeResponseData> {
  List<CountryCode> record;
}

class CountryCode with JsonConvert<CountryCode> {
  int id;
  String name;
  @JSONField(name: "iso_code")
  String isoCode;
  @JSONField(name: "phone_code")
  String phoneCode;
  @JSONField(name: "phone_code_text")
  String phoneCodeText;
}
