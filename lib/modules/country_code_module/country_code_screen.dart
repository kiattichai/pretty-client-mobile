import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/modules/country_code_module/country_code_view_model.dart';
import 'package:pretty_client_mobile/modules/country_code_module/model/country_code_response_entity.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';

class CountryCodeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CountryCodeScreenState();
  }
}

class CountryCodeScreenState extends BaseStateProvider<CountryCodeScreen, CountryCodeViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = CountryCodeViewModel();
  }

  Widget itemCountryCode(CountryCode record) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 8,
          ),
          Stack(children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  record.name,
                  style: TextStyle(
                    color: Design.theme.colorActive,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: -0.1777777777777778,
                  ),
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.only(right: 20),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    record.phoneCodeText,
                    style: TextStyle(color: Design.theme.colorText),
                  ),
                ))
          ]),
          SizedBox(
            height: 8,
          ),
          Container(
            width: Screen.width - 40,
            height: 1,
            color: Color(0xffeeeeee).withOpacity(0.14),
          )
        ],
      ),
      height: Screen.convertHeightSize(45),
    );
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return BaseWidget<CountryCodeViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        color: Colors.transparent,
        progressIndicator: CircleLoading(),
        inAsyncCall: model.loading,
        child: Scaffold(
            backgroundColor: Design.theme.colorMainBackground,
            appBar: CustomHeaderDetail(
              title: appLocal.countrySelectTitle,
              height: Screen.convertHeightSize(55),
            ),
            body: Builder(builder: (context) {
              if (model.loading) {
                return Container();
              }
              if (model.countryCodeList.isNotEmpty && !model.loading) {
                return RefreshIndicator(
                    color: Design.theme.colorMainBackground,
                    backgroundColor: Design.theme.colorPrimary,
                    key: model.refreshIndicatorKey,
                    onRefresh: () {
                      return model.getCountryCodeList();
                    },
                    child: ListView.builder(
                        physics: const AlwaysScrollableScrollPhysics(),
                        itemCount: model.countryCodeList.length,
                        controller: model.scrollController,
                        itemBuilder: (context, int) {
                          return InkWell(
                            child: itemCountryCode(model.countryCodeList[int]),
                            onTap: () {
                              RouterService.instance.pop(data: model.countryCodeList[int]);
                            },
                          );
                        }));
              } else {
                return RefreshIndicator(
                    color: Design.theme.colorMainBackground,
                    backgroundColor: Design.theme.colorPrimary,
                    key: model.refreshIndicatorKey,
                    onRefresh: () {
                      return model.getCountryCodeList();
                    },
                    child: ListView(
                      physics: const AlwaysScrollableScrollPhysics(),
                      controller: model.scrollController,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 150.0),
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  'images/icons_empty.png',
                                  width: Screen.convertWidthSize(52),
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  "ไม่พบรายการ",
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 22),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ));
              }
            })),
      ),
    );
  }
}
