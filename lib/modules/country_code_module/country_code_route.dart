import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/navigator.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/country_code_module/country_code_screen.dart';

class CountryCodeRoute extends BaseRoute {
  static String buildPath = '/country_code';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => CountryCodeScreen());
  }
}
