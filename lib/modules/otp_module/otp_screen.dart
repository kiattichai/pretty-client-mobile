import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/forgot_password_module/routes/forgot_create_password_route.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_flow_type.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_request_screen.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_view_model.dart';
import 'package:pretty_client_mobile/modules/register_module/routes/register_form_route.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/pin_code_view.dart';
import 'package:pretty_client_mobile/widgets/primary_button_widget.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

class OtpScreen extends StatefulWidget {
  final OtpRequestScreen otpRequestScreen;

  const OtpScreen({Key key, this.otpRequestScreen}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return OtpScreenState();
  }
}

enum SingingCharacter { lafayette, jefferson }
SingingCharacter _character = SingingCharacter.lafayette;

class OtpScreenState extends BaseStateProvider<OtpScreen, OTPViewModel> {
  final TextEditingController controller = TextEditingController();
  GlobalKey _scaffold = GlobalKey();
  final focusInputOTP = FocusNode();

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
        keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
        keyboardBarColor: Colors.grey[200],
        nextFocus: true,
        actions: [
          KeyboardAction(
            focusNode: focusInputOTP,
          ),
        ]);
  }

  @override
  void initState() {
    super.initState();
    viewModel = OTPViewModel(widget.otpRequestScreen);
    viewModel.handleVerifyOtpSuccess = handleVerifyOtpSuccess;
    viewModel.handleError = handleError;
  }

  void handleVerifyOtpSuccess() {
    if (widget.otpRequestScreen.otpFlowType == OTPFlowType.REGISTER) {
      RouterService.instance.navigateTo(RegisterFormRoute.buildPath, data: viewModel.modelInScreen);
    } else if (widget.otpRequestScreen.otpFlowType == OTPFlowType.REGISTER_SOCIAL) {
      RouterService.instance.navigateTo(RegisterFormRoute.buildPath, data: viewModel.modelInScreen);
    } else {
      RouterService.instance
          .navigateTo(ForgotCreatePasswordRoute.buildPath, data: viewModel.modelInScreen);
    }
  }

  void handleError(BaseError error) {
    _showAlert(context, error.message);
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return BaseWidget<OTPViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        color: Colors.transparent,
        progressIndicator: CircleLoading(),
        inAsyncCall: model.loading,
        child: Scaffold(
          backgroundColor: Design.theme.colorMainBackground,
          resizeToAvoidBottomPadding: false,
          key: _scaffold,
          appBar: CustomHeaderDetail(
            height: Screen.convertHeightSize(55),
            title: model.modelInScreen.otpFlowType == OTPFlowType.REGISTER
                ? appLocal.registerHeader
                : appLocal.forgotPasswordTitle,
          ),
          body: KeyboardActions(
              isDialog: true,
              config: _buildConfig(context),
              child: Container(
                child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
                  Center(
                    child: Padding(
                      padding: EdgeInsets.only(top: Screen.convertHeightSize(45)),
                      child: Text(
                        appLocal.confirmOtpTitle,
                        style: TextStyle(
                          color: Design.theme.colorActive,
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        ),
                        // maxLines: 1,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: Screen.convertHeightSize(8)),
                    child: FutureBuilder<bool>(
                      future: delayKeyboard(),
                      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                        switch (snapshot.data) {
                          case true:
                            return PinCodeTextField(
                              keyboardType: TextInputType.number,
                              pinBoxWidth: Screen.convertWidthSize(45),
                              pinBoxHeight: Screen.convertHeightSize(45),
                              autofocus: true,
                              controller: controller,
                              hideCharacter: false,
                              highlight: true,
                              highlightColor: Design.theme.colorLine,
                              defaultBorderColor: Design.theme.colorLine,
                              hasTextBorderColor: Design.theme.colorLine,
                              maxLength: 4,
                              onTextChanged: (text) {
                                model.otp = text;
                                model.enableButton(text.length == 4);
                              },
                              onDone: (text) async {
                                model.otp = text;
                              },
                              pinCodeTextFieldLayoutType:
                                  PinCodeTextFieldLayoutType.AUTO_ADJUST_WIDTH,
                              wrapAlignment: WrapAlignment.spaceBetween,
                              pinBoxDecoration: ProvidedPinBoxDecoration.customPinboxDecoration,
                              pinTextStyle: TextStyle(
                                fontSize: 26.0,
                                color: Design.theme.colorActive,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ),
                              focusNode: focusInputOTP,
                            );
                          case false:
                            return Container();
                        }
                        return Container();
                      },
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: EdgeInsets.only(top: Screen.convertHeightSize(10)),
                      child: Text(model.modelInScreen.mobile,
                          style: TextStyle(
                            color: Design.theme.colorPrimary,
                            fontSize: 22,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                          )),
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: EdgeInsets.only(top: Screen.convertHeightSize(8)),
                      child: new Text(
                          "( ${appLocal.confirmOtpReference} : ${model.modelInScreen.refCode})",
                          style: TextStyle(
                            color: Design.theme.colorText,
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          )),
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: EdgeInsets.only(top: Screen.convertHeightSize(15)),
                      child: Text(appLocal.channelVerificationTitle,
                          style: TextStyle(
                            color: Design.theme.colorActive,
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                          )),
                    ),
                  ),
                  Padding(
                    child: radioGroup(model),
                    padding: EdgeInsets.only(top: Screen.convertHeightSize(0), left: 20, right: 35),
                  ),
                  Center(
                    child: Padding(
                      padding:
                          EdgeInsets.only(top: Screen.convertHeightSize(0), left: 30, right: 30),
                      child: Text(
                        appLocal.confirmOtpMessage,
                        style: TextStyle(
                          color: Design.theme.colorInactive.withOpacity(0.93),
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Center(
                    child: Padding(
                        padding: EdgeInsets.only(
                            top: Screen.convertHeightSize(12), left: 30, right: 30, bottom: 20),
                        child: GestureDetector(
                          child: Text(appLocal.confirmOtpSendAgain,
                              style: TextStyle(
                                color: Design.theme.colorPrimary,
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )),
                          onTap: () {
                            model.requestOTP();
                          },
                        )),
                  ),
                  Padding(
                    child: PrimaryButtonWidget(
                        buttonTitle: appLocal.commonNext,
                        onBtnPressed: !model.isEnableButton ? null : () => model.verifyOTP()),
                    padding:
                        EdgeInsets.only(top: Screen.convertHeightSize(30), left: 30, right: 30),
                  )
                ]),
              )),
        ),
      ),
    );
  }

  Widget radioGroup(OTPViewModel model) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          children: <Widget>[
            new Radio(
              activeColor: Design.theme.colorPrimary,
              value: "sms",
              groupValue: model.modelInScreen.channel,
              onChanged: (value) => model.updateChannelVerify(value),
            ),
            new Text(appLocal.channelVerificationSms,
                style: TextStyle(
                  color: Design.theme.colorInactive,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                )),
          ],
        ),
        Row(
          children: <Widget>[
            new Radio(
              activeColor: Design.theme.colorPrimary,
              value: "call",
              groupValue: model.modelInScreen.channel,
              onChanged: (value) => model.updateChannelVerify(value),
            ),
            new Text(appLocal.channelVerificationVoiceCall,
                style: TextStyle(
                  color: Design.theme.colorInactive,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ))
          ],
        )
      ],
    );
  }

  Future<Widget> _showAlert(BuildContext context, String message, [bool isSuccess = false]) async {
    return showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text(message),
          actions: <Widget>[
            new FlatButton(
                onPressed: () => {
                      setState(() {
                        controller.clear();
                      }),
                      Navigator.pop(context)
                    },
                child: new Text('Ok'))
          ],
        ));
  }

  Future<bool> delayKeyboard() async {
    await Future.delayed(Duration(milliseconds: 300));
    return true;
  }
}
