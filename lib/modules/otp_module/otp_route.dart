import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_screen.dart';

class OtpRoute extends BaseRoute {
  static String buildPath = '/otp';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
        builder: (context) => OtpScreen(
              otpRequestScreen: data,
            ));
  }
}
