class RequestOtpEntity {
  String countryCode;
  String mobile;
  String channel;

  RequestOtpEntity({this.countryCode, this.mobile, this.channel});

  RequestOtpEntity.fromJson(Map<String, dynamic> json) {
    countryCode = json['country_code'];
    mobile = json['mobile'];
    channel = json['channel'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['country_code'] = this.countryCode;
    data['mobile'] = this.mobile;
    data['channel'] = this.channel;
    return data;
  }
}
