import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class OtpResponseEntity with JsonConvert<OtpResponseEntity> {
  OtpResponseData data;
}

class OtpResponseData with JsonConvert<OtpResponseData> {
  String message;
  String mobile;
  String ref;
  @JSONField(name: "expired_at")
  String expiredAt;
}
