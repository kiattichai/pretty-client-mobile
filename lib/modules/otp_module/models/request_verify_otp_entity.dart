class RequestVerifyOtpEntity {
  String countryCode;
  String code;
  String mobile;
  String channel;

  RequestVerifyOtpEntity({this.countryCode, this.code, this.mobile, this.channel});

  RequestVerifyOtpEntity.fromJson(Map<String, dynamic> json) {
    countryCode = json['country_code'];
    code = json['code'];
    mobile = json['mobile'];
    channel = json['channel'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['country_code'] = this.countryCode;
    data['code'] = this.code;
    data['mobile'] = this.mobile;
    data['channel'] = this.channel;
    return data;
  }
}
