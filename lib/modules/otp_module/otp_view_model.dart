import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/modules/otp_module/models/request_otp_entity.dart';
import 'package:pretty_client_mobile/modules/otp_module/models/request_verify_otp_entity.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_request_screen.dart';

class OTPViewModel extends BaseViewModel {
  bool isEnableButton = false;
  OtpRequestScreen modelInScreen;
  String otp;
  Function handleVerifyOtpSuccess;
  Function handleRequestOtpSuccess;
  Function handleError;

  OTPViewModel(OtpRequestScreen otpRequestScreen) {
    modelInScreen = otpRequestScreen;
  }

  void updateChannelVerify(String value) {
    modelInScreen.channel = value;
    print(modelInScreen.toString());
    notifyListeners();
  }

  Future<void> verifyOTP() async {
    setLoading(true);
    final request = RequestVerifyOtpEntity(
        countryCode: modelInScreen.countryCode,
        code: otp,
        mobile: modelInScreen.mobile,
        channel: modelInScreen.channel);
    catchError(() async {
      await di.otpRepository.verifyOtp(request);
      setLoading(false);
      handleVerifyOtpSuccess();
    });
  }

  Future<Null> requestOTP() async {
    setLoading(true);
    final request = RequestOtpEntity(
        channel: modelInScreen.channel,
        mobile: modelInScreen.mobile,
        countryCode: modelInScreen.countryCode);

    catchError(() async {
      final model = await di.otpRepository.requestOtp(request);
      modelInScreen.refCode = model.ref;
      setLoading(false);
      handleRequestOtpSuccess();
    });
  }

  void enableButton(bool value) {
    isEnableButton = value;
    notifyListeners();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    handleError(error);
  }
}
