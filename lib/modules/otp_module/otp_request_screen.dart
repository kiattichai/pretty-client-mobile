import 'package:pretty_client_mobile/modules/otp_module/otp_flow_type.dart';
import 'package:pretty_client_mobile/modules/register_module/models/facebook_info_entity.dart';

class OtpRequestScreen {
  final String mobile;
  final OTPFlowType otpFlowType;
  String refCode;
  final String countryCode;
  String channel;
  int userId;
  String token;
  FacebookInfoEntity facebookInfoEntity;

  OtpRequestScreen(
      {this.mobile,
      this.otpFlowType,
      this.refCode,
      this.countryCode,
      this.channel,
      this.userId,
      this.token = '',
      this.facebookInfoEntity});
}
