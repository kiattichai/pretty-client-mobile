import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/otp_module/data/otp_api.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/modules/otp_module/models/otp_response_entity.dart';
import 'package:pretty_client_mobile/modules/otp_module/models/request_otp_entity.dart';
import 'package:pretty_client_mobile/modules/otp_module/models/request_verify_otp_entity.dart';

class OtpRepository extends BaseRepository {
  final OtpApi otpApi;

  @provide
  OtpRepository(this.otpApi);

  Future<OtpResponseData> requestOtp(RequestOtpEntity model) async {
    final response = await otpApi.requestOtp(model, BaseErrorEntity.badRequestToModelError);
    return response.data;
  }

  Future<void> verifyOtp(RequestVerifyOtpEntity model) async {
    await otpApi.verifyOtp(model, BaseErrorEntity.badRequestToModelError);
  }
}
