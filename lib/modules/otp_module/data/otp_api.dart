import 'package:dio/dio.dart';
import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/model/base_response_entity.dart';
import 'package:pretty_client_mobile/modules/otp_module/models/otp_response_entity.dart';
import 'package:pretty_client_mobile/modules/otp_module/models/request_otp_entity.dart';
import 'package:pretty_client_mobile/modules/otp_module/models/request_verify_otp_entity.dart';
import 'package:inject/inject.dart';

class OtpApi {
  final CoreApi coreApi;
  final String pathOtp = "/otps";

  @provide
  @singleton
  OtpApi(this.coreApi);

  Future<OtpResponseEntity> requestOtp(
      RequestOtpEntity requestOtpEntity, Function badRequestToModelError) async {
    Response response =
        await coreApi.post(pathOtp, requestOtpEntity.toJson(), badRequestToModelError);
    return OtpResponseEntity().fromJson(response.data);
  }

  Future<BaseResponseEntity> verifyOtp(
      RequestVerifyOtpEntity requestVerifyOtpEntity, Function badRequestToModelError) async {
    Response response =
        await coreApi.get(pathOtp, requestVerifyOtpEntity.toJson(), badRequestToModelError);
    return BaseResponseEntity().fromJson(response.data);
  }
}
