import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/register_social_module/register_mobile_social_screen.dart';

class RegisterSocialRoute extends BaseRoute {
  static String buildPath = '/register_social';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(builder: (context) => RegisterMobileSocialScreen(data));
  }
}
