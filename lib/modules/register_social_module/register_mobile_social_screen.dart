import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/country_code_module/country_code_screen.dart';
import 'package:pretty_client_mobile/modules/country_code_module/model/country_code_response_entity.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_flow_type.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_request_screen.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_route.dart';
import 'package:pretty_client_mobile/modules/register_module/register_view_model.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/widgets/input_mobile_widget.dart';
import 'package:pretty_client_mobile/widgets/radio_button_select_verify_channel_widget.dart';
import 'package:pretty_client_mobile/widgets/checkbox_aspect_termandcond_widget.dart';
import 'package:pretty_client_mobile/widgets/primary_button_widget.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

class RegisterMobileSocialScreen extends StatefulWidget {
  final FacebookLoginResult result;
  RegisterMobileSocialScreen(this.result);

  @override
  RegisterMobileSocialState createState() {
    return RegisterMobileSocialState();
  }
}

class RegisterMobileSocialState
    extends BaseStateProvider<RegisterMobileSocialScreen, RegisterMobileViewModel> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController controllerMobile = TextEditingController();
  final focusInputMobile = FocusNode();
  bool autoFocus = true;
  String socialUsername = "Lisa";
  String countryCode = "TH";
  String phoneCode = "+66";
  bool _isLoading = false;
  String currentGroupRadioValue = "sms";
  bool acceptTerm = false;
  bool _btnEnabled = false;

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
        keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
        keyboardBarColor: Colors.grey[200],
        nextFocus: true,
        actions: [
          KeyboardAction(
            focusNode: focusInputMobile,
          ),
        ]);
  }

  @override
  void initState() {
    super.initState();
    viewModel = RegisterMobileViewModel()..result = widget.result;
    viewModel.handleError = handleError;
  }

  Widget avatarInfo(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 0, top: 20, right: 0),
        child: Row(children: <Widget>[
          SizedBox(
            height: 48,
            width: 48,
            child: CircleAvatar(
              radius: 50.0,
              backgroundImage: NetworkImage(viewModel.facebookInfoEntity == null
                  ? ''
                  : viewModel.facebookInfoEntity.picture.data.url),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, top: 0, right: 0),
            child: SizedBox(
                height: 21,
                width: 150,
                child: Text(
                    "${viewModel.facebookInfoEntity == null ? '' : viewModel.facebookInfoEntity.name}",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontFamily: 'Sarabun',
                      color: Design.theme.colorActive,
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ))),
          )
        ]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return BaseWidget<RegisterMobileViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        color: Colors.transparent,
        progressIndicator: CircleLoading(),
        inAsyncCall: viewModel.loading,
        child: Scaffold(
            resizeToAvoidBottomPadding: false,
            appBar: CustomHeaderDetail(
              height: Screen.convertHeightSize(55),
              title: appLocal.registerHeader,
            ),
            body: KeyboardActions(
              isDialog: true,
              config: _buildConfig(context),
              child: Container(
                alignment: FractionalOffset.topCenter,
                padding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                child: SafeArea(
                  child: SingleChildScrollView(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          this.avatarInfo(context),
                          InputMobileWidget(
                            onCountrySelected: () async {
                              final _countryCode = await Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => CountryCodeScreen()),
                              ) as CountryCode;
                              print(_countryCode.toJson());
                              countryCode = _countryCode.isoCode;
                              phoneCode = _countryCode.phoneCodeText;
                            },
                            countryCode: countryCode,
                            phoneCode: phoneCode,
                            onMobileInputChanged: (String val) {
                              if (val.length >= 10) {
                                setState(() {
                                  _btnEnabled = true;
                                });
                              } else {
                                setState(() {
                                  _btnEnabled = false;
                                });
                              }
                            },
                            onMobileInputValidator: (String val) {
                              if (val.length >= 10) {
                                setState(() {
                                  _btnEnabled = true;
                                });
                              } else {
                                setState(() {
                                  _btnEnabled = false;
                                });
                              }
                            },
                            controllerMobile: controllerMobile,
                            focusInputMobile: focusInputMobile,
                            onFieldSubmitted: onInputFieldSubmitted,
                            appLocale: appLocal,
                          ),
                          RadioButtonSelectVerifyChannelWidget(
                            currentGroupRadioValue: currentGroupRadioValue,
                            onChannelSelected: (String val) {
                              _handleRadioValueChange(val);
                            },
                            appLocale: appLocal,
                          ),
                          CheckboxAspectTermandCondWidget(
                            acceptTerm: acceptTerm,
                            onAspectTermAndCondSelected: (bool val) {
                              _handleCheckBoxChange(val);
                            },
                            appLocale: appLocal,
                          ),
                          PrimaryButtonWidget(
                            buttonTitle: appLocal.registerBtnConfirm,
                            onBtnPressed: _btnEnabled == true && acceptTerm == true
                                ? () => _requestOTP(model)
                                : null,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )),
      ),
    );
  }

  bool shouldEnableButton() {
    if (controllerMobile.text.isEmpty || !acceptTerm) {
      return false;
    }
    return true;
  }

  void _handleRadioValueChange(String value) {
    setState(() {
      currentGroupRadioValue = value;
    });
  }

  void _handleCheckBoxChange(bool value) {
    setState(() {
      acceptTerm = value;
    });
  }

  void _requestOTP(RegisterMobileViewModel model) async {
    model.checkRegister(controllerMobile.text, countryCode, 'register', currentGroupRadioValue,
        checkRegisterSuccess,
        verifyMobile: false);
  }

  void onInputFieldSubmitted(String value) {
    focusInputMobile.unfocus();
  }

  void checkRegisterSuccess(String refOtp) {
    RouterService.instance.navigateTo(OtpRoute.buildPath,
        data: OtpRequestScreen(
            otpFlowType: OTPFlowType.REGISTER_SOCIAL,
            mobile: controllerMobile.text,
            countryCode: countryCode,
            channel: currentGroupRadioValue,
            refCode: refOtp,
            token: widget.result.accessToken.token,
            facebookInfoEntity: viewModel.facebookInfoEntity));
  }

  void handleError(BaseError error) {
    _showAlert(error.message);
  }

  void _showAlert(String message) {
    AlertDialog alertDialog = AlertDialog(
      content: Text(message),
      actions: <Widget>[],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alertDialog;
        });
  }
}
