import 'package:flutter/material.dart';

class MockHomeScreen extends StatelessWidget {
  final String title;

  const MockHomeScreen({Key key, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment(-0.87, -0.95),
            child: Text(
              title,
              style: TextStyle(fontSize: 32, fontWeight: FontWeight.w500),
            ),
          )
        ],
      ),
    );
  }
}
