import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/modules/coins_module/coins_transaction_view_model.dart';
import 'package:pretty_client_mobile/modules/coins_module/models/topup_coins_request_entity.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/utils/string_format.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:pretty_client_mobile/widgets/input_text_field_form_widget.dart';
import 'package:pretty_client_mobile/widgets/modal_select_time_widget.dart';
import 'package:pretty_client_mobile/widgets/input_picker_widget.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:flutter_picker/flutter_picker.dart';

import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:pretty_client_mobile/widgets/input_birthday_picker_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:pretty_client_mobile/widgets/primary_button_widget.dart';
import 'package:mime_type/mime_type.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:pretty_client_mobile/modules/coins_module/models/topup_transactions_response_entity.dart';
import 'package:pretty_client_mobile/modules/coins_module/topup_coins/topup_coins_view_model.dart';
import 'package:provider/provider.dart';
import 'package:pretty_client_mobile/app/app_state.dart';
import 'package:pretty_client_mobile/widgets/custom_modal.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/utils/validator.dart';
import 'package:pretty_client_mobile/modules/coins_module/models/banks_entity.dart';


const String MIN_DATETIME = '1960-01-01';
const String MAX_DATETIME = '2021-11-25';
const String INIT_DATETIME = '2019-05-17';

const double _kPickerSheetHeight = 216.0;

const PickerData = '''
[
  [  
    "ธนาคารกสิกรไทย จำกัด (มหาชน)"
  ]
    
]
    ''';

const PickerData2 = '''
[
  [  
    "Kasikornbank Public Company Limited"
  ]
    
]
    ''';

class TopupCoinsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TopupCoinsState();
  }
}

class TopupCoinsState extends BaseStateProvider<TopupCoinsScreen, TopupCoinsViewModel> {
  final numberFormat = new NumberFormat("#,###.##", "en_US");
  GlobalKey<FormState> _formKey = GlobalKey<FormState>(debugLabel: '_topupScreenkey');//GlobalKey<FormState>();
  final focusAmount = FocusNode();
  final TextEditingController controllerBank = TextEditingController();
  final TextEditingController controllerAmount = TextEditingController();
  final TextEditingController controllerTime = TextEditingController();
  final TextEditingController controllerUploadDate = TextEditingController();
  DateTime timeString = DateTime.now();
  bool _validate = false;
  bool _showTitle = true;
  DateTime dateTime;
  DateTime uploadTime;
  String _format = 'yyyy-MMM-dd';
  //String uploadTime = "";
  //String uploadDate = "";
  String bankAccount = "";
  String base64String = "";
  String mimeType = "";
  DateTimePickerLocale _locale = DateTimePickerLocale.en_us;
  File _image;
  bool isEnableButton = false;
  BanksDataRecord bankData;
  //final focusConfirmPassword = FocusNode();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
      keyboardBarColor: Colors.grey[200],
      nextFocus: true,
      actions: [
        KeyboardAction(
          focusNode: focusAmount,
        ),
      ]
    );
  }

  @override
  void initState() {
    super.initState();
    viewModel = TopupCoinsViewModel();
    viewModel.handleError = handleError;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    appLocal = AppLocalizations.of(context);
    appState = Provider.of<AppState>(context);

    Path customPath = Path()
      ..moveTo(10.5, 15)
      ..lineTo(10.5, 65);
    List<double> lineStep = [4, 6];
    Color colorLine = Color(0xffa4a4a4);

    Screen(context);
    return BaseWidget<TopupCoinsViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        progressIndicator: CircleLoading(),
        inAsyncCall: model.loading,
        color: Colors.transparent,
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          appBar: CustomHeaderDetail(
            height: Screen.convertHeightSize(55),
            title: "Topup Coins",
            onTap: () {
              RouterService.instance.pop();
            },
          ),
          body: KeyboardActions(
            isDialog: false,
            config: _buildConfig(context),
            child: Container(
              alignment: FractionalOffset.topCenter,
              padding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              child: SafeArea(
                child: SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 10),
                        InputPickerWidget(
                          titleTextField: 
                          appState.appLocal == Locale('th') 
                          ? 'โอนเงินไปธนาคาร'
                          : 'Transfer money to account',
                          hintTextField: 
                          appState.appLocal == Locale('th') 
                          ? 'เลือกธนาคาร'
                          : 'Choose bank',
                          appLocal: appLocal,
                          controller: controllerBank,
                          onFieldTap: () {
                            showBankPicker(context, model);
                          },
                        ), 
                        renderBankInfo(context, model),
                        InputPickerWidget(
                          titleTextField: 
                          appState.appLocal == Locale('th') 
                          ? 'วันที่โอน'
                          : 'Transfer date',
                          hintTextField: 
                          appState.appLocal == Locale('th') 
                          ? 'วันที่'
                          : 'Date',
                          appLocal: appLocal,
                          controller: controllerUploadDate,
                          onFieldTap: () {
                            FocusScope.of(context).requestFocus(new FocusNode());
                            showDatePicker();
                          },
                        ), 

                        InputPickerWidget(
                          titleTextField: 
                          appState.appLocal == Locale('th') 
                          ? 'เวลาที่โอน'
                          : 'Transfer time',
                          hintTextField: 
                          appState.appLocal == Locale('th') 
                          ? 'เวลาที่โอน'
                          : 'Transfer time',
                          appLocal: appLocal,
                          controller: controllerTime,
                          onFieldTap: () {
                            FocusScope.of(context).requestFocus(new FocusNode());
                            showModalTime();
                          },
                        ), 
                        
                        SizedBox(height: 5),
                        InputTextFieldFormWidget(
                          autoFocus: false,
                          keyboardType: TextInputType.numberWithOptions(decimal: true),
                          title: appState.appLocal == Locale('th') 
                          ? 'จำนวนเงิน'
                          : 'Amount of money',
                          hintText: "0.00",
                          validate: _validate,
                          textEditingController: controllerAmount,
                          onInputChanged: (val) {

                            // if (val != null) {
                            //   if (val.length == 2 && val.startsWith('0')) {
                            //     controllerAmount.text = val.replaceAll('0', '');
                            //     final selection = TextSelection.collapsed(offset: 1);
                            //     controllerAmount.selection = selection;
                            //   } 
                            // }

                            setState(() {
                              updateEnableButton();
                            });
                          },
                          inputFormatter: ValidatorInputFormatter(
                            editingValidator: DecimalNumberEditingRegexValidator(),
                          ),
                          onInputValidator: (String value) {
                            double amount = double.tryParse(value);

                            if (value.isEmpty) {
                              return "กรุณากรอกข้อมูล";
                            } else if (amount > 100000) {
                              return 'จำนวนเงินต้องไม่เกิน 100,000';
                            }
                            // var regExpResult = _regExpCheckString(value);

                            // if (value.isEmpty) {
                            //   return "กรุณากรอกข้อมูล";
                            // } else if (!regExpResult) {
                            //   return "ห้ามมี 0 ข้างหน้า";
                            // } else {
                            //   return null;
                            // }
                          },
                          onFieldSubmitted: (val) {
                            FocusScope.of(context).requestFocus(focusAmount);
                          },
                          
                          focusInput: focusAmount
                        ),
                        SizedBox(height: 20),
                        _image == null 
                        ?
                        GestureDetector(
                          child: Container(
                            width: Screen.width,
                            height: 78,
                            child: DottedBorder(
                              // customPath: customPath,
                              color: Design.theme.colorInactive,
                              borderType: BorderType.RRect,
                              radius: Radius.circular(8),
                              // dashPattern: [9, 5],
                              // strokeWidth: 1.5,
                              child: Container(
                                child: Center(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[

                                      SvgPicture.asset(
                                        'images/cloud_upload.svg',
                                        // 'images/icon_arrow_left.svg',
                                        width: 36,
                                        height: 25,
                                        color: Design.theme.colorText2,
                                      ),
                                      SizedBox(height: 5),
                                      Text(
                                        appState.appLocal == Locale('th') 
                                        ? 'คลิกเลือกภาพเพื่ออัพโหลดสลิป'
                                        : 'Click to select an image to upload a slip.',
                                        style: TextStyle(
                                          fontFamily: 'Sarabun',
                                          color: Design.theme.colorText2,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )
                                      ),
                                        
                              
                                    ]
                                  ),
                                ),
                                decoration: ShapeDecoration(
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                                  color: Colors.transparent,
                                ),
                              )
                            ),
                          ),
                          onTap: handleOpenUploadSlip,
                        )
                        
                        : previewBox(context),
                        SizedBox(
                          height: 20,
                        ),
                        PrimaryButtonWidget(
                          buttonTitle: 
                          appState.appLocal == Locale('th') 
                          ? 'อัพโหลดสลิปการชำระเงิน'
                          : 'Upload Payment Slip',
                          onBtnPressed: isEnableButton ? () => uploadSlip() : null
                        )
                          // onBtnPressed: isEnableButton ? () => updateProfile() : null)
                      ],
                      
                    )
                  ) 
                ),
              ),
            ),
             
            
          ),
        ),
      ),
    );
  }

  handleDisplayTime(DateTime value) {
    FocusScope.of(context).requestFocus(FocusNode());
    setState(() {
      controllerTime.text = displayTime(value);
      if (value != null) {
        timeString = value;
      } else {
        timeString = DateTime.now();
      }
      updateEnableButton();
    });
  }

  handleDisplayTimeOnClickCancel() {
    FocusScope.of(context).requestFocus(FocusNode());
    controllerTime.text = "";
  }

  void containerForSheet<T>({BuildContext context, Widget child}) {
    showCupertinoModalPopup<T>(
      context: context,
      builder: (BuildContext context) => child,
    ).then<void>((T value) {
      // Scaffold.of(context).showSnackBar(new SnackBar(
      //   content: new Text('You clicked $value'),
      //   duration: Duration(milliseconds: 800),
      // ));
    });
  }

  Future handleOpenUploadSlip() async {
  
    containerForSheet<String>(
      context: context,
      child: CupertinoActionSheet(
        actions: <Widget>[
          CupertinoActionSheetAction(
            child: Text('Camera'),
            onPressed: () {
              openCamera();
            },
          ),
          CupertinoActionSheetAction(
            child: Text('Photo Library'),
            onPressed: () {
              openPicker();
            },
          )
        ],
        cancelButton: CupertinoActionSheetAction(
          child: Text('Cancel'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      )
    );
  }

  Future openPicker() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxWidth: 450, maxHeight: 450, imageQuality: 80);
    setState(() {
      _image = image;
      updateEnableButton();
    });

    List<int> imageBytes = await image.readAsBytes();
    print("imageBytes : ${imageBytes}");
    print("${image.path}");
    print("path : ${image.path}");
    String mimeType = mime(image.path);
    print("mimeType : ${mimeType}");
    String base64Image = base64Encode(imageBytes);
    print("${getFileFormat(mimeType)}");
    String fileFormat = getFileFormat(mimeType);
    uploadImage(base64Image, fileFormat);
  }

  Future openCamera() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.camera, maxWidth: 450, maxHeight: 450, imageQuality: 80);
    setState(() {
      _image = image;
      updateEnableButton();
    });

    List<int> imageBytes = await image.readAsBytes();
    print(imageBytes);
    print("path : ${image.path}");
    String mimeType = mime(image.path);
    print("mimeType : ${mimeType}");
    String base64Image = base64Encode(imageBytes);

    String fileFormat = getFileFormat(mimeType);
    uploadImage(base64Image, fileFormat);
  }

  void uploadImage(String base64Image, String mime) {
    if (base64Image == null) {
      _showAlertError("Can't get image");
    } else {
      setState(() {
        mimeType = mime;
        base64String = base64Image;
      });

      Navigator.of(context).pop();
      //viewModel.uploadAvatar(base64Image, mime, onUpdateProfileSuccess);
    }
  }

  String getFileFormat(String fileName) {
    int lastDot = fileName.lastIndexOf('/', fileName.length - 1);
    if (lastDot != -1) {
      String result = fileName.substring(lastDot + 1);
      if (result == "jpeg") {
        return "jpg";
      } else {
        return result;
      }
    } else
      return null;
  }

  bool _regExpCheckString(String value) {
    print(value);
    RegExp regExp = new RegExp(
      //r"^\$|^(0|([1-9][0-9]{0,3}))(\\.[0-9]{0,2})?\$",
      r"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$",
      caseSensitive: false,
      multiLine: false,
    );
    print('regExp : ${regExp.hasMatch(value)}');
    if (regExp.hasMatch(value)) {
      return true;
    }
    return false;
    // RegExp regExp = new RegExp(
    //   r"^[a-zA-Z0-9ก-๙]*$",
    //   caseSensitive: false,
    //   multiLine: false,
    // );
    // print('regExp : ${regExp.hasMatch(value)}');
    // if (regExp.hasMatch(value)) {
    //   return true;
    // }
    // return false;
  }

  void showModalTime() {
    
    DatePicker.showDatePicker(
      context,
      pickerMode: DateTimePickerMode.time,
      pickerTheme: DateTimePickerTheme(
        showTitle: _showTitle,
        confirm: Text(appLocal.commonConfirm, style: TextStyle(fontFamily: 'Sarabun', color: Color(0xff3a3a3a))),
        cancel: Text(appLocal.commonCancel, style: TextStyle(fontFamily: 'Sarabun', color: Color(0xff848484))),
      ),
      initialDateTime: uploadTime,
      maxDateTime: DateTime.now(),
      locale: _locale,
      onClose: () => print("----- onClose -----"),
      onCancel: () => print('onCancel'),
      onChange: (dateTime, List<int> index) {
        setState(() {
          uploadTime = dateTime;
          // controllerUploadDate.text = DateFormat("yyyy-MM-dd").format(_dateTime);
          updateEnableButton();
        });
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          FocusScope.of(context).requestFocus(FocusNode());
          uploadTime = dateTime;
          controllerTime.text = convertDateToDisplay(dateTime, 'HH:mm');//DateFormat("hh:mm").format(dateTime);
      
          updateEnableButton();
        });
      },
    );

  }

  String convertDateToDisplay(DateTime dateTime, String formatDate) {
    String dateStr = DateFormat(formatDate, "en_US").format(dateTime);
    return dateStr;
  }

  showBankPicker(BuildContext context, TopupCoinsViewModel model) {
    String jsonBank = jsonEncode(model.banksName);  
    Picker(
      adapter: PickerDataAdapter<String>(
        pickerdata: JsonDecoder().convert(jsonBank),
        isArray: false,
      ),
      changeToFirst: true,
      hideHeader: false,
      cancelText: "ยกเลิก",
      confirmText: "ตกลง",
      selectedTextStyle: TextStyle(fontFamily: 'Sarabun', color: Color(0xff3a3a3a)),
      confirmTextStyle: TextStyle(fontFamily: 'Sarabun', color: Color(0xff3a3a3a)),
      cancelTextStyle: TextStyle(fontFamily: 'Sarabun', color: Color(0xff3a3a3a)),
      
      onConfirm: (Picker picker, List value) {
        print(value.toString());
        print(picker.adapter.text);
        setState(() {
          controllerBank.text = picker.getSelectedValues().first;
          bankAccount = value.toString();

          print(value.toString());
          var getBankAccount = model.banksRecord.where((item) => item.name == picker.getSelectedValues().first);
          bankData = getBankAccount.first;
        });
      },
      onCancel: () {
        setState(() {
          controllerBank.text = "";
          bankAccount = "";  
          bankData = null;
        });
      }
      
    ).showModal(this.context);
  }

  // Display date picker.
  void showDatePicker() {
    DatePicker.showDatePicker(
      context,
      pickerTheme: DateTimePickerTheme(
        showTitle: _showTitle,
        confirm: Text(appLocal.commonConfirm, style: TextStyle(fontFamily: 'Sarabun', color: Color(0xff3a3a3a))),
        cancel: Text(appLocal.commonCancel, style: TextStyle(fontFamily: 'Sarabun', color: Color(0xff848484))),
      ),
      minDateTime: DateTime.parse(MIN_DATETIME),
      maxDateTime: DateTime.now(),//DateTime.parse(MAX_DATETIME),
      initialDateTime: dateTime,
      dateFormat: _format,
      locale: _locale,
      onClose: () => print("----- onClose -----"),
      onCancel: () {
        setState(() {
          controllerUploadDate.text = null;
          updateEnableButton();
        });
      },
      onChange: (dateTime, List<int> index) {
        setState(() {
          dateTime = dateTime;
          controllerUploadDate.text = DateFormat("yyyy-MM-dd").format(dateTime);
          updateEnableButton();
        });
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          FocusScope.of(context).requestFocus(FocusNode());
          dateTime = dateTime;
          controllerUploadDate.text = DateFormat("yyyy-MM-dd").format(dateTime);
          updateEnableButton();
        });
      },
    );

    // DatePicker.showDatePicker(context,
    //     showTitleActions: true,
    //     minTime: DateTime(2018, 3, 5),
    //     maxTime: DateTime(2019, 6, 7), onChanged: (date) {
    //   print('change $date');
    // }, onConfirm: (date) {
    //   print('confirm $date');
    // }, currentTime: DateTime.now(), locale: LocaleType.th);
  }

  String displayTime(DateTime value) {
    print(value);
    if (value == null) {
      return "";
    } else {
      return value.hour.toString().padLeft(2, '0') + ':' + value.minute.toString().padLeft(2, '0');
    }
  }

  void updateEnableButton() {
    double amount = double.tryParse(controllerAmount.text);

    print(amount);
    if (controllerBank.text.length == 0 ||
        controllerUploadDate.text.length == 0 ||
        controllerTime.text.length == 0 ||
        controllerAmount.text.length == 0 ||
        _image == null ||
        amount <= 0) {
      isEnableButton = false;
      //isEnableButton = true;
    } else {
      isEnableButton = true;
    }

   
  }

  void _showAlertSuccess(String message) {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomModal(
            type: ModalType.NORMAL,
            title: appLocal.confirmation,
            desc: message,
            appLocal: appLocal,
            onTapConfirm: () {
              Navigator.of(context).pop();
            },
          );
        });
  
  }

  void _showAlertError(String message) {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomModal(
            type: ModalType.NORMAL,
            title: appLocal.errorDialogTitle,
            desc: message,
            appLocal: appLocal,
            onTapConfirm: () {
              
            },
          );
        });
  
  }

  uploadSlip() {
    if (_formKey.currentState.validate()) {
      // No any error in validation
      print('uploadSlip');
      _formKey.currentState.save();
      FocusScope.of(context).requestFocus(FocusNode());
      viewModel.uploadSlip(
          TopupCoinsRequestEntity()
            ..amount = double.parse(controllerAmount.text)
            ..bankId = 1
            ..paidAt = "${controllerUploadDate.text} ${timeString.hour.toString().padLeft(2, '0')}:${timeString.minute.toString().padLeft(2, '0')}:${timeString.second.toString().padLeft(2, '0')}" 
            ..fileName = mimeType
            ..fileData = base64String,
          onUploadSlipSuccess);
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }

  void onUploadSlipSuccess(TopupTransactionsResponseEntity baseResponse) {
    _showAlertSuccess(baseResponse.data.message);
  }

  Widget removeImageButton(BuildContext context) {
    return Positioned(
        right: 5.0,
        top: 2.0,
        child: Stack(
          children: <Widget>[
            Container(
                width: 28,
                height: 28,
            ),
            Positioned.fill(
              child: Align(
                alignment: Alignment.center,
                child: SvgPicture.asset('images/cancel.svg', color: Design.theme.fontColorSecondary,),
              ),
            )
          ],
        ));
  }

  Widget previewBox(BuildContext context) {
    return Container(
      
      width: Screen.width,
      height: 98,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 0,
            child: Container(
              // padding: EdgeInsets.only(top: 10),
              width: 98,
              height: 98,
              child: InkWell(
                child: Stack(
                  // alignment: Alignment.topLeft, 
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 10),
                          child: Image.file(
                            _image,
                            fit: BoxFit.cover,
                            width: 78,
                            height: 78,
                          ),
                        )
                      ],
                    ),
                    removeImageButton(context)
                  ]
                ),
                onTap: () => {
                  setState(() {
                    _image = null;
                  })
                }
              )
            ),
          ),
          Expanded(flex: 1, child: Container(height: 78,))
        ],
      )
    );
  }

  Widget renderBankInfo(BuildContext context, TopupCoinsViewModel model) {

    if (bankData == null) {
      return Container(
        height: 0,
      );
    } else {
      return Container(
        padding: EdgeInsets.only(top: 5),
        width: Screen.width,
        
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 5, left: 5),
             
              width: Screen.width,
              height: 30,
              child: Text(
                appState.appLocal == Locale('th') 
                ? "ข้อมูลการโอนเงิน"
                : "Money transfer information",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontFamily: 'Sarabun',
                  color: Design.theme.colorActive,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ),
              )
            ),
            Container(
              padding: EdgeInsets.only(top: 5),
              child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 10),
                Container(
                  width: 43,
                  height: 43,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      fit: BoxFit.fitWidth,
                      image: NetworkImage(bankData.image.mobile)
                    ),
                    border: Border.all(color: Design.theme.colorInactive, width: 1),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 5, left: 10, bottom: 5, right: 5),
                  
                  child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(bankData.name,
            
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontFamily: 'Sarabun',
                        color: Design.theme.colorActive,
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    Row(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      // mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          flex: 0, 
                          child: Text(
                            appState.appLocal == Locale('th') 
                            ? 'สาขา'
                            : 'Branch',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontFamily: 'Sarabun',
                              color: Design.theme.colorActive,
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                        SizedBox(width: 20),
                        Expanded(
                          flex: 0, 
                          child: Text(bankData.branch,
                    
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontFamily: 'Sarabun',
                              color: Design.theme.colorActive,
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            )
                          ),
                        )
                      ]
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 0, 
                          child: Text(
                            appState.appLocal == Locale('th') 
                            ? 'เลขที่บัญชี'
                            : 'Account number',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontFamily: 'Sarabun',
                              color: Design.theme.colorActive,
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            )
                          ),
                        ),
                        SizedBox(width: 20),
                        Expanded(
                          flex: 0, 
                          child: Text(
                            bankData.account.number,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontFamily: 'Sarabun',
                              color: Design.theme.colorActive,
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            )
                          ),
                        )
                      ]
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 0, 
                          child: Text(
                            appState.appLocal == Locale('th') 
                            ? 'ประเภท'
                            : 'Type',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontFamily: 'Sarabun',
                              color: Design.theme.colorActive,
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            )
                          ),
                        ),
                        SizedBox(width: 20),
                        Expanded(
                          flex: 0, 
                          child: Text(bankData.type,
                       
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontFamily: 'Sarabun',
                              color: Design.theme.colorActive,
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            )
                          ),
                        )
                      ]
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 0, 
                          child: Text(
                            appState.appLocal == Locale('th')
                            ? 'ชื่อบัญชี'
                            : 'Account Name',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontFamily: 'Sarabun',
                              color: Design.theme.colorActive,
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            )
                          ),
                        ),
                        SizedBox(width: 20),
                        Expanded(
                          flex: 0, 
                          child: Text(bankData.account.name,
                   
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontFamily: 'Sarabun',
                              color: Design.theme.colorActive,
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            )
                          ),
                        )
                      ]
                    )
                  ]
                ),
                )
                
              ]
            ),
            )
          ]
        ),
      );
    }
  }

  void handleError(BaseError error) {
    print(error.toJson());

    _showAlertError(error.message);
  }
}

class DecimalNumberEditingRegexValidator extends RegexValidator {
  DecimalNumberEditingRegexValidator()
      : super(regexSource: "^\$|^(0|([1-9][0-9]{0,5}))(\\.[0-9]{0,2})?\$");
}

class DecimalNumberSubmitValidator implements StringValidator {
  @override
  bool isValid(String value) {
    try {
      final number = double.parse(value);
      return number > 0.0;
    } catch (e) {
      return false;
    }
  }
}