import 'dart:ffi';
import 'dart:math';

import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/coins_module/models/topup_coins_request_entity.dart';
import 'package:pretty_client_mobile/modules/coins_module/models/banks_entity.dart';

class TopupCoinsViewModel extends BaseViewModel {
 
  Function handleError;
  Double amount;
  int bankId;
  String paidAt;
  List<BanksDataRecord> banksRecord = [];
  List<String> banksName = [];
  TopupCoinsRequestEntity topupCoinsRequestEntity;

  @override
  void postInit() {
    super.postInit();
    getBanks();
  }

  void uploadSlip(TopupCoinsRequestEntity request, Function uploadSlipSuccess) {
    print('request : ${request}');
    catchError(() async {
      setLoading(true);
      final result = await di.coinsRepository.topupCoinsTransactions(request);
      uploadSlipSuccess(result);
      setLoading(false);
    });
  }
  
  void getBanks() {
    catchError(() async {
      setLoading(true);
      final result = await di.coinsRepository.banksAccount();
      banksRecord..addAll(result.data.record);
      banksRecord.forEach((item) {
        banksName.add(item.name);
      });
      print(banksRecord);
      
      setLoading(false);
    });
  }

  bool shouldLogin() {
    return di.sharePrefInterface.shouldLogin();
  }

  @override
  void onError(error) {
    super.onError(error);
    setLoading(false);
    if (error is BaseError) {
      handleError(error);
    }
  }
}
