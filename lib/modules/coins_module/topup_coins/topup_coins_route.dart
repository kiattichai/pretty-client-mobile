import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/coins_module/topup_coins/topup_coins_screen.dart';

class TopupCoinsRoute extends BaseRoute {
  static String buildPath = '/topup_coins';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
      builder: (context) => TopupCoinsScreen(),
    );
  }
}