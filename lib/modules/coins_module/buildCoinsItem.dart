import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/env_config.dart';
import 'package:pretty_client_mobile/modules/coins_module/models/coins_transactions_response_entity.dart';
import 'package:pretty_client_mobile/utils/screen.dart';

class BuildCoinsItem extends StatelessWidget {
  final CoinsTransactionsResponseDataRecord recordCoins;
  final String type;

  const BuildCoinsItem({this.recordCoins, this.type = ""});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return buildItem(this.recordCoins);
  }

  Widget buildItem(CoinsTransactionsResponseDataRecord record) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Row(
                  children: <Widget>[
                    record.image.id != null && record.type.id != 3
                        ? Image.network(
                            getImgUrl(record.image, 70, 70),
                            width: 50,
                          )
                        : Container(
                            width: 50,
                            child: SvgPicture.asset(
                              "images/icon_coins_white.svg",
                              width: 40,
//                              color:
//                                  (record.type.id == 5 || record.type.id == 2)
//                                      ? (Colors.grey)
//                                      : Design.theme.primaryColor,
                            ),
                          ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: Screen.convertWidthSize(220),
                            child: Text(
                              record.description,
                              style: TextStyle(
                                color: Design.theme.fontColorPrimary,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Container(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                this.type == "topup"
                                    ? Text(
                                        record.documentStatus.text,
                                        style: TextStyle(
                                          color: record.documentStatus.id == 0
                                              ? Design.theme.fontColorPrimary
                                              : (record.documentStatus.id == 1
                                                  ? Colors.green
                                                  : Colors.redAccent),
                                        ),
                                      )
                                    : Text(
                                        record.createdAt != null
                                            ? "${record.createdAt.date} ${record.createdAt.time}"
                                            : "",
                                        style: TextStyle(
                                          color:
                                              Design.theme.fontColorSecondary,
                                        ),
                                      ),
//                                    : Text(
//                                        record.title,
//                                        style: TextStyle(
//                                          color:
//                                              Design.theme.fontColorSecondary,
//                                        ),
//                                      ),
                                Align(
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    "${record.amountText}",
                                    style: TextStyle(
                                      color: (record.type.id == 5 ||
                                              record.type.id == 2)
                                          ? Colors.redAccent
                                          : record.documentStatus.id == 0
                                              ? Design.theme.fontColorPrimary
                                              : (record.documentStatus.id == 1
                                                  ? Color(0xff2d8206)
                                                  : Colors.redAccent),
                                      fontSize: 15,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          this.type == "topup"?Text(
                            record.createdAt!=null ? "${record.createdAt.date} ${record.createdAt.time}": "",
                            style: TextStyle(
                              color: Design.theme.fontColorSecondary,
                            ),
                          ) : Container(),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        lineBottom(),
      ],
    );
  }

  Widget lineBottom({double size = 1, Color color}) {
    color = color == null ? Design.theme.colorLine.withOpacity(0.6) : color;
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: size,
            color: color,
          ),
        ),
      ),
    );
  }

  String getImgUrl(
      CoinsTransactionsResponseDataRecordImage img, double w, double h) {
    String result = "https://admin.prettyhub.me/images/image-empty.png";
    if (img.id != null) {
      result =
          "${EnvConfig.instance.resUrl}/w_${w.round()},h_${h.round()},c_crop/${img.id}/${img.name}";
    }
    return result;
  }
}
