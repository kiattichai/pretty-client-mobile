import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/modules/coins_module/coins_transaction_screen.dart';

class CoinsTransactionRoute extends BaseRoute {
  static String buildPath = '/coins';

  @override
  String get path => buildPath;

  @override
  Route routeTo(data) {
    return MaterialPageRoute(
      builder: (context) => CoinsTransactionScreen(
        coins : data
      ),
    );
  }
}