import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/modules/coins_module/coins_transaction_view_model.dart';
import 'package:pretty_client_mobile/modules/coins_module/all_tab.dart';
import 'package:pretty_client_mobile/modules/coins_module/earning_tab.dart';
import 'package:pretty_client_mobile/modules/coins_module/spending_tab.dart';
import 'package:pretty_client_mobile/modules/coins_module/topup_tab.dart';
import 'package:pretty_client_mobile/modules/main_module/main_screen_route.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/custom_header_detail.dart';
import 'package:pretty_client_mobile/modules/coins_module/topup_coins/topup_coins_route.dart';

class CoinsTransactionScreen extends StatefulWidget {
  final double coins;

  const CoinsTransactionScreen({Key key, this.coins}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CoinsTransactionState();
  }
}

class CoinsTransactionState
    extends BaseStateProvider<CoinsTransactionScreen, CoinsTransactionViewModel>
    with TickerProviderStateMixin {
  final numberFormat = new NumberFormat("#,###.##", "en_US");
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: 4, vsync: this);
    viewModel = CoinsTransactionViewModel();
    viewModel
//      ..onErrorNotificationList = handleError
      ..scrollController.addListener(() {
        if (viewModel.scrollController.position.extentAfter == 0) {
          viewModel.setPage(viewModel.page + 1);
          viewModel.getCoinsTransactions(viewModel.limit, viewModel.page);
        }
      });

    _tabController.addListener(_handleTabSelection);
  }

  _handleTabSelection() {
    viewModel.getMyProfile();
    print("_handleTabSelection ${_tabController.index}");
  }

  Widget lineBottom({double size = 1, Color color}) {
    color = color == null ? Design.theme.colorLine.withOpacity(0.6) : color;
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: size,
            color: color,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return BaseWidget<CoinsTransactionViewModel>(
      model: viewModel,
      builder: (context, model, child) => Scaffold(
        appBar: CustomHeaderDetail(
          height: Screen.convertHeightSize(55),
          title: appLocal.myCoins,
          onTap: () {
            RouterService.instance.navigateTo(MainScreenRoute.buildPath,
                data: 4, clearStack: true);
          },
        ),
        body: Builder(
          builder: (context) {
            return Column(
              children: <Widget>[
                Container(
                  padding:
                      EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 15),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              child: SvgPicture.asset(
                                "images/icon_coins2.svg",
                              ),
                              padding: EdgeInsets.only(right: 10),
                            ),
                            RichText(
                              overflow: TextOverflow.ellipsis,
                              text: TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                    text:
                                        "${model.userModel != null ? numberFormat.format(model.userModel.wallet) : numberFormat.format(widget.coins)} ",
                                    style: TextStyle(
                                      color: Design.theme.colorInactive,
                                      fontSize: 26,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  TextSpan(
                                    text: "coins",
                                    style: TextStyle(
                                        color: Design.theme.colorText,
                                        fontSize: 18),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: () => RouterService.instance.navigateTo(TopupCoinsRoute.buildPath),
                        child: Container(
                          child: SvgPicture.asset(
                            "images/icon_topup_coins.svg",
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                lineBottom(size: 2),
                SizedBox(
                  height: 5,
                ),
                Container(
                  child: TabBar(
                    controller: _tabController,
                    labelColor: Design.theme.colorPrimary,
                    unselectedLabelColor: Design.theme.colorInactive,
                    labelStyle: TextStyle(
                      fontFamily: 'Sarabun',
                      color: Design.theme.colorPrimary,
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                    unselectedLabelStyle: TextStyle(
                      fontFamily: 'Sarabun',
                      color: Design.theme.colorInactive,
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ),
                    indicator: UnderlineTabIndicator(
                      borderSide: BorderSide(
                        width: 2.0,
                        color: Design.theme.colorPrimary,
                      ),
                    ),
                    labelPadding: EdgeInsets.only(left: 22, right: 22),
                    isScrollable: true,
                    tabs: <Widget>[
                      Tab(
                        text: appLocal.allHistory,
                      ),
                      Tab(
                        text: appLocal.earnings,
                      ),
                      Tab(
                        text: appLocal.spendings,
                      ),
                      Tab(
                        text: appLocal.topup,
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: TabBarView(
                    children: <Widget>[
                      AllTab(),
                      EarningTab(),
                      SpendingTab(),
                      TopupTab(),
                    ],
                    controller: _tabController,
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
