import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class CoinsTransactionsResponseEntity with JsonConvert<CoinsTransactionsResponseEntity> {
	CoinsTransactionsResponseData data;
	CoinsTransactionsResponseBench bench;
}

class CoinsTransactionsResponseData with JsonConvert<CoinsTransactionsResponseData> {
	CoinsTransactionsResponseDataPagination pagination;
	List<CoinsTransactionsResponseDataRecord> record;
}

class CoinsTransactionsResponseDataPagination with JsonConvert<CoinsTransactionsResponseDataPagination> {
	@JSONField(name: "current_page")
	int currentPage;
	@JSONField(name: "last_page")
	int lastPage;
	int limit;
	int total;
}

class CoinsTransactionsResponseDataRecord with JsonConvert<CoinsTransactionsResponseDataRecord> {
	int id;
	@JSONField(name: "transaction_no")
	String transactionNo;
	CoinsTransactionsResponseDataRecordType type;
	String title;
	String description;
	CoinsTransactionsResponseDataRecordUser user;
	String amount;
	@JSONField(name: "amount_text")
	String amountText;
	@JSONField(name: "payment_provider")
	String paymentProvider;
	@JSONField(name: "payment_method")
	String paymentMethod;
	@JSONField(name: "created_at")
	CoinsTransactionsResponseDataRecordCreatedAt createdAt;
	@JSONField(name: "updated_at")
	CoinsTransactionsResponseDataRecordUpdatedAt updatedAt;
	@JSONField(name: "paid_at")
	String paidAt;
	CoinsTransactionsResponseDataRecordStatus status;
	@JSONField(name: "document_status")
	CoinsTransactionsResponseDataRecordDocumentStatus documentStatus;
	CoinsTransactionsResponseDataRecordImage image;
	CoinsTransactionsResponseDataRecordBank bank;
	CoinsTransactionsResponseDataRecordStaff staff;
	String remark;
}

class CoinsTransactionsResponseDataRecordType with JsonConvert<CoinsTransactionsResponseDataRecordType> {
	int id;
	String text;
}

class CoinsTransactionsResponseDataRecordUser with JsonConvert<CoinsTransactionsResponseDataRecordUser> {
	int id;
	String username;
	@JSONField(name: "country_code")
	String countryCode;
	@JSONField(name: "first_name")
	String firstName;
	@JSONField(name: "last_name")
	String lastName;
}

class CoinsTransactionsResponseDataRecordCreatedAt with JsonConvert<CoinsTransactionsResponseDataRecordCreatedAt> {
	String value;
	String date;
	String time;
}

class CoinsTransactionsResponseDataRecordUpdatedAt with JsonConvert<CoinsTransactionsResponseDataRecordUpdatedAt> {
	String value;
	String date;
	String time;
}

class CoinsTransactionsResponseDataRecordStatus with JsonConvert<CoinsTransactionsResponseDataRecordStatus> {
	int id;
	String text;
}

class CoinsTransactionsResponseDataRecordDocumentStatus with JsonConvert<CoinsTransactionsResponseDataRecordDocumentStatus> {
	int id;
	String text;
}

class CoinsTransactionsResponseDataRecordImage with JsonConvert<CoinsTransactionsResponseDataRecordImage> {
	String id;
	dynamic tag;
	String name;
	int width;
	int height;
	String mime;
	int size;
	String url;
}

class CoinsTransactionsResponseDataRecordBank with JsonConvert<CoinsTransactionsResponseDataRecordBank> {

}

class CoinsTransactionsResponseDataRecordStaff with JsonConvert<CoinsTransactionsResponseDataRecordStaff> {

}

class CoinsTransactionsResponseBench with JsonConvert<CoinsTransactionsResponseBench> {
	int second;
	double millisecond;
	String format;
}
