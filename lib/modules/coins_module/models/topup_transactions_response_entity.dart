import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';

class TopupTransactionsResponseEntity with JsonConvert<TopupTransactionsResponseEntity> {
	TopupTransactionsResponseData data;
	TopupTransactionsResponseBench bench;
}

class TopupTransactionsResponseData with JsonConvert<TopupTransactionsResponseData> {
	String message;
	String ref;
	String file;
}

class TopupTransactionsResponseBench with JsonConvert<TopupTransactionsResponseBench> {
	int second;
	double millisecond;
	String format;
}
