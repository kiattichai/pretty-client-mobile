import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class TopupCoinsRequestEntity with JsonConvert<TopupCoinsRequestEntity> {
  double amount;
  @JSONField(name: "bank_id")
  int bankId;
  @JSONField(name: "paid_at")
  String paidAt;
  @JSONField(name: "file_name")
  String fileName;
  @JSONField(name: "file_data")
  String fileData;
}
