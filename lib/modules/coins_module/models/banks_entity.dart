import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';

class BanksEntity with JsonConvert<BanksEntity> {
	BanksData data;
	BanksBench bench;
}

class BanksData with JsonConvert<BanksData> {
	List<BanksDataRecord> record;
	bool cache;
}

class BanksDataRecord with JsonConvert<BanksDataRecord> {
	int id;
	String name;
	BanksDataRecordAccount account;
	String type;
	String branch;
	BanksDataRecordImage image;
	bool status;
}

class BanksDataRecordAccount with JsonConvert<BanksDataRecordAccount> {
	String number;
	String name;
}

class BanksDataRecordImage with JsonConvert<BanksDataRecordImage> {
	String desktop;
	String mobile;
}

class BanksBench with JsonConvert<BanksBench> {
	int second;
	double millisecond;
	String format;
}
