import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/core/base_repository.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/coins_module/data/coins_api.dart';
import 'package:pretty_client_mobile/modules/coins_module/models/coins_transactions_response_entity.dart';
import 'package:pretty_client_mobile/modules/coins_module/models/topup_coins_request_entity.dart';
import 'package:pretty_client_mobile/modules/coins_module/models/topup_transactions_response_entity.dart';
import 'package:pretty_client_mobile/modules/coins_module/models/banks_entity.dart';

class CoinsRepository extends BaseRepository {
  final CoinsApi coinsApi;
  @provide
  CoinsRepository(this.coinsApi);

  Future<CoinsTransactionsResponseEntity> coinsTransactions(limit, page, status, types, documentStatus) async {
    final CoinsTransactionsResponseEntity response = await coinsApi.coinsTransactions(limit, page, status, types, documentStatus, BaseErrorEntity.badRequestToModelError );
    return response;
  }

  Future<TopupTransactionsResponseEntity> topupCoinsTransactions(TopupCoinsRequestEntity request) async {
    final TopupTransactionsResponseEntity response = await coinsApi.topupCoinsTransactions(request, BaseErrorEntity.badRequestToModelError);
    return response;
  }

  Future<BanksEntity> banksAccount() async {
    final BanksEntity response = await coinsApi.banksAccount(BaseErrorEntity.badRequestToModelError);
    return response;
  }
}