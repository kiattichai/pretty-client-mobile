import 'package:flutter/material.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/api/core_api.dart';
import 'package:pretty_client_mobile/errors/base_error_entity.dart';
import 'package:pretty_client_mobile/modules/coins_module/models/coins_transactions_response_entity.dart';
import 'package:pretty_client_mobile/modules/coins_module/models/topup_coins_request_entity.dart';
import 'package:pretty_client_mobile/modules/coins_module/models/topup_transactions_response_entity.dart';
import 'package:pretty_client_mobile/modules/coins_module/models/banks_entity.dart';

class CoinsApi {
  final CoreApi coreApi;

  @provide
  @singleton
  CoinsApi(this.coreApi);

  final String pathWalletTransactions = "/users/wallet/transactions";
  final String pathTopupTransactions = "/users/wallet/topup";
  final String pathBankAccount = "/banks";

  Future<CoinsTransactionsResponseEntity> coinsTransactions(int limit, int page, String status, String type, String documentStatus, Function badRequestToModelError) async {
    Map<String, String> query = {
      "limit": limit.toString(),
      "page": page.toString(),
      "statuses": status,
      "types" : type,
      "document_statuses": documentStatus
    };
    final response = await coreApi.get(pathWalletTransactions, query, badRequestToModelError, hasPermission: true);
    return CoinsTransactionsResponseEntity().fromJson(response.data);
  }

  Future<TopupTransactionsResponseEntity> topupCoinsTransactions(TopupCoinsRequestEntity request, Function badRequestToModelError) async {
    final response = await coreApi.post(pathTopupTransactions, request.toJson(), BaseErrorEntity.badRequestToModelError);
    return TopupTransactionsResponseEntity().fromJson(response.data);
  }
  
  Future<BanksEntity> banksAccount(Function badRequestToModelError) async {
   
    final response = await coreApi.get(pathBankAccount, null, badRequestToModelError, hasPermission: false);
    return BanksEntity().fromJson(response.data);
  }
}