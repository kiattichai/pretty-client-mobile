import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/modules/coins_module/models/coins_transactions_response_entity.dart';
import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_response_entity.dart';

class CoinsTransactionViewModel extends BaseViewModel {
  UserProfileResponseData userModel;
  List<CoinsTransactionsResponseDataRecord> coinsRecord = [];
  List<CoinsTransactionsResponseDataRecord> coinsRecord2 = [];
  List<CoinsTransactionsResponseDataRecord> coinsRecord3 = [];
  ScrollController scrollController;
  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;
  int limit = 10;
  int page = 1;
  bool isComplete = false;
  bool loadMore = false;
  String status= "complete";
  String types = "earnings,spendings,topup";
  String documentStatus = "approved";

  CoinsTransactionViewModel() {
    refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    scrollController = ScrollController();
  }

  @override
  void postInit() {
    // TODO: implement postInit
    super.postInit();
    getMyProfile();
    getCoinsTransactions(limit, page);
  }

  void getMyProfile() {
    setLoading(true);
    catchError(() async {
      final result = await di.profileRepository.getProfile();
      await di.favoriteRepository.getFavoriteList();
      print('result ${result.firstName}');
      if (result == null) {
        print("firstName null");
      } else {
        userModel = result;
        print("firstName ${result.firstName}");
      }
      setLoading(false);
    });
  }

  void getCoinsTransactions(limit, page) async {
    if (page == 1) {
      setLoading(true);
    } else {
      setLoadMore(true);
    }

    catchError(() async {
      CoinsTransactionsResponseEntity tmp = null;
      if (isComplete == false) {
        tmp = await di.coinsRepository.coinsTransactions(limit, page, status, types, documentStatus);
      }
      if (tmp !=null && tmp.data.record.length > 0 && tmp.data.pagination.currentPage <= tmp.data.pagination.lastPage) {
        coinsRecord.addAll(tmp.data.record);
      } else {
        if(coinsRecord.length==0){
          coinsRecord = null;
        }
        setIsComplete(true);
      }
      setLoading(false);
      setLoadMore(false);
    });
  }

  void setPage(int value) {
    page = value;
  }

  void setIsComplete(bool value) {
    isComplete = value;
  }

  void setLoadMore(bool value) {
    loadMore = value;
  }

  void setStatus(String value) {
    status = value;
  }

  void setTypes(String value) {
    types = value;
  }
  void setDocumentStatus(String value) {
    documentStatus = value;
  }



  void resetRecord() {
    setPage(1);
    setIsComplete(false);
    coinsRecord.clear();
  }

}
