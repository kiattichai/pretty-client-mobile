import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/core/base_state_ful.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/modules/coins_module/buildCoinsItem.dart';
import 'package:pretty_client_mobile/modules/coins_module/coins_transaction_view_model.dart';
import 'package:pretty_client_mobile/modules/coins_module/models/coins_transactions_response_entity.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/widgets/progress_hub_widget.dart';

import '../../env_config.dart';

class AllTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AllTabState();
  }
}

class AllTabState extends BaseStateProvider<AllTab, CoinsTransactionViewModel> {
  @override
  void initState() {
    super.initState();
    viewModel = CoinsTransactionViewModel();
    viewModel
      ..setTypes("earnings,spendings,topup")
      ..scrollController.addListener(() {
        if (viewModel.scrollController.position.extentAfter == 0) {
          viewModel.setPage(viewModel.page + 1);
          viewModel.getCoinsTransactions(viewModel.limit, viewModel.page);
        }
      });
  }

  Widget lineBottom({double size = 1, Color color}) {
    color = color == null ? Design.theme.colorLine.withOpacity(0.6) : color;
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: size,
            color: color,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    appLocal = AppLocalizations.of(context);
    Screen(context);
    return BaseWidget<CoinsTransactionViewModel>(
      model: viewModel,
      builder: (context, model, child) => ProgressHUD(
        progressIndicator: CircleLoading(),
        inAsyncCall: model.loading,
        color: Colors.transparent,
        child: RefreshIndicator(
          color: Design.theme.colorMainBackground,
          backgroundColor: Design.theme.colorPrimary,
          key: model.refreshIndicatorKey,
          onRefresh: () async {
            model.resetRecord();
            return model.getCoinsTransactions(model.limit, model.page);
          },
          child: Builder(
            builder: (context) {
              if (model.coinsRecord != null) {
                return ListView.builder(
                  physics: const AlwaysScrollableScrollPhysics(),
                  itemCount: model.coinsRecord.length,
                  controller: model.scrollController,
                  itemBuilder: (context, i) {
                    return BuildCoinsItem(recordCoins: model.coinsRecord[i]);
                  },
                );
              } else {
                return Padding(
                  padding: const EdgeInsets.only(top: 150.0),
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        SvgPicture.asset(
                          'images/inbox_fill.svg',
                        ),
                        SizedBox(
                          height: Screen.convertHeightSize(10),
                        ),
                        Text(
                          appLocal.mybookingEmptyText,
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                );
              }
            },
          ),
        ),
      ),
    );
  }

}
