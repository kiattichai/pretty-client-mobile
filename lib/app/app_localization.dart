import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pretty_client_mobile/model/language_entity.dart';

class AppLocalizations {
  final Locale locale;

  AppLocalizations(this.locale);
  static LanguageEntity of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations).languageEntity;
  }

  static const LocalizationsDelegate<AppLocalizations> delegate = _AppLocalizationsDelegate();

  LanguageEntity languageEntity;

  Future<bool> load() async {
    String jsonString = await rootBundle.loadString('i18n/${locale.languageCode}.json');
    Map<String, dynamic> jsonMap = await json.decode(jsonString);
    languageEntity = LanguageEntity().fromJson(jsonMap);
    return true;
  }
}

class _AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    // Include all of your supported language codes here
    return ['en', 'th'].contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) async {
    // AppLocalizations class is where the JSON loading actually runs
    AppLocalizations localizations = new AppLocalizations(locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}
