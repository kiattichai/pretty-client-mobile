import 'package:pretty_client_mobile/api/dio_di.dart';
import 'package:inject/inject.dart';
import 'package:pretty_client_mobile/app/share_preference.dart';
import 'package:pretty_client_mobile/app/share_preference_di.dart';
import 'package:pretty_client_mobile/modules/booking_module/data/booking_repository.dart';
import 'package:pretty_client_mobile/modules/booking_module/data/order_repository.dart';
import 'package:pretty_client_mobile/modules/coins_module/data/coins_repository.dart';
import 'package:pretty_client_mobile/modules/country_code_module/data/country_code_repository.dart';
import 'package:pretty_client_mobile/modules/favorite_module/data/favorite_repository.dart';
import 'package:pretty_client_mobile/modules/feedback_module/data/feedback_repository.dart';
import 'package:pretty_client_mobile/modules/forgot_password_module/data/forgot_repository.dart';
import 'package:pretty_client_mobile/modules/friends_module/data/friends_repository.dart';
import 'package:pretty_client_mobile/modules/friends_module/data/product_repository.dart';
import 'package:pretty_client_mobile/modules/login_module/data/login_repository.dart';
import 'package:pretty_client_mobile/modules/news_module/data/news_repository.dart';
import 'package:pretty_client_mobile/modules/notification_module/data/notification_repository.dart';
import 'package:pretty_client_mobile/modules/otp_module/data/otp_repository.dart';
import 'package:pretty_client_mobile/modules/profile_module/data/notification_setting_repository.dart';
import 'package:pretty_client_mobile/modules/profile_module/data/profile_repository.dart';
import 'package:pretty_client_mobile/modules/register_module/data/register_repository.dart';
import 'package:pretty_client_mobile/modules/shared_module/data/refresh_token_repository.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/upcoming/data/mybooking_repository.dart';
import 'package:pretty_client_mobile/modules/home_tab_module/data/home_tab_repository.dart';

import 'app_di.inject.dart' as $gid;

@Injector([DioDi, SharePreferenceDi])
abstract class AppDi {
  static Future<AppDi> _instance;
  static void reset() => _instance = null;
  static Future<AppDi> get instance {
    _instance ??= $gid.AppDi$Injector.create(DioDi(), SharePreferenceDi());
    return _instance;
  }

  LoginRepository get loginRepository;
  OtpRepository get otpRepository;
  RegisterRepository get registerRepository;
  CountryCodeRepository get countryCodeRepository;
  ForgotRepository get forgotRepository;
  ProfileRepository get profileRepository;
  RefreshTokenRepository get refreshTokenRepository;
  NewsRepository get newsRepository;
  NotificationRepository get notificationRepository;
  NotificationSettingRepository get notificationSettingRepository;
  FriendsRepository get friendsRepository;
  SharePrefInterface get sharePrefInterface;
  BookingRepository get bookingRepository;
  OrderRepository get orderRepository;
  MyBookingRepository get myBookingRepository;
  FavoriteRepository get favoriteRepository;
  CoinsRepository get coinsRepository;
  HomeTabRepository get homeTabRepository;
  ProductRepository get productRepository;
  FeedbackRepository get feedbackRepository;
}
