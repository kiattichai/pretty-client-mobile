import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/app/app_state.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/widgets/splash_screen.dart';
import 'package:provider/provider.dart';

class MyApp extends StatelessWidget {
  static GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  final AppState appState;

  MyApp({this.appState});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness: Brightness.dark,
      systemNavigationBarColor: Design.theme.colorMainBackground,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.light,
    ));
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
    return ChangeNotifierProvider<AppState>(
      child: Consumer<AppState>(builder: (context, model, child) {
        return OverlaySupport(
            child: MaterialApp(
                debugShowCheckedModeBanner: false,
                key: model.key,
                theme: ThemeData(
                    scaffoldBackgroundColor: Design.theme.colorMainBackground,
                    fontFamily: 'Sarabun',
                    //remove ripple when click material icon
                    unselectedWidgetColor: Colors.white.withOpacity(0.6),
                    primaryColor: null,
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    bottomSheetTheme: BottomSheetThemeData(backgroundColor: Colors.transparent)),
                navigatorKey: navigatorKey,
                locale: model.appLocal,
                supportedLocales: [
                  Locale('en', 'US'),
                  Locale('th', ''),
                ],
                localizationsDelegates: [
                  AppLocalizations.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                  DefaultCupertinoLocalizations.delegate
                ],
                initialRoute: '/',
                onGenerateRoute: RouterService.instance.generator,
                home: new SplashScreen()));
      }),
      create: (_) => appState,
    );
  }
}
