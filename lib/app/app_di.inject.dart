import 'app_di.dart' as _i1;
import '../api/dio_di.dart' as _i2;
import 'share_preference_di.dart' as _i3;
import 'share_preference.dart' as _i4;
import '../api/core_api.dart' as _i5;
import '../modules/login_module/data/login_api.dart' as _i6;
import '../modules/otp_module/data/otp_api.dart' as _i7;
import '../modules/register_module/data/register_api.dart' as _i8;
import '../modules/country_code_module/data/country_code_api.dart' as _i9;
import '../modules/forgot_password_module/data/forgot_api.dart' as _i10;
import '../modules/profile_module/data/profile_api.dart' as _i11;
import '../modules/shared_module/data/refresh_token_api.dart' as _i12;
import '../modules/news_module/data/content_api.dart' as _i13;
import '../modules/notification_module/data/notification_api.dart' as _i14;
import '../modules/profile_module/data/notification_setting_api.dart' as _i15;
import '../modules/friends_module/data/friends_api.dart' as _i16;
import '../modules/booking_module/data/jobs_api.dart' as _i17;
import '../modules/booking_module/data/order_api.dart' as _i18;
import '../modules/favorite_module/data/favorite_api.dart' as _i19;
import '../modules/favorite_module/data/favorite_repository.dart' as _i20;
import '../modules/coins_module/data/coins_api.dart' as _i21;
import '../modules/home_tab_module/data/home_tab_api.dart' as _i22;
import '../modules/friends_module/data/product_api.dart' as _i23;
import '../modules/feedback_module/data/feedback_api.dart' as _i24;
import 'dart:async' as _i25;
import '../modules/login_module/data/login_repository.dart' as _i26;
import 'package:dio/src/dio.dart' as _i27;
import '../modules/otp_module/data/otp_repository.dart' as _i28;
import '../modules/register_module/data/register_repository.dart' as _i29;
import '../modules/country_code_module/data/country_code_repository.dart'
    as _i30;
import '../modules/forgot_password_module/data/forgot_repository.dart' as _i31;
import '../modules/profile_module/data/profile_repository.dart' as _i32;
import '../modules/shared_module/data/refresh_token_repository.dart' as _i33;
import '../modules/news_module/data/news_repository.dart' as _i34;
import '../modules/notification_module/data/notification_repository.dart'
    as _i35;
import '../modules/profile_module/data/notification_setting_repository.dart'
    as _i36;
import '../modules/friends_module/data/friends_repository.dart' as _i37;
import '../modules/booking_module/data/booking_repository.dart' as _i38;
import '../modules/booking_module/data/order_repository.dart' as _i39;
import '../modules/mybooking_module/upcoming/data/mybooking_repository.dart'
    as _i40;
import '../modules/coins_module/data/coins_repository.dart' as _i41;
import '../modules/home_tab_module/data/home_tab_repository.dart' as _i42;
import '../modules/friends_module/data/product_repository.dart' as _i43;
import '../modules/feedback_module/data/feedback_repository.dart' as _i44;

class AppDi$Injector implements _i1.AppDi {
  AppDi$Injector._(this._dioDi, this._sharePreferenceDi);

  final _i2.DioDi _dioDi;

  final _i3.SharePreferenceDi _sharePreferenceDi;

  _i4.SharePrefInterface _singletonSharePrefInterface;

  _i5.CoreApi _singletonCoreApi;

  _i6.LoginApi _singletonLoginApi;

  _i7.OtpApi _singletonOtpApi;

  _i8.RegisterApi _singletonRegisterApi;

  _i9.CountryCodeApi _singletonCountryCodeApi;

  _i10.ForgotApi _singletonForgotApi;

  _i11.ProfileApi _singletonProfileApi;

  _i12.RefreshTokenApi _singletonRefreshTokenApi;

  _i13.ContentApi _singletonContentApi;

  _i14.NotificationApi _singletonNotificationApi;

  _i15.NotificationSettingApi _singletonNotificationSettingApi;

  _i16.FriendsApi _singletonFriendsApi;

  _i17.JobsApi _singletonJobsApi;

  _i18.OrderApi _singletonOrderApi;

  _i19.FavoriteApi _singletonFavoriteApi;

  _i20.FavoriteRepository _singletonFavoriteRepository;

  _i21.CoinsApi _singletonCoinsApi;

  _i22.ContentsApi _singletonContentsApi;

  _i23.ProductApi _singletonProductApi;

  _i24.FeedbackApi _singletonFeedbackApi;

  static _i25.Future<_i1.AppDi> create(
      _i2.DioDi dioDi, _i3.SharePreferenceDi sharePreferenceDi) async {
    final injector = AppDi$Injector._(dioDi, sharePreferenceDi);

    return injector;
  }

  _i26.LoginRepository _createLoginRepository() =>
      _i26.LoginRepository(_createLoginApi());
  _i6.LoginApi _createLoginApi() => _singletonLoginApi ??=
      _i6.LoginApi(_createCoreApi(), _createSharePrefInterface());
  _i5.CoreApi _createCoreApi() => _singletonCoreApi ??=
      _i5.CoreApi(_createDio(), _createSharePrefInterface());
  _i27.Dio _createDio() => _dioDi.dio();
  _i4.SharePrefInterface _createSharePrefInterface() =>
      _singletonSharePrefInterface ??= _i4.SharePrefInterface(_createFuture());
  _i25.Future _createFuture() => _sharePreferenceDi.sharedPreferences();
  _i28.OtpRepository _createOtpRepository() =>
      _i28.OtpRepository(_createOtpApi());
  _i7.OtpApi _createOtpApi() =>
      _singletonOtpApi ??= _i7.OtpApi(_createCoreApi());
  _i29.RegisterRepository _createRegisterRepository() =>
      _i29.RegisterRepository(_createRegisterApi());
  _i8.RegisterApi _createRegisterApi() =>
      _singletonRegisterApi ??= _i8.RegisterApi(_createCoreApi());
  _i30.CountryCodeRepository _createCountryCodeRepository() =>
      _i30.CountryCodeRepository(_createCountryCodeApi());
  _i9.CountryCodeApi _createCountryCodeApi() =>
      _singletonCountryCodeApi ??= _i9.CountryCodeApi(_createCoreApi());
  _i31.ForgotRepository _createForgotRepository() =>
      _i31.ForgotRepository(_createForgotApi());
  _i10.ForgotApi _createForgotApi() =>
      _singletonForgotApi ??= _i10.ForgotApi(_createCoreApi());
  _i32.ProfileRepository _createProfileRepository() =>
      _i32.ProfileRepository(_createProfileApi());
  _i11.ProfileApi _createProfileApi() =>
      _singletonProfileApi ??= _i11.ProfileApi(_createCoreApi());
  _i33.RefreshTokenRepository _createRefreshTokenRepository() =>
      _i33.RefreshTokenRepository(_createRefreshTokenApi());
  _i12.RefreshTokenApi _createRefreshTokenApi() =>
      _singletonRefreshTokenApi ??= _i12.RefreshTokenApi(_createCoreApi());
  _i34.NewsRepository _createNewsRepository() =>
      _i34.NewsRepository(_createContentApi());
  _i13.ContentApi _createContentApi() =>
      _singletonContentApi ??= _i13.ContentApi(_createCoreApi());
  _i35.NotificationRepository _createNotificationRepository() =>
      _i35.NotificationRepository(_createNotificationApi());
  _i14.NotificationApi _createNotificationApi() =>
      _singletonNotificationApi ??= _i14.NotificationApi(_createCoreApi());
  _i36.NotificationSettingRepository _createNotificationSettingRepository() =>
      _i36.NotificationSettingRepository(_createNotificationSettingApi());
  _i15.NotificationSettingApi _createNotificationSettingApi() =>
      _singletonNotificationSettingApi ??=
          _i15.NotificationSettingApi(_createCoreApi());
  _i37.FriendsRepository _createFriendsRepository() =>
      _i37.FriendsRepository(_createFriendsApi());
  _i16.FriendsApi _createFriendsApi() =>
      _singletonFriendsApi ??= _i16.FriendsApi(_createCoreApi());
  _i38.BookingRepository _createBookingRepository() =>
      _i38.BookingRepository(_createJobsApi());
  _i17.JobsApi _createJobsApi() =>
      _singletonJobsApi ??= _i17.JobsApi(_createCoreApi());
  _i39.OrderRepository _createOrderRepository() =>
      _i39.OrderRepository(_createOrderApi());
  _i18.OrderApi _createOrderApi() =>
      _singletonOrderApi ??= _i18.OrderApi(_createCoreApi());
  _i40.MyBookingRepository _createMyBookingRepository() =>
      _i40.MyBookingRepository(_createOrderApi());
  _i20.FavoriteRepository _createFavoriteRepository() =>
      _singletonFavoriteRepository ??= _i20.FavoriteRepository(
          _createFavoriteApi(), _createSharePrefInterface());
  _i19.FavoriteApi _createFavoriteApi() =>
      _singletonFavoriteApi ??= _i19.FavoriteApi(_createCoreApi());
  _i41.CoinsRepository _createCoinsRepository() =>
      _i41.CoinsRepository(_createCoinsApi());
  _i21.CoinsApi _createCoinsApi() =>
      _singletonCoinsApi ??= _i21.CoinsApi(_createCoreApi());
  _i42.HomeTabRepository _createHomeTabRepository() =>
      _i42.HomeTabRepository(_createContentsApi());
  _i22.ContentsApi _createContentsApi() =>
      _singletonContentsApi ??= _i22.ContentsApi(_createCoreApi());
  _i43.ProductRepository _createProductRepository() =>
      _i43.ProductRepository(_createProductApi());
  _i23.ProductApi _createProductApi() =>
      _singletonProductApi ??= _i23.ProductApi(_createCoreApi());
  _i44.FeedbackRepository _createFeedbackRepository() =>
      _i44.FeedbackRepository(_createFeedbackApi());
  _i24.FeedbackApi _createFeedbackApi() =>
      _singletonFeedbackApi ??= _i24.FeedbackApi(_createCoreApi());
  @override
  _i26.LoginRepository get loginRepository => _createLoginRepository();
  @override
  _i28.OtpRepository get otpRepository => _createOtpRepository();
  @override
  _i29.RegisterRepository get registerRepository => _createRegisterRepository();
  @override
  _i30.CountryCodeRepository get countryCodeRepository =>
      _createCountryCodeRepository();
  @override
  _i31.ForgotRepository get forgotRepository => _createForgotRepository();
  @override
  _i32.ProfileRepository get profileRepository => _createProfileRepository();
  @override
  _i33.RefreshTokenRepository get refreshTokenRepository =>
      _createRefreshTokenRepository();
  @override
  _i34.NewsRepository get newsRepository => _createNewsRepository();
  @override
  _i35.NotificationRepository get notificationRepository =>
      _createNotificationRepository();
  @override
  _i36.NotificationSettingRepository get notificationSettingRepository =>
      _createNotificationSettingRepository();
  @override
  _i37.FriendsRepository get friendsRepository => _createFriendsRepository();
  @override
  _i4.SharePrefInterface get sharePrefInterface => _createSharePrefInterface();
  @override
  _i38.BookingRepository get bookingRepository => _createBookingRepository();
  @override
  _i39.OrderRepository get orderRepository => _createOrderRepository();
  @override
  _i40.MyBookingRepository get myBookingRepository =>
      _createMyBookingRepository();
  @override
  _i20.FavoriteRepository get favoriteRepository => _createFavoriteRepository();
  @override
  _i41.CoinsRepository get coinsRepository => _createCoinsRepository();
  @override
  _i42.HomeTabRepository get homeTabRepository => _createHomeTabRepository();
  @override
  _i43.ProductRepository get productRepository => _createProductRepository();
  @override
  _i44.FeedbackRepository get feedbackRepository => _createFeedbackRepository();
}
