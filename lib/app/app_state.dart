import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppState extends ChangeNotifier {
  Locale _appLocale;
  Key key = new UniqueKey();

  void reStartApp() {
    key = new UniqueKey();
    MyApp.navigatorKey = GlobalKey<NavigatorState>();
    notifyListeners();
  }

  Locale get appLocal => _appLocale ?? Locale("th");
  fetchLocale() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getString('language_code') == null) {
      //default install app language TH
      _appLocale = Locale('th');
      await prefs.setString('language_code', 'th');
      await prefs.setString('countryCode', '');
//      List languages = await Devicelocale.preferredLanguages;
//      _appLocale = Locale(languages.first.toString().substring(0, 2));
      print('language set' + prefs.getString('language_code'));
      return Null;
    }
    _appLocale = Locale(prefs.getString('language_code'));
    return Null;
  }

  void changeLanguage(Locale type) async {
    var prefs = await SharedPreferences.getInstance();
    if (_appLocale == type) {
      return;
    }
    if (type == Locale("th")) {
      _appLocale = Locale("th");
      await prefs.setString('language_code', 'th');
      await prefs.setString('countryCode', '');
    } else {
      _appLocale = Locale("en");
      await prefs.setString('language_code', 'en');
      await prefs.setString('countryCode', 'US');
    }
    notifyListeners();
  }
}
