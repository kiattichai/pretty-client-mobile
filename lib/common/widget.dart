import 'package:flutter/material.dart';

extension UtilsUI on State {
  sizeBox({double height = 8.0}) {
    return SizedBox(
      height: height,
    );
  }
}

extension on StatelessWidget {}
