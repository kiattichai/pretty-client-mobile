import 'package:flutter/material.dart';

TextStyle textStyle(double fontSize) {
  return TextStyle(
    fontFamily: 'Sarabun',
    color: Color(0xffeeeeee),
    fontSize: fontSize,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
  );
}
