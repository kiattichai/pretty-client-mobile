import 'dart:async';

import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:pretty_client_mobile/app/app.dart';
import 'package:pretty_client_mobile/design/component_style.dart';
import 'package:pretty_client_mobile/design/themes/theme_black.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/env_config.dart';
import 'package:pretty_client_mobile/app/app_state.dart';
import 'package:pretty_client_mobile/services/analytics_service.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:sentry/sentry.dart';
import 'dart:io';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  RouterService();
  AnalyticService();
  final baseUrl = "https://api.prettyhub.me/v1.0";
  final resUrl = "https://res.prettyhub.me";
  final frontUrl = "https://www.prettyhub.me";
  final sentryTokenAndroid = "https://9d5cb916836946f4aeab75082e58fbda@sentry.io/2706598";
  final sentryTokenIOS = "https://dfa556c675b1431bba998aea0d003e32@sentry.io/2706629";
  final packageInfo = await PackageInfo.fromPlatform();
  final appState = AppState();
  final SentryClient sentry =
      new SentryClient(dsn: Platform.isAndroid ? sentryTokenAndroid : sentryTokenIOS);
  EnvConfig(
      env: Env.ALPHA,
      baseUrl: baseUrl,
      debugApi: true,
      resUrl: resUrl,
      frontUrl: frontUrl,
      version: '${packageInfo.version}(${packageInfo.buildNumber})');
  Design(themeApp: ThemeBlack(), designText: DesignText(), designColor: DesignColor());

  await appState.fetchLocale();

  runZoned(
      () => runApp(MyApp(
            appState: appState,
          )), onError: (error, stackTrace) {
    try {
      sentry.capture(
          event: Event(
              environment: EnvConfig.instance.env.toString(),
              exception: error,
              stackTrace: stackTrace));
      print('Error sent to sentry.io: $error');
      print(stackTrace);
    } catch (e) {
      print('Sending report to sentry.io failed: $e');
      print('Original error: $error');
    }
  });
}
