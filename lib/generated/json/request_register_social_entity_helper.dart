import 'package:pretty_client_mobile/modules/register_module/models/request_register_social_entity.dart';

requestRegisterSocialEntityFromJson(RequestRegisterSocialEntity data, Map<String, dynamic> json) {
	if (json['service'] != null) {
		data.service = json['service']?.toString();
	}
	if (json['token'] != null) {
		data.token = json['token']?.toString();
	}
	if (json['username'] != null) {
		data.username = json['username']?.toString();
	}
	if (json['country_code'] != null) {
		data.countryCode = json['country_code']?.toString();
	}
	if (json['first_name'] != null) {
		data.firstName = json['first_name']?.toString();
	}
	if (json['last_name'] != null) {
		data.lastName = json['last_name']?.toString();
	}
	if (json['gender'] != null) {
		data.gender = json['gender']?.toString();
	}
	if (json['birthday'] != null) {
		data.birthday = json['birthday']?.toString();
	}
	return data;
}

Map<String, dynamic> requestRegisterSocialEntityToJson(RequestRegisterSocialEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['service'] = entity.service;
	data['token'] = entity.token;
	data['username'] = entity.username;
	data['country_code'] = entity.countryCode;
	data['first_name'] = entity.firstName;
	data['last_name'] = entity.lastName;
	data['gender'] = entity.gender;
	data['birthday'] = entity.birthday;
	return data;
}