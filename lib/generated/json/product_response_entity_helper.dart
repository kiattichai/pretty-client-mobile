import 'package:pretty_client_mobile/modules/friends_module/models/product_response_entity.dart';

productResponseEntityFromJson(ProductResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new ProductResponseData().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new ProductResponseBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> productResponseEntityToJson(ProductResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

productDetailResponseEntityFromJson(ProductDetailResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new Product().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new ProductResponseBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> productDetailResponseEntityToJson(ProductDetailResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

productResponseDataFromJson(ProductResponseData data, Map<String, dynamic> json) {
	if (json['pagination'] != null) {
		data.pagination = new ProductResponseDataPagination().fromJson(json['pagination']);
	}
	if (json['record'] != null) {
		data.record = new List<Product>();
		(json['record'] as List).forEach((v) {
			data.record.add(new Product().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> productResponseDataToJson(ProductResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.pagination != null) {
		data['pagination'] = entity.pagination.toJson();
	}
	if (entity.record != null) {
		data['record'] =  entity.record.map((v) => v.toJson()).toList();
	}
	return data;
}

productResponseDataPaginationFromJson(ProductResponseDataPagination data, Map<String, dynamic> json) {
	if (json['current_page'] != null) {
		data.currentPage = json['current_page']?.toInt();
	}
	if (json['last_page'] != null) {
		data.lastPage = json['last_page']?.toInt();
	}
	if (json['limit'] != null) {
		data.limit = json['limit']?.toInt();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toInt();
	}
	return data;
}

Map<String, dynamic> productResponseDataPaginationToJson(ProductResponseDataPagination entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['current_page'] = entity.currentPage;
	data['last_page'] = entity.lastPage;
	data['limit'] = entity.limit;
	data['total'] = entity.total;
	return data;
}

productFromJson(Product data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	if (json['option'] != null) {
		data.option = json['option'];
	}
	if (json['price'] != null) {
		data.price = new ProductPrice().fromJson(json['price']);
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['description'] != null) {
		data.description = json['description']?.toString();
	}
	if (json['store'] != null) {
		data.store = new ProductStore().fromJson(json['store']);
	}
	if (json['categories'] != null) {
		data.categories = new List<ProductCategory>();
		(json['categories'] as List).forEach((v) {
			data.categories.add(new ProductCategory().fromJson(v));
		});
	}
	if (json['variants'] != null) {
		data.variants = new List<ProductVariant>();
		(json['variants'] as List).forEach((v) {
			data.variants.add(new ProductVariant().fromJson(v));
		});
	}
	if (json['attributes'] != null) {
		data.attributes = new List<ProductAttribute>();
		(json['attributes'] as List).forEach((v) {
			data.attributes.add(new ProductAttribute().fromJson(v));
		});
	}
	if (json['images'] != null) {
		data.images = new List<ProductImage>();
		(json['images'] as List).forEach((v) {
			data.images.add(new ProductImage().fromJson(v));
		});
	}
	if (json['user'] != null) {
		data.user = new ProductUser().fromJson(json['user']);
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['feedbacks'] != null) {
		data.feedbacks = new ProductFeedback().fromJson(json['feedbacks']);
	}
	if (json['publish_status'] != null) {
		data.publishStatus = json['publish_status'];
	}
	if (json['created_at'] != null) {
		data.createdAt = json['created_at']?.toString();
	}
	if (json['updated_at'] != null) {
		data.updatedAt = json['updated_at']?.toString();
	}
	if (json['onFloor'] != null) {
		data.onFloor = json['onFloor'];
	}
	return data;
}

Map<String, dynamic> productToJson(Product entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['position'] = entity.position;
	data['option'] = entity.option;
	if (entity.price != null) {
		data['price'] = entity.price.toJson();
	}
	data['name'] = entity.name;
	data['description'] = entity.description;
	if (entity.store != null) {
		data['store'] = entity.store.toJson();
	}
	if (entity.categories != null) {
		data['categories'] =  entity.categories.map((v) => v.toJson()).toList();
	}
	if (entity.variants != null) {
		data['variants'] =  entity.variants.map((v) => v.toJson()).toList();
	}
	if (entity.attributes != null) {
		data['attributes'] =  entity.attributes.map((v) => v.toJson()).toList();
	}
	if (entity.images != null) {
		data['images'] =  entity.images.map((v) => v.toJson()).toList();
	}
	if (entity.user != null) {
		data['user'] = entity.user.toJson();
	}
	data['status'] = entity.status;
	if (entity.feedbacks != null) {
		data['feedbacks'] = entity.feedbacks.toJson();
	}
	data['publish_status'] = entity.publishStatus;
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	data['onFloor'] = entity.onFloor;
	return data;
}

productPriceFromJson(ProductPrice data, Map<String, dynamic> json) {
	if (json['min'] != null) {
		data.min = json['min']?.toInt();
	}
	if (json['max'] != null) {
		data.max = json['max']?.toInt();
	}
	if (json['min_text'] != null) {
		data.minText = json['min_text']?.toString();
	}
	if (json['max_text'] != null) {
		data.maxText = json['max_text']?.toString();
	}
	return data;
}

Map<String, dynamic> productPriceToJson(ProductPrice entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['min'] = entity.min;
	data['max'] = entity.max;
	data['min_text'] = entity.minText;
	data['max_text'] = entity.maxText;
	return data;
}

productStoreFromJson(ProductStore data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['image'] != null) {
		data.image = json['image'];
	}
	return data;
}

Map<String, dynamic> productStoreToJson(ProductStore entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['image'] = entity.image;
	return data;
}

productCategoryFromJson(ProductCategory data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['description'] != null) {
		data.description = json['description']?.toString();
	}
	return data;
}

Map<String, dynamic> productCategoryToJson(ProductCategory entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['position'] = entity.position;
	data['name'] = entity.name;
	data['description'] = entity.description;
	return data;
}

productVariantFromJson(ProductVariant data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['sku'] != null) {
		data.sku = json['sku']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['description'] != null) {
		data.description = json['description']?.toString();
	}
	if (json['cost_price'] != null) {
		data.costPrice = json['cost_price']?.toInt();
	}
	if (json['cost_price_text'] != null) {
		data.costPriceText = json['cost_price_text']?.toString();
	}
	if (json['price'] != null) {
		data.price = json['price']?.toInt();
	}
	if (json['price_text'] != null) {
		data.priceText = json['price_text']?.toString();
	}
	if (json['compare_at_price'] != null) {
		data.compareAtPrice = json['compare_at_price']?.toInt();
	}
	if (json['compare_at_price_text'] != null) {
		data.compareAtPriceText = json['compare_at_price_text']?.toString();
	}
	if (json['quantity'] != null) {
		data.quantity = json['quantity']?.toInt();
	}
	if (json['hold'] != null) {
		data.hold = json['hold']?.toInt();
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	if (json['weight'] != null) {
		data.weight = json['weight']?.toInt();
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['options'] != null) {
		data.options = new List<ProductVariantsOption>();
		(json['options'] as List).forEach((v) {
			data.options.add(new ProductVariantsOption().fromJson(v));
		});
	}
	if (json['image'] != null) {
		data.image = new ProductVariantsImage().fromJson(json['image']);
	}
	return data;
}

Map<String, dynamic> productVariantToJson(ProductVariant entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['sku'] = entity.sku;
	data['name'] = entity.name;
	data['description'] = entity.description;
	data['cost_price'] = entity.costPrice;
	data['cost_price_text'] = entity.costPriceText;
	data['price'] = entity.price;
	data['price_text'] = entity.priceText;
	data['compare_at_price'] = entity.compareAtPrice;
	data['compare_at_price_text'] = entity.compareAtPriceText;
	data['quantity'] = entity.quantity;
	data['hold'] = entity.hold;
	data['position'] = entity.position;
	data['weight'] = entity.weight;
	data['status'] = entity.status;
	if (entity.options != null) {
		data['options'] =  entity.options.map((v) => v.toJson()).toList();
	}
	if (entity.image != null) {
		data['image'] = entity.image.toJson();
	}
	return data;
}

productVariantsOptionFromJson(ProductVariantsOption data, Map<String, dynamic> json) {
	if (json['value'] != null) {
		data.value = new ProductVariantsOptionsValue().fromJson(json['value']);
	}
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	return data;
}

Map<String, dynamic> productVariantsOptionToJson(ProductVariantsOption entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.value != null) {
		data['value'] = entity.value.toJson();
	}
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['name'] = entity.name;
	return data;
}

productVariantsOptionsValueFromJson(ProductVariantsOptionsValue data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	return data;
}

Map<String, dynamic> productVariantsOptionsValueToJson(ProductVariantsOptionsValue entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['name'] = entity.name;
	return data;
}

productVariantsImageFromJson(ProductVariantsImage data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['store_id'] != null) {
		data.storeId = json['store_id']?.toInt();
	}
	if (json['album_id'] != null) {
		data.albumId = json['album_id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	return data;
}

Map<String, dynamic> productVariantsImageToJson(ProductVariantsImage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['store_id'] = entity.storeId;
	data['album_id'] = entity.albumId;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	return data;
}

productAttributeFromJson(ProductAttribute data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['type'] != null) {
		data.type = new ProductAttributesType().fromJson(json['type']);
	}
	if (json['require'] != null) {
		data.require = json['require'];
	}
	if (json['metafield'] != null) {
		data.metafield = new ProductAttributesMetafield().fromJson(json['metafield']);
	}
	if (json['value'] != null) {
		data.value = new ProductAttributesValue().fromJson(json['value']);
	}
	if (json['value_text'] != null) {
		data.valueText = json['value_text']?.toString();
	}
	return data;
}

Map<String, dynamic> productAttributeToJson(ProductAttribute entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['name'] = entity.name;
	if (entity.type != null) {
		data['type'] = entity.type.toJson();
	}
	data['require'] = entity.require;
	if (entity.metafield != null) {
		data['metafield'] = entity.metafield.toJson();
	}
	if (entity.value != null) {
		data['value'] = entity.value.toJson();
	}
	data['value_text'] = entity.valueText;
	return data;
}

productAttributesTypeFromJson(ProductAttributesType data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	return data;
}

Map<String, dynamic> productAttributesTypeToJson(ProductAttributesType entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

productAttributesMetafieldFromJson(ProductAttributesMetafield data, Map<String, dynamic> json) {
	return data;
}

Map<String, dynamic> productAttributesMetafieldToJson(ProductAttributesMetafield entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	return data;
}

productAttributesValueFromJson(ProductAttributesValue data, Map<String, dynamic> json) {
	return data;
}

Map<String, dynamic> productAttributesValueToJson(ProductAttributesValue entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	return data;
}

productImageFromJson(ProductImage data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['store_id'] != null) {
		data.storeId = json['store_id']?.toInt();
	}
	if (json['tag'] != null) {
		data.tag = json['tag'];
	}
	if (json['album_id'] != null) {
		data.albumId = json['album_id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	return data;
}

Map<String, dynamic> productImageToJson(ProductImage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['store_id'] = entity.storeId;
	data['tag'] = entity.tag;
	data['album_id'] = entity.albumId;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['position'] = entity.position;
	return data;
}

productUserFromJson(ProductUser data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['username'] != null) {
		data.username = json['username']?.toString();
	}
	if (json['country_code'] != null) {
		data.countryCode = json['country_code']?.toString();
	}
	if (json['first_name'] != null) {
		data.firstName = json['first_name']?.toString();
	}
	if (json['last_name'] != null) {
		data.lastName = json['last_name']?.toString();
	}
	return data;
}

Map<String, dynamic> productUserToJson(ProductUser entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['username'] = entity.username;
	data['country_code'] = entity.countryCode;
	data['first_name'] = entity.firstName;
	data['last_name'] = entity.lastName;
	return data;
}

productFeedbackFromJson(ProductFeedback data, Map<String, dynamic> json) {
	if (json['summary'] != null) {
		data.summary = json['summary']?.toDouble();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toInt();
	}
	if (json['record'] != null) {
		data.record = new List<ProductFeedbackRecord>();
		(json['record'] as List).forEach((v) {
			data.record.add(new ProductFeedbackRecord().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> productFeedbackToJson(ProductFeedback entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['summary'] = entity.summary;
	data['total'] = entity.total;
	if (entity.record != null) {
		data['record'] =  entity.record.map((v) => v.toJson()).toList();
	}
	return data;
}

productFeedbackRecordFromJson(ProductFeedbackRecord data, Map<String, dynamic> json) {
	if (json['score'] != null) {
		data.score = json['score']?.toInt();
	}
	if (json['count'] != null) {
		data.count = json['count']?.toInt();
	}
	return data;
}

Map<String, dynamic> productFeedbackRecordToJson(ProductFeedbackRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['score'] = entity.score;
	data['count'] = entity.count;
	return data;
}

productResponseBenchFromJson(ProductResponseBench data, Map<String, dynamic> json) {
	if (json['second'] != null) {
		data.second = json['second']?.toInt();
	}
	if (json['millisecond'] != null) {
		data.millisecond = json['millisecond']?.toDouble();
	}
	if (json['format'] != null) {
		data.format = json['format']?.toString();
	}
	return data;
}

Map<String, dynamic> productResponseBenchToJson(ProductResponseBench entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}