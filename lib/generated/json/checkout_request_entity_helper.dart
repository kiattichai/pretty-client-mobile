import 'package:pretty_client_mobile/modules/booking_module/models/checkout_request_entity.dart';

checkoutRequestEntityFromJson(CheckoutRequestEntity data, Map<String, dynamic> json) {
	if (json['store_id'] != null) {
		data.storeId = json['store_id']?.toInt();
	}
	if (json['products'] != null) {
		data.products = new List<CheckoutRequestProduct>();
		(json['products'] as List).forEach((v) {
			data.products.add(new CheckoutRequestProduct().fromJson(v));
		});
	}
	if (json['shipping'] != null) {
		data.shipping = new CheckoutRequestShipping().fromJson(json['shipping']);
	}
	return data;
}

Map<String, dynamic> checkoutRequestEntityToJson(CheckoutRequestEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['store_id'] = entity.storeId;
	if (entity.products != null) {
		data['products'] =  entity.products.map((v) => v.toJson()).toList();
	}
	if (entity.shipping != null) {
		data['shipping'] = entity.shipping.toJson();
	}
	return data;
}

checkoutRequestProductFromJson(CheckoutRequestProduct data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['job_id'] != null) {
		data.jobId = json['job_id']?.toInt();
	}
	if (json['variant_id'] != null) {
		data.variantId = json['variant_id']?.toInt();
	}
	if (json['quantity'] != null) {
		data.quantity = json['quantity']?.toInt();
	}
	return data;
}

Map<String, dynamic> checkoutRequestProductToJson(CheckoutRequestProduct entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['job_id'] = entity.jobId;
	data['variant_id'] = entity.variantId;
	data['quantity'] = entity.quantity;
	return data;
}

checkoutRequestShippingFromJson(CheckoutRequestShipping data, Map<String, dynamic> json) {
	if (json['address'] != null) {
		data.address = json['address']?.toString();
	}
	if (json['lat'] != null) {
		data.lat = json['lat']?.toDouble();
	}
	if (json['long'] != null) {
		data.long = json['long']?.toDouble();
	}
	if (json['time'] != null) {
		data.time = json['time']?.toString();
	}
	return data;
}

Map<String, dynamic> checkoutRequestShippingToJson(CheckoutRequestShipping entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['address'] = entity.address;
	data['lat'] = entity.lat;
	data['long'] = entity.long;
	data['time'] = entity.time;
	return data;
}