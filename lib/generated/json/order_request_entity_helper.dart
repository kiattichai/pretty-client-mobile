import 'package:pretty_client_mobile/modules/booking_module/models/order_request_entity.dart';

orderRequestEntityFromJson(OrderRequestEntity data, Map<String, dynamic> json) {
	if (json['store_id'] != null) {
		data.storeId = json['store_id']?.toInt();
	}
	if (json['store_name'] != null) {
		data.storeName = json['store_name']?.toString();
	}
	if (json['firstname'] != null) {
		data.firstname = json['firstname']?.toString();
	}
	if (json['lastname'] != null) {
		data.lastname = json['lastname']?.toString();
	}
	if (json['email'] != null) {
		data.email = json['email']?.toString();
	}
	if (json['telephone'] != null) {
		data.telephone = json['telephone']?.toString();
	}
	if (json['price'] != null) {
		data.price = json['price']?.toDouble();
	}
	if (json['discount'] != null) {
		data.discount = json['discount']?.toDouble();
	}
	if (json['payment_method'] != null) {
		data.paymentMethod = json['payment_method']?.toString();
	}
	if (json['payment_provider'] != null) {
		data.paymentProvider = json['payment_provider']?.toString();
	}
	if (json['vat'] != null) {
		data.vat = json['vat']?.toDouble();
	}
	if (json['fee'] != null) {
		data.fee = json['fee']?.toDouble();
	}
	if (json['service_fee'] != null) {
		data.serviceFee = json['service_fee']?.toDouble();
	}
	if (json['payment_fee'] != null) {
		data.paymentFee = json['payment_fee']?.toDouble();
	}
	if (json['tax'] != null) {
		data.tax = json['tax']?.toDouble();
	}
	if (json['organizer_fee'] != null) {
		data.organizerFee = json['organizer_fee']?.toDouble();
	}
	if (json['organizer_service_fee'] != null) {
		data.organizerServiceFee = json['organizer_service_fee']?.toDouble();
	}
	if (json['organizer_payment_fee'] != null) {
		data.organizerPaymentFee = json['organizer_payment_fee']?.toDouble();
	}
	if (json['organizer_tax'] != null) {
		data.organizerTax = json['organizer_tax']?.toDouble();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toDouble();
	}
	if (json['income'] != null) {
		data.income = json['income']?.toDouble();
	}
	if (json['user_wallet'] != null) {
		data.userWallet = json['user_wallet']?.toDouble();
	}
	if (json['shipping_address'] != null) {
		data.shippingAddress = json['shipping_address']?.toString();
	}
	if (json['shipping_lat'] != null) {
		data.shippingLat = json['shipping_lat']?.toDouble();
	}
	if (json['shipping_long'] != null) {
		data.shippingLong = json['shipping_long']?.toDouble();
	}
	if (json['shipping_at'] != null) {
		data.shippingAt = json['shipping_at']?.toString();
	}
	if (json['order_products'] != null) {
		data.orderProducts = new List<OrderRequestOrderProduct>();
		(json['order_products'] as List).forEach((v) {
			data.orderProducts.add(new OrderRequestOrderProduct().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> orderRequestEntityToJson(OrderRequestEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['store_id'] = entity.storeId;
	data['store_name'] = entity.storeName;
	data['firstname'] = entity.firstname;
	data['lastname'] = entity.lastname;
	data['email'] = entity.email;
	data['telephone'] = entity.telephone;
	data['price'] = entity.price;
	data['discount'] = entity.discount;
	data['payment_method'] = entity.paymentMethod;
	data['payment_provider'] = entity.paymentProvider;
	data['vat'] = entity.vat;
	data['fee'] = entity.fee;
	data['service_fee'] = entity.serviceFee;
	data['payment_fee'] = entity.paymentFee;
	data['tax'] = entity.tax;
	data['organizer_fee'] = entity.organizerFee;
	data['organizer_service_fee'] = entity.organizerServiceFee;
	data['organizer_payment_fee'] = entity.organizerPaymentFee;
	data['organizer_tax'] = entity.organizerTax;
	data['total'] = entity.total;
	data['income'] = entity.income;
	data['user_wallet'] = entity.userWallet;
	data['shipping_address'] = entity.shippingAddress;
	data['shipping_lat'] = entity.shippingLat;
	data['shipping_long'] = entity.shippingLong;
	data['shipping_at'] = entity.shippingAt;
	if (entity.orderProducts != null) {
		data['order_products'] =  entity.orderProducts.map((v) => v.toJson()).toList();
	}
	return data;
}

orderRequestOrderProductFromJson(OrderRequestOrderProduct data, Map<String, dynamic> json) {
	if (json['job_id'] != null) {
		data.jobId = json['job_id']?.toInt();
	}
	if (json['pretty_id'] != null) {
		data.prettyId = json['pretty_id']?.toInt();
	}
	if (json['pretty_code'] != null) {
		data.prettyCode = json['pretty_code']?.toString();
	}
	if (json['pretty_name'] != null) {
		data.prettyName = json['pretty_name']?.toString();
	}
	if (json['pretty_nick_name'] != null) {
		data.prettyNickName = json['pretty_nick_name']?.toString();
	}
	if (json['product_id'] != null) {
		data.productId = json['product_id']?.toInt();
	}
	if (json['product_variant_id'] != null) {
		data.productVariantId = json['product_variant_id']?.toInt();
	}
	if (json['product_code'] != null) {
		data.productCode = json['product_code']?.toString();
	}
	if (json['product_name'] != null) {
		data.productName = json['product_name']?.toString();
	}
	if (json['product_option'] != null) {
		data.productOption = json['product_option']?.toString();
	}
	if (json['pretty_group_name'] != null) {
		data.prettyGroupName = json['pretty_group_name']?.toString();
	}
	if (json['image_id'] != null) {
		data.imageId = json['image_id']?.toString();
	}
	if (json['image_name'] != null) {
		data.imageName = json['image_name']?.toString();
	}
	if (json['venue_id'] != null) {
		data.venueId = json['venue_id']?.toInt();
	}
	if (json['venue_name'] != null) {
		data.venueName = json['venue_name']?.toString();
	}
	if (json['lat'] != null) {
		data.lat = json['lat']?.toDouble();
	}
	if (json['long'] != null) {
		data.long = json['long']?.toDouble();
	}
	if (json['quantity'] != null) {
		data.quantity = json['quantity']?.toInt();
	}
	if (json['price'] != null) {
		data.price = json['price']?.toDouble();
	}
	if (json['discount'] != null) {
		data.discount = json['discount']?.toDouble();
	}
	if (json['fee'] != null) {
		data.fee = json['fee']?.toDouble();
	}
	if (json['service_fee'] != null) {
		data.serviceFee = json['service_fee']?.toDouble();
	}
	if (json['payment_fee'] != null) {
		data.paymentFee = json['payment_fee']?.toDouble();
	}
	if (json['tax'] != null) {
		data.tax = json['tax']?.toDouble();
	}
	if (json['organizer_fee'] != null) {
		data.organizerFee = json['organizer_fee']?.toDouble();
	}
	if (json['organizer_service_fee'] != null) {
		data.organizerServiceFee = json['organizer_service_fee']?.toDouble();
	}
	if (json['organizer_payment_fee'] != null) {
		data.organizerPaymentFee = json['organizer_payment_fee']?.toDouble();
	}
	if (json['organizer_tax'] != null) {
		data.organizerTax = json['organizer_tax']?.toDouble();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toDouble();
	}
	if (json['income'] != null) {
		data.income = json['income']?.toDouble();
	}
	if (json['job_at'] != null) {
		data.jobAt = json['job_at']?.toString();
	}
	if (json['venue_phone'] != null) {
		data.venuePhone = json['venue_phone']?.toString();
	}
	return data;
}

Map<String, dynamic> orderRequestOrderProductToJson(OrderRequestOrderProduct entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['job_id'] = entity.jobId;
	data['pretty_id'] = entity.prettyId;
	data['pretty_code'] = entity.prettyCode;
	data['pretty_name'] = entity.prettyName;
	data['pretty_nick_name'] = entity.prettyNickName;
	data['product_id'] = entity.productId;
	data['product_variant_id'] = entity.productVariantId;
	data['product_code'] = entity.productCode;
	data['product_name'] = entity.productName;
	data['product_option'] = entity.productOption;
	data['pretty_group_name'] = entity.prettyGroupName;
	data['image_id'] = entity.imageId;
	data['image_name'] = entity.imageName;
	data['venue_id'] = entity.venueId;
	data['venue_name'] = entity.venueName;
	data['lat'] = entity.lat;
	data['long'] = entity.long;
	data['quantity'] = entity.quantity;
	data['price'] = entity.price;
	data['discount'] = entity.discount;
	data['fee'] = entity.fee;
	data['service_fee'] = entity.serviceFee;
	data['payment_fee'] = entity.paymentFee;
	data['tax'] = entity.tax;
	data['organizer_fee'] = entity.organizerFee;
	data['organizer_service_fee'] = entity.organizerServiceFee;
	data['organizer_payment_fee'] = entity.organizerPaymentFee;
	data['organizer_tax'] = entity.organizerTax;
	data['total'] = entity.total;
	data['income'] = entity.income;
	data['job_at'] = entity.jobAt;
	data['venue_phone'] = entity.venuePhone;
	return data;
}