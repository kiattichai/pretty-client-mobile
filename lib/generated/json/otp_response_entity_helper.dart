import 'package:pretty_client_mobile/modules/otp_module/models/otp_response_entity.dart';

otpResponseEntityFromJson(OtpResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new OtpResponseData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> otpResponseEntityToJson(OtpResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	return data;
}

otpResponseDataFromJson(OtpResponseData data, Map<String, dynamic> json) {
	if (json['message'] != null) {
		data.message = json['message']?.toString();
	}
	if (json['mobile'] != null) {
		data.mobile = json['mobile']?.toString();
	}
	if (json['ref'] != null) {
		data.ref = json['ref']?.toString();
	}
	if (json['expired_at'] != null) {
		data.expiredAt = json['expired_at']?.toString();
	}
	return data;
}

Map<String, dynamic> otpResponseDataToJson(OtpResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['message'] = entity.message;
	data['mobile'] = entity.mobile;
	data['ref'] = entity.ref;
	data['expired_at'] = entity.expiredAt;
	return data;
}