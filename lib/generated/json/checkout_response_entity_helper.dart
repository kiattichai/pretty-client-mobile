import 'package:pretty_client_mobile/modules/booking_module/models/checkout_response_entity.dart';

checkoutResponseEntityFromJson(CheckoutResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new CheckoutResponseData().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new CheckoutResponseBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> checkoutResponseEntityToJson(CheckoutResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

checkoutResponseDataFromJson(CheckoutResponseData data, Map<String, dynamic> json) {
	if (json['message'] != null) {
		data.message = json['message']?.toString();
	}
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['expired_time'] != null) {
		data.expiredTime = json['expired_time']?.toInt();
	}
	if (json['expired_at'] != null) {
		data.expiredAt = json['expired_at']?.toInt();
	}
	return data;
}

Map<String, dynamic> checkoutResponseDataToJson(CheckoutResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['message'] = entity.message;
	data['id'] = entity.id;
	data['expired_time'] = entity.expiredTime;
	data['expired_at'] = entity.expiredAt;
	return data;
}

checkoutResponseBenchFromJson(CheckoutResponseBench data, Map<String, dynamic> json) {
	if (json['second'] != null) {
		data.second = json['second']?.toInt();
	}
	if (json['millisecond'] != null) {
		data.millisecond = json['millisecond']?.toDouble();
	}
	if (json['format'] != null) {
		data.format = json['format']?.toString();
	}
	return data;
}

Map<String, dynamic> checkoutResponseBenchToJson(CheckoutResponseBench entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}