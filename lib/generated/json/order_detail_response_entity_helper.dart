import 'package:pretty_client_mobile/modules/booking_module/models/order_detail_response_entity.dart';

orderDetailResponseEntityFromJson(OrderDetailResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new OrderDetailResponseData().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new OrderDetailResponseBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> orderDetailResponseEntityToJson(OrderDetailResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

orderDetailResponseDataFromJson(OrderDetailResponseData data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['order_no'] != null) {
		data.orderNo = json['order_no']?.toString();
	}
	if (json['store_id'] != null) {
		data.storeId = json['store_id']?.toInt();
	}
	if (json['store_name'] != null) {
		data.storeName = json['store_name']?.toString();
	}
	if (json['user_id'] != null) {
		data.userId = json['user_id']?.toInt();
	}
	if (json['firstname'] != null) {
		data.firstname = json['firstname']?.toString();
	}
	if (json['lastname'] != null) {
		data.lastname = json['lastname']?.toString();
	}
	if (json['email'] != null) {
		data.email = json['email'];
	}
	if (json['telephone'] != null) {
		data.telephone = json['telephone']?.toString();
	}
	if (json['payment_provider'] != null) {
		data.paymentProvider = json['payment_provider']?.toString();
	}
	if (json['payment_method'] != null) {
		data.paymentMethod = json['payment_method']?.toString();
	}
	if (json['payment_method_text'] != null) {
		data.paymentMethodText = json['payment_method_text']?.toString();
	}
	if (json['payment_ref'] != null) {
		data.paymentRef = json['payment_ref']?.toString();
	}
	if (json['payment_ref_no1'] != null) {
		data.paymentRefNo1 = json['payment_ref_no1'];
	}
	if (json['payment_ref_no2'] != null) {
		data.paymentRefNo2 = json['payment_ref_no2'];
	}
	if (json['payment_message'] != null) {
		data.paymentMessage = json['payment_message']?.toString();
	}
	if (json['shipping_provider'] != null) {
		data.shippingProvider = json['shipping_provider']?.toString();
	}
	if (json['shipping_method'] != null) {
		data.shippingMethod = json['shipping_method']?.toString();
	}
	if (json['shipping_address'] != null) {
		data.shippingAddress = json['shipping_address']?.toString();
	}
	if (json['shipping_lat'] != null) {
		data.shippingLat = json['shipping_lat']?.toDouble();
	}
	if (json['shipping_long'] != null) {
		data.shippingLong = json['shipping_long']?.toDouble();
	}
	if (json['shipping_at'] != null) {
		data.shippingAt = new OrderDetailResponseDataShippingAt().fromJson(json['shipping_at']);
	}
	if (json['price'] != null) {
		data.price = json['price']?.toInt();
	}
	if (json['discount'] != null) {
		data.discount = json['discount']?.toInt();
	}
	if (json['summary'] != null) {
		data.summary = json['summary']?.toInt();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toInt();
	}
	if (json['currency_code'] != null) {
		data.currencyCode = json['currency_code']?.toString();
	}
	if (json['currency_symbol'] != null) {
		data.currencySymbol = json['currency_symbol']?.toString();
	}
	if (json['price_text'] != null) {
		data.priceText = json['price_text']?.toString();
	}
	if (json['discount_text'] != null) {
		data.discountText = json['discount_text']?.toString();
	}
	if (json['summary_text'] != null) {
		data.summaryText = json['summary_text']?.toString();
	}
	if (json['total_text'] != null) {
		data.totalText = json['total_text']?.toString();
	}
	if (json['comment'] != null) {
		data.comment = json['comment'];
	}
	if (json['ip'] != null) {
		data.ip = json['ip']?.toString();
	}
	if (json['user'] != null) {
		data.user = new OrderDetailResponseDataUser().fromJson(json['user']);
	}
	if (json['products'] != null) {
		data.products = new List<OrderDetailResponseDataProduct>();
		(json['products'] as List).forEach((v) {
			data.products.add(new OrderDetailResponseDataProduct().fromJson(v));
		});
	}
	if (json['quantity'] != null) {
		data.quantity = json['quantity']?.toInt();
	}
	if (json['order_total'] != null) {
		data.orderTotal = new List<OrderDetailResponseDataOrderTotal>();
		(json['order_total'] as List).forEach((v) {
			data.orderTotal.add(new OrderDetailResponseDataOrderTotal().fromJson(v));
		});
	}
	if (json['payment_status'] != null) {
		data.paymentStatus = new OrderDetailResponseDataPaymentStatus().fromJson(json['payment_status']);
	}
	if (json['order_status'] != null) {
		data.orderStatus = new OrderDetailResponseDataOrderStatus().fromJson(json['order_status']);
	}
	if (json['order_expired_at'] != null) {
		data.orderExpiredAt = new OrderDetailResponseDataOrderExpiredAt().fromJson(json['order_expired_at']);
	}
	if (json['payment_at'] != null) {
		data.paymentAt = new OrderDetailResponseDataPaymentAt().fromJson(json['payment_at']);
	}
	if (json['created_at'] != null) {
		data.createdAt = new OrderDetailResponseDataCreatedAt().fromJson(json['created_at']);
	}
	if (json['updated_at'] != null) {
		data.updatedAt = new OrderDetailResponseDataUpdatedAt().fromJson(json['updated_at']);
	}
	return data;
}

Map<String, dynamic> orderDetailResponseDataToJson(OrderDetailResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['order_no'] = entity.orderNo;
	data['store_id'] = entity.storeId;
	data['store_name'] = entity.storeName;
	data['user_id'] = entity.userId;
	data['firstname'] = entity.firstname;
	data['lastname'] = entity.lastname;
	data['email'] = entity.email;
	data['telephone'] = entity.telephone;
	data['payment_provider'] = entity.paymentProvider;
	data['payment_method'] = entity.paymentMethod;
	data['payment_method_text'] = entity.paymentMethodText;
	data['payment_ref'] = entity.paymentRef;
	data['payment_ref_no1'] = entity.paymentRefNo1;
	data['payment_ref_no2'] = entity.paymentRefNo2;
	data['payment_message'] = entity.paymentMessage;
	data['shipping_provider'] = entity.shippingProvider;
	data['shipping_method'] = entity.shippingMethod;
	data['shipping_address'] = entity.shippingAddress;
	data['shipping_lat'] = entity.shippingLat;
	data['shipping_long'] = entity.shippingLong;
	if (entity.shippingAt != null) {
		data['shipping_at'] = entity.shippingAt.toJson();
	}
	data['price'] = entity.price;
	data['discount'] = entity.discount;
	data['summary'] = entity.summary;
	data['total'] = entity.total;
	data['currency_code'] = entity.currencyCode;
	data['currency_symbol'] = entity.currencySymbol;
	data['price_text'] = entity.priceText;
	data['discount_text'] = entity.discountText;
	data['summary_text'] = entity.summaryText;
	data['total_text'] = entity.totalText;
	data['comment'] = entity.comment;
	data['ip'] = entity.ip;
	if (entity.user != null) {
		data['user'] = entity.user.toJson();
	}
	if (entity.products != null) {
		data['products'] =  entity.products.map((v) => v.toJson()).toList();
	}
	data['quantity'] = entity.quantity;
	if (entity.orderTotal != null) {
		data['order_total'] =  entity.orderTotal.map((v) => v.toJson()).toList();
	}
	if (entity.paymentStatus != null) {
		data['payment_status'] = entity.paymentStatus.toJson();
	}
	if (entity.orderStatus != null) {
		data['order_status'] = entity.orderStatus.toJson();
	}
	if (entity.orderExpiredAt != null) {
		data['order_expired_at'] = entity.orderExpiredAt.toJson();
	}
	if (entity.paymentAt != null) {
		data['payment_at'] = entity.paymentAt.toJson();
	}
	if (entity.createdAt != null) {
		data['created_at'] = entity.createdAt.toJson();
	}
	if (entity.updatedAt != null) {
		data['updated_at'] = entity.updatedAt.toJson();
	}
	return data;
}

orderDetailResponseDataShippingAtFromJson(OrderDetailResponseDataShippingAt data, Map<String, dynamic> json) {
	if (json['value'] != null) {
		data.value = json['value']?.toString();
	}
	if (json['date'] != null) {
		data.date = json['date']?.toString();
	}
	if (json['time'] != null) {
		data.time = json['time']?.toString();
	}
	return data;
}

Map<String, dynamic> orderDetailResponseDataShippingAtToJson(OrderDetailResponseDataShippingAt entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['value'] = entity.value;
	data['date'] = entity.date;
	data['time'] = entity.time;
	return data;
}

orderDetailResponseDataUserFromJson(OrderDetailResponseDataUser data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['username'] != null) {
		data.username = json['username']?.toString();
	}
	if (json['country_code'] != null) {
		data.countryCode = json['country_code']?.toString();
	}
	if (json['gender'] != null) {
		data.gender = json['gender']?.toString();
	}
	if (json['first_name'] != null) {
		data.firstName = json['first_name']?.toString();
	}
	if (json['last_name'] != null) {
		data.lastName = json['last_name']?.toString();
	}
	if (json['avatar'] != null) {
		data.avatar = new OrderDetailResponseDataUserAvatar().fromJson(json['avatar']);
	}
	return data;
}

Map<String, dynamic> orderDetailResponseDataUserToJson(OrderDetailResponseDataUser entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['username'] = entity.username;
	data['country_code'] = entity.countryCode;
	data['gender'] = entity.gender;
	data['first_name'] = entity.firstName;
	data['last_name'] = entity.lastName;
	if (entity.avatar != null) {
		data['avatar'] = entity.avatar.toJson();
	}
	return data;
}

orderDetailResponseDataUserAvatarFromJson(OrderDetailResponseDataUserAvatar data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['resize_url'] != null) {
		data.resizeUrl = json['resize_url']?.toString();
	}
	return data;
}

Map<String, dynamic> orderDetailResponseDataUserAvatarToJson(OrderDetailResponseDataUserAvatar entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['resize_url'] = entity.resizeUrl;
	return data;
}

orderDetailResponseDataProductFromJson(OrderDetailResponseDataProduct data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['product_id'] != null) {
		data.productId = json['product_id']?.toInt();
	}
	if (json['product_variant_id'] != null) {
		data.productVariantId = json['product_variant_id']?.toInt();
	}
	if (json['product_code'] != null) {
		data.productCode = json['product_code']?.toString();
	}
	if (json['product_name'] != null) {
		data.productName = json['product_name']?.toString();
	}
	if (json['product_option'] != null) {
		data.productOption = json['product_option']?.toString();
	}
	if (json['quantity'] != null) {
		data.quantity = json['quantity']?.toInt();
	}
	if (json['price'] != null) {
		data.price = json['price']?.toInt();
	}
	if (json['sum_price'] != null) {
		data.sumPrice = json['sum_price']?.toInt();
	}
	if (json['discount'] != null) {
		data.discount = json['discount']?.toInt();
	}
	if (json['summary'] != null) {
		data.summary = json['summary']?.toInt();
	}
	if (json['service_fee'] != null) {
		data.serviceFee = json['service_fee']?.toInt();
	}
	if (json['payment_fee'] != null) {
		data.paymentFee = json['payment_fee']?.toInt();
	}
	if (json['tax'] != null) {
		data.tax = json['tax']?.toInt();
	}
	if (json['exclude_tax'] != null) {
		data.excludeTax = json['exclude_tax']?.toInt();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toInt();
	}
	if (json['price_text'] != null) {
		data.priceText = json['price_text']?.toString();
	}
	if (json['sum_price_text'] != null) {
		data.sumPriceText = json['sum_price_text']?.toString();
	}
	if (json['discount_text'] != null) {
		data.discountText = json['discount_text']?.toString();
	}
	if (json['total_text'] != null) {
		data.totalText = json['total_text']?.toString();
	}
	if (json['status'] != null) {
		data.status = new OrderDetailResponseDataProductsStatus().fromJson(json['status']);
	}
	if (json['image'] != null) {
		data.image = new OrderDetailResponseDataProductsImage().fromJson(json['image']);
	}
	return data;
}

Map<String, dynamic> orderDetailResponseDataProductToJson(OrderDetailResponseDataProduct entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['product_id'] = entity.productId;
	data['product_variant_id'] = entity.productVariantId;
	data['product_code'] = entity.productCode;
	data['product_name'] = entity.productName;
	data['product_option'] = entity.productOption;
	data['quantity'] = entity.quantity;
	data['price'] = entity.price;
	data['sum_price'] = entity.sumPrice;
	data['discount'] = entity.discount;
	data['summary'] = entity.summary;
	data['service_fee'] = entity.serviceFee;
	data['payment_fee'] = entity.paymentFee;
	data['tax'] = entity.tax;
	data['exclude_tax'] = entity.excludeTax;
	data['total'] = entity.total;
	data['price_text'] = entity.priceText;
	data['sum_price_text'] = entity.sumPriceText;
	data['discount_text'] = entity.discountText;
	data['total_text'] = entity.totalText;
	if (entity.status != null) {
		data['status'] = entity.status.toJson();
	}
	if (entity.image != null) {
		data['image'] = entity.image.toJson();
	}
	return data;
}

orderDetailResponseDataProductsStatusFromJson(OrderDetailResponseDataProductsStatus data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['text'] != null) {
		data.text = json['text']?.toString();
	}
	return data;
}

Map<String, dynamic> orderDetailResponseDataProductsStatusToJson(OrderDetailResponseDataProductsStatus entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['text'] = entity.text;
	return data;
}

orderDetailResponseDataProductsImageFromJson(OrderDetailResponseDataProductsImage data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['tag'] != null) {
		data.tag = json['tag'];
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	return data;
}

Map<String, dynamic> orderDetailResponseDataProductsImageToJson(OrderDetailResponseDataProductsImage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['tag'] = entity.tag;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	return data;
}

orderDetailResponseDataOrderTotalFromJson(OrderDetailResponseDataOrderTotal data, Map<String, dynamic> json) {
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['value'] != null) {
		data.value = json['value']?.toInt();
	}
	if (json['value_text'] != null) {
		data.valueText = json['value_text']?.toString();
	}
	return data;
}

Map<String, dynamic> orderDetailResponseDataOrderTotalToJson(OrderDetailResponseDataOrderTotal entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['code'] = entity.code;
	data['title'] = entity.title;
	data['value'] = entity.value;
	data['value_text'] = entity.valueText;
	return data;
}

orderDetailResponseDataPaymentStatusFromJson(OrderDetailResponseDataPaymentStatus data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	return data;
}

Map<String, dynamic> orderDetailResponseDataPaymentStatusToJson(OrderDetailResponseDataPaymentStatus entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

orderDetailResponseDataOrderStatusFromJson(OrderDetailResponseDataOrderStatus data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	return data;
}

Map<String, dynamic> orderDetailResponseDataOrderStatusToJson(OrderDetailResponseDataOrderStatus entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

orderDetailResponseDataOrderExpiredAtFromJson(OrderDetailResponseDataOrderExpiredAt data, Map<String, dynamic> json) {
	if (json['value'] != null) {
		data.value = json['value']?.toString();
	}
	if (json['date'] != null) {
		data.date = json['date']?.toString();
	}
	if (json['time'] != null) {
		data.time = json['time']?.toString();
	}
	return data;
}

Map<String, dynamic> orderDetailResponseDataOrderExpiredAtToJson(OrderDetailResponseDataOrderExpiredAt entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['value'] = entity.value;
	data['date'] = entity.date;
	data['time'] = entity.time;
	return data;
}

orderDetailResponseDataPaymentAtFromJson(OrderDetailResponseDataPaymentAt data, Map<String, dynamic> json) {
	if (json['value'] != null) {
		data.value = json['value']?.toString();
	}
	if (json['date'] != null) {
		data.date = json['date']?.toString();
	}
	if (json['time'] != null) {
		data.time = json['time']?.toString();
	}
	return data;
}

Map<String, dynamic> orderDetailResponseDataPaymentAtToJson(OrderDetailResponseDataPaymentAt entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['value'] = entity.value;
	data['date'] = entity.date;
	data['time'] = entity.time;
	return data;
}

orderDetailResponseDataCreatedAtFromJson(OrderDetailResponseDataCreatedAt data, Map<String, dynamic> json) {
	if (json['value'] != null) {
		data.value = json['value']?.toString();
	}
	if (json['date'] != null) {
		data.date = json['date']?.toString();
	}
	if (json['time'] != null) {
		data.time = json['time']?.toString();
	}
	return data;
}

Map<String, dynamic> orderDetailResponseDataCreatedAtToJson(OrderDetailResponseDataCreatedAt entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['value'] = entity.value;
	data['date'] = entity.date;
	data['time'] = entity.time;
	return data;
}

orderDetailResponseDataUpdatedAtFromJson(OrderDetailResponseDataUpdatedAt data, Map<String, dynamic> json) {
	if (json['value'] != null) {
		data.value = json['value']?.toString();
	}
	if (json['date'] != null) {
		data.date = json['date']?.toString();
	}
	if (json['time'] != null) {
		data.time = json['time']?.toString();
	}
	return data;
}

Map<String, dynamic> orderDetailResponseDataUpdatedAtToJson(OrderDetailResponseDataUpdatedAt entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['value'] = entity.value;
	data['date'] = entity.date;
	data['time'] = entity.time;
	return data;
}

orderDetailResponseBenchFromJson(OrderDetailResponseBench data, Map<String, dynamic> json) {
	if (json['second'] != null) {
		data.second = json['second']?.toInt();
	}
	if (json['millisecond'] != null) {
		data.millisecond = json['millisecond']?.toDouble();
	}
	if (json['format'] != null) {
		data.format = json['format']?.toString();
	}
	return data;
}

Map<String, dynamic> orderDetailResponseBenchToJson(OrderDetailResponseBench entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}