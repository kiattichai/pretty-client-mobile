import 'package:pretty_client_mobile/modules/coins_module/models/topup_coins_request_entity.dart';

topupCoinsRequestEntityFromJson(TopupCoinsRequestEntity data, Map<String, dynamic> json) {
	if (json['amount'] != null) {
		data.amount = json['amount']?.toDouble();
	}
	if (json['bank_id'] != null) {
		data.bankId = json['bank_id']?.toInt();
	}
	if (json['paid_at'] != null) {
		data.paidAt = json['paid_at']?.toString();
	}
	if (json['file_name'] != null) {
		data.fileName = json['file_name']?.toString();
	}
	if (json['file_data'] != null) {
		data.fileData = json['file_data']?.toString();
	}
	return data;
}

Map<String, dynamic> topupCoinsRequestEntityToJson(TopupCoinsRequestEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['amount'] = entity.amount;
	data['bank_id'] = entity.bankId;
	data['paid_at'] = entity.paidAt;
	data['file_name'] = entity.fileName;
	data['file_data'] = entity.fileData;
	return data;
}