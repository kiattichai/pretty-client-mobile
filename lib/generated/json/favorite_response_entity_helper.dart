import 'package:pretty_client_mobile/modules/favorite_module/model/favorite_response_entity.dart';

favoriteResponseEntityFromJson(FavoriteResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new FavoriteData().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new FavoriteResponseBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> favoriteResponseEntityToJson(FavoriteResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

favoriteDataFromJson(FavoriteData data, Map<String, dynamic> json) {
	if (json['pagination'] != null) {
		data.pagination = new FavoritePagination().fromJson(json['pagination']);
	}
	if (json['record'] != null) {
		data.record = new List<Favorite>();
		(json['record'] as List).forEach((v) {
			data.record.add(new Favorite().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> favoriteDataToJson(FavoriteData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.pagination != null) {
		data['pagination'] = entity.pagination.toJson();
	}
	if (entity.record != null) {
		data['record'] =  entity.record.map((v) => v.toJson()).toList();
	}
	return data;
}

favoritePaginationFromJson(FavoritePagination data, Map<String, dynamic> json) {
	if (json['current_page'] != null) {
		data.currentPage = json['current_page']?.toInt();
	}
	if (json['last_page'] != null) {
		data.lastPage = json['last_page']?.toInt();
	}
	if (json['limit'] != null) {
		data.limit = json['limit']?.toInt();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toInt();
	}
	return data;
}

Map<String, dynamic> favoritePaginationToJson(FavoritePagination entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['current_page'] = entity.currentPage;
	data['last_page'] = entity.lastPage;
	data['limit'] = entity.limit;
	data['total'] = entity.total;
	return data;
}

favoriteFromJson(Favorite data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['product'] != null) {
		data.product = new FavoriteProduct().fromJson(json['product']);
	}
	if (json['created_at'] != null) {
		data.createdAt = json['created_at']?.toString();
	}
	if (json['updated_at'] != null) {
		data.updatedAt = json['updated_at']?.toString();
	}
	return data;
}

Map<String, dynamic> favoriteToJson(Favorite entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	if (entity.product != null) {
		data['product'] = entity.product.toJson();
	}
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	return data;
}

favoriteProductFromJson(FavoriteProduct data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['attributes'] != null) {
		data.attributes = new List<dynamic>();
		data.attributes.addAll(json['attributes']);
	}
	if (json['avatar'] != null) {
		data.avatar = new FavoriteProductAvatar().fromJson(json['avatar']);
	}
	if (json['onFloor'] != null) {
		data.onFloor = json['onFloor'];
	}
	if (json['feedbacks'] != null) {
		data.feedbacks = new FavoriteProductFeedback().fromJson(json['feedbacks']);
	}
	return data;
}

Map<String, dynamic> favoriteProductToJson(FavoriteProduct entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['name'] = entity.name;
	if (entity.attributes != null) {
		data['attributes'] =  [];
	}
	if (entity.avatar != null) {
		data['avatar'] = entity.avatar.toJson();
	}
	data['onFloor'] = entity.onFloor;
	if (entity.feedbacks != null) {
		data['feedbacks'] = entity.feedbacks.toJson();
	}
	return data;
}

favoriteProductAvatarFromJson(FavoriteProductAvatar data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['store_id'] != null) {
		data.storeId = json['store_id']?.toInt();
	}
	if (json['tag'] != null) {
		data.tag = json['tag']?.toString();
	}
	if (json['album_id'] != null) {
		data.albumId = json['album_id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	return data;
}

Map<String, dynamic> favoriteProductAvatarToJson(FavoriteProductAvatar entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['store_id'] = entity.storeId;
	data['tag'] = entity.tag;
	data['album_id'] = entity.albumId;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['position'] = entity.position;
	return data;
}

favoriteProductFeedbackFromJson(FavoriteProductFeedback data, Map<String, dynamic> json) {
	if (json['summary'] != null) {
		data.summary = json['summary']?.toDouble();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toInt();
	}
	if (json['record'] != null) {
		data.record = new List<FeedbackRecord>();
		(json['record'] as List).forEach((v) {
			data.record.add(new FeedbackRecord().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> favoriteProductFeedbackToJson(FavoriteProductFeedback entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['summary'] = entity.summary;
	data['total'] = entity.total;
	if (entity.record != null) {
		data['record'] =  entity.record.map((v) => v.toJson()).toList();
	}
	return data;
}

feedbackRecordFromJson(FeedbackRecord data, Map<String, dynamic> json) {
	if (json['score'] != null) {
		data.score = json['score']?.toInt();
	}
	if (json['count'] != null) {
		data.count = json['count']?.toInt();
	}
	return data;
}

Map<String, dynamic> feedbackRecordToJson(FeedbackRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['score'] = entity.score;
	data['count'] = entity.count;
	return data;
}

favoriteResponseBenchFromJson(FavoriteResponseBench data, Map<String, dynamic> json) {
	if (json['second'] != null) {
		data.second = json['second']?.toInt();
	}
	if (json['millisecond'] != null) {
		data.millisecond = json['millisecond']?.toDouble();
	}
	if (json['format'] != null) {
		data.format = json['format']?.toString();
	}
	return data;
}

Map<String, dynamic> favoriteResponseBenchToJson(FavoriteResponseBench entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}