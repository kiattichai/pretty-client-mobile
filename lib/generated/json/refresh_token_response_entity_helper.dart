import 'package:pretty_client_mobile/modules/shared_module/models/refresh_token_response_entity.dart';

refreshTokenResponseEntityFromJson(RefreshTokenResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new RefreshTokenResponseData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> refreshTokenResponseEntityToJson(RefreshTokenResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	return data;
}

refreshTokenResponseDataFromJson(RefreshTokenResponseData data, Map<String, dynamic> json) {
	if (json['token_type'] != null) {
		data.tokenType = json['token_type']?.toString();
	}
	if (json['access_token'] != null) {
		data.accessToken = json['access_token']?.toString();
	}
	if (json['refresh_token'] != null) {
		data.refreshToken = json['refresh_token']?.toString();
	}
	return data;
}

Map<String, dynamic> refreshTokenResponseDataToJson(RefreshTokenResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['token_type'] = entity.tokenType;
	data['access_token'] = entity.accessToken;
	data['refresh_token'] = entity.refreshToken;
	return data;
}