import 'package:pretty_client_mobile/modules/coins_module/models/coins_transactions_response_entity.dart';

coinsTransactionsResponseEntityFromJson(CoinsTransactionsResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new CoinsTransactionsResponseData().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new CoinsTransactionsResponseBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> coinsTransactionsResponseEntityToJson(CoinsTransactionsResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

coinsTransactionsResponseDataFromJson(CoinsTransactionsResponseData data, Map<String, dynamic> json) {
	if (json['pagination'] != null) {
		data.pagination = new CoinsTransactionsResponseDataPagination().fromJson(json['pagination']);
	}
	if (json['record'] != null) {
		data.record = new List<CoinsTransactionsResponseDataRecord>();
		(json['record'] as List).forEach((v) {
			data.record.add(new CoinsTransactionsResponseDataRecord().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> coinsTransactionsResponseDataToJson(CoinsTransactionsResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.pagination != null) {
		data['pagination'] = entity.pagination.toJson();
	}
	if (entity.record != null) {
		data['record'] =  entity.record.map((v) => v.toJson()).toList();
	}
	return data;
}

coinsTransactionsResponseDataPaginationFromJson(CoinsTransactionsResponseDataPagination data, Map<String, dynamic> json) {
	if (json['current_page'] != null) {
		data.currentPage = json['current_page']?.toInt();
	}
	if (json['last_page'] != null) {
		data.lastPage = json['last_page']?.toInt();
	}
	if (json['limit'] != null) {
		data.limit = json['limit']?.toInt();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toInt();
	}
	return data;
}

Map<String, dynamic> coinsTransactionsResponseDataPaginationToJson(CoinsTransactionsResponseDataPagination entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['current_page'] = entity.currentPage;
	data['last_page'] = entity.lastPage;
	data['limit'] = entity.limit;
	data['total'] = entity.total;
	return data;
}

coinsTransactionsResponseDataRecordFromJson(CoinsTransactionsResponseDataRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['transaction_no'] != null) {
		data.transactionNo = json['transaction_no']?.toString();
	}
	if (json['type'] != null) {
		data.type = new CoinsTransactionsResponseDataRecordType().fromJson(json['type']);
	}
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['description'] != null) {
		data.description = json['description']?.toString();
	}
	if (json['user'] != null) {
		data.user = new CoinsTransactionsResponseDataRecordUser().fromJson(json['user']);
	}
	if (json['amount'] != null) {
		data.amount = json['amount']?.toString();
	}
	if (json['amount_text'] != null) {
		data.amountText = json['amount_text']?.toString();
	}
	if (json['payment_provider'] != null) {
		data.paymentProvider = json['payment_provider']?.toString();
	}
	if (json['payment_method'] != null) {
		data.paymentMethod = json['payment_method']?.toString();
	}
	if (json['created_at'] != null) {
		data.createdAt = new CoinsTransactionsResponseDataRecordCreatedAt().fromJson(json['created_at']);
	}
	if (json['updated_at'] != null) {
		data.updatedAt = new CoinsTransactionsResponseDataRecordUpdatedAt().fromJson(json['updated_at']);
	}
	if (json['paid_at'] != null) {
		data.paidAt = json['paid_at']?.toString();
	}
	if (json['status'] != null) {
		data.status = new CoinsTransactionsResponseDataRecordStatus().fromJson(json['status']);
	}
	if (json['document_status'] != null) {
		data.documentStatus = new CoinsTransactionsResponseDataRecordDocumentStatus().fromJson(json['document_status']);
	}
	if (json['image'] != null) {
		data.image = new CoinsTransactionsResponseDataRecordImage().fromJson(json['image']);
	}
	if (json['bank'] != null) {
		data.bank = new CoinsTransactionsResponseDataRecordBank().fromJson(json['bank']);
	}
	if (json['staff'] != null) {
		data.staff = new CoinsTransactionsResponseDataRecordStaff().fromJson(json['staff']);
	}
	if (json['remark'] != null) {
		data.remark = json['remark']?.toString();
	}
	return data;
}

Map<String, dynamic> coinsTransactionsResponseDataRecordToJson(CoinsTransactionsResponseDataRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['transaction_no'] = entity.transactionNo;
	if (entity.type != null) {
		data['type'] = entity.type.toJson();
	}
	data['title'] = entity.title;
	data['description'] = entity.description;
	if (entity.user != null) {
		data['user'] = entity.user.toJson();
	}
	data['amount'] = entity.amount;
	data['amount_text'] = entity.amountText;
	data['payment_provider'] = entity.paymentProvider;
	data['payment_method'] = entity.paymentMethod;
	if (entity.createdAt != null) {
		data['created_at'] = entity.createdAt.toJson();
	}
	if (entity.updatedAt != null) {
		data['updated_at'] = entity.updatedAt.toJson();
	}
	data['paid_at'] = entity.paidAt;
	if (entity.status != null) {
		data['status'] = entity.status.toJson();
	}
	if (entity.documentStatus != null) {
		data['document_status'] = entity.documentStatus.toJson();
	}
	if (entity.image != null) {
		data['image'] = entity.image.toJson();
	}
	if (entity.bank != null) {
		data['bank'] = entity.bank.toJson();
	}
	if (entity.staff != null) {
		data['staff'] = entity.staff.toJson();
	}
	data['remark'] = entity.remark;
	return data;
}

coinsTransactionsResponseDataRecordTypeFromJson(CoinsTransactionsResponseDataRecordType data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['text'] != null) {
		data.text = json['text']?.toString();
	}
	return data;
}

Map<String, dynamic> coinsTransactionsResponseDataRecordTypeToJson(CoinsTransactionsResponseDataRecordType entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['text'] = entity.text;
	return data;
}

coinsTransactionsResponseDataRecordUserFromJson(CoinsTransactionsResponseDataRecordUser data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['username'] != null) {
		data.username = json['username']?.toString();
	}
	if (json['country_code'] != null) {
		data.countryCode = json['country_code']?.toString();
	}
	if (json['first_name'] != null) {
		data.firstName = json['first_name']?.toString();
	}
	if (json['last_name'] != null) {
		data.lastName = json['last_name']?.toString();
	}
	return data;
}

Map<String, dynamic> coinsTransactionsResponseDataRecordUserToJson(CoinsTransactionsResponseDataRecordUser entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['username'] = entity.username;
	data['country_code'] = entity.countryCode;
	data['first_name'] = entity.firstName;
	data['last_name'] = entity.lastName;
	return data;
}

coinsTransactionsResponseDataRecordCreatedAtFromJson(CoinsTransactionsResponseDataRecordCreatedAt data, Map<String, dynamic> json) {
	if (json['value'] != null) {
		data.value = json['value']?.toString();
	}
	if (json['date'] != null) {
		data.date = json['date']?.toString();
	}
	if (json['time'] != null) {
		data.time = json['time']?.toString();
	}
	return data;
}

Map<String, dynamic> coinsTransactionsResponseDataRecordCreatedAtToJson(CoinsTransactionsResponseDataRecordCreatedAt entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['value'] = entity.value;
	data['date'] = entity.date;
	data['time'] = entity.time;
	return data;
}

coinsTransactionsResponseDataRecordUpdatedAtFromJson(CoinsTransactionsResponseDataRecordUpdatedAt data, Map<String, dynamic> json) {
	if (json['value'] != null) {
		data.value = json['value']?.toString();
	}
	if (json['date'] != null) {
		data.date = json['date']?.toString();
	}
	if (json['time'] != null) {
		data.time = json['time']?.toString();
	}
	return data;
}

Map<String, dynamic> coinsTransactionsResponseDataRecordUpdatedAtToJson(CoinsTransactionsResponseDataRecordUpdatedAt entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['value'] = entity.value;
	data['date'] = entity.date;
	data['time'] = entity.time;
	return data;
}

coinsTransactionsResponseDataRecordStatusFromJson(CoinsTransactionsResponseDataRecordStatus data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['text'] != null) {
		data.text = json['text']?.toString();
	}
	return data;
}

Map<String, dynamic> coinsTransactionsResponseDataRecordStatusToJson(CoinsTransactionsResponseDataRecordStatus entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['text'] = entity.text;
	return data;
}

coinsTransactionsResponseDataRecordDocumentStatusFromJson(CoinsTransactionsResponseDataRecordDocumentStatus data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['text'] != null) {
		data.text = json['text']?.toString();
	}
	return data;
}

Map<String, dynamic> coinsTransactionsResponseDataRecordDocumentStatusToJson(CoinsTransactionsResponseDataRecordDocumentStatus entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['text'] = entity.text;
	return data;
}

coinsTransactionsResponseDataRecordImageFromJson(CoinsTransactionsResponseDataRecordImage data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['tag'] != null) {
		data.tag = json['tag'];
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	return data;
}

Map<String, dynamic> coinsTransactionsResponseDataRecordImageToJson(CoinsTransactionsResponseDataRecordImage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['tag'] = entity.tag;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	return data;
}

coinsTransactionsResponseDataRecordBankFromJson(CoinsTransactionsResponseDataRecordBank data, Map<String, dynamic> json) {
	return data;
}

Map<String, dynamic> coinsTransactionsResponseDataRecordBankToJson(CoinsTransactionsResponseDataRecordBank entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	return data;
}

coinsTransactionsResponseDataRecordStaffFromJson(CoinsTransactionsResponseDataRecordStaff data, Map<String, dynamic> json) {
	return data;
}

Map<String, dynamic> coinsTransactionsResponseDataRecordStaffToJson(CoinsTransactionsResponseDataRecordStaff entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	return data;
}

coinsTransactionsResponseBenchFromJson(CoinsTransactionsResponseBench data, Map<String, dynamic> json) {
	if (json['second'] != null) {
		data.second = json['second']?.toInt();
	}
	if (json['millisecond'] != null) {
		data.millisecond = json['millisecond']?.toDouble();
	}
	if (json['format'] != null) {
		data.format = json['format']?.toString();
	}
	return data;
}

Map<String, dynamic> coinsTransactionsResponseBenchToJson(CoinsTransactionsResponseBench entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}