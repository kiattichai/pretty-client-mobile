import 'package:pretty_client_mobile/modules/profile_module/models/notification_setting_response_entity.dart';

notificationSettingResponseEntityFromJson(NotificationSettingResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new NotificationSettingData().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new NotificationSettingBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> notificationSettingResponseEntityToJson(NotificationSettingResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

notificationSettingDataFromJson(NotificationSettingData data, Map<String, dynamic> json) {
	if (json['summary'] != null) {
		data.summary = json['summary'];
	}
	if (json['record'] != null) {
		data.record = new List<NotificationSettingRecord>();
		(json['record'] as List).forEach((v) {
			data.record.add(new NotificationSettingRecord().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> notificationSettingDataToJson(NotificationSettingData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['summary'] = entity.summary;
	if (entity.record != null) {
		data['record'] =  entity.record.map((v) => v.toJson()).toList();
	}
	return data;
}

notificationSettingRecordFromJson(NotificationSettingRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	return data;
}

Map<String, dynamic> notificationSettingRecordToJson(NotificationSettingRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['status'] = entity.status;
	return data;
}

notificationSettingBenchFromJson(NotificationSettingBench data, Map<String, dynamic> json) {
	if (json['second'] != null) {
		data.second = json['second']?.toInt();
	}
	if (json['millisecond'] != null) {
		data.millisecond = json['millisecond']?.toDouble();
	}
	if (json['format'] != null) {
		data.format = json['format']?.toString();
	}
	return data;
}

Map<String, dynamic> notificationSettingBenchToJson(NotificationSettingBench entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}