import 'package:pretty_client_mobile/modules/register_module/models/facebook_info_entity.dart';

facebookInfoEntityFromJson(FacebookInfoEntity data, Map<String, dynamic> json) {
	if (json['picture'] != null) {
		data.picture = new FacebookInfoPicture().fromJson(json['picture']);
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['first_name'] != null) {
		data.firstName = json['first_name']?.toString();
	}
	if (json['last_name'] != null) {
		data.lastName = json['last_name']?.toString();
	}
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	return data;
}

Map<String, dynamic> facebookInfoEntityToJson(FacebookInfoEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.picture != null) {
		data['picture'] = entity.picture.toJson();
	}
	data['name'] = entity.name;
	data['first_name'] = entity.firstName;
	data['last_name'] = entity.lastName;
	data['id'] = entity.id;
	return data;
}

facebookInfoPictureFromJson(FacebookInfoPicture data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new FacebookInfoPictureData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> facebookInfoPictureToJson(FacebookInfoPicture entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	return data;
}

facebookInfoPictureDataFromJson(FacebookInfoPictureData data, Map<String, dynamic> json) {
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['is_silhouette'] != null) {
		data.isSilhouette = json['is_silhouette'];
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	return data;
}

Map<String, dynamic> facebookInfoPictureDataToJson(FacebookInfoPictureData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['height'] = entity.height;
	data['is_silhouette'] = entity.isSilhouette;
	data['url'] = entity.url;
	data['width'] = entity.width;
	return data;
}