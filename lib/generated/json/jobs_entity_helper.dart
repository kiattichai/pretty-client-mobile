import 'package:pretty_client_mobile/modules/booking_module/models/jobs_entity.dart';
import 'package:pretty_client_mobile/modules/booking_module/models/BookingTime.dart';

jobsEntityFromJson(JobsEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new JobsData().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new JobsBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> jobsEntityToJson(JobsEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

jobsDataFromJson(JobsData data, Map<String, dynamic> json) {
	if (json['pagination'] != null) {
		data.pagination = new JobsDataPagination().fromJson(json['pagination']);
	}
	if (json['record'] != null) {
		data.record = new List<JobsDataRecord>();
		(json['record'] as List).forEach((v) {
			data.record.add(new JobsDataRecord().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> jobsDataToJson(JobsData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.pagination != null) {
		data['pagination'] = entity.pagination.toJson();
	}
	if (entity.record != null) {
		data['record'] =  entity.record.map((v) => v.toJson()).toList();
	}
	return data;
}

jobsDataPaginationFromJson(JobsDataPagination data, Map<String, dynamic> json) {
	if (json['current_page'] != null) {
		data.currentPage = json['current_page']?.toInt();
	}
	if (json['last_page'] != null) {
		data.lastPage = json['last_page']?.toInt();
	}
	if (json['limit'] != null) {
		data.limit = json['limit']?.toInt();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toInt();
	}
	return data;
}

Map<String, dynamic> jobsDataPaginationToJson(JobsDataPagination entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['current_page'] = entity.currentPage;
	data['last_page'] = entity.lastPage;
	data['limit'] = entity.limit;
	data['total'] = entity.total;
	return data;
}

jobsDataRecordFromJson(JobsDataRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['date'] != null) {
		data.date = json['date']?.toString();
	}
	if (json['track'] != null) {
		data.track = json['track']?.toInt();
	}
	if (json['store'] != null) {
		data.store = new JobsDataRecordStore().fromJson(json['store']);
	}
	if (json['pretty'] != null) {
		data.pretty = new JobsDataRecordPretty().fromJson(json['pretty']);
	}
	if (json['venue'] != null) {
		data.venue = new JobsDataRecordVenue().fromJson(json['venue']);
	}
	if (json['booking'] != null) {
		data.booking = new JobsDataRecordBooking().fromJson(json['booking']);
	}
	if (json['status'] != null) {
		data.status = new JobsDataRecordStatus().fromJson(json['status']);
	}
	if (json['bookingTime'] != null) {
		data.bookingTime = new List<BookingTime>();
		(json['bookingTime'] as List).forEach((v) {
			data.bookingTime.add(new BookingTime().fromJson(v));
		});
	}
	if (json['canBooking'] != null) {
		data.canBooking = json['canBooking'];
	}
	return data;
}

Map<String, dynamic> jobsDataRecordToJson(JobsDataRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['date'] = entity.date;
	data['track'] = entity.track;
	if (entity.store != null) {
		data['store'] = entity.store.toJson();
	}
	if (entity.pretty != null) {
		data['pretty'] = entity.pretty.toJson();
	}
	if (entity.venue != null) {
		data['venue'] = entity.venue.toJson();
	}
	if (entity.booking != null) {
		data['booking'] = entity.booking.toJson();
	}
	if (entity.status != null) {
		data['status'] = entity.status.toJson();
	}
	if (entity.bookingTime != null) {
		data['bookingTime'] =  entity.bookingTime.map((v) => v.toJson()).toList();
	}
	data['canBooking'] = entity.canBooking;
	return data;
}

jobsDataRecordStoreFromJson(JobsDataRecordStore data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	return data;
}

Map<String, dynamic> jobsDataRecordStoreToJson(JobsDataRecordStore entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	return data;
}

jobsDataRecordPrettyFromJson(JobsDataRecordPretty data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['group'] != null) {
		data.group = new JobsDataRecordPrettyGroup().fromJson(json['group']);
	}
	if (json['first_name'] != null) {
		data.firstName = json['first_name']?.toString();
	}
	if (json['last_name'] != null) {
		data.lastName = json['last_name']?.toString();
	}
	if (json['nick_name'] != null) {
		data.nickName = json['nick_name']?.toString();
	}
	if (json['gender'] != null) {
		data.gender = json['gender']?.toString();
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['avatar'] != null) {
		data.avatar = new JobsDataRecordPrettyAvatar().fromJson(json['avatar']);
	}
	return data;
}

Map<String, dynamic> jobsDataRecordPrettyToJson(JobsDataRecordPretty entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['code'] = entity.code;
	if (entity.group != null) {
		data['group'] = entity.group.toJson();
	}
	data['first_name'] = entity.firstName;
	data['last_name'] = entity.lastName;
	data['nick_name'] = entity.nickName;
	data['gender'] = entity.gender;
	data['status'] = entity.status;
	if (entity.avatar != null) {
		data['avatar'] = entity.avatar.toJson();
	}
	return data;
}

jobsDataRecordPrettyGroupFromJson(JobsDataRecordPrettyGroup data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	return data;
}

Map<String, dynamic> jobsDataRecordPrettyGroupToJson(JobsDataRecordPrettyGroup entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

jobsDataRecordPrettyAvatarFromJson(JobsDataRecordPrettyAvatar data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['tag'] != null) {
		data.tag = json['tag'];
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	return data;
}

Map<String, dynamic> jobsDataRecordPrettyAvatarToJson(JobsDataRecordPrettyAvatar entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['tag'] = entity.tag;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['position'] = entity.position;
	return data;
}

jobsDataRecordVenueFromJson(JobsDataRecordVenue data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['lat'] != null) {
		data.lat = json['lat']?.toDouble();
	}
	if (json['long'] != null) {
		data.long = json['long']?.toDouble();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['address'] != null) {
		data.address = json['address']?.toString();
	}
	return data;
}

Map<String, dynamic> jobsDataRecordVenueToJson(JobsDataRecordVenue entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['lat'] = entity.lat;
	data['long'] = entity.long;
	data['name'] = entity.name;
	data['address'] = entity.address;
	return data;
}

jobsDataRecordBookingFromJson(JobsDataRecordBooking data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['order_id'] != null) {
		data.orderId = json['order_id']?.toInt();
	}
	if (json['store_id'] != null) {
		data.storeId = json['store_id']?.toInt();
	}
	if (json['pretty_id'] != null) {
		data.prettyId = json['pretty_id']?.toInt();
	}
	if (json['pretty_code'] != null) {
		data.prettyCode = json['pretty_code']?.toString();
	}
	if (json['pretty_group_name'] != null) {
		data.prettyGroupName = json['pretty_group_name']?.toString();
	}
	if (json['pretty_name'] != null) {
		data.prettyName = json['pretty_name']?.toString();
	}
	if (json['pretty_nick_name'] != null) {
		data.prettyNickName = json['pretty_nick_name']?.toString();
	}
	if (json['venue_id'] != null) {
		data.venueId = json['venue_id']?.toInt();
	}
	if (json['venue_name'] != null) {
		data.venueName = json['venue_name']?.toString();
	}
	if (json['lat'] != null) {
		data.lat = json['lat']?.toDouble();
	}
	if (json['long'] != null) {
		data.long = json['long']?.toDouble();
	}
	if (json['price'] != null) {
		data.price = json['price']?.toInt();
	}
	if (json['discount'] != null) {
		data.discount = json['discount']?.toInt();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toInt();
	}
	if (json['job_start'] != null) {
		data.jobStart = json['job_start']?.toString();
	}
	if (json['job_end'] != null) {
		data.jobEnd = json['job_end']?.toString();
	}
	if (json['created_at'] != null) {
		data.createdAt = json['created_at']?.toString();
	}
	if (json['updated_at'] != null) {
		data.updatedAt = json['updated_at']?.toString();
	}
	return data;
}

Map<String, dynamic> jobsDataRecordBookingToJson(JobsDataRecordBooking entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['order_id'] = entity.orderId;
	data['store_id'] = entity.storeId;
	data['pretty_id'] = entity.prettyId;
	data['pretty_code'] = entity.prettyCode;
	data['pretty_group_name'] = entity.prettyGroupName;
	data['pretty_name'] = entity.prettyName;
	data['pretty_nick_name'] = entity.prettyNickName;
	data['venue_id'] = entity.venueId;
	data['venue_name'] = entity.venueName;
	data['lat'] = entity.lat;
	data['long'] = entity.long;
	data['price'] = entity.price;
	data['discount'] = entity.discount;
	data['total'] = entity.total;
	data['job_start'] = entity.jobStart;
	data['job_end'] = entity.jobEnd;
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	return data;
}

jobsDataRecordStatusFromJson(JobsDataRecordStatus data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['text'] != null) {
		data.text = json['text']?.toString();
	}
	if (json['job_text'] != null) {
		data.jobText = json['job_text']?.toString();
	}
	return data;
}

Map<String, dynamic> jobsDataRecordStatusToJson(JobsDataRecordStatus entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['text'] = entity.text;
	data['job_text'] = entity.jobText;
	return data;
}

jobsBenchFromJson(JobsBench data, Map<String, dynamic> json) {
	if (json['second'] != null) {
		data.second = json['second']?.toInt();
	}
	if (json['millisecond'] != null) {
		data.millisecond = json['millisecond']?.toDouble();
	}
	if (json['format'] != null) {
		data.format = json['format']?.toString();
	}
	return data;
}

Map<String, dynamic> jobsBenchToJson(JobsBench entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}