import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_update_request_entity.dart';

userProfileUpdateRequestEntityFromJson(UserProfileUpdateRequestEntity data, Map<String, dynamic> json) {
	if (json['first_name'] != null) {
		data.firstName = json['first_name']?.toString();
	}
	if (json['last_name'] != null) {
		data.lastName = json['last_name']?.toString();
	}
	if (json['idcard'] != null) {
		data.idcard = json['idcard']?.toString();
	}
	if (json['display_name'] != null) {
		data.displayName = json['display_name']?.toString();
	}
	if (json['gender'] != null) {
		data.gender = json['gender']?.toString();
	}
	if (json['birthday'] != null) {
		data.birthday = json['birthday']?.toString();
	}
	if (json['terms_accepted'] != null) {
		data.termsAccepted = json['terms_accepted'];
	}
	return data;
}

Map<String, dynamic> userProfileUpdateRequestEntityToJson(UserProfileUpdateRequestEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['first_name'] = entity.firstName;
	data['last_name'] = entity.lastName;
	data['idcard'] = entity.idcard;
	data['display_name'] = entity.displayName;
	data['gender'] = entity.gender;
	data['birthday'] = entity.birthday;
	data['terms_accepted'] = entity.termsAccepted;
	return data;
}