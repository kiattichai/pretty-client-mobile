import 'package:pretty_client_mobile/modules/profile_module/models/user_profile_response_entity.dart';

userProfileResponseEntityFromJson(UserProfileResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new UserProfileResponseData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> userProfileResponseEntityToJson(UserProfileResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	return data;
}

userProfileResponseDataFromJson(UserProfileResponseData data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['username'] != null) {
		data.username = json['username']?.toString();
	}
	if (json['first_name'] != null) {
		data.firstName = json['first_name']?.toString();
	}
	if (json['last_name'] != null) {
		data.lastName = json['last_name']?.toString();
	}
	if (json['idcard'] != null) {
		data.idcard = json['idcard']?.toString();
	}
	if (json['display_name'] != null) {
		data.displayName = json['display_name']?.toString();
	}
	if (json['avatar'] != null) {
		data.avatar = new UserProfileAvatar().fromJson(json['avatar']);
	}
	if (json['gender'] != null) {
		data.gender = json['gender']?.toString();
	}
	if (json['birthday'] != null) {
		data.birthday = json['birthday']?.toString();
	}
	if (json['terms_accepted'] != null) {
		data.termsAccepted = json['terms_accepted'];
	}
	if (json['activated'] != null) {
		data.activated = json['activated'];
	}
	if (json['activated_at'] != null) {
		data.activatedAt = json['activated_at']?.toString();
	}
	if (json['blocked'] != null) {
		data.blocked = json['blocked'];
	}
	if (json['profile_score'] != null) {
		data.profileScore = json['profile_score']?.toInt();
	}
	if (json['last_login_at'] != null) {
		data.lastLoginAt = json['last_login_at']?.toString();
	}
	if (json['last_login_ip'] != null) {
		data.lastLoginIp = json['last_login_ip']?.toString();
	}
	if (json['created_at'] != null) {
		data.createdAt = json['created_at']?.toString();
	}
	if (json['updated_at'] != null) {
		data.updatedAt = json['updated_at']?.toString();
	}
	if (json['referral'] != null) {
		data.referral = json['referral']?.toString();
	}
	if (json['lang'] != null) {
		data.lang = json['lang']?.toString();
	}
	if (json['group'] != null) {
		data.group = new UserProfileGroup().fromJson(json['group']);
	}
	if (json['agent'] != null) {
		data.agent = new UserProfileAgent().fromJson(json['agent']);
	}
	if (json['wallet'] != null) {
		data.wallet = json['wallet']?.toDouble();
	}
	return data;
}

Map<String, dynamic> userProfileResponseDataToJson(UserProfileResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['username'] = entity.username;
	data['first_name'] = entity.firstName;
	data['last_name'] = entity.lastName;
	data['idcard'] = entity.idcard;
	data['display_name'] = entity.displayName;
	if (entity.avatar != null) {
		data['avatar'] = entity.avatar.toJson();
	}
	data['gender'] = entity.gender;
	data['birthday'] = entity.birthday;
	data['terms_accepted'] = entity.termsAccepted;
	data['activated'] = entity.activated;
	data['activated_at'] = entity.activatedAt;
	data['blocked'] = entity.blocked;
	data['profile_score'] = entity.profileScore;
	data['last_login_at'] = entity.lastLoginAt;
	data['last_login_ip'] = entity.lastLoginIp;
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	data['referral'] = entity.referral;
	data['lang'] = entity.lang;
	if (entity.group != null) {
		data['group'] = entity.group.toJson();
	}
	if (entity.agent != null) {
		data['agent'] = entity.agent.toJson();
	}
	data['wallet'] = entity.wallet;
	return data;
}

userProfileAvatarFromJson(UserProfileAvatar data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['resize_url'] != null) {
		data.resizeUrl = json['resize_url']?.toString();
	}
	return data;
}

Map<String, dynamic> userProfileAvatarToJson(UserProfileAvatar entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['resize_url'] = entity.resizeUrl;
	return data;
}

userProfileWalletFromJson(UserProfileWallet data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['amount'] != null) {
		data.amount = json['amount']?.toDouble();
	}
	return data;
}

Map<String, dynamic> userProfileWalletToJson(UserProfileWallet entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['amount'] = entity.amount;
	return data;
}

userProfileGroupFromJson(UserProfileGroup data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	return data;
}

Map<String, dynamic> userProfileGroupToJson(UserProfileGroup entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

userProfileAgentFromJson(UserProfileAgent data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	return data;
}

Map<String, dynamic> userProfileAgentToJson(UserProfileAgent entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	return data;
}