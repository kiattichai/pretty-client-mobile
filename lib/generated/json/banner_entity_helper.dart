import 'package:pretty_client_mobile/modules/home_tab_module/models/banner_entity.dart';

bannerEntityFromJson(BannerEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new BannerData().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new BannerBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> bannerEntityToJson(BannerEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

bannerDataFromJson(BannerData data, Map<String, dynamic> json) {
	if (json['pagination'] != null) {
		data.pagination = new BannerDataPagination().fromJson(json['pagination']);
	}
	if (json['record'] != null) {
		data.record = new List<BannerDataRecord>();
		(json['record'] as List).forEach((v) {
			data.record.add(new BannerDataRecord().fromJson(v));
		});
	}
	if (json['cache'] != null) {
		data.cache = json['cache'];
	}
	return data;
}

Map<String, dynamic> bannerDataToJson(BannerData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.pagination != null) {
		data['pagination'] = entity.pagination.toJson();
	}
	if (entity.record != null) {
		data['record'] =  entity.record.map((v) => v.toJson()).toList();
	}
	data['cache'] = entity.cache;
	return data;
}

bannerDataPaginationFromJson(BannerDataPagination data, Map<String, dynamic> json) {
	if (json['current_page'] != null) {
		data.currentPage = json['current_page']?.toInt();
	}
	if (json['last_page'] != null) {
		data.lastPage = json['last_page']?.toInt();
	}
	if (json['limit'] != null) {
		data.limit = json['limit']?.toInt();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toInt();
	}
	return data;
}

Map<String, dynamic> bannerDataPaginationToJson(BannerDataPagination entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['current_page'] = entity.currentPage;
	data['last_page'] = entity.lastPage;
	data['limit'] = entity.limit;
	data['total'] = entity.total;
	return data;
}

bannerDataRecordFromJson(BannerDataRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['slug'] != null) {
		data.slug = json['slug'];
	}
	if (json['group'] != null) {
		data.group = new BannerDataRecordGroup().fromJson(json['group']);
	}
	if (json['category'] != null) {
		data.category = new BannerDataRecordCategory().fromJson(json['category']);
	}
	if (json['fields'] != null) {
		data.fields = new List<BannerDataRecordField>();
		(json['fields'] as List).forEach((v) {
			data.fields.add(new BannerDataRecordField().fromJson(v));
		});
	}
	if (json['images'] != null) {
		data.images = new List<BannerDataRecordImage>();
		(json['images'] as List).forEach((v) {
			data.images.add(new BannerDataRecordImage().fromJson(v));
		});
	}
	if (json['viewed'] != null) {
		data.viewed = json['viewed']?.toInt();
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['share_url'] != null) {
		data.shareUrl = json['share_url']?.toString();
	}
	if (json['webview_url'] != null) {
		data.webviewUrl = json['webview_url']?.toString();
	}
	if (json['published_at'] != null) {
		data.publishedAt = json['published_at']?.toString();
	}
	if (json['created_at'] != null) {
		data.createdAt = json['created_at']?.toString();
	}
	if (json['updated_at'] != null) {
		data.updatedAt = json['updated_at']?.toString();
	}
	return data;
}

Map<String, dynamic> bannerDataRecordToJson(BannerDataRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['slug'] = entity.slug;
	if (entity.group != null) {
		data['group'] = entity.group.toJson();
	}
	if (entity.category != null) {
		data['category'] = entity.category.toJson();
	}
	if (entity.fields != null) {
		data['fields'] =  entity.fields.map((v) => v.toJson()).toList();
	}
	if (entity.images != null) {
		data['images'] =  entity.images.map((v) => v.toJson()).toList();
	}
	data['viewed'] = entity.viewed;
	data['status'] = entity.status;
	data['share_url'] = entity.shareUrl;
	data['webview_url'] = entity.webviewUrl;
	data['published_at'] = entity.publishedAt;
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	return data;
}

bannerDataRecordGroupFromJson(BannerDataRecordGroup data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['description'] != null) {
		data.description = json['description']?.toString();
	}
	return data;
}

Map<String, dynamic> bannerDataRecordGroupToJson(BannerDataRecordGroup entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['name'] = entity.name;
	data['description'] = entity.description;
	return data;
}

bannerDataRecordCategoryFromJson(BannerDataRecordCategory data, Map<String, dynamic> json) {
	if (json['current'] != null) {
		data.current = new BannerDataRecordCategoryCurrent().fromJson(json['current']);
	}
	if (json['items'] != null) {
		data.items = new List<BannerDataRecordCategoryItem>();
		(json['items'] as List).forEach((v) {
			data.items.add(new BannerDataRecordCategoryItem().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> bannerDataRecordCategoryToJson(BannerDataRecordCategory entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.current != null) {
		data['current'] = entity.current.toJson();
	}
	if (entity.items != null) {
		data['items'] =  entity.items.map((v) => v.toJson()).toList();
	}
	return data;
}

bannerDataRecordCategoryCurrentFromJson(BannerDataRecordCategoryCurrent data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['description'] != null) {
		data.description = json['description'];
	}
	if (json['meta_title'] != null) {
		data.metaTitle = json['meta_title'];
	}
	if (json['meta_description'] != null) {
		data.metaDescription = json['meta_description'];
	}
	if (json['meta_keyword'] != null) {
		data.metaKeyword = json['meta_keyword'];
	}
	return data;
}

Map<String, dynamic> bannerDataRecordCategoryCurrentToJson(BannerDataRecordCategoryCurrent entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['status'] = entity.status;
	data['position'] = entity.position;
	data['name'] = entity.name;
	data['description'] = entity.description;
	data['meta_title'] = entity.metaTitle;
	data['meta_description'] = entity.metaDescription;
	data['meta_keyword'] = entity.metaKeyword;
	return data;
}

bannerDataRecordCategoryItemFromJson(BannerDataRecordCategoryItem data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	return data;
}

Map<String, dynamic> bannerDataRecordCategoryItemToJson(BannerDataRecordCategoryItem entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['status'] = entity.status;
	data['position'] = entity.position;
	data['name'] = entity.name;
	return data;
}

bannerDataRecordFieldFromJson(BannerDataRecordField data, Map<String, dynamic> json) {
	if (json['label'] != null) {
		data.label = json['label']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['type'] != null) {
		data.type = new BannerDataRecordFieldsType().fromJson(json['type']);
	}
	if (json['has_lang'] != null) {
		data.hasLang = json['has_lang'];
	}
	if (json['require'] != null) {
		data.require = json['require'];
	}
	if (json['options'] != null) {
		data.options = json['options'];
	}
	if (json['lang'] != null) {
		data.lang = json['lang']?.toString();
	}
	return data;
}

Map<String, dynamic> bannerDataRecordFieldToJson(BannerDataRecordField entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['label'] = entity.label;
	data['name'] = entity.name;
	if (entity.type != null) {
		data['type'] = entity.type.toJson();
	}
	data['has_lang'] = entity.hasLang;
	data['require'] = entity.require;
	data['options'] = entity.options;
	data['lang'] = entity.lang;
	return data;
}

bannerDataRecordFieldsTypeFromJson(BannerDataRecordFieldsType data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['value'] != null) {
		data.value = json['value']?.toString();
	}
	return data;
}

Map<String, dynamic> bannerDataRecordFieldsTypeToJson(BannerDataRecordFieldsType entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['value'] = entity.value;
	return data;
}

bannerDataRecordImageFromJson(BannerDataRecordImage data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['tag'] != null) {
		data.tag = json['tag']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	return data;
}

Map<String, dynamic> bannerDataRecordImageToJson(BannerDataRecordImage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['tag'] = entity.tag;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['position'] = entity.position;
	return data;
}

bannerBenchFromJson(BannerBench data, Map<String, dynamic> json) {
	if (json['second'] != null) {
		data.second = json['second']?.toInt();
	}
	if (json['millisecond'] != null) {
		data.millisecond = json['millisecond']?.toDouble();
	}
	if (json['format'] != null) {
		data.format = json['format']?.toString();
	}
	return data;
}

Map<String, dynamic> bannerBenchToJson(BannerBench entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}