import 'package:pretty_client_mobile/modules/profile_module/models/avatar_response_entity.dart';

avatarResponseEntityFromJson(AvatarResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new AvatarResponseData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> avatarResponseEntityToJson(AvatarResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	return data;
}

avatarResponseDataFromJson(AvatarResponseData data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['resize_url'] != null) {
		data.resizeUrl = json['resize_url']?.toString();
	}
	return data;
}

Map<String, dynamic> avatarResponseDataToJson(AvatarResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['resize_url'] = entity.resizeUrl;
	return data;
}