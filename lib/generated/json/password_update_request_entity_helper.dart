import 'package:pretty_client_mobile/modules/profile_module/models/password_update_request_entity.dart';

passwordUpdateRequestEntityFromJson(PasswordUpdateRequestEntity data, Map<String, dynamic> json) {
	if (json['password_old'] != null) {
		data.passwordOld = json['password_old']?.toString();
	}
	if (json['password'] != null) {
		data.password = json['password']?.toString();
	}
	if (json['password_confirmation'] != null) {
		data.passwordConfirmation = json['password_confirmation']?.toString();
	}
	return data;
}

Map<String, dynamic> passwordUpdateRequestEntityToJson(PasswordUpdateRequestEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['password_old'] = entity.passwordOld;
	data['password'] = entity.password;
	data['password_confirmation'] = entity.passwordConfirmation;
	return data;
}