import 'package:pretty_client_mobile/modules/notification_module/models/notification_response_entity.dart';

notificationResponseEntityFromJson(NotificationResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new NotificationResponseData().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new NotificationResponseBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> notificationResponseEntityToJson(NotificationResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

notificationResponseDataFromJson(NotificationResponseData data, Map<String, dynamic> json) {
	if (json['pagination'] != null) {
		data.pagination = new NotificationResponseDataPagination().fromJson(json['pagination']);
	}
	if (json['record'] != null) {
		data.record = new List<NotificationRecord>();
		(json['record'] as List).forEach((v) {
			data.record.add(new NotificationRecord().fromJson(v));
		});
	}
	if (json['cache'] != null) {
		data.cache = json['cache'];
	}
	return data;
}

Map<String, dynamic> notificationResponseDataToJson(NotificationResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.pagination != null) {
		data['pagination'] = entity.pagination.toJson();
	}
	if (entity.record != null) {
		data['record'] =  entity.record.map((v) => v.toJson()).toList();
	}
	data['cache'] = entity.cache;
	return data;
}

notificationResponseDataPaginationFromJson(NotificationResponseDataPagination data, Map<String, dynamic> json) {
	if (json['current_page'] != null) {
		data.currentPage = json['current_page']?.toInt();
	}
	if (json['last_page'] != null) {
		data.lastPage = json['last_page']?.toInt();
	}
	if (json['limit'] != null) {
		data.limit = json['limit']?.toInt();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toInt();
	}
	return data;
}

Map<String, dynamic> notificationResponseDataPaginationToJson(NotificationResponseDataPagination entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['current_page'] = entity.currentPage;
	data['last_page'] = entity.lastPage;
	data['limit'] = entity.limit;
	data['total'] = entity.total;
	return data;
}

notificationRecordFromJson(NotificationRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['type'] != null) {
		data.type = new NotificationType().fromJson(json['type']);
	}
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['description'] != null) {
		data.description = json['description']?.toString();
	}
	if (json['user'] != null) {
		data.user = new NotificationUser().fromJson(json['user']);
	}
	if (json['read'] != null) {
		data.read = json['read'];
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['ref_path'] != null) {
		data.refPath = json['ref_path']?.toString();
	}
	if (json['ref_code'] != null) {
		data.refCode = json['ref_code']?.toString();
	}
	if (json['images'] != null) {
		data.images = new List<NotificationImage>();
		(json['images'] as List).forEach((v) {
			data.images.add(new NotificationImage().fromJson(v));
		});
	}
	if (json['created_at'] != null) {
		data.createdAt = new NotificationResponseDataRecordCreatedAt().fromJson(json['created_at']);
	}
	if (json['updated_at'] != null) {
		data.updatedAt = new NotificationResponseDataRecordUpdatedAt().fromJson(json['updated_at']);
	}
	if (json['meta'] != null) {
		data.meta = json['meta'];
	}
	return data;
}

Map<String, dynamic> notificationRecordToJson(NotificationRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	if (entity.type != null) {
		data['type'] = entity.type.toJson();
	}
	data['title'] = entity.title;
	data['description'] = entity.description;
	if (entity.user != null) {
		data['user'] = entity.user.toJson();
	}
	data['read'] = entity.read;
	data['status'] = entity.status;
	data['ref_path'] = entity.refPath;
	data['ref_code'] = entity.refCode;
	if (entity.images != null) {
		data['images'] =  entity.images.map((v) => v.toJson()).toList();
	}
	if (entity.createdAt != null) {
		data['created_at'] = entity.createdAt.toJson();
	}
	if (entity.updatedAt != null) {
		data['updated_at'] = entity.updatedAt.toJson();
	}
	data['meta'] = entity.meta;
	return data;
}

notificationTypeFromJson(NotificationType data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['text'] != null) {
		data.text = json['text']?.toString();
	}
	return data;
}

Map<String, dynamic> notificationTypeToJson(NotificationType entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['text'] = entity.text;
	return data;
}

notificationUserFromJson(NotificationUser data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['username'] != null) {
		data.username = json['username']?.toString();
	}
	if (json['country_code'] != null) {
		data.countryCode = json['country_code']?.toString();
	}
	if (json['first_name'] != null) {
		data.firstName = json['first_name']?.toString();
	}
	if (json['last_name'] != null) {
		data.lastName = json['last_name']?.toString();
	}
	return data;
}

Map<String, dynamic> notificationUserToJson(NotificationUser entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['username'] = entity.username;
	data['country_code'] = entity.countryCode;
	data['first_name'] = entity.firstName;
	data['last_name'] = entity.lastName;
	return data;
}

notificationImageFromJson(NotificationImage data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	return data;
}

Map<String, dynamic> notificationImageToJson(NotificationImage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	return data;
}

notificationResponseDataRecordCreatedAtFromJson(NotificationResponseDataRecordCreatedAt data, Map<String, dynamic> json) {
	if (json['value'] != null) {
		data.value = json['value']?.toString();
	}
	if (json['date'] != null) {
		data.date = json['date']?.toString();
	}
	if (json['time'] != null) {
		data.time = json['time']?.toString();
	}
	return data;
}

Map<String, dynamic> notificationResponseDataRecordCreatedAtToJson(NotificationResponseDataRecordCreatedAt entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['value'] = entity.value;
	data['date'] = entity.date;
	data['time'] = entity.time;
	return data;
}

notificationResponseDataRecordUpdatedAtFromJson(NotificationResponseDataRecordUpdatedAt data, Map<String, dynamic> json) {
	if (json['value'] != null) {
		data.value = json['value']?.toString();
	}
	if (json['date'] != null) {
		data.date = json['date']?.toString();
	}
	if (json['time'] != null) {
		data.time = json['time']?.toString();
	}
	return data;
}

Map<String, dynamic> notificationResponseDataRecordUpdatedAtToJson(NotificationResponseDataRecordUpdatedAt entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['value'] = entity.value;
	data['date'] = entity.date;
	data['time'] = entity.time;
	return data;
}

notificationResponseBenchFromJson(NotificationResponseBench data, Map<String, dynamic> json) {
	if (json['second'] != null) {
		data.second = json['second']?.toInt();
	}
	if (json['millisecond'] != null) {
		data.millisecond = json['millisecond']?.toDouble();
	}
	if (json['format'] != null) {
		data.format = json['format']?.toString();
	}
	return data;
}

Map<String, dynamic> notificationResponseBenchToJson(NotificationResponseBench entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}