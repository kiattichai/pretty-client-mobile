import 'package:pretty_client_mobile/modules/login_module/entitiies/authen_entity.dart';

authenEntityFromJson(AuthenEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new AuthenData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> authenEntityToJson(AuthenEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	return data;
}

authenDataFromJson(AuthenData data, Map<String, dynamic> json) {
	if (json['token_type'] != null) {
		data.tokenType = json['token_type']?.toString();
	}
	if (json['access_token'] != null) {
		data.accessToken = json['access_token']?.toString();
	}
	if (json['refresh_token'] != null) {
		data.refreshToken = json['refresh_token']?.toString();
	}
	return data;
}

Map<String, dynamic> authenDataToJson(AuthenData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['token_type'] = entity.tokenType;
	data['access_token'] = entity.accessToken;
	data['refresh_token'] = entity.refreshToken;
	return data;
}