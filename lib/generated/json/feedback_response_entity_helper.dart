import 'package:pretty_client_mobile/modules/feedback_module/models/feedback_response_entity.dart';

feedbackResponseEntityFromJson(FeedbackResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new FeedbackResponseData().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new FeedbackResponseBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> feedbackResponseEntityToJson(FeedbackResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

feedbackResponseDataFromJson(FeedbackResponseData data, Map<String, dynamic> json) {
	if (json['pagination'] != null) {
		data.pagination = new FeedbackResponseDataPagination().fromJson(json['pagination']);
	}
	if (json['record'] != null) {
		data.record = new List<FeedbackResponseDataRecord>();
		(json['record'] as List).forEach((v) {
			data.record.add(new FeedbackResponseDataRecord().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> feedbackResponseDataToJson(FeedbackResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.pagination != null) {
		data['pagination'] = entity.pagination.toJson();
	}
	if (entity.record != null) {
		data['record'] =  entity.record.map((v) => v.toJson()).toList();
	}
	return data;
}

feedbackResponseDataPaginationFromJson(FeedbackResponseDataPagination data, Map<String, dynamic> json) {
	if (json['current_page'] != null) {
		data.currentPage = json['current_page']?.toInt();
	}
	if (json['last_page'] != null) {
		data.lastPage = json['last_page']?.toInt();
	}
	if (json['limit'] != null) {
		data.limit = json['limit']?.toInt();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toInt();
	}
	return data;
}

Map<String, dynamic> feedbackResponseDataPaginationToJson(FeedbackResponseDataPagination entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['current_page'] = entity.currentPage;
	data['last_page'] = entity.lastPage;
	data['limit'] = entity.limit;
	data['total'] = entity.total;
	return data;
}

feedbackResponseDataRecordFromJson(FeedbackResponseDataRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['description'] != null) {
		data.description = json['description'];
	}
	if (json['ip'] != null) {
		data.ip = json['ip'];
	}
	if (json['reason_id'] != null) {
		data.reasonId = json['reason_id']?.toInt();
	}
	if (json['score'] != null) {
		data.score = json['score']?.toInt();
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['user'] != null) {
		data.user = new FeedbackResponseDataRecordUser().fromJson(json['user']);
	}
	if (json['updated_by'] != null) {
		data.updatedBy = json['updated_by']?.toInt();
	}
	if (json['staff'] != null) {
		data.staff = new FeedbackResponseDataRecordStaff().fromJson(json['staff']);
	}
	if (json['created_at'] != null) {
		data.createdAt = new FeedbackResponseDataRecordCreatedAt().fromJson(json['created_at']);
	}
	if (json['updated_at'] != null) {
		data.updatedAt = new FeedbackResponseDataRecordUpdatedAt().fromJson(json['updated_at']);
	}
	return data;
}

Map<String, dynamic> feedbackResponseDataRecordToJson(FeedbackResponseDataRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['description'] = entity.description;
	data['ip'] = entity.ip;
	data['reason_id'] = entity.reasonId;
	data['score'] = entity.score;
	data['status'] = entity.status;
	if (entity.user != null) {
		data['user'] = entity.user.toJson();
	}
	data['updated_by'] = entity.updatedBy;
	if (entity.staff != null) {
		data['staff'] = entity.staff.toJson();
	}
	if (entity.createdAt != null) {
		data['created_at'] = entity.createdAt.toJson();
	}
	if (entity.updatedAt != null) {
		data['updated_at'] = entity.updatedAt.toJson();
	}
	return data;
}

feedbackResponseDataRecordUserFromJson(FeedbackResponseDataRecordUser data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['username'] != null) {
		data.username = json['username']?.toString();
	}
	if (json['country_code'] != null) {
		data.countryCode = json['country_code']?.toString();
	}
	if (json['gender'] != null) {
		data.gender = json['gender']?.toString();
	}
	if (json['first_name'] != null) {
		data.firstName = json['first_name']?.toString();
	}
	if (json['last_name'] != null) {
		data.lastName = json['last_name']?.toString();
	}
	if (json['avatar'] != null) {
		data.avatar = new FeedbackResponseDataRecordUserAvatar().fromJson(json['avatar']);
	}
	return data;
}

Map<String, dynamic> feedbackResponseDataRecordUserToJson(FeedbackResponseDataRecordUser entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['username'] = entity.username;
	data['country_code'] = entity.countryCode;
	data['gender'] = entity.gender;
	data['first_name'] = entity.firstName;
	data['last_name'] = entity.lastName;
	if (entity.avatar != null) {
		data['avatar'] = entity.avatar.toJson();
	}
	return data;
}

feedbackResponseDataRecordUserAvatarFromJson(FeedbackResponseDataRecordUserAvatar data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['resize_url'] != null) {
		data.resizeUrl = json['resize_url']?.toString();
	}
	return data;
}

Map<String, dynamic> feedbackResponseDataRecordUserAvatarToJson(FeedbackResponseDataRecordUserAvatar entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['resize_url'] = entity.resizeUrl;
	return data;
}

feedbackResponseDataRecordStaffFromJson(FeedbackResponseDataRecordStaff data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['username'] != null) {
		data.username = json['username']?.toString();
	}
	if (json['first_name'] != null) {
		data.firstName = json['first_name']?.toString();
	}
	if (json['last_name'] != null) {
		data.lastName = json['last_name']?.toString();
	}
	return data;
}

Map<String, dynamic> feedbackResponseDataRecordStaffToJson(FeedbackResponseDataRecordStaff entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['username'] = entity.username;
	data['first_name'] = entity.firstName;
	data['last_name'] = entity.lastName;
	return data;
}

feedbackResponseDataRecordCreatedAtFromJson(FeedbackResponseDataRecordCreatedAt data, Map<String, dynamic> json) {
	if (json['value'] != null) {
		data.value = json['value']?.toString();
	}
	if (json['date'] != null) {
		data.date = json['date']?.toString();
	}
	if (json['time'] != null) {
		data.time = json['time']?.toString();
	}
	return data;
}

Map<String, dynamic> feedbackResponseDataRecordCreatedAtToJson(FeedbackResponseDataRecordCreatedAt entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['value'] = entity.value;
	data['date'] = entity.date;
	data['time'] = entity.time;
	return data;
}

feedbackResponseDataRecordUpdatedAtFromJson(FeedbackResponseDataRecordUpdatedAt data, Map<String, dynamic> json) {
	if (json['value'] != null) {
		data.value = json['value']?.toString();
	}
	if (json['date'] != null) {
		data.date = json['date']?.toString();
	}
	if (json['time'] != null) {
		data.time = json['time']?.toString();
	}
	return data;
}

Map<String, dynamic> feedbackResponseDataRecordUpdatedAtToJson(FeedbackResponseDataRecordUpdatedAt entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['value'] = entity.value;
	data['date'] = entity.date;
	data['time'] = entity.time;
	return data;
}

feedbackResponseBenchFromJson(FeedbackResponseBench data, Map<String, dynamic> json) {
	if (json['second'] != null) {
		data.second = json['second']?.toInt();
	}
	if (json['millisecond'] != null) {
		data.millisecond = json['millisecond']?.toDouble();
	}
	if (json['format'] != null) {
		data.format = json['format']?.toString();
	}
	return data;
}

Map<String, dynamic> feedbackResponseBenchToJson(FeedbackResponseBench entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}