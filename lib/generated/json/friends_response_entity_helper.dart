import 'package:pretty_client_mobile/modules/friends_module/models/friends_response_entity.dart';

friendsResponseEntityFromJson(FriendsResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new FriendsResponseData().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new FriendsResponseBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> friendsResponseEntityToJson(FriendsResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

friendsDetailResponseEntityFromJson(FriendsDetailResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new Friend().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new FriendsResponseBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> friendsDetailResponseEntityToJson(FriendsDetailResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

friendsResponseDataFromJson(FriendsResponseData data, Map<String, dynamic> json) {
	if (json['pagination'] != null) {
		data.pagination = new FriendsPagination().fromJson(json['pagination']);
	}
	if (json['record'] != null) {
		data.record = new List<Friend>();
		(json['record'] as List).forEach((v) {
			data.record.add(new Friend().fromJson(v));
		});
	}
	if (json['cache'] != null) {
		data.cache = json['cache'];
	}
	return data;
}

Map<String, dynamic> friendsResponseDataToJson(FriendsResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.pagination != null) {
		data['pagination'] = entity.pagination.toJson();
	}
	if (entity.record != null) {
		data['record'] =  entity.record.map((v) => v.toJson()).toList();
	}
	data['cache'] = entity.cache;
	return data;
}

friendsPaginationFromJson(FriendsPagination data, Map<String, dynamic> json) {
	if (json['current_page'] != null) {
		data.currentPage = json['current_page']?.toInt();
	}
	if (json['last_page'] != null) {
		data.lastPage = json['last_page']?.toInt();
	}
	if (json['limit'] != null) {
		data.limit = json['limit']?.toInt();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toInt();
	}
	return data;
}

Map<String, dynamic> friendsPaginationToJson(FriendsPagination entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['current_page'] = entity.currentPage;
	data['last_page'] = entity.lastPage;
	data['limit'] = entity.limit;
	data['total'] = entity.total;
	return data;
}

friendFromJson(Friend data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['group'] != null) {
		data.group = new FriendsGroup().fromJson(json['group']);
	}
	if (json['first_name'] != null) {
		data.firstName = json['first_name']?.toString();
	}
	if (json['last_name'] != null) {
		data.lastName = json['last_name']?.toString();
	}
	if (json['nick_name'] != null) {
		data.nickName = json['nick_name']?.toString();
	}
	if (json['birthday'] != null) {
		data.birthday = json['birthday']?.toString();
	}
	if (json['age'] != null) {
		data.age = json['age']?.toInt();
	}
	if (json['gender'] != null) {
		data.gender = json['gender']?.toString();
	}
	if (json['weight'] != null) {
		data.weight = json['weight']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['chest'] != null) {
		data.chest = json['chest']?.toInt();
	}
	if (json['waistline'] != null) {
		data.waistline = json['waistline']?.toInt();
	}
	if (json['hip'] != null) {
		data.hip = json['hip']?.toInt();
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['avatar'] != null) {
		data.avatar = new FriendsAvatar().fromJson(json['avatar']);
	}
	if (json['images'] != null) {
		data.images = new List<FriendsImage>();
		(json['images'] as List).forEach((v) {
			data.images.add(new FriendsImage().fromJson(v));
		});
	}
	if (json['created_at'] != null) {
		data.createdAt = json['created_at']?.toString();
	}
	if (json['updated_at'] != null) {
		data.updatedAt = json['updated_at']?.toString();
	}
	if (json['price'] != null) {
		data.price = json['price']?.toInt();
	}
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['onfloor'] != null) {
		data.onfloor = json['onfloor'];
	}
	return data;
}

Map<String, dynamic> friendToJson(Friend entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	if (entity.group != null) {
		data['group'] = entity.group.toJson();
	}
	data['first_name'] = entity.firstName;
	data['last_name'] = entity.lastName;
	data['nick_name'] = entity.nickName;
	data['birthday'] = entity.birthday;
	data['age'] = entity.age;
	data['gender'] = entity.gender;
	data['weight'] = entity.weight;
	data['height'] = entity.height;
	data['chest'] = entity.chest;
	data['waistline'] = entity.waistline;
	data['hip'] = entity.hip;
	data['status'] = entity.status;
	if (entity.avatar != null) {
		data['avatar'] = entity.avatar.toJson();
	}
	if (entity.images != null) {
		data['images'] =  entity.images.map((v) => v.toJson()).toList();
	}
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	data['price'] = entity.price;
	data['code'] = entity.code;
	data['onfloor'] = entity.onfloor;
	return data;
}

friendsGroupFromJson(FriendsGroup data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	return data;
}

Map<String, dynamic> friendsGroupToJson(FriendsGroup entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

friendsAvatarFromJson(FriendsAvatar data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['tag'] != null) {
		data.tag = json['tag'];
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	return data;
}

Map<String, dynamic> friendsAvatarToJson(FriendsAvatar entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['tag'] = entity.tag;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['position'] = entity.position;
	return data;
}

friendsImageFromJson(FriendsImage data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['tag'] != null) {
		data.tag = json['tag'];
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	return data;
}

Map<String, dynamic> friendsImageToJson(FriendsImage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['tag'] = entity.tag;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['position'] = entity.position;
	return data;
}

friendsResponseBenchFromJson(FriendsResponseBench data, Map<String, dynamic> json) {
	if (json['second'] != null) {
		data.second = json['second']?.toInt();
	}
	if (json['millisecond'] != null) {
		data.millisecond = json['millisecond']?.toDouble();
	}
	if (json['format'] != null) {
		data.format = json['format']?.toString();
	}
	return data;
}

Map<String, dynamic> friendsResponseBenchToJson(FriendsResponseBench entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}