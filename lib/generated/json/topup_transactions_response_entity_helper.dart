import 'package:pretty_client_mobile/modules/coins_module/models/topup_transactions_response_entity.dart';

topupTransactionsResponseEntityFromJson(TopupTransactionsResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new TopupTransactionsResponseData().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new TopupTransactionsResponseBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> topupTransactionsResponseEntityToJson(TopupTransactionsResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

topupTransactionsResponseDataFromJson(TopupTransactionsResponseData data, Map<String, dynamic> json) {
	if (json['message'] != null) {
		data.message = json['message']?.toString();
	}
	if (json['ref'] != null) {
		data.ref = json['ref']?.toString();
	}
	if (json['file'] != null) {
		data.file = json['file']?.toString();
	}
	return data;
}

Map<String, dynamic> topupTransactionsResponseDataToJson(TopupTransactionsResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['message'] = entity.message;
	data['ref'] = entity.ref;
	data['file'] = entity.file;
	return data;
}

topupTransactionsResponseBenchFromJson(TopupTransactionsResponseBench data, Map<String, dynamic> json) {
	if (json['second'] != null) {
		data.second = json['second']?.toInt();
	}
	if (json['millisecond'] != null) {
		data.millisecond = json['millisecond']?.toDouble();
	}
	if (json['format'] != null) {
		data.format = json['format']?.toString();
	}
	return data;
}

Map<String, dynamic> topupTransactionsResponseBenchToJson(TopupTransactionsResponseBench entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}