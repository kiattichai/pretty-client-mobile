import 'package:pretty_client_mobile/modules/booking_module/models/get_checkout_response_entity.dart';

getCheckoutResponseEntityFromJson(GetCheckoutResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new GetCheckoutResponseData().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new GetCheckoutResponseBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseEntityToJson(GetCheckoutResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

getCheckoutResponseDataFromJson(GetCheckoutResponseData data, Map<String, dynamic> json) {
	if (json['total_cost'] != null) {
		data.totalCost = new GetCheckoutResponseDataTotalCost().fromJson(json['total_cost']);
	}
	if (json['shipping'] != null) {
		data.shipping = new GetCheckoutResponseDataShipping().fromJson(json['shipping']);
	}
	if (json['expired_time'] != null) {
		data.expiredTime = json['expired_time']?.toInt();
	}
	if (json['expired_at'] != null) {
		data.expiredAt = json['expired_at']?.toInt();
	}
	if (json['record'] != null) {
		data.record = new List<GetCheckoutResponseDataRecord>();
		(json['record'] as List).forEach((v) {
			data.record.add(new GetCheckoutResponseDataRecord().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataToJson(GetCheckoutResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.totalCost != null) {
		data['total_cost'] = entity.totalCost.toJson();
	}
	if (entity.shipping != null) {
		data['shipping'] = entity.shipping.toJson();
	}
	data['expired_time'] = entity.expiredTime;
	data['expired_at'] = entity.expiredAt;
	if (entity.record != null) {
		data['record'] =  entity.record.map((v) => v.toJson()).toList();
	}
	return data;
}

getCheckoutResponseDataTotalCostFromJson(GetCheckoutResponseDataTotalCost data, Map<String, dynamic> json) {
	if (json['quantity'] != null) {
		data.quantity = json['quantity']?.toInt();
	}
	if (json['price'] != null) {
		data.price = json['price']?.toDouble();
	}
	if (json['discount'] != null) {
		data.discount = json['discount']?.toDouble();
	}
	if (json['vat'] != null) {
		data.vat = json['vat']?.toDouble();
	}
	if (json['fee'] != null) {
		data.fee = json['fee']?.toDouble();
	}
	if (json['service_fee'] != null) {
		data.serviceFee = json['service_fee']?.toInt();
	}
	if (json['payment_fee'] != null) {
		data.paymentFee = json['payment_fee']?.toDouble();
	}
	if (json['tax'] != null) {
		data.tax = json['tax']?.toDouble();
	}
	if (json['organizer_fee'] != null) {
		data.organizerFee = json['organizer_fee']?.toDouble();
	}
	if (json['organizer_service_fee'] != null) {
		data.organizerServiceFee = json['organizer_service_fee']?.toDouble();
	}
	if (json['organizer_payment_fee'] != null) {
		data.organizerPaymentFee = json['organizer_payment_fee']?.toDouble();
	}
	if (json['organizer_tax'] != null) {
		data.organizerTax = json['organizer_tax']?.toDouble();
	}
	if (json['exclude_tax'] != null) {
		data.excludeTax = json['exclude_tax']?.toDouble();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toDouble();
	}
	if (json['income'] != null) {
		data.income = json['income']?.toDouble();
	}
	if (json['currency_code'] != null) {
		data.currencyCode = json['currency_code']?.toString();
	}
	if (json['currency_symbol'] != null) {
		data.currencySymbol = json['currency_symbol']?.toString();
	}
	if (json['vat_text'] != null) {
		data.vatText = json['vat_text']?.toString();
	}
	if (json['quantity_text'] != null) {
		data.quantityText = json['quantity_text']?.toString();
	}
	if (json['price_text'] != null) {
		data.priceText = json['price_text']?.toString();
	}
	if (json['discount_text'] != null) {
		data.discountText = json['discount_text']?.toString();
	}
	if (json['fee_text'] != null) {
		data.feeText = json['fee_text']?.toString();
	}
	if (json['service_fee_text'] != null) {
		data.serviceFeeText = json['service_fee_text']?.toString();
	}
	if (json['payment_fee_text'] != null) {
		data.paymentFeeText = json['payment_fee_text']?.toString();
	}
	if (json['tax_text'] != null) {
		data.taxText = json['tax_text']?.toString();
	}
	if (json['organizer_fee_text'] != null) {
		data.organizerFeeText = json['organizer_fee_text']?.toString();
	}
	if (json['organizer_service_fee_text'] != null) {
		data.organizerServiceFeeText = json['organizer_service_fee_text']?.toString();
	}
	if (json['organizer_payment_fee_text'] != null) {
		data.organizerPaymentFeeText = json['organizer_payment_fee_text']?.toString();
	}
	if (json['organizer_tax_text'] != null) {
		data.organizerTaxText = json['organizer_tax_text']?.toString();
	}
	if (json['exclude_tax_text'] != null) {
		data.excludeTaxText = json['exclude_tax_text']?.toString();
	}
	if (json['total_text'] != null) {
		data.totalText = json['total_text']?.toString();
	}
	if (json['income_text'] != null) {
		data.incomeText = json['income_text']?.toString();
	}
	if (json['server_time'] != null) {
		data.serverTime = json['server_time']?.toString();
	}
	if (json['payment_channel'] != null) {
		data.paymentChannel = new GetCheckoutResponseDataTotalCostPaymentChannel().fromJson(json['payment_channel']);
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataTotalCostToJson(GetCheckoutResponseDataTotalCost entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['quantity'] = entity.quantity;
	data['price'] = entity.price;
	data['discount'] = entity.discount;
	data['vat'] = entity.vat;
	data['fee'] = entity.fee;
	data['service_fee'] = entity.serviceFee;
	data['payment_fee'] = entity.paymentFee;
	data['tax'] = entity.tax;
	data['organizer_fee'] = entity.organizerFee;
	data['organizer_service_fee'] = entity.organizerServiceFee;
	data['organizer_payment_fee'] = entity.organizerPaymentFee;
	data['organizer_tax'] = entity.organizerTax;
	data['exclude_tax'] = entity.excludeTax;
	data['total'] = entity.total;
	data['income'] = entity.income;
	data['currency_code'] = entity.currencyCode;
	data['currency_symbol'] = entity.currencySymbol;
	data['vat_text'] = entity.vatText;
	data['quantity_text'] = entity.quantityText;
	data['price_text'] = entity.priceText;
	data['discount_text'] = entity.discountText;
	data['fee_text'] = entity.feeText;
	data['service_fee_text'] = entity.serviceFeeText;
	data['payment_fee_text'] = entity.paymentFeeText;
	data['tax_text'] = entity.taxText;
	data['organizer_fee_text'] = entity.organizerFeeText;
	data['organizer_service_fee_text'] = entity.organizerServiceFeeText;
	data['organizer_payment_fee_text'] = entity.organizerPaymentFeeText;
	data['organizer_tax_text'] = entity.organizerTaxText;
	data['exclude_tax_text'] = entity.excludeTaxText;
	data['total_text'] = entity.totalText;
	data['income_text'] = entity.incomeText;
	data['server_time'] = entity.serverTime;
	if (entity.paymentChannel != null) {
		data['payment_channel'] = entity.paymentChannel.toJson();
	}
	return data;
}

getCheckoutResponseDataTotalCostPaymentChannelFromJson(GetCheckoutResponseDataTotalCostPaymentChannel data, Map<String, dynamic> json) {
	if (json['FREE'] != null) {
		data.fREE = new GetCheckoutResponseDataTotalCostPaymentChannelFREE().fromJson(json['FREE']);
	}
	if (json['WALLET'] != null) {
		data.wALLET = new GetCheckoutResponseDataTotalCostPaymentChannelWALLET().fromJson(json['WALLET']);
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataTotalCostPaymentChannelToJson(GetCheckoutResponseDataTotalCostPaymentChannel entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.fREE != null) {
		data['FREE'] = entity.fREE.toJson();
	}
	if (entity.wALLET != null) {
		data['WALLET'] = entity.wALLET.toJson();
	}
	return data;
}

getCheckoutResponseDataTotalCostPaymentChannelFREEFromJson(GetCheckoutResponseDataTotalCostPaymentChannelFREE data, Map<String, dynamic> json) {
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['PRET'] != null) {
		data.pRET = new GetCheckoutResponseDataTotalCostPaymentChannelFREEPRET().fromJson(json['PRET']);
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataTotalCostPaymentChannelFREEToJson(GetCheckoutResponseDataTotalCostPaymentChannelFREE entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['status'] = entity.status;
	if (entity.pRET != null) {
		data['PRET'] = entity.pRET.toJson();
	}
	return data;
}

getCheckoutResponseDataTotalCostPaymentChannelFREEPRETFromJson(GetCheckoutResponseDataTotalCostPaymentChannelFREEPRET data, Map<String, dynamic> json) {
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['expire_time'] != null) {
		data.expireTime = json['expire_time']?.toInt();
	}
	if (json['payment_fee'] != null) {
		data.paymentFee = json['payment_fee'];
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataTotalCostPaymentChannelFREEPRETToJson(GetCheckoutResponseDataTotalCostPaymentChannelFREEPRET entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['status'] = entity.status;
	data['expire_time'] = entity.expireTime;
	data['payment_fee'] = entity.paymentFee;
	return data;
}

getCheckoutResponseDataTotalCostPaymentChannelWALLETFromJson(GetCheckoutResponseDataTotalCostPaymentChannelWALLET data, Map<String, dynamic> json) {
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['PRET'] != null) {
		data.pRET = new GetCheckoutResponseDataTotalCostPaymentChannelWALLETPRET().fromJson(json['PRET']);
	}
	if (json['WIZDOM'] != null) {
		data.wIZDOM = new GetCheckoutResponseDataTotalCostPaymentChannelWALLETWIZDOM().fromJson(json['WIZDOM']);
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataTotalCostPaymentChannelWALLETToJson(GetCheckoutResponseDataTotalCostPaymentChannelWALLET entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['status'] = entity.status;
	if (entity.pRET != null) {
		data['PRET'] = entity.pRET.toJson();
	}
	if (entity.wIZDOM != null) {
		data['WIZDOM'] = entity.wIZDOM.toJson();
	}
	return data;
}

getCheckoutResponseDataTotalCostPaymentChannelWALLETPRETFromJson(GetCheckoutResponseDataTotalCostPaymentChannelWALLETPRET data, Map<String, dynamic> json) {
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['expire_time'] != null) {
		data.expireTime = json['expire_time']?.toInt();
	}
	if (json['payment_fee'] != null) {
		data.paymentFee = json['payment_fee'];
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataTotalCostPaymentChannelWALLETPRETToJson(GetCheckoutResponseDataTotalCostPaymentChannelWALLETPRET entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['status'] = entity.status;
	data['expire_time'] = entity.expireTime;
	data['payment_fee'] = entity.paymentFee;
	return data;
}

getCheckoutResponseDataTotalCostPaymentChannelWALLETWIZDOMFromJson(GetCheckoutResponseDataTotalCostPaymentChannelWALLETWIZDOM data, Map<String, dynamic> json) {
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['expire_time'] != null) {
		data.expireTime = json['expire_time']?.toInt();
	}
	if (json['payment_fee'] != null) {
		data.paymentFee = json['payment_fee'];
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataTotalCostPaymentChannelWALLETWIZDOMToJson(GetCheckoutResponseDataTotalCostPaymentChannelWALLETWIZDOM entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['status'] = entity.status;
	data['expire_time'] = entity.expireTime;
	data['payment_fee'] = entity.paymentFee;
	return data;
}

getCheckoutResponseDataShippingFromJson(GetCheckoutResponseDataShipping data, Map<String, dynamic> json) {
	if (json['address'] != null) {
		data.address = json['address']?.toString();
	}
	if (json['lat'] != null) {
		data.lat = json['lat']?.toDouble();
	}
	if (json['long'] != null) {
		data.long = json['long']?.toDouble();
	}
	if (json['time'] != null) {
		data.time = json['time']?.toString();
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataShippingToJson(GetCheckoutResponseDataShipping entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['address'] = entity.address;
	data['lat'] = entity.lat;
	data['long'] = entity.long;
	data['time'] = entity.time;
	return data;
}

getCheckoutResponseDataRecordFromJson(GetCheckoutResponseDataRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	if (json['option'] != null) {
		data.option = json['option'];
	}
	if (json['price'] != null) {
		data.price = new GetCheckoutResponseDataRecordPrice().fromJson(json['price']);
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['description'] != null) {
		data.description = json['description'];
	}
	if (json['store'] != null) {
		data.store = new GetCheckoutResponseDataRecordStore().fromJson(json['store']);
	}
	if (json['categories'] != null) {
		data.categories = new List<GetCheckoutResponseDataRecordCategory>();
		(json['categories'] as List).forEach((v) {
			data.categories.add(new GetCheckoutResponseDataRecordCategory().fromJson(v));
		});
	}
	if (json['variants'] != null) {
		data.variants = new List<GetCheckoutResponseDataRecordVariant>();
		(json['variants'] as List).forEach((v) {
			data.variants.add(new GetCheckoutResponseDataRecordVariant().fromJson(v));
		});
	}
	if (json['attributes'] != null) {
		data.attributes = new List<GetCheckoutResponseDataRecordAttribute>();
		(json['attributes'] as List).forEach((v) {
			data.attributes.add(new GetCheckoutResponseDataRecordAttribute().fromJson(v));
		});
	}
	if (json['images'] != null) {
		data.images = new List<GetCheckoutResponseDataRecordImage>();
		(json['images'] as List).forEach((v) {
			data.images.add(new GetCheckoutResponseDataRecordImage().fromJson(v));
		});
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['publish_status'] != null) {
		data.publishStatus = json['publish_status'];
	}
	if (json['created_at'] != null) {
		data.createdAt = json['created_at']?.toString();
	}
	if (json['updated_at'] != null) {
		data.updatedAt = json['updated_at']?.toString();
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataRecordToJson(GetCheckoutResponseDataRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['position'] = entity.position;
	data['option'] = entity.option;
	if (entity.price != null) {
		data['price'] = entity.price.toJson();
	}
	data['name'] = entity.name;
	data['description'] = entity.description;
	if (entity.store != null) {
		data['store'] = entity.store.toJson();
	}
	if (entity.categories != null) {
		data['categories'] =  entity.categories.map((v) => v.toJson()).toList();
	}
	if (entity.variants != null) {
		data['variants'] =  entity.variants.map((v) => v.toJson()).toList();
	}
	if (entity.attributes != null) {
		data['attributes'] =  entity.attributes.map((v) => v.toJson()).toList();
	}
	if (entity.images != null) {
		data['images'] =  entity.images.map((v) => v.toJson()).toList();
	}
	data['status'] = entity.status;
	data['publish_status'] = entity.publishStatus;
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	return data;
}

getCheckoutResponseDataRecordPriceFromJson(GetCheckoutResponseDataRecordPrice data, Map<String, dynamic> json) {
	if (json['min'] != null) {
		data.min = json['min']?.toInt();
	}
	if (json['max'] != null) {
		data.max = json['max']?.toInt();
	}
	if (json['min_text'] != null) {
		data.minText = json['min_text']?.toString();
	}
	if (json['max_text'] != null) {
		data.maxText = json['max_text']?.toString();
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataRecordPriceToJson(GetCheckoutResponseDataRecordPrice entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['min'] = entity.min;
	data['max'] = entity.max;
	data['min_text'] = entity.minText;
	data['max_text'] = entity.maxText;
	return data;
}

getCheckoutResponseDataRecordStoreFromJson(GetCheckoutResponseDataRecordStore data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['image'] != null) {
		data.image = json['image'];
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataRecordStoreToJson(GetCheckoutResponseDataRecordStore entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['image'] = entity.image;
	return data;
}

getCheckoutResponseDataRecordCategoryFromJson(GetCheckoutResponseDataRecordCategory data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['description'] != null) {
		data.description = json['description']?.toString();
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataRecordCategoryToJson(GetCheckoutResponseDataRecordCategory entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['position'] = entity.position;
	data['name'] = entity.name;
	data['description'] = entity.description;
	return data;
}

getCheckoutResponseDataRecordVariantFromJson(GetCheckoutResponseDataRecordVariant data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['sku'] != null) {
		data.sku = json['sku']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['description'] != null) {
		data.description = json['description']?.toString();
	}
	if (json['cost_price'] != null) {
		data.costPrice = json['cost_price']?.toInt();
	}
	if (json['cost_price_text'] != null) {
		data.costPriceText = json['cost_price_text']?.toString();
	}
	if (json['price'] != null) {
		data.price = json['price']?.toDouble();
	}
	if (json['price_text'] != null) {
		data.priceText = json['price_text']?.toString();
	}
	if (json['compare_at_price'] != null) {
		data.compareAtPrice = json['compare_at_price']?.toInt();
	}
	if (json['compare_at_price_text'] != null) {
		data.compareAtPriceText = json['compare_at_price_text']?.toString();
	}
	if (json['quantity'] != null) {
		data.quantity = json['quantity']?.toInt();
	}
	if (json['hold'] != null) {
		data.hold = json['hold']?.toInt();
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	if (json['weight'] != null) {
		data.weight = json['weight']?.toInt();
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	if (json['options'] != null) {
		data.options = new List<GetCheckoutResponseDataRecordVariantsOption>();
		(json['options'] as List).forEach((v) {
			data.options.add(new GetCheckoutResponseDataRecordVariantsOption().fromJson(v));
		});
	}
	if (json['image'] != null) {
		data.image = new GetCheckoutResponseDataRecordVariantsImage().fromJson(json['image']);
	}
	if (json['discount'] != null) {
		data.discount = json['discount']?.toDouble();
	}
	if (json['total'] != null) {
		data.total = json['total']?.toDouble();
	}
	if (json['fee'] != null) {
		data.fee = json['fee']?.toInt();
	}
	if (json['service_fee'] != null) {
		data.serviceFee = json['service_fee']?.toDouble();
	}
	if (json['payment_fee'] != null) {
		data.paymentFee = json['payment_fee']?.toDouble();
	}
	if (json['tax'] != null) {
		data.tax = json['tax']?.toDouble();
	}
	if (json['organizer_fee'] != null) {
		data.organizerFee = json['organizer_fee']?.toDouble();
	}
	if (json['organizer_service_fee'] != null) {
		data.organizerServiceFee = json['organizer_service_fee']?.toDouble();
	}
	if (json['organizer_payment_fee'] != null) {
		data.organizerPaymentFee = json['organizer_payment_fee']?.toDouble();
	}
	if (json['organizer_tax'] != null) {
		data.organizerTax = json['organizer_tax']?.toDouble();
	}
	if (json['income'] != null) {
		data.income = json['income']?.toDouble();
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataRecordVariantToJson(GetCheckoutResponseDataRecordVariant entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['sku'] = entity.sku;
	data['name'] = entity.name;
	data['description'] = entity.description;
	data['cost_price'] = entity.costPrice;
	data['cost_price_text'] = entity.costPriceText;
	data['price'] = entity.price;
	data['price_text'] = entity.priceText;
	data['compare_at_price'] = entity.compareAtPrice;
	data['compare_at_price_text'] = entity.compareAtPriceText;
	data['quantity'] = entity.quantity;
	data['hold'] = entity.hold;
	data['position'] = entity.position;
	data['weight'] = entity.weight;
	data['status'] = entity.status;
	if (entity.options != null) {
		data['options'] =  entity.options.map((v) => v.toJson()).toList();
	}
	if (entity.image != null) {
		data['image'] = entity.image.toJson();
	}
	data['discount'] = entity.discount;
	data['total'] = entity.total;
	data['fee'] = entity.fee;
	data['service_fee'] = entity.serviceFee;
	data['payment_fee'] = entity.paymentFee;
	data['tax'] = entity.tax;
	data['organizer_fee'] = entity.organizerFee;
	data['organizer_service_fee'] = entity.organizerServiceFee;
	data['organizer_payment_fee'] = entity.organizerPaymentFee;
	data['organizer_tax'] = entity.organizerTax;
	data['income'] = entity.income;
	return data;
}

getCheckoutResponseDataRecordVariantsOptionFromJson(GetCheckoutResponseDataRecordVariantsOption data, Map<String, dynamic> json) {
	if (json['value'] != null) {
		data.value = new GetCheckoutResponseDataRecordVariantsOptionsValue().fromJson(json['value']);
	}
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataRecordVariantsOptionToJson(GetCheckoutResponseDataRecordVariantsOption entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.value != null) {
		data['value'] = entity.value.toJson();
	}
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['name'] = entity.name;
	return data;
}

getCheckoutResponseDataRecordVariantsOptionsValueFromJson(GetCheckoutResponseDataRecordVariantsOptionsValue data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataRecordVariantsOptionsValueToJson(GetCheckoutResponseDataRecordVariantsOptionsValue entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['name'] = entity.name;
	return data;
}

getCheckoutResponseDataRecordVariantsImageFromJson(GetCheckoutResponseDataRecordVariantsImage data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['store_id'] != null) {
		data.storeId = json['store_id']?.toInt();
	}
	if (json['album_id'] != null) {
		data.albumId = json['album_id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataRecordVariantsImageToJson(GetCheckoutResponseDataRecordVariantsImage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['store_id'] = entity.storeId;
	data['album_id'] = entity.albumId;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	return data;
}

getCheckoutResponseDataRecordAttributeFromJson(GetCheckoutResponseDataRecordAttribute data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['type'] != null) {
		data.type = new GetCheckoutResponseDataRecordAttributesType().fromJson(json['type']);
	}
	if (json['require'] != null) {
		data.require = json['require'];
	}
	if (json['metafield'] != null) {
		data.metafield = new GetCheckoutResponseDataRecordAttributesMetafield().fromJson(json['metafield']);
	}
	if (json['value'] != null) {
		data.value = new GetCheckoutResponseDataRecordAttributesValue().fromJson(json['value']);
	}
	if (json['value_text'] != null) {
		data.valueText = json['value_text']?.toString();
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataRecordAttributeToJson(GetCheckoutResponseDataRecordAttribute entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['name'] = entity.name;
	if (entity.type != null) {
		data['type'] = entity.type.toJson();
	}
	data['require'] = entity.require;
	if (entity.metafield != null) {
		data['metafield'] = entity.metafield.toJson();
	}
	if (entity.value != null) {
		data['value'] = entity.value.toJson();
	}
	data['value_text'] = entity.valueText;
	return data;
}

getCheckoutResponseDataRecordAttributesTypeFromJson(GetCheckoutResponseDataRecordAttributesType data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataRecordAttributesTypeToJson(GetCheckoutResponseDataRecordAttributesType entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

getCheckoutResponseDataRecordAttributesMetafieldFromJson(GetCheckoutResponseDataRecordAttributesMetafield data, Map<String, dynamic> json) {
	return data;
}

Map<String, dynamic> getCheckoutResponseDataRecordAttributesMetafieldToJson(GetCheckoutResponseDataRecordAttributesMetafield entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	return data;
}

getCheckoutResponseDataRecordAttributesValueFromJson(GetCheckoutResponseDataRecordAttributesValue data, Map<String, dynamic> json) {
	return data;
}

Map<String, dynamic> getCheckoutResponseDataRecordAttributesValueToJson(GetCheckoutResponseDataRecordAttributesValue entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	return data;
}

getCheckoutResponseDataRecordImageFromJson(GetCheckoutResponseDataRecordImage data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['store_id'] != null) {
		data.storeId = json['store_id']?.toInt();
	}
	if (json['tag'] != null) {
		data.tag = json['tag'];
	}
	if (json['album_id'] != null) {
		data.albumId = json['album_id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseDataRecordImageToJson(GetCheckoutResponseDataRecordImage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['store_id'] = entity.storeId;
	data['tag'] = entity.tag;
	data['album_id'] = entity.albumId;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['position'] = entity.position;
	return data;
}

getCheckoutResponseBenchFromJson(GetCheckoutResponseBench data, Map<String, dynamic> json) {
	if (json['second'] != null) {
		data.second = json['second']?.toInt();
	}
	if (json['millisecond'] != null) {
		data.millisecond = json['millisecond']?.toDouble();
	}
	if (json['format'] != null) {
		data.format = json['format']?.toString();
	}
	return data;
}

Map<String, dynamic> getCheckoutResponseBenchToJson(GetCheckoutResponseBench entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}