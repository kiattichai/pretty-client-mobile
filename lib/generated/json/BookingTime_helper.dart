import 'package:pretty_client_mobile/modules/booking_module/models/BookingTime.dart';

bookingTimeFromJson(BookingTime data, Map<String, dynamic> json) {
	if (json['textTime'] != null) {
		data.textTime = json['textTime']?.toString();
	}
	if (json['selected'] != null) {
		data.selected = json['selected'];
	}
	if (json['timeValue'] != null) {
		data.timeValue = json['timeValue']?.toString();
	}
	return data;
}

Map<String, dynamic> bookingTimeToJson(BookingTime entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['textTime'] = entity.textTime;
	data['selected'] = entity.selected;
	data['timeValue'] = entity.timeValue;
	return data;
}