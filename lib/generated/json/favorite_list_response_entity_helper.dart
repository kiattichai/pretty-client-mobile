import 'package:pretty_client_mobile/modules/favorite_module/model/favorite_list_response_entity.dart';

favoriteListResponseEntityFromJson(FavoriteListResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new FavoriteListResponseData().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new FavoriteListResponseBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> favoriteListResponseEntityToJson(FavoriteListResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

favoriteListResponseDataFromJson(FavoriteListResponseData data, Map<String, dynamic> json) {
	if (json['record'] != null) {
		data.record = new List<FavoriteListResponseDataRecord>();
		(json['record'] as List).forEach((v) {
			data.record.add(new FavoriteListResponseDataRecord().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> favoriteListResponseDataToJson(FavoriteListResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.record != null) {
		data['record'] =  entity.record.map((v) => v.toJson()).toList();
	}
	return data;
}

favoriteListResponseDataRecordFromJson(FavoriteListResponseDataRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['pretty'] != null) {
		data.pretty = new FavoriteListResponseDataRecordPretty().fromJson(json['pretty']);
	}
	if (json['created_at'] != null) {
		data.createdAt = json['created_at']?.toString();
	}
	if (json['updated_at'] != null) {
		data.updatedAt = json['updated_at']?.toString();
	}
	return data;
}

Map<String, dynamic> favoriteListResponseDataRecordToJson(FavoriteListResponseDataRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	if (entity.pretty != null) {
		data['pretty'] = entity.pretty.toJson();
	}
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	return data;
}

favoriteListResponseDataRecordPrettyFromJson(FavoriteListResponseDataRecordPretty data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['code'] != null) {
		data.code = json['code']?.toString();
	}
	if (json['group'] != null) {
		data.group = new FavoriteListResponseDataRecordPrettyGroup().fromJson(json['group']);
	}
	if (json['first_name'] != null) {
		data.firstName = json['first_name']?.toString();
	}
	if (json['last_name'] != null) {
		data.lastName = json['last_name']?.toString();
	}
	if (json['nick_name'] != null) {
		data.nickName = json['nick_name']?.toString();
	}
	if (json['birthday'] != null) {
		data.birthday = json['birthday']?.toString();
	}
	if (json['age'] != null) {
		data.age = json['age']?.toInt();
	}
	if (json['gender'] != null) {
		data.gender = json['gender']?.toString();
	}
	if (json['weight'] != null) {
		data.weight = json['weight']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['chest'] != null) {
		data.chest = json['chest']?.toInt();
	}
	if (json['waistline'] != null) {
		data.waistline = json['waistline']?.toInt();
	}
	if (json['hip'] != null) {
		data.hip = json['hip']?.toInt();
	}
	if (json['avatar'] != null) {
		data.avatar = new FavoriteListResponseDataRecordPrettyAvatar().fromJson(json['avatar']);
	}
	return data;
}

Map<String, dynamic> favoriteListResponseDataRecordPrettyToJson(FavoriteListResponseDataRecordPretty entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['code'] = entity.code;
	if (entity.group != null) {
		data['group'] = entity.group.toJson();
	}
	data['first_name'] = entity.firstName;
	data['last_name'] = entity.lastName;
	data['nick_name'] = entity.nickName;
	data['birthday'] = entity.birthday;
	data['age'] = entity.age;
	data['gender'] = entity.gender;
	data['weight'] = entity.weight;
	data['height'] = entity.height;
	data['chest'] = entity.chest;
	data['waistline'] = entity.waistline;
	data['hip'] = entity.hip;
	if (entity.avatar != null) {
		data['avatar'] = entity.avatar.toJson();
	}
	return data;
}

favoriteListResponseDataRecordPrettyGroupFromJson(FavoriteListResponseDataRecordPrettyGroup data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['start_drink'] != null) {
		data.startDrink = json['start_drink']?.toInt();
	}
	return data;
}

Map<String, dynamic> favoriteListResponseDataRecordPrettyGroupToJson(FavoriteListResponseDataRecordPrettyGroup entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['start_drink'] = entity.startDrink;
	return data;
}

favoriteListResponseDataRecordPrettyAvatarFromJson(FavoriteListResponseDataRecordPrettyAvatar data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['tag'] != null) {
		data.tag = json['tag']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['width'] != null) {
		data.width = json['width']?.toInt();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toInt();
	}
	if (json['mime'] != null) {
		data.mime = json['mime']?.toString();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['position'] != null) {
		data.position = json['position']?.toInt();
	}
	return data;
}

Map<String, dynamic> favoriteListResponseDataRecordPrettyAvatarToJson(FavoriteListResponseDataRecordPrettyAvatar entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['tag'] = entity.tag;
	data['name'] = entity.name;
	data['width'] = entity.width;
	data['height'] = entity.height;
	data['mime'] = entity.mime;
	data['size'] = entity.size;
	data['url'] = entity.url;
	data['position'] = entity.position;
	return data;
}

favoriteListResponseBenchFromJson(FavoriteListResponseBench data, Map<String, dynamic> json) {
	if (json['second'] != null) {
		data.second = json['second']?.toInt();
	}
	if (json['millisecond'] != null) {
		data.millisecond = json['millisecond']?.toDouble();
	}
	if (json['format'] != null) {
		data.format = json['format']?.toString();
	}
	return data;
}

Map<String, dynamic> favoriteListResponseBenchToJson(FavoriteListResponseBench entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}