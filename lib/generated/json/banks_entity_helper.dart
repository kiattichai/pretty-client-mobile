import 'package:pretty_client_mobile/modules/coins_module/models/banks_entity.dart';

banksEntityFromJson(BanksEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new BanksData().fromJson(json['data']);
	}
	if (json['bench'] != null) {
		data.bench = new BanksBench().fromJson(json['bench']);
	}
	return data;
}

Map<String, dynamic> banksEntityToJson(BanksEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	if (entity.bench != null) {
		data['bench'] = entity.bench.toJson();
	}
	return data;
}

banksDataFromJson(BanksData data, Map<String, dynamic> json) {
	if (json['record'] != null) {
		data.record = new List<BanksDataRecord>();
		(json['record'] as List).forEach((v) {
			data.record.add(new BanksDataRecord().fromJson(v));
		});
	}
	if (json['cache'] != null) {
		data.cache = json['cache'];
	}
	return data;
}

Map<String, dynamic> banksDataToJson(BanksData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.record != null) {
		data['record'] =  entity.record.map((v) => v.toJson()).toList();
	}
	data['cache'] = entity.cache;
	return data;
}

banksDataRecordFromJson(BanksDataRecord data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['account'] != null) {
		data.account = new BanksDataRecordAccount().fromJson(json['account']);
	}
	if (json['type'] != null) {
		data.type = json['type']?.toString();
	}
	if (json['branch'] != null) {
		data.branch = json['branch']?.toString();
	}
	if (json['image'] != null) {
		data.image = new BanksDataRecordImage().fromJson(json['image']);
	}
	if (json['status'] != null) {
		data.status = json['status'];
	}
	return data;
}

Map<String, dynamic> banksDataRecordToJson(BanksDataRecord entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	if (entity.account != null) {
		data['account'] = entity.account.toJson();
	}
	data['type'] = entity.type;
	data['branch'] = entity.branch;
	if (entity.image != null) {
		data['image'] = entity.image.toJson();
	}
	data['status'] = entity.status;
	return data;
}

banksDataRecordAccountFromJson(BanksDataRecordAccount data, Map<String, dynamic> json) {
	if (json['number'] != null) {
		data.number = json['number']?.toString();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	return data;
}

Map<String, dynamic> banksDataRecordAccountToJson(BanksDataRecordAccount entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['number'] = entity.number;
	data['name'] = entity.name;
	return data;
}

banksDataRecordImageFromJson(BanksDataRecordImage data, Map<String, dynamic> json) {
	if (json['desktop'] != null) {
		data.desktop = json['desktop']?.toString();
	}
	if (json['mobile'] != null) {
		data.mobile = json['mobile']?.toString();
	}
	return data;
}

Map<String, dynamic> banksDataRecordImageToJson(BanksDataRecordImage entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['desktop'] = entity.desktop;
	data['mobile'] = entity.mobile;
	return data;
}

banksBenchFromJson(BanksBench data, Map<String, dynamic> json) {
	if (json['second'] != null) {
		data.second = json['second']?.toInt();
	}
	if (json['millisecond'] != null) {
		data.millisecond = json['millisecond']?.toDouble();
	}
	if (json['format'] != null) {
		data.format = json['format']?.toString();
	}
	return data;
}

Map<String, dynamic> banksBenchToJson(BanksBench entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['second'] = entity.second;
	data['millisecond'] = entity.millisecond;
	data['format'] = entity.format;
	return data;
}