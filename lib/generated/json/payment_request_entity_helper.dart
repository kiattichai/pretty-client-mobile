import 'package:pretty_client_mobile/modules/booking_module/models/payment_request_entity.dart';

paymentRequestEntityFromJson(PaymentRequestEntity data, Map<String, dynamic> json) {
	if (json['order_id'] != null) {
		data.orderId = json['order_id']?.toInt();
	}
	if (json['payment_method'] != null) {
		data.paymentMethod = json['payment_method']?.toString();
	}
	if (json['payment_provider'] != null) {
		data.paymentProvider = json['payment_provider']?.toString();
	}
	return data;
}

Map<String, dynamic> paymentRequestEntityToJson(PaymentRequestEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['order_id'] = entity.orderId;
	data['payment_method'] = entity.paymentMethod;
	data['payment_provider'] = entity.paymentProvider;
	return data;
}