import 'package:pretty_client_mobile/modules/profile_module/models/avatar_updated_response_entity.dart';

avatarUpdatedResponseEntityFromJson(AvatarUpdatedResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new AvatarUpdatedResponseData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> avatarUpdatedResponseEntityToJson(AvatarUpdatedResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	return data;
}

avatarUpdatedResponseDataFromJson(AvatarUpdatedResponseData data, Map<String, dynamic> json) {
	if (json['message'] != null) {
		data.message = json['message']?.toString();
	}
	if (json['image'] != null) {
		data.image = json['image']?.toString();
	}
	return data;
}

Map<String, dynamic> avatarUpdatedResponseDataToJson(AvatarUpdatedResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['message'] = entity.message;
	data['image'] = entity.image;
	return data;
}