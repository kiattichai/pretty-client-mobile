import 'package:pretty_client_mobile/modules/country_code_module/model/country_code_response_entity.dart';

countryCodeResponseEntityFromJson(CountryCodeResponseEntity data, Map<String, dynamic> json) {
	if (json['data'] != null) {
		data.data = new CountryCodeResponseData().fromJson(json['data']);
	}
	return data;
}

Map<String, dynamic> countryCodeResponseEntityToJson(CountryCodeResponseEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	return data;
}

countryCodeResponseDataFromJson(CountryCodeResponseData data, Map<String, dynamic> json) {
	if (json['record'] != null) {
		data.record = new List<CountryCode>();
		(json['record'] as List).forEach((v) {
			data.record.add(new CountryCode().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> countryCodeResponseDataToJson(CountryCodeResponseData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.record != null) {
		data['record'] =  entity.record.map((v) => v.toJson()).toList();
	}
	return data;
}

countryCodeFromJson(CountryCode data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toInt();
	}
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['iso_code'] != null) {
		data.isoCode = json['iso_code']?.toString();
	}
	if (json['phone_code'] != null) {
		data.phoneCode = json['phone_code']?.toString();
	}
	if (json['phone_code_text'] != null) {
		data.phoneCodeText = json['phone_code_text']?.toString();
	}
	return data;
}

Map<String, dynamic> countryCodeToJson(CountryCode entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['iso_code'] = entity.isoCode;
	data['phone_code'] = entity.phoneCode;
	data['phone_code_text'] = entity.phoneCodeText;
	return data;
}