import 'package:pretty_client_mobile/core/theme.dart';
import 'package:pretty_client_mobile/design/component_style.dart';

class Design {
  final ThemeConfig themeConfig;
  final DesignText designText;
  final DesignColor designColor;
  static Design _instance;

  factory Design({ThemeConfig themeApp, DesignText designText, DesignColor designColor}) {
    _instance ??= Design._internal(themeApp, designText, designColor);
    return _instance;
  }

  Design._internal(this.themeConfig, this.designText, this.designColor);
  static ThemeConfig get theme {
    return _instance.themeConfig;
  }

  static DesignText get text {
    return _instance.designText;
  }

  static DesignColor get color {
    return _instance.designColor;
  }
}
