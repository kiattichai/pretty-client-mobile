import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/theme.dart';

class ThemeBlack extends ThemeConfig {
  @override
  Color get primaryColor => Color(0xffe7c260);
  @override
  Color get colorButtonRed => Color(0xff1f1f1f);
  @override
  Color get backgroundColor => Color(0xff151515);
  @override
  Color get fontColorPrimary => Color(0xffffffff);
  @override
  Color get fontColorSecondary => Color(0xffa4a4a4);
  @override
  Color get fontColorSecondary2 => Color(0xffa7a7a7);
  @override
  Color get fontColorSecondary3 => Color(0xffeeeeee);
  @override
  Color get fontColorSecondary4 => Color(0xff777777);
  @override
  Color get fontColorBlack => Colors.black;
  @override
  Color get colorButtonBookNow => Color(0xffb60000);
  @override
  Color get colorButtonPrimary => Color(0xffe7c260);
  @override
  Color get fontColorLinkButton => Color(0xfff7b500);
  @override
  Color get fontColorPlaceholder => Color(0xffbbbbbb);
  @override
  Color get inputColor => Color(0xff353535);
  @override
  Color get inputBorderColor => Color(0xff121212);
  @override
  Color get navBackgroundColor => Color(0xff1c1c1c);
  @override
  Color get activeMenuColor => Color(0xffe7c260);
  @override
  Color get titleBackgroundColor => Color(0xff151515);
  @override
  Color get lineColor => Color(0xff2a2a2a);
  @override
  Color get borderColor => Color(0xff343434);
  @override
  Color get fontColorWhite => Color(0xffffffff);
  @override
  Color get underlineColor => Color(0xff3a3a3a);
  @override
  Color get menuTitleColor => Color(0xffbababa);
  @override
  Color get profileTitleColor => Color(0xffe6e6e6);
  @override
  Color get avatarBorderColor => Color(0xffe6e6e6);
  @override
  Color get subTitleColor => Color(0xffcccccc);
  @override
  Color get backgroundColorSecondary => Color(0xff1d1d1d);
  @override
  Color get fontColorInactive => Color(0xffd5d5d5);

  @override
  Color get colorButtonFacebook => Color(0xff1777F2);
  @override
  Color get colorPrimary => Color(0xffe7c260);
  @override
  Color get colorMainBackground => Color(0xff151515);
  @override
  Color get colorMainMenu => Color(0xff1c1c1c);
  @override
  Color get colorBox => Color(0xff1d1d1d);
  @override
  Color get colorLine => Color(0xff525252);
  @override
  Color get colorText => Color(0xff636362);
  @override
  Color get colorText2 => Color(0xffa4a4a4);
  @override
  Color get colorInactive => Color(0xffd5d5d5);
  @override
  Color get colorActive => Color(0xffffffff);
}
