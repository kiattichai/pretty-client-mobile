import 'package:flutter/material.dart';

class DesignColor {
  final bgColorBlackGradient = LinearGradient(
      colors: [Color(0xff282727), Color(0xff101010)],
      stops: [0, 1],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter);

  final boxFilterInActive = BoxDecoration(
      border: Border.all(color: Color(0xff979797), width: 1),
      borderRadius: BorderRadius.circular(6));
  final boxFilterActive = BoxDecoration(
      border: Border.all(color: Color(0xffe7c260), width: 1),
      borderRadius: BorderRadius.circular(6));

  final bgColorNewsList = LinearGradient(
      colors: [Color(0xff101010), Color(0xff471111), Color(0xff101010)],
      stops: [0, 0.6, 1],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter);

  final bgColorBooking = LinearGradient(
      colors: [Color(0xff2a2b2b), Color(0xff323335)],
      stops: [0, 1],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter);
}
