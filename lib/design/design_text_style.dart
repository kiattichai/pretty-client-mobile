import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';

class DesignText {
  TextStyle get buttonBooking => TextStyle(
        color: Color(0xffffffff),
        fontSize: 16,
        fontWeight: FontWeight.w700,
        fontStyle: FontStyle.normal,
        letterSpacing: 1.5,
      );
  TextStyle headerFriendInfo = TextStyle(
    color: Color(0xffffffff),
    fontSize: 22,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
    letterSpacing: -1.527777777777778,
  );
  TextStyle textFriendInfo = TextStyle(
    fontFamily: 'Sarabun',
    decoration: TextDecoration.none,
    color: Color(0xffd8d8d8),
    fontSize: 16,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
    letterSpacing: 0,
  );
  TextStyle filterStatusInActive = TextStyle(
    color: Color(0xffaaaaaa),
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
  );
  TextStyle filterStatusActive = TextStyle(
    color: Color(0xffffffff),
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
  );

  TextStyle get headerMenu => TextStyle(
        fontFamily: 'Sarabun',
        color: Design.theme.colorActive,
        fontSize: 18,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      );

  TextStyle get headerContent => TextStyle(
        fontFamily: 'Sarabun',
        color: Design.theme.colorActive,
        fontSize: 18,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
        letterSpacing: 0,
      );
  TextStyle get textHighlight => TextStyle(
        fontFamily: 'Sarabun',
        color: Design.theme.colorInactive,
        fontSize: 14,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
        letterSpacing: 0,
      );
  TextStyle get textButton => TextStyle(
        fontFamily: 'Sarabun',
        color: Color(0xff151515),
        fontSize: 16,
        fontWeight: FontWeight.w700,
        fontStyle: FontStyle.normal,
      );
  TextStyle get textFacebookButton => TextStyle(
        fontFamily: 'Sarabun',
        color: Design.theme.colorActive,
        fontSize: 16,
        fontWeight: FontWeight.w700,
        fontStyle: FontStyle.normal,
      );
}
