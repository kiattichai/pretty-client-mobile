import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/widgets/primary_button_widget.dart';

class ModalGiveFeedback extends StatelessWidget {
  final Function closeModal;

  const ModalGiveFeedback({Key key, @required this.closeModal})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Scaffold(
        backgroundColor: Colors.black.withOpacity(0.7),
        body: Stack(
          children: <Widget>[
            Align(
              child: Padding(
                child: Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(left: 20, right: 20),
                    child: Stack(
                      children: <Widget>[
                        Align(
                          child: Padding(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                SvgPicture.asset(
                                  'images/magic_fill.svg',
                                ),
                                Container(
                                  width: double.infinity,
                                  alignment: Alignment.center,
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        "ขอบคุณสำหรับการให้คะแนน",
                                        style: TextStyle(
                                          fontFamily: 'Sarabun',
                                          color:
                                              Design.theme.colorMainBackground,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                          letterSpacing: 0,
                                        ),
                                      ),
                                      Text(
                                        "และการเขียนรีวิว",
                                        style: TextStyle(
                                          fontFamily: 'Sarabun',
                                          color:
                                              Design.theme.colorMainBackground,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                          letterSpacing: 0,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: 200,
                                  child: PrimaryButtonWidget(
                                    buttonTitle: "ตกลง",
                                    onBtnPressed: closeModal,
                                  ),
                                ),
                              ],
                            ),
                            padding: EdgeInsets.all(15),
                          ),
                          alignment: Alignment(0, 0),
                        ),
//                        Positioned(
//                          top: 15,
//                          right: 15,
//                          child: InkWell(
//                            onTap: closeModal,
//                            child: SvgPicture.asset(
//                              'images/icon_close.svg',
//                              color: Design.theme.colorMainBackground,
//                            ),
//                          ),
//                        )
                      ],
                    ),
                    height: 245,
                    decoration: new BoxDecoration(
                        color: Color(0xffffffff),
                        borderRadius: BorderRadius.circular(8))),
                padding: EdgeInsets.only(left: 15, right: 15),
              ),
//              alignment: Alignment(0, -0.2),
            )
          ],
        ),
      ),
    );
  }
}
