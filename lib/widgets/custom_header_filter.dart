import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/utils/screen.dart';

class CustomHeaderFilter extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final double height;
  final Function onTap;
  final bool isFiltered;

  const CustomHeaderFilter(
      {Key key, @required this.height, @required this.title, this.onTap, this.isFiltered})
      : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return Container(
      height: height,
      decoration: BoxDecoration(
        color: Design.theme.colorMainBackground,
        boxShadow: [
          BoxShadow(color: Color(0xe6000000), offset: Offset(0, 3), blurRadius: 14, spreadRadius: 0)
        ],
      ),
      child: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: Stack(
          fit: StackFit.loose,
          children: <Widget>[
            Positioned(
              child: Container(
                child: Text(title,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: Design.theme.colorActive,
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal)),
              ),
              left: 20,
              top: 20,
            ),
            Positioned(
              child: InkWell(
                child: Container(
                  width: 50,
                  child: Center(
                    child: SvgPicture.asset(
                      'images/icon_filter.svg',
                      color: Design.theme.colorPrimary,
                    ),
                  ),
                ),
                onTap: onTap,
              ),
              top: 16,
              right: 10,
            )
          ],
        ),
      ),
    );
  }
}
