import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/model/language_entity.dart';

enum Gender { male, female, unknown }

class Consts {
  Consts._();

  static const double padding = 16.0;
  static const double avatarRadius = 66.0;
}

class ModalSelectGender extends StatefulWidget {
  final Function onSelectGender;
  final String initialGender;
  final LanguageEntity appLocale;
  const ModalSelectGender(
      {Key key, this.onSelectGender, this.initialGender, @required this.appLocale})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ModalSelectGenderState();
  }
}

class ModalSelectGenderState extends State<ModalSelectGender> {
  String selectedGender = "";
  Gender _genderValue = Gender.male;

  void initState() {
    super.initState();
    setState(() {
      _genderValue =
          widget.initialGender == null ? Gender.male : handleGenderEnum(widget.initialGender);
    });
  }

  dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            top: Consts.padding,
            bottom: Consts.padding,
            left: Consts.padding,
            right: Consts.padding,
          ),
          decoration: new BoxDecoration(
            color: Color(0xffffffff).withOpacity(0.9241071428571429),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(Consts.padding),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min, // To make the card compact
            children: <Widget>[
              SizedBox(height: 16.0),
              radioGroup3(context),
              SizedBox(height: 24.0),
              Align(
                  alignment: Alignment.bottomRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                        textColor: Colors.white,
                        onPressed: () {
                          Navigator.of(context).pop(); // To close the dialog
                        },
                        child: Text(widget.appLocale.commonCancel,
                            style: TextStyle(
                              color: Color(0xff848484),
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            )),
                      ),
                      FlatButton(
                        textColor: Colors.white,
                        onPressed: () {
                          widget.onSelectGender(handleGender(_genderValue));
                          Navigator.of(context).pop();
                        },
                        child: Text(widget.appLocale.commonConfirm,
                            style: TextStyle(
                                color: Color(0xff333333),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal)),
                      ),
                    ],
                  )),
            ],
          ),
        ),
      ],
    );
  }

  Widget radioGroup() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(
          children: <Widget>[
            new Radio(
              activeColor: Design.theme.colorPrimary,
              value: "male",
              groupValue: selectedGender,
              onChanged: (value) => this.setState(() {
                selectedGender = value;
              }),
            ),
            new Text(
              widget.appLocale.genderDialogMale,
              style: new TextStyle(fontSize: 16.0),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            new Radio(
              activeColor: Design.theme.colorPrimary,
              value: "female",
              groupValue: selectedGender,
              onChanged: (value) => this.setState(() {
                selectedGender = value;
              }),
            ),
            new Text(
              widget.appLocale.genderDialogFemale,
              style: new TextStyle(
                fontSize: 16.0,
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            new Radio(
              activeColor: Design.theme.colorPrimary,
              value: "unknow",
              groupValue: selectedGender,
              onChanged: (value) => this.setState(() {
                selectedGender = value;
              }),
            ),
            new Text(
              widget.appLocale.genderDialogNotSpecified,
              style: new TextStyle(
                fontSize: 16.0,
              ),
            )
          ],
        )
      ],
    );
  }

  Widget radioGroup2(BuildContext context) {
    return Align(
        alignment: Alignment.topLeft,
        child: Column(
          children: <Widget>[
            FlatButton.icon(
              label: Text(widget.appLocale.genderDialogMale,
                  style: TextStyle(
                    color: Color(0xff3a3a3a),
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  )),
              icon: Radio(
                value: Gender.male,
                groupValue: _genderValue,
                onChanged: (value) => this.setState(() {
                  _genderValue = value;
                }),
              ),
              onPressed: () {
                setState(() {
                  _genderValue = Gender.male;
                });
              },
            ),
            FlatButton.icon(
              label: Text(widget.appLocale.genderDialogFemale,
                  style: TextStyle(
                    color: Color(0xff3a3a3a),
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  )),
              icon: Radio(
                value: Gender.female,
                groupValue: _genderValue,
                onChanged: (value) => this.setState(() {
                  _genderValue = value;
                }),
              ),
              onPressed: () {
                setState(() {
                  _genderValue = Gender.female;
                });
              },
            ),
            FlatButton.icon(
              label: Text(widget.appLocale.genderDialogNotSpecified,
                  style: TextStyle(
                    color: Color(0xff3a3a3a),
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  )),
              icon: Radio(
                value: Gender.unknown,
                groupValue: _genderValue,
                onChanged: (value) => this.setState(() {
                  _genderValue = value;
                }),
              ),
              onPressed: () {
                setState(() {
                  _genderValue = Gender.unknown;
                });
              },
            )
          ],
        ));
  }

  Widget radioGroup3(BuildContext context) {
    return Theme(
        data: ThemeData.light(),
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            RadioListTile(
              activeColor: Design.theme.colorPrimary,
              value: Gender.male,
              groupValue: _genderValue,
              onChanged: (Gender value) {
                setState(() {
                  _genderValue = value;
                });
              },
              title: Text(widget.appLocale.genderDialogMale,
                  style: TextStyle(
                    color: Design.theme.colorLine,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  )),
            ),
            RadioListTile(
              activeColor: Design.theme.colorPrimary,
              value: Gender.female,
              groupValue: _genderValue,
              onChanged: (Gender value) {
                setState(() {
                  _genderValue = value;
                });
              },
              title: Text(widget.appLocale.genderDialogFemale,
                  style: TextStyle(
                    color: Design.theme.colorLine,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  )),
            ),
            RadioListTile(
              activeColor: Design.theme.colorPrimary,
              value: Gender.unknown,
              groupValue: _genderValue,
              onChanged: (Gender value) {
                setState(() {
                  _genderValue = value;
                });
              },
              title: Text(widget.appLocale.genderDialogNotSpecified,
                  style: TextStyle(
                    color: Design.theme.colorLine,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  )),
            )
          ],
        )));
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  String handleGender(Gender _gender) {
    switch (_gender) {
      case Gender.male:
        return "male";
        break;
      case Gender.female:
        return "female";
        break;
      case Gender.unknown:
        return "unknown";
        break;
      default:
        return null;
    }
  }

  Gender handleGenderEnum(String _gender) {
    switch (_gender) {
      case "male":
        return Gender.male;
        break;
      case "female":
        return Gender.female;
        break;
      case "unknown":
        return Gender.unknown;
        break;
      default:
        return null;
    }
  }
}
