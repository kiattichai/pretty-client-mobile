import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/model/language_entity.dart';

class RadioButtonSelectVerifyChannelWidget extends StatelessWidget {
  String currentGroupRadioValue;
  final Function(String) onChannelSelected;
  final LanguageEntity appLocale;
  RadioButtonSelectVerifyChannelWidget(
      {@required this.currentGroupRadioValue, this.onChannelSelected, @required this.appLocale});

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
      titleChooseChannel(context),
      chooseChannel(context),
    ]);
  }

  Widget titleChooseChannel(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 24, right: 0),
      child: SizedBox(
        width: double.infinity,
        child: Container(
          child: Text(
            appLocale.channelVerificationTitle,
            style: TextStyle(
              color: Design.theme.colorActive,
              fontSize: 16,
              fontWeight: FontWeight.w700,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
      ),
    );
  }

  Widget chooseChannel(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 0,
        top: 8,
        right: 0,
      ),
      child: Container(
        height: 25,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Row(
              children: <Widget>[
                Radio(
                  activeColor: Design.theme.colorPrimary,
                  value: "sms",
                  groupValue: currentGroupRadioValue,
                  onChanged: onChannelSelected,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                ),
                Text(
                  appLocale.channelVerificationSms,
                  style: TextStyle(
                    color: Design.theme.colorInactive,
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ],
            ),
            SizedBox(
              width: 10,
            ),
            Row(
              children: <Widget>[
                Radio(
                  activeColor: Design.theme.colorPrimary,
                  value: "call",
                  groupValue: currentGroupRadioValue,
                  onChanged: onChannelSelected,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                ),
                Text(
                  appLocale.channelVerificationVoiceCall,
                  style: TextStyle(
                    color: Design.theme.colorInactive,
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
