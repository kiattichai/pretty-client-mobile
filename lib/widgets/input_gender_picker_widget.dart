import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:flutter/services.dart';
import 'package:pretty_client_mobile/model/language_entity.dart';

class InputGenderPickerWidget extends StatelessWidget {
  final TextEditingController controllerGender;
  final VoidCallback onGenderFieldTap;
  final LanguageEntity appLocal;

  InputGenderPickerWidget(
      {@required this.controllerGender, @required this.onGenderFieldTap, @required this.appLocal});

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[titleGender(context), inputGenderInfo(context)]);
  }

  Widget titleGender(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 15, right: 0),
      child: SizedBox(
        width: double.infinity,
        child: Container(
          child: Text(appLocal.userInfoGender,
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Design.theme.colorInactive,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              )),
        ),
      ),
    );
  }

  Widget inputGenderInfo(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(
          top: 8,
        ),
        child: InkWell(
          child: TextFormField(
            enabled: false,
            showCursor: false,
            focusNode: FocusNode(),
            enableInteractiveSelection: false,
            controller: controllerGender,
            keyboardType: TextInputType.text,
            autofocus: false,
            style: new TextStyle(
              color: Design.theme.colorActive,
              fontSize: 16,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              letterSpacing: -0.2,
            ),
            decoration: InputDecoration(
              suffixIcon: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.keyboard_arrow_right,
                  color: Design.theme.colorInactive,
                ),
              ),
              hintText: appLocal.userInfoGenderHint,
              hintStyle: TextStyle(
                color: Design.theme.colorInactive.withOpacity(0.57),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
              contentPadding: new EdgeInsets.symmetric(vertical: 12.0, horizontal: 0.0),
            ),
          ),
          onTap: onGenderFieldTap,
        ),
        decoration: new BoxDecoration(
            border: Border(
          bottom: BorderSide(width: 1.0, color: Design.theme.colorLine),
        )));
  }
}
