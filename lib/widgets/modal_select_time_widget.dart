import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/model/language_entity.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';

class Consts {
  Consts._();

  static const double padding = 16.0;
  static const double avatarRadius = 66.0;
}

class ModalSelectTime extends StatefulWidget {
  final Function onSelectTime;
  final Function onCancelSelected;
  final String initialTime;
  final LanguageEntity appLocale;
  const ModalSelectTime(
      {Key key, 
      @required this.onSelectTime, 
      @required this.onCancelSelected,
      this.initialTime, 
      @required this.appLocale})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ModalSelectTimeState();
  }
}

class ModalSelectTimeState extends State<ModalSelectTime> {
  
  DateTime _timeValue = DateTime.now();

  void initState() {
    super.initState();
    // setState(() {
    //    _timeValue = widget.initialTime == null ? "" : "";
    // });
  }

  dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            top: Consts.padding,
            bottom: Consts.padding,
            left: Consts.padding,
            right: Consts.padding,
          ),
          decoration: new BoxDecoration(
            color: Color(0xffffffff).withOpacity(0.9241071428571429),
            shape: BoxShape.rectangle,
            // borderRadius: BorderRadius.circular(Consts.padding),
            borderRadius: BorderRadius.circular(6),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min, // To make the card compact
            children: <Widget>[
              SizedBox(height: 16.0),  
              timePicker(context),
              SizedBox(height: 24.0),
              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Expanded(flex: 1,
                      child: InkWell(
                        child: Container(
                          child: Center(
                            child: Text("ยกเลิก",
                              style: TextStyle(
                                fontFamily: 'Sarabun',
                                color: Design.theme.colorMainBackground,
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )
                            ),
                          ),
                          height: 44,
                          decoration: new BoxDecoration(
                            color: Design.theme.fontColorInactive,
                            borderRadius: BorderRadius.circular(10)
                          )
                        ),
                        onTap: () {
                          widget.onCancelSelected();
                          Navigator.of(context).pop();
                        }
                      )
                    ),
                    SizedBox(
                      width: 10,
                      height: 44,
                    ),
                    Expanded(flex: 1,
                      child: InkWell(
                        child: Container(
                          child: Center(
                            child: Text("ตกลง",
                              style: TextStyle(
                                fontFamily: 'Sarabun',
                                color: Design.theme.colorMainBackground,
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )
                            ),
                          ),
                          height: 44,
                          decoration: new BoxDecoration(
                            color: Design.theme.primaryColor,
                            borderRadius: BorderRadius.circular(10)
                          )
                        ),
                        onTap: () {
                          widget.onSelectTime(_timeValue);
                          Navigator.of(context).pop();
                        },
                      )
                    )
                 
                    
                  ],
                )
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget timePicker(BuildContext context) {
    return Theme(
      data: ThemeData.light(),
      child: Center(
        child: TimePickerSpinner(
          spacing: 40,
          // minutesInterval: 15,
          onTimeChange: (time) {
            setState(() {
              _timeValue = time;
            });
          },
        ),
      )
    );
  }

  @override
  
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: <Widget>[
            Align(
              child: Padding(
                padding: EdgeInsets.only(left: 15, right: 15),
                child: Container(
                  width: double.infinity,
                  child: dialogContent(context)
                )
              ),
              alignment: Alignment(0, -0.2),
            )
          ]
        )
      ),
      // onTap: () => Navigator.pop(context),
    );
  }
}
