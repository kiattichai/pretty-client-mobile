import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/utils/screen.dart';
import 'package:pretty_client_mobile/design_system.dart';

class CustomHeader extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final double height;

  const CustomHeader({Key key, @required this.height, @required this.title}) : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return Container(
      height: height,
      decoration: BoxDecoration(
        color: Design.theme.colorMainBackground,
        boxShadow: [
          BoxShadow(color: Color(0xe6000000), offset: Offset(0, 3), blurRadius: 14, spreadRadius: 0)
        ],
      ),
      child: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: Row(
          children: <Widget>[
            SizedBox(
              width: Screen.convertWidthSize(20),
            ),
            Expanded(
              child: Text(title,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: Design.theme.colorActive,
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal)),
            )
          ],
        ),
      ),
    );
  }
}
