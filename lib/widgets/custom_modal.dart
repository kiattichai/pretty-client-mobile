import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/model/language_entity.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/widgets/primary_button_widget.dart';

enum ModalType { NORMAL, CANCEL, CLOSE }

class CustomModal extends StatelessWidget {
  final Function onTapConfirm;
  final Function onTapCancel;
  final String title;
  final String desc;
  final LanguageEntity appLocal;
  final ModalType type;

  const CustomModal(
      {Key key,
      this.onTapConfirm,
      this.onTapCancel,
      this.title,
      this.desc,
      this.appLocal,
      this.type = ModalType.NORMAL})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: <Widget>[
            Align(
              child: Padding(
                child: Container(
                  padding: EdgeInsets.all(21),
                  width: double.infinity,
                  height: 233,
                  decoration: BoxDecoration(
                      color: Color(0xffffffff), borderRadius: BorderRadius.circular(6)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(title == null ? appLocal.errorDialogTitle : title,
                          style: TextStyle(
                            fontFamily: 'Sarabun',
                            color: Design.theme.colorLine,
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                          )),
                      Text(desc == null ? '' : desc,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'Sarabun',
                            color: Design.theme.colorLine,
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          )),
                      Builder(
                        builder: (context) {
                          switch (type) {
                            case ModalType.NORMAL:
                              return buildOneButton();
                              break;
                            case ModalType.CANCEL:
                              return buildTwoButton();
                              break;
                            case ModalType.CLOSE:
                              return buildThreeButton();
                              break;
                            default:
                              return Container();
                          }
                        },
                      ),
                    ],
                  ),
                ),
                padding: EdgeInsets.only(left: 20, right: 20),
              ),
              alignment: Alignment(0, -0.2),
            )
          ],
        ),
      ),
    );
  }

  Widget buildOneButton() {
    return Container(
      width: 150,
      child: PrimaryButtonWidget(
          buttonTitle: appLocal.commonConfirm,
          onBtnPressed: () {
            RouterService.instance.pop();
            onTapConfirm();
          }),
    );
  }

  Widget buildTwoButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: MaterialButton(
            height: 48.0,
            disabledColor: Design.theme.colorLine,
            disabledTextColor: Design.theme.colorInactive.withOpacity(0.44),
            color: Design.theme.colorInactive,
            textColor: Design.text.textButton.color,
            child: Text(
              appLocal.commonCancel,
              style: TextStyle(
                color: Design.theme.colorMainBackground,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
            ),
            onPressed: () {
              RouterService.instance.pop();
              onTapCancel();
            },
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
          flex: 5,
        ),
        Expanded(
          child: SizedBox(
            width: 1,
          ),
          flex: 1,
        ),
        Expanded(
          child: MaterialButton(
            height: 48.0,
            disabledColor: Design.theme.colorLine,
            disabledTextColor: Design.theme.colorInactive.withOpacity(0.44),
            color: Design.theme.colorPrimary,
            textColor: Design.text.textButton.color,
            child: Text(
              appLocal.commonConfirm,
              style: TextStyle(
                color: Design.theme.colorMainBackground,
                fontSize: 16,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
              ),
            ),
            onPressed: () {
              RouterService.instance.pop();
              onTapConfirm();
            },
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
          flex: 5,
        ),
      ],
    );
  }

  Widget buildThreeButton() {
    return Container(
      width: 150,
      child: PrimaryButtonWidget(
          buttonTitle: appLocal.commonCancel,
          onBtnPressed: () {
            RouterService.instance.pop();
            onTapConfirm();
          }),
    );
  }
}
