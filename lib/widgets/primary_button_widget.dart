import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';

class PrimaryButtonWidget extends StatelessWidget {
  final String buttonTitle;
  final VoidCallback onBtnPressed;
  PrimaryButtonWidget({@required this.buttonTitle, @required this.onBtnPressed});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 17, right: 0),
      child: SizedBox(
        width: double.infinity,
        child: MaterialButton(
          height: 48.0,
          minWidth: 70.0,
          disabledColor: Design.theme.colorLine,
          disabledTextColor: Design.theme.colorInactive.withOpacity(0.44),
          color: Design.theme.colorPrimary,
          textColor: Design.text.textButton.color,
          child: Text(
            buttonTitle,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w700,
              fontStyle: FontStyle.normal,
            ),
          ),
          onPressed: onBtnPressed,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24.0),
          ),
        ),
      ),
    );
  }
}
