import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/model/language_entity.dart';

class MyCustomMenu extends StatelessWidget {
  //final VoidCallback onMenuTap;
  final Function(int) onMenuTap;
  final List<String> menus;
  final int currentSelected;
  LanguageEntity appLocal;
  MyCustomMenu({
    
    this.currentSelected,
    this.menus,
    this.onMenuTap,
    this.appLocal
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Design.theme.underlineColor,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Row(
        children: List.generate(menus.length, (index){
          return Expanded(
            child: GestureDetector(
              onTap: () => onMenuTap(index),
              child: Container(
                decoration: currentSelected == index
                    ? BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Design.theme.colorLine,
                      )
                    : BoxDecoration(),
                padding: EdgeInsets.all(8.0),
                alignment: Alignment.center,
                child: Text(
                  menus[index],
                  style: TextStyle(
                    color: currentSelected == index 
                        ? Design.theme.primaryColor
                        : Design.theme.fontColorInactive,
                  ),
                ),
              ),
            ),
          );
        }) 
          
        
      ),
    );
  }
}



// import 'package:flutter/material.dart';

// enum activeMenu {footprint, collection}

// class MyCustomMenu extends StatefulWidget {
//   //final VoidCallback onMenuTap;
//   final Function(int) onMenuTap;
//   final List<String> menus;
//   final int currentSelected;
//   const MyCustomMenu({
//     Key key,
//     this.currentSelected,
//     this.menus,
//     this.onMenuTap
//   }) : super(key: key);

//   @override
//   _MyCustomMenuState createState() => _MyCustomMenuState();
// }

// class _MyCustomMenuState extends State<MyCustomMenu> {
//   // var _active = activeMenu.footprint;
//   String currentValue;
//   //int currentSelected = 0;
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: const EdgeInsets.symmetric(vertical: 9.0, horizontal: 15.0),
//       decoration: BoxDecoration(
//         color: Color(0xff3a3a3a),
//         borderRadius: BorderRadius.circular(25),
//       ),
//       child: Row(
//         children: List.generate(widget.menus.length, (index){
//           return Expanded(
//             child: GestureDetector(
//               onTap: widget.onMenuTap(index),
//               // onTap: () {
//               //   setState(() {
//               //     //_active = activeMenu.footprint;
//               //     //currentValue = text[newValue];
//               //     widget.currentSelected = index;
//               //   });
//               // },
//               child: Container(
//                 decoration: widget.currentSelected == index//_active == activeMenu.footprint
//                     ? BoxDecoration(
//                         borderRadius: BorderRadius.circular(25.0),
//                         color: Color(0xff525252),
//                       )
//                     : BoxDecoration(),
//                 padding: EdgeInsets.all(15.0),
//                 alignment: Alignment.center,
//                 child: Text(
//                   widget.menus[index],
//                   style: TextStyle(
//                     color: widget.currentSelected == index //_active == activeMenu.footprint
//                         ? Color(0xffe7c260)
//                         : Color(0xffd5d5d5),
//                   ),
//                 ),
//               ),
//             ),
//           );
//         }) 
          
        
//       ),
//     );
//   }
// }
