import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/core/base_widget.dart';
import 'package:pretty_client_mobile/design_system.dart';

class InputPasswordWidget extends StatelessWidget {
  final Function(String) onChanged;
  final Function(String) onSummitted;
  final TextEditingController controllerPassword;
  final bool autoFocus;
  final bool autoValidate;
  final FocusNode focusPassword;
  final String hintText;

  const InputPasswordWidget(
      {Key key,
      this.onChanged,
      this.onSummitted,
      this.controllerPassword,
      this.autoFocus,
      this.autoValidate,
      this.focusPassword,
      this.hintText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget<InputPasswordViewModel>(
      model: InputPasswordViewModel(),
      builder: (context, model, child) {
        return Container(
          margin: EdgeInsets.only(
            top: 8,
          ),
          child: TextFormField(
            onChanged: onChanged,
            controller: controllerPassword,
            keyboardType: TextInputType.text,
            autofocus: autoFocus,
            obscureText: model.hidePassword,
            style: TextStyle(
                color: Design.theme.colorActive,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal),
            textInputAction: TextInputAction.next,
            focusNode: focusPassword,
            onFieldSubmitted: (v) {
              focusPassword.unfocus();
              onSummitted(v);
            },
            autovalidate: autoValidate,
            validator: (String value) {
              var regExpResult = validatePasswordString(value);
              if (value.isEmpty) {
                return "กรุณากรอกข้อมูล";
              } else if (value.length < 8) {
                return "รหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวอักษร";
              } else if (!regExpResult) {
                return "รหัสผ่านต้องมีตัวอักษร และ ตัวเลขอย่างน้อย 1 ตัว";
              } else {
                return null;
              }
            },
            decoration: InputDecoration(
              suffixIcon: GestureDetector(
                onTap: () {
                  model.updateObscureText();
                },
                child: Icon(
                  model.hidePassword ? Icons.visibility_off : Icons.visibility,
                  semanticLabel: model.hidePassword ? 'hide password' : 'show password',
                  color: Design.theme.colorInactive,
                ),
              ),
              hintText: hintText,
              filled: true,
              hintStyle: TextStyle(
                color: Design.theme.colorInactive.withOpacity(0.6),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: -0.2,
              ),
              fillColor: Design.theme.colorLine.withOpacity(0.2),
              contentPadding: new EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  borderSide: BorderSide(width: 0, color: Design.theme.colorLine.withOpacity(0.2))),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  borderSide: BorderSide(width: 0, color: Design.theme.colorLine.withOpacity(0.2))),
            ),
          ),
        );
      },
    );
  }

  bool validatePasswordString(String value) {
    RegExp regExp = new RegExp(
      r"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$",
      caseSensitive: false,
      multiLine: false,
    );

    print(value);
    if (regExp.hasMatch(value)) {
      return true;
    }
    return false;
  }
}

class InputPasswordViewModel extends BaseViewModelNoDi {
  bool hidePassword = true;

  void updateObscureText() {
    hidePassword = !hidePassword;
    notifyListeners();
  }
}
