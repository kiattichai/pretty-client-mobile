import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:flutter/services.dart';
import 'package:pretty_client_mobile/model/language_entity.dart';

class InputMobileWidget extends StatelessWidget {
  bool _validate = false;
  bool autoFocus = true;

  final String countryCode;
  final String phoneCode;
  final VoidCallback onCountrySelected;
  final Function(String) onMobileInputChanged;
  final Function(String) onMobileInputValidator;
  final Function(String) onFieldSubmitted;
  final TextEditingController controllerMobile;
  final FocusNode focusInputMobile;

  final LanguageEntity appLocale;

  InputMobileWidget(
      {@required this.onCountrySelected,
      @required this.countryCode,
      @required this.phoneCode,
      @required this.onMobileInputChanged,
      @required this.onMobileInputValidator,
      @required this.controllerMobile,
      @required this.focusInputMobile,
      this.onFieldSubmitted,
      @required this.appLocale});

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
      titleInputMobile(context),
      inputMobile(context),
      subTitleInputMobile(context)
    ]);
  }

  Widget prefixCountryCode() {
    return GestureDetector(
      child: Container(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                left: 15,
                right: 15,
              ),
              child: Text(
                "$countryCode $phoneCode",
                style: TextStyle(
                  color: Design.theme.colorActive,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  letterSpacing: -0.2,
                ),
              ),
            ),
            Container(
              height: 30,
              width: 1,
              color: Color.fromRGBO(187, 187, 187, 1),
            ),
            SizedBox(
              width: 8,
            ),
          ],
        ),
      ),
      onTap: () => onCountrySelected(),
    );
  }

  Widget titleInputMobile(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 40, right: 0),
      child: SizedBox(
        width: double.infinity,
        child: Container(
          child: Text(appLocale.registerInputPhoneTitle,
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Design.theme.colorActive,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              )),
        ),
      ),
    );
  }

  Widget inputMobile(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: 8,
      ),
      child: TextFormField(
        controller: controllerMobile,
        keyboardType: TextInputType.phone,
        autofocus: autoFocus,
        style: TextStyle(
          color: Design.theme.colorActive,
          fontSize: 16,
          fontWeight: FontWeight.w400,
          fontStyle: FontStyle.normal,
          letterSpacing: -0.2,
        ),
        inputFormatters: <TextInputFormatter>[WhitelistingTextInputFormatter.digitsOnly],
        textInputAction: TextInputAction.next,
        focusNode: focusInputMobile,
        onFieldSubmitted: (val) {
          onFieldSubmitted(val);
        },
        onChanged: (String val) {
          onMobileInputChanged(val);
        },
        autovalidate: _validate,
        validator: (String val) {
          onMobileInputValidator(val);
        },
        decoration: InputDecoration(
          prefixIcon: this.prefixCountryCode(),
          filled: true,
          fillColor: Design.theme.colorLine.withOpacity(0.2),
          contentPadding: EdgeInsets.symmetric(
            vertical: 10.0,
            horizontal: 10,
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(8),
            ),
            borderSide: BorderSide(
              width: 0,
              color: Design.theme.colorLine.withOpacity(0.2),
            ),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            borderSide: BorderSide(width: 0, color: Design.theme.colorLine.withOpacity(0.2)),
          ),
          hintStyle: TextStyle(
            color: Design.theme.colorInactive.withOpacity(0.6),
          ),
          hintText: appLocale.registerInputPhoneHint,
          counterText: '',
        ),
      ),
    );
  }

  Widget subTitleInputMobile(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 3, right: 0),
      child: SizedBox(
        width: double.infinity,
        child: Container(
          child: Text(
            appLocale.registerOtpMessage,
            style: TextStyle(
                color: Design.theme.colorText2,
                fontSize: 12,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal),
          ),
        ),
      ),
    );
  }
}
