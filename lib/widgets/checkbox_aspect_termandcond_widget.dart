import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/model/language_entity.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/modules/profile_module/static_contents/static_web_view_content_route.dart';
import 'package:pretty_client_mobile/env_config.dart';
import 'package:pretty_client_mobile/app/app_localization.dart';

class CheckboxAspectTermandCondWidget extends StatelessWidget {
  final bool acceptTerm;
  final Function(bool) onAspectTermAndCondSelected;
  final LanguageEntity appLocale;
  CheckboxAspectTermandCondWidget(
      {@required this.acceptTerm,
      @required this.onAspectTermAndCondSelected,
      @required this.appLocale});

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
      acceptTermAndCons(context),
    ]);
  }

  Widget acceptTermAndCons(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, top: 86, right: 20),
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
        Checkbox(
          activeColor: Design.theme.colorPrimary,
          checkColor: Design.theme.colorMainBackground,
          value: acceptTerm,
          onChanged: onAspectTermAndCondSelected,
        ),
        Text(appLocale.registerAccept,
            style: TextStyle(
              color: Design.theme.colorText2,
              fontSize: 16,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
            )),
        SizedBox(
          width: 5,
        ),
        GestureDetector(
          child: Text(appLocale.registerTermsAndConditions,
            style: TextStyle(
              color: Design.theme.colorText2,
              fontSize: 16,
              fontWeight: FontWeight.w600,
              fontStyle: FontStyle.normal,
            )),
          onTap: () {
            RouterService.instance.navigateTo(StaticWebViewRoute.buildPath, data: {
              'url': "${EnvConfig.instance.frontUrl}/webview/c/term-and-conditions?lang=th",
              'title': appLocale.profilePrivacyPolicy
            });
          } 
        )
      ]),
    );
  }
}
