import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/widgets/primary_button_widget.dart';

class ModalBookingSuccess extends StatelessWidget {
  final Function openOrderDetail;

  const ModalBookingSuccess({Key key, @required this.openOrderDetail}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Scaffold(
        backgroundColor: Colors.black.withOpacity(0.7),
        body: Stack(
          children: <Widget>[
            Align(
              child: Padding(
                child: Container(
                    width: double.infinity,
                    child: Stack(
                      children: <Widget>[
                        Align(
                          child: Padding(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                SvgPicture.asset(
                                  'images/icon_check_correct.svg',
                                ),
                                Text("จองสำเร็จ",
                                    style: TextStyle(
                                      fontFamily: 'Sarabun',
                                      color: Design.theme.colorMainBackground,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                                Text(
                                    "คุณได้ทำการจองเรียบร้อยแล้ว กรุณารอการตอบกลับจากทางร้านภายใน  15 นาที",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontFamily: 'Sarabun',
                                      color: Design.theme.colorMainMenu,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                                PrimaryButtonWidget(
                                    buttonTitle: 'ดูรายละเอียดการจอง',
                                    onBtnPressed: openOrderDetail)
                              ],
                            ),
                            padding: EdgeInsets.all(15),
                          ),
                          alignment: Alignment(0, 0),
                        ),
                        Positioned(
                          top: 15,
                          right: 15,
                          child: InkWell(
                            onTap: openOrderDetail,
                            child: SvgPicture.asset(
                              'images/icon_close.svg',
                              color: Design.theme.colorMainBackground,
                            ),
                          ),
                        )
                      ],
                    ),
                    height: 345,
                    decoration: new BoxDecoration(
                        color: Color(0xffffffff), borderRadius: BorderRadius.circular(20))),
                padding: EdgeInsets.only(left: 15, right: 15),
              ),
              alignment: Alignment(0, -0.2),
            )
          ],
        ),
      ),
    );
  }
}
