import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:flutter/services.dart';
import 'package:pretty_client_mobile/model/language_entity.dart';

class InputBirthDayPickerWidget extends StatelessWidget {
  final TextEditingController controllerBirthday;
  final VoidCallback onBirthDayFieldTap;
  final LanguageEntity appLocal;
  InputBirthDayPickerWidget(
      {@required this.controllerBirthday,
      @required this.onBirthDayFieldTap,
      @required this.appLocal});

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[titleBirthday(context), inputBirthdayInfo(context)]);
  }

  Widget titleBirthday(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 15, right: 0),
      child: SizedBox(
        width: double.infinity,
        child: Container(
          child: Text(appLocal.userInfoBirthday,
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Design.theme.colorInactive,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              )),
        ),
      ),
    );
  }

  Widget inputBirthdayInfo(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(
          top: 8,
        ),
        child: InkWell(
          child: TextFormField(
            enabled: false,
            showCursor: false,
            focusNode: FocusNode(),
            enableInteractiveSelection: false,
            controller: controllerBirthday,
            autofocus: false,
            style: new TextStyle(
              color: Design.theme.colorActive,
              fontSize: 16,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              letterSpacing: -0.2,
            ),
            decoration: InputDecoration(
              suffixIcon: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.keyboard_arrow_right,
                  color: Design.theme.colorInactive,
                ),
              ),
              hintText: appLocal.userInfoBirthdayHint,
              hintStyle: TextStyle(
                color: Design.theme.colorInactive.withOpacity(0.57),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
              contentPadding: new EdgeInsets.symmetric(vertical: 12.0, horizontal: 0.0),
            ),
          ),
          onTap: onBirthDayFieldTap,
        ),
        decoration: new BoxDecoration(
            border: Border(
          bottom: BorderSide(width: 1.0, color: Design.theme.colorLine),
        )));
  }
}
