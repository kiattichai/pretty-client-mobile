import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/model/language_entity.dart';
import 'package:pretty_client_mobile/utils/screen.dart';

class StatusBar extends StatelessWidget {
  final int step;
  final int status;
  final List<String> date;
  final List<String> time;
  final LanguageEntity appLocal;

  final String imgCheckbox = "images/checkbox_circle_line.svg";
  final String imgUncheckbox = "images/uncheckbox_circle_line.svg";
  final String tmpDate = "31/12/2020";
  final String tmpTime = "24:59 น.";

  const StatusBar(this.step, this.status, this.date, this.time, this.appLocal);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return buildStepBar();
  }

  Widget buildStepBar() {
    final lineWidth = Screen.width / 2 - Screen.convertWidthSize(50);
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
                width: Screen.convertWidthSize(95),
                alignment: Alignment.centerLeft,
                child: Text(
                  appLocal.appointment,
                  style: TextStyle(
                    fontSize: 14,
                    color: step >= 1 ? Color(0xffd5d5d5) : Color(0xffa4a4a4),
                  ),
                )),
            Container(
                width: Screen.convertWidthSize(95),
                alignment: Alignment.center,
                child: Text(
                  appLocal.confirmation,
                  style: TextStyle(
                    fontSize: 14,
                    color: step >= 2 ? Color(0xffd5d5d5) : Color(0xffa4a4a4),
                  ),
                )),
            Container(
              width: Screen.convertWidthSize(95),
              alignment: Alignment.centerRight,
              child: Text(
                status >= 3
                    ? appLocal.bookingCanceled
                    : appLocal.bookingSuccessful,
                style: TextStyle(
                  fontSize: 14,
                  color: step >= 3 ? Color(0xffd5d5d5) : Color(0xffa4a4a4),
                ),
              ),
            ),
          ],
        ),
        Stack(
          children: <Widget>[
            Container(
              height: 4,
              padding: EdgeInsets.only(
                  left: Screen.convertWidthSize(25),
                  right: Screen.convertWidthSize(25)),
              margin: EdgeInsets.only(top: 15),
              child: Row(
                children: <Widget>[
                  Container(
                    width: lineWidth,
                    child: Flex(
                      direction: Axis.vertical,
                      children: [
                        Expanded(child: Container()),
                        step >= 2
                            ? Separator(
                                width: lineWidth,
                                color: Design.theme.primaryColor,
                                isLine: true,
                              )
                            : Separator(color: Color(0xffa4a4a4)),
                        Container(height: 1),
                      ],
                    ),
                  ),
                  Container(
                    width: lineWidth,
                    child: Flex(
                      direction: Axis.vertical,
                      children: [
                        Expanded(child: Container()),
                        step >= 3
                            ? Separator(
                                width: lineWidth,
                                color: Design.theme.primaryColor,
                                isLine: true,
                              )
                            : Separator(color: Color(0xffa4a4a4)),
                        Container(height: 1),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
                margin: EdgeInsets.only(top: 5),
                padding: EdgeInsets.only(
                    left: Screen.convertWidthSize(20),
                    right: Screen.convertWidthSize(20)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: SvgPicture.asset(
                        step >= 1 ? imgCheckbox : imgUncheckbox,
                        width: 22,
                      ),
                    ),
                    Container(
                      child: SvgPicture.asset(
                        step >= 2 ? imgCheckbox : imgUncheckbox,
                        width: 22,
                      ),
                    ),
                    Container(
                      child: SvgPicture.asset(
                        step >= 3 ? imgCheckbox : imgUncheckbox,
                        width: 22,
                      ),
                    ),
                  ],
                ))
          ],
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                child: Builder(
                  builder: (context) {
                    if (date.length > 0) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            date.length >= 1
                                ? date[0]
                                : (date.length == 0 ? "" : tmpDate),
                            style: TextStyle(
                              color: step >= 1
                                  ? Color(0xff636362)
                                  : Colors.transparent,
                              fontSize: 12,
                            ),
                          ),
                          Text(
                            time.length >= 1
                                ? time[0]
                                : (date.length == 0 ? "" : tmpTime),
                            style: TextStyle(
                              color: step >= 1
                                  ? Color(0xff636362)
                                  : Colors.transparent,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      );
                    } else {
                      return Container();
                    }
                  },
                ),
              ),
              Container(
                alignment: Alignment.center,
                child: Builder(
                  builder: (context) {
                    if (date.length > 0) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            date.length >= 2 ? date[0] : tmpDate,
                            style: TextStyle(
                              color: step >= 2
                                  ? Color(0xff636362)
                                  : Colors.transparent,
                              fontSize: 12,
                            ),
                          ),
                          Text(
                            time.length >= 2 ? time[0] : tmpTime,
                            style: TextStyle(
                              color: step >= 2
                                  ? Color(0xff636362)
                                  : Colors.transparent,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      );
                    } else {
                      return Container();
                    }
                  },
                ),
              ),
              Container(
                alignment: Alignment.center,
                child: Builder(
                  builder: (context) {
                    if (date.length > 0) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            date.length >= 3 ? date[0] : tmpDate,
                            style: TextStyle(
                              color: step >= 3
                                  ? Color(0xff636362)
                                  : Colors.transparent,
                              fontSize: 12,
                            ),
                          ),
                          Text(
                            time.length >= 3 ? time[0] : tmpTime,
                            style: TextStyle(
                              color: step >= 3
                                  ? Color(0xff636362)
                                  : Colors.transparent,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      );
                    } else {
                      return Container();
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class Separator extends StatelessWidget {
  final double height;
  final double width;
  final Color color;
  final bool isLine;

  const Separator(
      {this.width = 5.0,
      this.height = 1,
      this.color = Colors.black,
      this.isLine = false});

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashWidth = width;
        final dashHeight = height;
        int dashCount = (boxWidth / (2 * dashWidth)).floor();

        if (isLine) {
          dashCount = 1;
        }
        return Flex(
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: dashHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(color: color),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
        );
      },
    );
  }
}
