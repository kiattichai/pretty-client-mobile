import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:flutter/services.dart';
import 'package:pretty_client_mobile/model/language_entity.dart';

class InputPickerWidget extends StatelessWidget {
  final String titleTextField;
  final String hintTextField;
  final TextEditingController controller;
  final VoidCallback onFieldTap;
  final LanguageEntity appLocal;

  InputPickerWidget(
    {
      @required this.titleTextField,
      @required this.hintTextField,
      @required this.controller, 
      @required this.onFieldTap, 
      @required this.appLocal
    }
  );

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[title(context), inputInfo(context)]);
  }

  Widget title(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 15, right: 0),
      child: SizedBox(
        width: double.infinity,
        child: Container(
          child: Text(titleTextField,
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Design.theme.colorInactive,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              )),
        ),
      ),
    );
  }

  Widget inputInfo(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(
          top: 8,
        ),
        child: InkWell(
          child: TextFormField(
            enabled: false,
            showCursor: false,
            focusNode: FocusNode(),
            enableInteractiveSelection: false,
            controller: controller,
            keyboardType: TextInputType.text,
            autofocus: false,
            style: new TextStyle(
              color: Design.theme.colorActive,
              fontSize: 16,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              letterSpacing: -0.2,
            ),
            decoration: InputDecoration(
              suffixIcon: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.keyboard_arrow_right,
                  color: Design.theme.colorInactive,
                ),
              ),
              hintText: hintTextField,
              hintStyle: TextStyle(
                color: Design.theme.colorInactive.withOpacity(0.57),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
              contentPadding: new EdgeInsets.symmetric(vertical: 12.0, horizontal: 0.0),
            ),
          ),
          onTap: onFieldTap,
        ),
        decoration: new BoxDecoration(
            border: Border(
          bottom: BorderSide(width: 1.0, color: Design.theme.colorLine),
        )));
  }
}
