import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/utils/screen.dart';

class RangeComponent extends StatelessWidget {
  final String rangeTitle;
  final int startValue;
  final int endValue;
  final String unitTitle;
  final double maxValue;
  final double minValue;
  final RangeValues rangeValues;
  final Function(RangeValues values) onValueChange;

  const RangeComponent(
      {Key key,
      this.rangeTitle,
      this.startValue,
      this.endValue,
      this.unitTitle,
      this.maxValue,
      this.minValue,
      this.rangeValues,
      this.onValueChange})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
//        Expanded(
        Container(
          width: Screen.convertWidthSize(55),
          padding:
              EdgeInsets.only(left: 0, bottom: Screen.convertHeightSize(10)),
          child: Text(
            rangeTitle,
            textAlign: TextAlign.left,
            style: TextStyle(
              color: Color(0xffd5d5d5),
              fontSize: 16,
              fontWeight: FontWeight.w600,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
//        ),
//        Expanded(
//          child: Stack(
//            children: <Widget>[
//              Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: startValue,
//                    child: SizedBox(),
//                  ),
//                  Container(
//                    width: Screen.convertHeightSize(50),
//                    child: Text(
//                      "$startValue $unitTitle",
//                      textAlign: TextAlign.center,
//                      style: TextStyle(color: Colors.white),
//                    ),
//                  ),
//                  Expanded(
//                    flex: maxValue.round() - startValue,
//                    child: SizedBox(),
//                  ),
//                ],
//              ),
//              Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: endValue,
//                    child: SizedBox(),
//                  ),
//                  Container(
//                    width: Screen.convertHeightSize(50),
//                    child: Text("$endValue $unitTitle",
//                        textAlign: TextAlign.center,
//                        style: TextStyle(color: Colors.white)),
//                  ),
//                  Expanded(
//                    flex: maxValue.round() - endValue,
//                    child: SizedBox(),
//                  ),
//                ],
//              ),
//            ],
//          ),
//        ),
        Expanded(
          child: Container(
//            color: Colors.redAccent,
            height: 65,
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Stack(
                    children: <Widget>[
                      SliderTheme(
                        data: SliderThemeData(
                          inactiveTrackColor: Colors.black,
                          activeTrackColor: Design.theme.colorPrimary,
                          thumbColor: Color(0xff636362),
                        ),
                        child: RangeSlider(
                          values: rangeValues,
                          min: minValue,
                          max: maxValue,
                          divisions: 1000,
                          onChanged: onValueChange,
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: startValue,
                            child: SizedBox(),
                          ),
                          Container(
                            width: Screen.convertHeightSize(42),
                            child: Text(
                              "$startValue",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Color(0xffa4a4a4),
                                fontSize: 14,
                              ),
                            ),
                          ),
                          Expanded(
                            flex: maxValue.round() - startValue,
                            child: SizedBox(),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: endValue,
                            child: SizedBox(),
                          ),
                          Container(
                            width: Screen.convertHeightSize(42),
                            child: Text("$endValue",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Color(0xffa4a4a4),
                                  fontSize: 14,
                                )),
                          ),
                          Expanded(
                            flex: maxValue.round() - endValue,
                            child: SizedBox(),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
//                SliderTheme(
//                  data: SliderThemeData(
//                    inactiveTrackColor: Colors.black,
//                    activeTrackColor: Design.theme.colorPrimary,
//                    thumbColor: Colors.white,
//                  ),
//                  child: RangeSlider(
//                    values: rangeValues,
//                    min: minValue,
//                    max: maxValue,
//                    divisions: 1000,
//                    onChanged: onValueChange,
//                  ),
//                ),
              ],
            ),
          ),
        ),
//        SliderTheme(
//          data: SliderThemeData(
//            inactiveTrackColor: Colors.black,
//            activeTrackColor: Design.theme.colorPrimary,
//            thumbColor: Colors.white,
//          ),
//          child: RangeSlider(
//            values: rangeValues,
//            min: minValue,
//            max: maxValue,
//            divisions: 1000,
//            onChanged: onValueChange,
//          ),
//        ),
      ],
    );
  }
}
