import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/services/router_service.dart';
import 'package:pretty_client_mobile/utils/screen.dart';

class CustomHeaderDetail extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final double height;
  final Function onTap;

  const CustomHeaderDetail({
    Key key,
    @required this.height,
    @required this.title,
    this.onTap,
  }) : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    Screen(context);
    return Container(
        decoration: BoxDecoration(
          color: Design.theme.colorMainBackground,
          boxShadow: [
            BoxShadow(
                color: Color(0xe6000000), offset: Offset(0, 3), blurRadius: 14, spreadRadius: 0)
          ],
        ),
        child: Padding(
          padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
          child: Stack(
            children: <Widget>[
              InkWell(
                onTap: onTap == null ? () => RouterService.instance.pop() : onTap,
                child: Container(
                  height: Screen.convertHeightSize(55),
                  width: 50,
                  child: Center(
                    child: Container(
                      height: 20,
                      child: SvgPicture.asset(
                        'images/icon_arrow_left.svg',
                        color: Design.theme.colorPrimary,
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment(0, 0),
                child: Text(title,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Design.theme.colorActive,
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    )),
              )
            ],
          ),
        ));
  }
}
