import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';

class CircleLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(Design.theme.colorPrimary),
    );
  }
}
