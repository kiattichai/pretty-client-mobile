import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:flutter/services.dart';

class InputTextFieldFormWidget extends StatelessWidget {
  final String title;
  final String hintText;
  final bool autoFocus;
  final bool validate;
  final TextEditingController textEditingController;
  final Function(String) onInputChanged;
  final Function(String) onInputValidator;
  final Function(String) onFieldSubmitted;
  final FocusNode focusInput;
  final TextInputFormatter inputFormatter;
  TextInputType keyboardType;
  InputTextFieldFormWidget(
      {@required this.title,
      @required this.hintText,
      @required this.validate,
      @required this.textEditingController,
      @required this.onInputChanged,
      @required this.onInputValidator,
      @required this.onFieldSubmitted,
      @required this.focusInput,
      this.autoFocus,
      this.keyboardType,
      this.inputFormatter
      });

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[titleWidget(context), inputFieldWidget(context)]);
  }

  Widget titleWidget(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Container(
        child: Text(title,
            textAlign: TextAlign.left,
            style: TextStyle(
              color: Design.theme.colorInactive,
              fontSize: 16,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
            )),
      ),
    );
  }

  Widget inputFieldWidget(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: 8,
      ),
      child: TextFormField(
        onChanged: onInputChanged,
        controller: textEditingController,
        keyboardType: keyboardType,
        autofocus: autoFocus,
        style: TextStyle(
          color: Design.theme.colorActive,
          fontSize: 16,
          fontWeight: FontWeight.w400,
          fontStyle: FontStyle.normal,
          letterSpacing: -0.2,
        ),
        textInputAction: TextInputAction.next,
        focusNode: focusInput,
        onFieldSubmitted: (val) {
          focusInput.unfocus();
          onFieldSubmitted(val);
        },       
        inputFormatters: [
          inputFormatter
        ],
        autovalidate: validate,
        validator: onInputValidator,
        decoration: InputDecoration(
          filled: true,
          hintStyle: TextStyle(
            color: Design.theme.colorInactive.withOpacity(0.6),
            fontSize: 16,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
            letterSpacing: -0.2,
          ),
          hintText: hintText,
          fillColor: Design.theme.colorLine.withOpacity(0.2),
          contentPadding: new EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              borderSide: BorderSide(width: 0, color: Design.theme.colorLine.withOpacity(0.2))),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              borderSide: BorderSide(width: 0, color: Design.theme.colorLine.withOpacity(0.2))),
        ),
      ),
    );
  }
}
