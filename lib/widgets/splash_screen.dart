import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pretty_client_mobile/design_system.dart';
import 'package:pretty_client_mobile/modules/feedback_module/feedback_route.dart';
import 'package:pretty_client_mobile/modules/main_module/main_screen_route.dart';
import 'package:pretty_client_mobile/modules/mock_home_screen.dart';
import 'package:pretty_client_mobile/services/router_service.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final FirebaseMessaging fcm = FirebaseMessaging();
  Function handleClickNotificationFeedback;

  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    if (handleClickNotificationFeedback == null) {
      RouterService.instance.navigateTo(MainScreenRoute.buildPath, clearStack: true);
    } else {
      handleClickNotificationFeedback();
    }
  }

  @override
  void initState() {
    super.initState();
    handleNotification();
    startTime();
  }

  void handleNotification() {
    fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
      },
      onLaunch: (Map<String, dynamic> message) async {
        dynamic data = message['data'] ?? message;
        handleOpenPageFeedback(data);
      },
      onResume: (Map<String, dynamic> message) async {
        dynamic data = message['data'] ?? message;
        handleOpenPageFeedback(data);
      },
    );
  }

  void handleOpenPageFeedback(dynamic data) {
    final String id = data['id'] ?? '';
    final String type = data['type'] ?? '';
    if (type == 'feedback') {
      dynamic dataOrder = {
        "order_id": int.parse(id),
        "ispop": false
      };
      handleClickNotificationFeedback = () {
        RouterService.instance.navigateTo(FeedbackRoute.buildPath, clearStack: true, data: dataOrder);
      };
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Design.theme.colorMainBackground,
      body: new Center(
        child: SvgPicture.asset(
          'images/night.svg',
          width: 200,
        ),
      ),
    );
  }
}
