import 'package:pretty_client_mobile/generated/json/base/json_convert_content.dart';
import 'package:pretty_client_mobile/generated/json/base/json_filed.dart';

class ErrorEntity with JsonConvert<ErrorEntity> {
  String type;
  int code;
  String message;
  String field;
  String line;
}
