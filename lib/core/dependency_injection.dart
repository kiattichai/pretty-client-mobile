import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_di.dart';

abstract class DependencyInjectionMixin {
  @protected
  @visibleForTesting
  AppDi di;

  @protected
  @visibleForTesting
  Future<void> inject() async {
    di = await AppDi.instance;
    diReady();
  }

  @protected
  @visibleForTesting
  void diReady() {}
}
