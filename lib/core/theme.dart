import 'package:flutter/material.dart';

abstract class ThemeConfig {
  Color colorPrimary;
  Color colorMainBackground;
  Color colorMainMenu;
  Color colorBox;
  Color colorLine;
  Color colorText;
  Color colorText2;
  Color colorInactive;
  Color colorActive;
  Color colorButtonFacebook;

  Color colorButtonRed;
  Color primaryColor;
  Color backgroundColor;
  Color fontColorPrimary;
  Color fontColorSecondary;
  Color fontColorSecondary2;
  Color fontColorSecondary3;
  Color fontColorSecondary4;
  Color colorButtonBookNow;
  Color colorButtonPrimary;
  Color fontColorBlack;
  Color fontColorLinkButton;
  Color fontColorPlaceholder;
  Color inputColor;
  Color inputBorderColor;
  Color navBackgroundColor;
  Color activeMenuColor;
  Color titleBackgroundColor;
  Color lineColor;
  Color borderColor;
  Color fontColorWhite;
  Color underlineColor;
  Color menuTitleColor;
  Color profileTitleColor;
  Color avatarBorderColor;
  Color subTitleColor;
  Color backgroundColorSecondary;
  Color fontColorInactive;
}
