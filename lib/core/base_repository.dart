import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/dependency_injection.dart';

abstract class BaseRepository with DependencyInjectionMixin {
  BaseRepository() {
    init();
  }

  @protected
  @mustCallSuper
  void init() {
    inject();
  }

  @override
  @mustCallSuper
  void diReady() async {}
}
