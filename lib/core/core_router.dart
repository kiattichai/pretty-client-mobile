import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/core/base_route.dart';

class CoreRouter {
  final List<BaseRoute> routes = [];

  Route<dynamic> generator(RouteSettings settings) {
    final routeMatch = routes.firstWhere((r) => r.path == settings.name);
    return routeMatch.routeTo(settings.arguments);
  }
}
