import 'package:flutter/material.dart';
import 'package:pretty_client_mobile/app/app_state.dart';
import 'package:pretty_client_mobile/core/base_view_model.dart';
import 'package:pretty_client_mobile/model/language_entity.dart';

abstract class BaseStateProvider<S extends StatefulWidget, P extends BaseViewModel>
    extends State<S> {
  @protected
  P viewModel;
  LanguageEntity appLocal;
  AppState appState;

  @override
  void dispose() {
    viewModel.dispose();
    super.dispose();
  }
}
