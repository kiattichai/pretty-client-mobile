import 'package:firebase_analytics/firebase_analytics.dart';

class AnalyticService {
  static AnalyticService get instance => AnalyticService();
  factory AnalyticService() => _singleton;
  static final AnalyticService _singleton = AnalyticService._init();

  FirebaseAnalytics analytics;

  AnalyticService._init() {
    analytics = FirebaseAnalytics();
    print('init object');
  }

  Future<void> logScreen(String name) async {
    print('Log screen $name');
    return await analytics.setCurrentScreen(
      screenName: name,
      screenClassOverride: name + '+',
    );
  }

  void setUser(int id) async {
    await analytics.setUserId('$id');
  }

  void unsetUser() {}
}
