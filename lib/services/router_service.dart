import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:pretty_client_mobile/app/app.dart';
import 'package:pretty_client_mobile/core/base_route.dart';
import 'package:pretty_client_mobile/core/core_router.dart';
import 'package:pretty_client_mobile/modules/booking_module/booking_route.dart';
import 'package:pretty_client_mobile/modules/booking_module/confirmation_route.dart';
import 'package:pretty_client_mobile/modules/coins_module/coins_transaction_route.dart';
import 'package:pretty_client_mobile/modules/country_code_module/country_code_route.dart';
import 'package:pretty_client_mobile/modules/feedback_module/feedback_route.dart';
import 'package:pretty_client_mobile/modules/forgot_password_module/routes/forgot_create_password_route.dart';
import 'package:pretty_client_mobile/modules/forgot_password_module/routes/forgot_password_route.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_detail_route.dart';
import 'package:pretty_client_mobile/modules/login_module/login_route.dart';
import 'package:pretty_client_mobile/modules/main_module/main_screen_route.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/order_detail_route.dart';
import 'package:pretty_client_mobile/modules/news_module/news_detail_route.dart';
import 'package:pretty_client_mobile/modules/notification_module/notification_list_route.dart';
import 'package:pretty_client_mobile/modules/otp_module/otp_route.dart';
import 'package:pretty_client_mobile/modules/pre_register_module/pre_register_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/notification_setting_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/setting_base_route.dart';
import 'package:pretty_client_mobile/modules/register_module/routes/register_create_password_route.dart';
import 'package:pretty_client_mobile/modules/register_module/routes/register_form_route.dart';
import 'package:pretty_client_mobile/modules/register_module/routes/register_mobile_route.dart';
import 'package:pretty_client_mobile/modules/register_module/routes/register_success_route.dart';
import 'package:pretty_client_mobile/modules/register_social_module/register_social_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/profile_route.dart';
import 'package:pretty_client_mobile/modules/change_password_module/change_password_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/static_contents/static_web_view_content_route.dart';
import 'package:pretty_client_mobile/modules/profile_module/edit_profile/edit_profile_route.dart';
import 'package:pretty_client_mobile/services/analytics_service.dart';
import 'package:pretty_client_mobile/widgets/circle_progress_indicator.dart';
import 'package:pretty_client_mobile/modules/profile_module/static_web_view_menus_route.dart';
import 'package:pretty_client_mobile/modules/mybooking_module/mybooking_screen_route.dart';
import 'package:pretty_client_mobile/modules/coins_module/topup_coins/topup_coins_route.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_route.dart';
import 'package:pretty_client_mobile/modules/friends_module/friends_landing_route.dart';
import 'package:pretty_client_mobile/modules/news_module/news_list_route.dart';
// import 'package:pretty_client_mobile/modules/home_tab_module/home_tab_route.dart';

class RouterService {
  static RouterService get instance => RouterService();
  factory RouterService() => _singleton;
  static final RouterService _singleton = RouterService._init();

  static final List<BaseRoute> routes = [
    LoginRoute(),
    CountryCodeRoute(),
    MainScreenRoute(),
    ForgotCreatePasswordRoute(),
    ForgotPasswordRoute(),
    OtpRoute(),
    PreRegisterRoute(),
    RegisterSocialRoute(),
    RegisterCreatePasswordRoute(),
    RegisterFormRoute(),
    RegisterMobileRoute(),
    RegisterSuccessRoute(),
    NewsDetailRoute(),
    NotificationListRoute(),
    NotificationSettingRoute(),
    FriendsDetailRoute(),
    ProfileRoute(),
    ChangePasswordRoute(),
    StaticWebViewRoute(),
    EditProfileRoute(),
    StaticWebViewRoute(),
    BookingRoute(),
    SettingBaseRoute(),
    StaticWebViewMenusRoute(),
    MybookingBaseScreenRoute(),
    OrderDetailRoute(),
    ConfirmationRoute(),
    CoinsTransactionRoute(),
    TopupCoinsRoute(),
    FriendsRoute(),
    FriendsLandingRoute(),
    NewsListRoute(),
    FeedbackRoute()
  ];

  final CoreRouter coreRouter = CoreRouter();

  RouterService._init() {
    coreRouter.routes.addAll(routes);
  }

  Route<dynamic> generator(RouteSettings routeSettings) => coreRouter.generator(routeSettings);

  void pop({Object data}) {
    MyApp.navigatorKey.currentState.pop(data);
  }

  Future<dynamic> navigateTo(String path,
      {Object data, bool clearStack, BuildContext context}) async {
    final routeMatch = coreRouter.routes.firstWhere((r) => r.path == path);
    if (routeMatch != null) {
      final loading = showLoading();
      final hasPermission = await routeMatch.hasPermission(data);
      loading.dismiss(animate: false);
      if (hasPermission) {
        await AnalyticService.instance.logScreen(path);
        if (clearStack ?? routeMatch.clearStack) {
          return await MyApp.navigatorKey.currentState
              .pushNamedAndRemoveUntil(path, (check) => false, arguments: data);
        } else {
          return await MyApp.navigatorKey.currentState.pushNamed(path, arguments: data);
        }
      }
    }
  }

  OverlaySupportEntry showLoading() {
    return showOverlay((context, t) {
      return Container(
        color: Colors.black12,
        child: Center(
          child: CircleLoading(),
        ),
      );
    }, duration: Duration.zero);
  }
}
