import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group('Counter App', () {
    // First, define the Finders and use them to locate widgets from the
    // test suite. Note: the Strings provided to the `byValueKey` method must
    // be the same as the Strings we used for the Keys in step 1.

//    final counterTextFinder = find.byValueKey('counter');
    final buttonOpenAlbum = find.byValueKey('2');
    final panelView = find.byValueKey('panel');

    FlutterDriver driver;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    test('increments the counter', () async {
      await driver.tap(buttonOpenAlbum);
    });

    test('show panelView', () async {
      await driver.waitFor(panelView);
    });

    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });
  });
}
